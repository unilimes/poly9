const PROXY_CONFIG = [
  {
    context: [
      "/realityserver"
    ],
    target: "http://nae-165-254-189-10.jarvice.com:8080",
    pathRewrite: {'^/realityserver' : 'http://nae-165-254-189-10.jarvice.com:8080'},
    changeOrigin: true,
    secure: false
  }
];

module.exports = PROXY_CONFIG;
