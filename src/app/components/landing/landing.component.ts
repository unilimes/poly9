import { Component, OnInit,ViewChild } from '@angular/core';

import {DataService,Storage} from '../../services/services';
import * as ENTITY from '../../entities/entities';
import {WebglView} from '../../directives/directives';
import {MainComponent} from '../main/main.component';
import {RoomComponent} from '../room/room.component';
import {RealityServerService,Command} from '../../services/services';

declare let window:any, THREE:any, $:any, async:any, claraplayer:any;

@Component({
    selector: 'app-landing',
    templateUrl: './landing.component.html',
    styleUrls: ['./style.sass', 'landing.component.css']
})
export class LandingComponent extends MainComponent implements OnInit {
    @ViewChild("roomModal")
        roomModal:RoomComponent;
    @ViewChild("acc")
        acc:HTMLElement;
    public models:any = [];//list of availabel to select models
    public selectedModel:any = {
        name: 'loading'
    };//current selected model
    private curModel:any;//cur editable detail of model
    private materials:any;//list of availabel loaded materials
    public parts:any = [{}];//list of availabel details of model
    public curMatCategories:any = [];//list of materials of selected category
    public curMats:any = [];//list of materials of selected category
    public matCategories:any = ["All"];//list of material categories
    public selectedMatCat:string;//cur selected material category
    public seachWord:any;//cur inputed text for filter
    public flagModal:boolean = true;
    public selectFlag:string = 'Select a Room';
    public claraRooms:any = {
        models: [{}]
    };
    public factorMaterials:string;//default domain for loading data for material
    public url:string = 'http://127.0.0.1:3000/';
    public color:string = "#ffffff";
    commads:any = {};//list of commands for realityserver for working with model

    public testIndex:number = 514;//default loaded model
    private currentPage:number = 1;//default page of materials

    constructor(public DataService:DataService ) {
        super();
        this.factorMaterials = ENTITY.Config.FACTOR.MATERIALS;
        this.materials = [{
            'Material Diffuse': './assets/models/blank.jpg'
        }];
        this.commads.list = [];
        this.commads.materials = {};
    }

    ngOnInit() {
        this.DataService.getDataList('./assets/models/materials.json', (materials:any)=> {
            this.materials = materials;
            for (let i = 0; i < materials.length; i++) {
                if (this.matCategories.indexOf(materials[i].Category) < 0) {
                    this.matCategories.push(materials[i].Category);
                }
            }
            this.selectedMatCat = this.matCategories[0];
            this.filter(null, this.CONFIG.MODELS.TYPES.MATERIAL);
            this.DataService.getDataList('./assets/models/parts.json', (parts:any)=> {
                this.parts = parts;
                this.DataService.getDataList('./assets/models/models.json', (models:any)=> {
                    this.models = models;
                    this.selectedModel = this.getFileName(this.models[this.testIndex]);
                    this.commads.list.push(
                        new Command(Command.REQUEST.SCENE.IMPORT_EL, {
                            block: "true",
                            filename: ENTITY.Config.FILE.DIR.PREVIOUS + ENTITY.Config.MODELS.URL.CUPS + this.selectedModel.name + ENTITY.Config.DELIMETER + this.selectedModel.fileName
                        }),
                        new Command(Command.REQUEST.GROUP.ATTACH, {
                            group_name: Command.SCENE.MAIN_GROUP,
                            item_name: Command.SCENE.LOADED_GROUP
                        })
                    );
                    this.options = {
                        curModel: this.selectedModel,
                        modelName: ENTITY.Config.MODELS.URL.MAIN + ENTITY.Config.MODELS.URL.CUPS + this.selectedModel.name + ENTITY.Config.DELIMETER + this.selectedModel.fileName,
                        component: this,
                        onFinish: (model)=> {
                            //this.curModel = model.children[2];
                            //this.changeMaterial(this.curMatCategories[0]);
                        }
                    };
                });
            });
        });
    }
    private filter(ev, type) {
        if (type == this.CONFIG.MODELS.TYPES.MATERIAL) {
            if(this.selectedMatCat == this.matCategories[0]){
                this.curMatCategories = this.materials.concat([]);
            }else{
                this.curMatCategories = [];
                for (let i = 0; i < this.materials.length; i++) {
                    if (this.materials[i].Category == this.selectedMatCat) {
                        this.curMatCategories.push(this.materials[i]);
                    }
                }

            }
            this.onFindMaterialByName();

        }
    }

    public claraioCtrl() {

    }


    private getFileName(file:any) {
        let _file = file.name.split(ENTITY.Config.DELIMETER);
        _file = _file[_file.length - 1];
        return {name: _file.split(".obj")[0], fileName: _file, model: file};
    }

    public changeMaterial(mat:any) {
        this.webglView.preloader.fade(true);
        let parameters:any = {
            "map": "Material Diffuse Image Path",
            "metalnessMap": "Material Metalness Image Path",
            "roughnessMap": "Material Roughness Image Path",
            "normalMap": "Material Normal Image Path",
            "displacementMap": "Material Displacement Image Path",
            "bumpMap": "Material Cavity Image Path"
        };
        let parametersAlternative:any = {
            "map": "Thumbnail",
            "metalnessMap": "Material Metalness",
            "roughnessMap": "Material Roughness",
            "normalMap": "Material Normal",
            "displacementMap": "Material Displacement",
            "bumpMap": "Material Cavity"
        };

        let callBack:any = {};
        let loader:any = new THREE.TextureLoader();
        loader.crossOrigin = '';
        loader.setCrossOrigin('anonymos');
        let mode = parametersAlternative,
            self = this;
        let leathreMode = ()=> {
            mode = parametersAlternative;
        };
        let mainMode = ()=> {
            mode = parameters;
        };
        var loadTeext = function (urlMaterial, callback, key) {
            loader.load(urlMaterial[0],
                (res:any)=> {
                    if (res) {
                        callBack[key] = res;
                        callBack[key].wrapS = callBack[key].wrapT = THREE.RepeatWrapping;
                        callBack[key].anisotropy =  self.webglView.app.gl.getMaxAnisotropy();
                    }
                    callback();
                }, ()=> {

                }, ()=> {
                    if (urlMaterial.length > 1) {
                        loadTeext([urlMaterial[1]], callback, key);
                    } else {
                        callback();
                    }
                });
        };

        mat["Category"] == "Leather" ? leathreMode() : mainMode();

        async.eachOf(mode, (item:any, key:any, callback:any)=> {
            if (mat[item]) {
                loadTeext(mat[item].length > 2 ? [this.CONFIG.MODELS.URL.MAIN + this.CONFIG.TEXTURE_STORAGE + mat[item], this.CONFIG.FACTOR.MATERIALS + mat[item]] : ['./assets/models/blank.jpg'], callback, key);
            } else {
                callback();
            }
        }, ()=> {

            let material:any = this.createPreset(mat);

            material.map = callBack["map"];
            material.metalnessMap = callBack["metalnessMap"];
            material.roughnessMap = callBack["roughnessMap"];
            if (callBack["normalMap"]) {
                material.normalMap = callBack["normalMap"];
            } else if (callBack["bumpMap"]) {
                material.bumpMap = callBack["bumpMap"];
            }
            if (callBack["displacementMap"])material.displacementMap = callBack["displacementMap"];
            material.bumpScale = 0.25;
            material.envMap = this.webglView.app.envMaps[2].maps;
            material.envMap.anisotropy = this.webglView.app.gl.getMaxAnisotropy();
            this.curModel.material = material;
            this.webglView.preloader.fade();
            let matCmd = this.commads.materials[this.curModel.userData.originName] = [
                new Command(Command.REQUEST.MATERIAL.CREATE_DEF,
                    {
                        "material_name": material.name,
                        material_definition_name: "mdl::nvidia::core_definitions::flex_material",
                        "arguments": {},
                    }
                ),
                new Command(Command.REQUEST.ELEMENT.SET_ATTRS,
                    {
                        "element_name": material.name,
                        "attributes": {
                            "is_metal": false,
                            "anisotropy": 0,
                            "reflection_roughness": 0.1,
                            "ior": 1.85,
                            "base_thickness": 0.0,
                            "diffuse_roughness": 0.15,
                            "thin_walled": false,
                            "transmission_roughness": 0.5
                        }
                    }
                ),
                new Command(Command.REQUEST.INSTANCE.SET_MATRL,
                    {
                        "instance_name": this.curModel.userData.originName,
                        "material_name": material.name,
                        "override": true
                    }
                )

            ];

            if (material.map)matCmd.push(new Command(Command.REQUEST.MATERIAL.DIFFUSE,
                {
                    "material_name": material.name,
                    "argument_name": "base_color",
                    "texture_name": this.CONFIG.CUR_PROJ + this.CONFIG.TEXTURE_STORAGE + mat[mode.map],
                    "texture_options": {
                        "scaling": material.map.repeat,
                        "color_offset": material.map.offset
                    },
                    "create_from_file": true
                }
            ));
            if (material.roughnessMap)matCmd.push(new Command(Command.REQUEST.MATERIAL.DIFFUSE,
                {
                    "material_name": material.name,
                    "argument_name": "reflectivity",
                    "texture_name": this.CONFIG.CUR_PROJ + this.CONFIG.TEXTURE_STORAGE + mat[mode.roughnessMap],
                    "texture_options": {
                        "scaling": material.roughnessMap.repeat,
                        "color_offset": material.roughnessMap.offset
                    },
                    "create_from_file": true
                }
            ));
            if (material.bumpMap || material.normalMap)matCmd.push(new Command(Command.REQUEST.MATERIAL.BUMP,
                {
                    "material_name": material.name,
                    "argument_name": "normal",
                    "texture_name": this.CONFIG.CUR_PROJ + this.CONFIG.TEXTURE_STORAGE + mat[(material.bumpMap ? mode.bumpMap : mode.normalMap)],
                    "texture_options": {
                        //"scaling": material.bumpMap.repeat,
                        //"color_offset": material.bumpMap?material.bumpMap.repeat:material.normalMap,
                        "bump_source": "mono_average",
                        "factor": material.bumpScale * 100
                    },
                    "create_from_file": true
                }
            ));

        });
    }

    private onFindMaterialByName() {
        let _f= this.seachWord?this.seachWord.toLowerCase():'';
        this.curMats = this.curMatCategories.filter((el)=> {
            return el["Material Name"] && el["Material Name"].toLowerCase().match(_f) instanceof Array;
        });
    }



    private reloadModel(event:any) {
        let _file = this.getFileName(event);
        this.webglView.preloader.fade(true);
        this.options.modelName = ENTITY.Config.MODELS.URL.MAIN + ENTITY.Config.MODELS.URL.CUPS + _file.name + ENTITY.Config.DELIMETER + _file.fileName;
        this.options.curModel = _file;
        this.webglView.app.loadModel((commands:any = [])=> {
            this.webglView.app.zoomCamera();
            this.webglView.app._animation.play();
            this.webglView.preloader.fade();
            this.commads.list = (commands.concat([
                new Command(Command.REQUEST.SCENE.IMPORT_EL, {
                    block: "true",
                    filename: ENTITY.Config.FILE.DIR.PREVIOUS + ENTITY.Config.MODELS.URL.CUPS + _file.name + ENTITY.Config.DELIMETER + _file.fileName
                }),
                new Command(Command.REQUEST.GROUP.ATTACH, {
                    group_name: Command.SCENE.MAIN_GROUP,
                    item_name: Command.SCENE.LOADED_GROUP
                })
            ])).concat(this.webglView.app.updateSceneMeshMatrix());
        });
    }

    public createPreset(mat:any) {
        let material = new THREE.MeshPhysicalMaterial({
            transparent: true,
            side: THREE.DoubleSide,
            name: 'exMaterial' + this.CONFIG.randomstr()
        });
        material.displacementScale = 0;
        material.displacementBias = 0;
        material.normalScale = new THREE.Vector2(1.2, 1.2);
        material.shading = THREE.SmoothShading;
        let category = mat["Category"].toLowerCase();
        switch (category) {
            case 'metal' :
            case   'marble - granite':
                material.envMapIntensity = 0.45;
                material.reflectivity = 2.1;
                material.clearCoat = 0;
                material.clearCoatRoughness = 1;
                material.metalness = 0.6;
                material.roughness = 1.7;
                material.opacity = 1;
                material.normalScale = new THREE.Vector2(0.72, -3.2);
                break;

            case 'ceramic'  :
            case  'paint':
                material.reflectivity = 0;
                material.clearCoat = 0.2;
                material.clearCoatRoughness = 1;
                material.metalness = 0.59;
                material.roughness = 0.96;
                material.opacity = 1;
                break;

            case 'concrete - asphalt':
                material.reflectivity = 0.2;
                material.clearCoat = 0.2;
                material.clearCoatRoughness = 1;
                material.metalness = 0.25;
                material.roughness = 0.97;
                material.opacity = 1;
                break;

            case 'fabric'  :
            case  'ground':
                material.reflectivity = 0.0;
                material.clearCoat = 0.4;
                material.clearCoatRoughness = 1;
                material.metalness = 0.02;
                material.roughness = 1;
                material.opacity = 1;
                break;

            case 'stone':
            case 'leather':
                material.reflectivity = 0.2;
                material.clearCoat = 0.5;
                material.clearCoatRoughness = 1;
                material.metalness = 0.25;
                material.roughness = 1;
                material.opacity = 1;
                break;

            case 'translucent':
                material.reflectivity = 0;
                material.clearCoat = 1;
                material.clearCoatRoughness = 1;
                material.metalness = 0.59;
                material.roughness = 0.96;
                material.opacity = 0.9;
                break;

            default:
                material.metalness = 0.15;
                material.clearCoat = 0;
                material.roughness = 0.973;
                material.clearCoatRoughness = 0.5;
                material.reflectivity = 0.5;
                material.opacity = 1;
        }

        return material;
    }


    public setSelectedImage(room:any) {
        this.flagModal = false;
        this.selectFlag = 'Select Position';
        setTimeout(()=> {
            this.createClaraIO(room.id);
        }, 1000)
    }

    public createClaraIO(id:string) {
        let clara = claraplayer('clara-embed');
        //"a11223cf-0b6b-4272-8e4b-d6516fda8f80"
        //clara.sceneIO.fetchAndUse("a11223cf-0b6b-4272-8e4b-d6516fda8f80");

        let convertModel = ()=> {
            let exporter = new THREE.OBJExporter();
            let model:any = this.curModel;
            let test = model.clone();
            let group = new THREE.Object3D();
            test.children.forEach((child, key)=> {
                group.add(test.children[key].clone());
            });

            return exporter.parse(group);
        };

        let rand = (to, from)=> {
            return Math.floor((Math.random() * from) + to);
        };

        let exportModel = (file)=> {
            let parent = clara.scene.find({'name': 'Objects'});
            let type = 'PolyMesh';
            let plugs = {
                Transform: [['Transform', {
                    translation: {
                        x: rand(1, 10),
                        y: rand(1, 10),
                        z: rand(1, 10)
                    }
                }]],
                Properties: [['Default', {}]]
            };
            plugs[type] = [['Torus', {}]];
            console.log(parent);
            clara.scene.addNode({name: 'TEST', type: type, plugs: plugs, parent: parent});
        };

        clara.sceneIO.fetchAndUse(id).then(()=> {
            document.getElementById('baseScene').setAttribute('href', 'https://clara.io/view/' + id);
        });

        let result = convertModel();
        exportModel(result);
        //this.exportModel(id);
    }

    public changeModelPart(event:any) {
        this.options.modelName = ENTITY.Config.MODELS.URL.MAIN + ENTITY.Config.MODELS.URL.CUPS_DECOR + this.testIndex + ENTITY.Config.DELIMETER + event.name,
            this.options.params = event.params;
        this.webglView.preloader.fade(true);
        this.webglView.app.loadModel((commands:any = [])=> {
            this.options.params = false;
            //this.webglView.app.zoomCamera();
            this.webglView.app._animation.play();
            this.webglView.preloader.fade();
            this.commads.list = this.commads.list.concat((commands.concat([
                new Command(Command.REQUEST.SCENE.IMPORT_EL, {
                    block: "true",
                    filename: ENTITY.Config.FILE.DIR.PREVIOUS + ENTITY.Config.MODELS.URL.CUPS_DECOR + this.testIndex + ENTITY.Config.DELIMETER + event.name
                }),
                new Command(Command.REQUEST.GROUP.ATTACH, {
                    group_name: Command.SCENE.MAIN_GROUP,
                    item_name: Command.SCENE.LOADED_GROUP
                })
            ])).concat(this.webglView.app.updateSceneMeshMatrix()));
        });

    }

}
