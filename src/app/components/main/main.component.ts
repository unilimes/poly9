import { Component,ViewChild } from '@angular/core';

import * as ENTITY from '../../entities/entities';
import {WebglView} from '../../directives/directives';

declare let window:any, THREE:any, $:any, async:any, claraplayer:any;

@Component({
    selector: 'app-main-component',
    template:'<main></main>'
})
export class MainComponent   {
    @ViewChild("webglView")
        webglView:WebglView;
      mainModel:any={};
    protected parent:any;
    protected showGlyphicon:any=[];
      options:any;
    protected CONFIG:any;
      commands:Array<any> = [];
    constructor() {
        this.CONFIG = ENTITY.Config;
        this.parent = this;
    }
}
