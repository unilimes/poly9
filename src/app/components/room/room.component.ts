import { Component, OnInit ,ViewChild,HostListener,Input} from '@angular/core';
import {document} from "ngx-bootstrap/utils/facade/browser";

import {MainComponent} from '../main/main.component';
import {LandingComponent} from '../landing/landing.component';
import {RealityServerService,Command} from '../../services/services';

declare let THREE:any, KUTE:any;

@Component({
    selector: 'app-room',
    templateUrl: './room.component.html',
    styleUrls: ['./room.sass']
})
export class RoomComponent extends MainComponent implements OnInit {
    curRoom:any;
    private rooms:Array<any> = [];//list of rooms
    private curQuad:any;//cur select quad position for model in the room
    private renderRemote:boolean = false;//is render remote in progress
    private canRender:boolean = false;//check if render is availble
    private canSave:boolean = false;//check if render is finish
    private showModal:boolean = false;//check if current component is visible
    @ViewChild("renderTarget")
        renderTarget:HTMLElement;//canvas dom elem
    @ViewChild("viewModal")
        viewModal:HTMLElement;//img preview dom elem
    @ViewChild("customBackground")
        customBackground:HTMLElement;//dark back dom elem
    @ViewChild("roomModal")
        roomModal:HTMLElement;//main componnt dom elem
    @Input() _parent:LandingComponent;//get the parent from inject attribute

    constructor(private realityServerService:RealityServerService) {
        super();
        this.rooms = [
            {
                id: "room_0",
                modelUrl: this.CONFIG.MODELS.URL.ROOMS + 'room/room.obj',
                preview: this.CONFIG.FILE.DIR.PROJECT_ROOM_PREVIEW + 'room-img-07-480x320.jpg',
                camera:{
                    m_world_to_obj: {
                        "ww": 1,
                        "wz": -2524.668265128712,
                        "wy": 839.6923608532521,
                        "wx": 128.78717191000828,
                        "zw": 0,
                        "zz": 0.32192535988403675,
                        "zy": -0.11032174072771123,
                        "zx": -0.9403154663124185,
                        "yw": 0,
                        "yz": 0.3241858317821754,
                        "yy": 0.9459934177739817,
                        "yx": 0,
                        "xw": 0,
                        "xz": 0.8895322417626201,
                        "xy": -0.3048369515841356,
                        "xx": 0.3403040167462895
                    },
                    m_focal:50,
                    m_aperture:100,
                    far:10000,
                },

                preset: [
                    {
                        id: "quad_0",
                        quad: {
                            x: 0.9,
                            y: 0.55
                        }
                    }, {
                        id: "quad_1",
                        quad: {
                            x: 0.4,
                            y: 0.7
                        }
                    }]
            },
            {
                id: "room_0",
                modelUrl: this.CONFIG.MODELS.URL.ROOMS + 'Inner_Apt145_AptType016/Inner_Apt145_AptType016.obj',
                preview: this.CONFIG.FILE.DIR.PROJECT_ROOM_PREVIEW + 'room-img-07-480x320.jpg',
                camera:{
                    m_world_to_obj: {
                        "ww": 1,
                        "wx": -203,
                        "wy": 133,
                        "wz": -320.52256482150733,
                        "xw": 0,
                        "xx": -0.95495090056124139,
                        "xy": 0.3624786312088406,
                        "xz": -0.7527609811007884,
                        "yw": 0,
                        "yx": -6.845793075767685e-8,
                        "yy": 0.9009837975107693,
                        "yz": 0.43385273610185826,
                        "zw": 0,
                        "zx": 0.8354877932985325,
                        "zy": 0.23840603713001626,
                        "zz": -0.49509868582850214,
                    },
                    m_focal:50,
                    m_aperture:100,
                    far:10000,
                },

                sun_dir: {"x": -0.743629442359553849, "y": 0.2301168728919780351, "z": -0.024483192611724406},
                preset: [
                    {
                        id: "quad_0",
                        quad: {
                            x: 0.9,
                            y: 0.55
                        }
                    }, {
                        id: "quad_1",
                        quad: {
                            x: 0.4,
                            y: 0.7
                        }
                    }]
            }
        ];

    }

    ngOnInit() {
    }

    ngOnDestroy() {
        this.destroy();
    }

    toggleModal() {
        this.showModal = !this.showModal;
        this.renderRemote = this.canSave = false;
        if (this.webglView && this.webglView.app && this.webglView.app.innerModel)this.webglView.app.innerModel.userData.refresh();
    }

    private save() {

        let _canvas = document.createElement('canvas'),
            backI = this.renderTarget['nativeElement'];
        _canvas.width = backI.clientWidth;
        _canvas.height = backI.clientHeight;
        let cntx = _canvas.getContext('2d');

        cntx.drawImage(backI, (_canvas.width - backI.clientWidth) / 2, 0, backI.clientWidth, backI.clientHeight);
        var link = document.createElement("a");
        link.setAttribute("href", _canvas.toDataURL());
        link.setAttribute("download", "poly9.png");
        link.click();
    }

    public render(img:any = null, cnt:MainComponent = this) {
        this.renderRemote = true;
        this.canRender = false;
        let _img = img || this.renderTarget['nativeElement'],
            _self = this;
        _img.onload = function () {
            this['_onload']();
            console.log("preview are refreshed");
            _self.canRender = _self.canSave = true;
        }
        this.realityServerService.renderScene(this.curRoom, {
            formatRenderImg: img ? "png" : null,
            component: cnt,
            self: this,
            _webgl: this._parent.webglView,
            img: _img
        });
    }

    private onBackFromRemoteRender() {
        this.realityServerService.cancelRender();
        this.renderRemote = false;
    }

    private select(room:any) {
        this.canRender = false;
        let hasRoom = /*this._parent.webglView.app._datGui.custom.renderWithRoom || */1,
            _webGL = this._parent.webglView;
        _webGL.app.camera.noChange = false;
        if (room.scopeName) {
            this.canRender = true;
            this.curRoom = room;
        } else {
            this.destroy(room, ()=> {
                if (hasRoom) {
                    this.options = {
                        modelName: this.CONFIG.MODELS.URL.MAIN + room.modelUrl,
                        component: this,
                        room: room,
                        _webgl: _webGL,
                        shadow: true,
                        onSelected: (quad)=> {
                            this._parent.commands = _webGL.app.setNewPst(this._parent.options.curModel.model.position ? this._parent.options.curModel.model.position[this.curRoom.id][quad.id] : {
                                x: 0,
                                y: 0,
                                z: 0
                            });
                            this.curQuad = quad;
                            this.renderRemote = true;
                            this.render();
                        },
                        //helper: true,
                        onFinish: (callback)=> {
                            this.realityServerService.initScope(room, {
                                hasRoom: hasRoom,
                                self: this,
                                component: this._parent,
                                _webgl: _webGL,
                                onSuccess: ()=> {
                                    this.canRender = true;
                                    if (callback)callback();
                                }
                            });
                        }
                    };
                } else {
                    this.toggleModal();
                    this._parent.webglView.app.setBackground(room.preview);

                    this.realityServerService.initScope(room, {
                        component: this._parent,
                        _webgl: this._parent.webglView,
                        onSuccess: ()=> {
                            this._parent.webglView.canRender = true;
                        }
                    });

                }
                this.curRoom = room;
            });

        }
    }

    destroy(room:any = null, callback:Function = null) {
        this.canRender = false;
        this.realityServerService.destroy(room || this.curRoom, callback);
    }

    @HostListener('window:unload', ['$event'])
    unloadWnHandler(event) {
        this.destroy();
    }

    @HostListener('window:onbeforeunload', ['$event'])
    befUnloadHandler(event) {
        this.destroy();
    }
}
