import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(value: any, args?: string): any {
    if (!value || !args) {
      return value;
    }
    args = args.toUpperCase();
    return value.filter((item: any) => item["Material Name"].toUpperCase().indexOf(args) !== -1);
  }

}
