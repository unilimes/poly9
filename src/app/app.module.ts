import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {LimitToPipe} from './pipes/limit-to.pipe';
import {SearchPipe} from './pipes/search.pipe';

import {DataService} from './services/data.service';
import {RealityServerService} from './services/reality-server.service';
import {Storage} from './services/storage';

import {AppComponent} from './app.component';
import {MainComponent} from './components/main/main.component';
import {LandingComponent} from './components/landing/landing.component';
import {RoomComponent} from './components/room/room.component';
import {WebglView,Preloader,PreSet,DefaultImage,Decor} from './directives/directives';
import {NgxPaginationModule} from 'ngx-pagination';

import { enableProdMode } from '@angular/core';

// enable production mode and thus disable debugging information
enableProdMode();
@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    LandingComponent,
    LimitToPipe,
    SearchPipe,
    WebglView,
    Preloader,
    PreSet,
    DefaultImage,
    Decor,
    RoomComponent
  ],
  imports: [
    NgbModule.forRoot(),
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    NgxPaginationModule,
  ],
  providers: [DataService, RealityServerService, Storage],
  bootstrap: [AppComponent]
})
export class AppModule {}
