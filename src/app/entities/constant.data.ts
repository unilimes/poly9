declare var location:any;
export class Config {
    static DELIMETER:any = '/';
    static EVENTS_NAME = {
        KEY: {
            DOWN: 'keydown',
            UP: 'keyup',
        },
        CNTXMENU: 'contextmenu',
        RESIZE: 'resize',
        DB_CLICK: 'dblclick',
        SELECT_START: 'selectstart',
        CLICK: 'click',
        TOUCH_START: 'touchstart',
        TOUCH_MOVE: 'touchmove',
        TOUCH_END: 'touchend',
        MOUSE_OUT: 'mouseout',
        MOUSE_DOWN: 'mousedown',
        MOUSE_MOVE: 'mousemove',
        MOUSE_UP: 'mouseup'
    };
    static FILE = {
        DIR: {
            CURRENT:'.',
            PREVIOUS:'',
            DELIMETER: "/",
            PROJECT_ROOM_PREVIEW: 'assets/images/rooms/',
            PROJECT_ROOM_MODELS: 'assets/models/rooms/',
            PROJECT_TEMPLATE: {
                NAME: 'assets/templates/',
                CSS: 'style.css',
                HTML: 'index.html',
                JS: 'index.js',
                TYPES: ['controls/', 'tooltip/', 'preloader/'],
                _TYPE: {
                    PRELOADER: 2,
                    TOOLTIP: 1,
                    CONTROLS: 0,
                }

            },
            PREVIEW: {
                LOW: 'low/',
                WEBP: 'webp/',
            }
        }

    };
    static PATTERNS:any = {
        URL: /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi,
        EMAIL: /\S+@\S+\.\S+/
    };
    static LOGO_TYPES:any={
        IMG:1,
        TEXT:2,
    };
    static MAT_MAPS:any =['map',"normalMap","bumpMap","metalnessMap","roughnessMap","displacementMap"];
    static IGNORE:string = 'ignore';
    static FACTOR:any = {
        MATERIALS: 'https://s3.ap-south-1.amazonaws.com/p9-platform/images/',
        MODELS: 'https://s3.ap-south-1.amazonaws.com/p9-platform/Models/',
    };
    static _CUR_PROJ:string='poly9/';
    static CUR_PROJ:string='';
    static ASSETS:string='assets/';
    static OBJ_STORAGE:string=  Config.ASSETS+'models/my_models/';
    static IMG_STORAGE:string= Config.ASSETS+'images/';
    static TEXTURE_STORAGE:string= Config.IMG_STORAGE+'Materials/';
    static UI_STORAGE:string= Config.IMG_STORAGE/*'./assets/images/'*/+'ui/';
    static MODELS:any = {

        URL:{
            MAIN:'http://54.164.221.209:8080/'+Config._CUR_PROJ,
            DEV_APP:location.protocol+"//"+location.hostname+':3009/public/uploadImg',
            ROOMS:Config.OBJ_STORAGE+'rooms/',
            CUPS:Config.OBJ_STORAGE+'cups/',
            CUPS_DECOR:Config.OBJ_STORAGE+'cups_decor/',
        },
        TYPES: {MESH: 'Mesh',MATERIAL:'material'},
        CATEGORY: {
            INNER: 'inner-from',
            EDITABLE_MAT: '--bla-bla-1'
        }
    };
    static randomInteger(min = 0, max = Date.now()) {
        return Math.round(min + Math.random() * (max - min))
    }

    static randomstr() {
        return Math.random().toString(36).replace(/[^a-z]+/g, '');
    }


}
export class ProjClasses {
    static IMG_SLIDER:string = 'img-slider-container';
    static CENTER_CONTAINER:string = 'center-container';
    static PROJ_TOOLTIP_CONTAINER:string = 'tooltip-container';
    static PROJ_BACK_AREA:string = 'back-area';
    static PROJ_CONTROLS:string = 'oxi-controls';
    static PROJ_CONTROLS_MOVE:string = 'oxi-controls-move';
    static PROJ_CONTROLS_CONTAINER:string = 'oxi-controls-container';
    static PROJ_COMPASS:string = 'kompass';
    static PROJ_TOOLTIPS:any = {
        CONTAINER: 'tooltip-container',
        TOOLTIP: 'tooltip',
        HEADER: 'header',
        BODY: 'body',
    };
    static ACTIVE:string = 'active';

}
