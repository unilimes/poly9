import {MApp} from "./MApp";
import {GLMain} from "./GLMain";
import {DecorData} from "../../directives";
import {Config} from "../../../entities/constant.data";
declare var THREE:any, document:any;
declare var NMO_FileDrop:any;
/*
 * ModelCustomImage - model texture(s) control
 * @angle - rotation decor
 * @scale - size decor
 * @activeArea - current XY decor position
 * @options - decor data for decoration
 * */
export class ModelCustomImage extends GLMain {
    private texture:any;
    material:any;
    private canvas:any;
    private canvasList:any;
    private mapsText:any;
    private cntx:any;
    private txtHalfLengthCenter:number;
    private fontSize:number = 30;
    private padding:number = 60;
    private defSize:number = 1024;
    private textureLoader:any;
    private lastEv:any;
    private mapsUpdate:Array<any> = Config.MAT_MAPS;
    id:number = Date.now();
    type:number;
    angle:number = 0;
    scale:number = 1;
    active:boolean = true;
    locked:boolean;
    private hasToBeLocked:boolean;
    activeArea:ActiveArea;
    options:DecorData;
    app:MApp;
    tabs:TabEdit;
    MAT_PREFIX:string = "_def"

    constructor(app:MApp, options:DecorData, onFinish:Function = ()=> {
    }) {

        super();

        //let canvas = this.canvas = document.createElement("canvas");
        //canvas.width = canvas.height = this.defSize;
        //this.type = this.options.image ? this.ENTITY.Config.LOGO_TYPES.IMG : this.ENTITY.Config.LOGO_TYPES.TEXT;
        this.app = app;
        this.canvasList = {};
        this.texture = {};
        this.mapsText = {};
        this.activeArea = new ActiveArea();
        this.options = options;
        this.tabs = new TabEdit();

        NMO_FileDrop.initHeightMapWithImg(options.preview, (maps)=> {
            this.mapsText = maps;
            for (let i = 0; i < this.mapsUpdate.length; i++) {
                let canvas = this.canvasList[this.mapsUpdate[i]] = document.createElement("canvas");
                canvas.width = canvas.height = this.defSize;
                this.texture[this.mapsUpdate[i]] = new THREE.Texture();
                this.texture[this.mapsUpdate[i]].image = canvas;
                this.texture[this.mapsUpdate[i]].category = "custom";
                this.updateContext(canvas);
            }
            this.updateMaterial();
            onFinish();
        });


    }

    private onReSize() {
        //this.updateContext();
    }

    private updateContext(canvas) {
        canvas.cntx = canvas.getContext('2d');
        canvas.cntx.fillStyle = '#ffffff';
        canvas.cntx.strokeStyle = '#000000';
        canvas.cntx.lineWidth = 2;
    }

    dataURItoBlob() {
        let byteString = atob(this.canvas.toDataURL().split(',')[1]);
        let ab = new ArrayBuffer(byteString.length);
        let ia = new Uint8Array(ab);
        for (let i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }
        return new Blob([ab], {type: 'image/jpeg'});
    }

    getImgBlob() {
        let imageData = this.cntx.getImageData(0, 0, this.canvas.width, this.canvas.height).data,
            buffer:any = new Uint8Array(imageData.length);
        for (let index = 0; index < imageData.length; index++)
            buffer[index] = imageData[index];

        let imageBlob:any = new Blob(buffer);
        imageBlob.lastModifiedDate = new Date();
        imageBlob.name = this.ENTITY.Config.randomstr() + '.jpg';

        return imageBlob; //
    }

    destroy() {
        for (let i = 0; i < this.material.logos.length; i++) {

            if (this.material.logos[i].id == this.id) {

                for (let d = 0; d < this.app.decorations.length; d++) {
                    if (this.app.decorations[d].id == this.id) {
                        this.material.logos.splice(i, 1);
                        this.app._events.lastLogo = null;
                        let droped = this.app.decorations.splice(d, 1)[0];
                        droped.locked = droped.active = false;
                        return droped.updateCanvas();
                    }
                }

            }
        }
    }

    setActive(active:boolean = false, uv:any = null, noneedRefreshArea:boolean = false) {
        let isAct = this.active;
        this.active = active;
        this.updateArea(uv, noneedRefreshArea);
        this.updateCanvas(uv);
    }

    updateArea(uv, noneedRefreshArea:boolean = false) {
        this.activeArea.updateOffsets(this.tabs.currentMode ? this.activeArea.uv : uv, this.canvasList[this.mapsUpdate[0]], noneedRefreshArea);
    }

    updateMaterial(mat:any = null, uv:any = null, ev:any = null) {
        let material = mat,
            isSameMat;
        if (!material)material = this.material;
        if (!material)material = this.options.material;
        if (this.material) {
            isSameMat = this.material.uuid == material.uuid;
            if (isSameMat) {

            } else {
                for (let i = 0, logs:Array<ModelCustomImage> = this.material.logos; i < logs.length; i++) {
                    if (logs[i].id == this.id) {
                        logs.splice(i, 1);
                        break;
                    }
                }
                if (this.material.logos.length) {
                    let lastLogo = this.material.logos[this.material.logos.length - 1];
                    for (let i = 0; i < this.mapsUpdate.length; i++) {
                        if (this.material[this.mapsUpdate[i]])this.material[this.mapsUpdate[i]] = lastLogo.texture[this.mapsUpdate[i]];
                    }
                    lastLogo.updateCanvas();
                } else {
                    for (let i = 0; i < this.mapsUpdate.length; i++) {
                        if (this.material[this.mapsUpdate[i]])this.material[this.mapsUpdate[i]] = this.material[this.MAT_PREFIX + this.mapsUpdate[i]];
                    }
                }
                this.material.needsUpdate = true;
            }


        }

        if (isSameMat) {
            if (this.material.lastEditDecor.id != this.id) {
                this.material.lastEditDecor = this;
                for (let i = 0; i < this.mapsUpdate.length; i++) {
                    if (this.material[this.mapsUpdate[i]])this.material[this.mapsUpdate[i]] = this.texture[this.mapsUpdate[i]];
                }
            }
        } else {
            this.material = material;
            material.lastEditDecor = this;
            if (!material.logos)material.logos = [];
            material.logos.push(this);

            for (let i = 0; i < this.mapsUpdate.length; i++) {
                if (i > 0 && !material[this.mapsUpdate[i]]) {
                    //this.canvasList[this.mapsUpdate[i]] = this.texture[this.mapsUpdate[i]] = null;
                } else {
                    if (
                        !material[this.MAT_PREFIX + this.mapsUpdate[i]] &&
                        material[this.mapsUpdate[i]]
                        && !material[this.mapsUpdate[i]].category
                    //&& material.logos.length < 2
                    ) {
                        console.log(this.mapsUpdate[i] + " was updated");
                        material[this.MAT_PREFIX + this.mapsUpdate[i]] = material[this.mapsUpdate[i]];
                    }
                    material[this.mapsUpdate[i]] = this.texture[this.mapsUpdate[i]];

                    if (material[this.MAT_PREFIX + this.mapsUpdate[i]]) {
                        this.canvasList[this.mapsUpdate[i]].width = material[this.MAT_PREFIX + this.mapsUpdate[i]].image.width;
                        this.canvasList[this.mapsUpdate[i]].height = material[this.MAT_PREFIX + this.mapsUpdate[i]].image.height;
                    } else {
                        this.canvasList[this.mapsUpdate[i]].width = this.canvasList[this.mapsUpdate[i]].height = this.defSize;
                    }
                    this.updateContext(this.canvasList[this.mapsUpdate[i]]);
                }


            }

        }
        this.material.needsUpdate = true;
        return this.updateCanvas(uv, ev);
    }

    isSelected(uv) {
        let
            diffuseCanvas = this.canvasList[this.mapsUpdate[0]],
            clientX = uv.x * diffuseCanvas.width,
            clientY = uv.y * diffuseCanvas.height,
            isSelc = clientX > this.activeArea.left_top_pos_x && clientX < this.activeArea.left_bottom_pos_x + diffuseCanvas.width / TabEdit.size &&
                clientY > this.activeArea.left_top_pos_y && clientY < this.activeArea.left_bottom_pos_y;
        if (isSelc) {
            this.tabs.selectMode(clientX, diffuseCanvas.height - clientY);
            if (this.tabs.currentMode == this.tabs.transform.pin) {
                this.tabs.currentMode = null;
                this.hasToBeLocked = !this.hasToBeLocked;
                if (!this.hasToBeLocked)this.locked = this.hasToBeLocked;
            } else if (this.tabs.currentMode == this.tabs.transform.drop) {
                return this.destroy();
            }

        }
        return isSelc;
    }


    updateTextures() {
        NMO_FileDrop.initHeightMapWithImg(this.options.preview, (maps)=> {
            this.mapsText = maps;
            this.updateMaterial();
        });
    }

    updateCanvas(uv:any = null, ev:any = null) {

        let
            center = uv || this.activeArea.uv,
            hasRedraw, item,
            loked;


        if (this.tabs.currentMode) {
            center = this.activeArea.uv;
            if (ev && this.lastEv) {
                if (this.tabs.currentMode == this.tabs.transform.rotate) {
                    this.angle += (/*uv.x > this.lastUv.x ||*/ ev.y > this.lastEv.y ? 1 : -1) * 0.01;
                } else if (this.tabs.currentMode == this.tabs.transform.resize) {
                    this.scale += ( ev.y > this.lastEv.y ? -1 : 1) * 0.01;
                } else if (this.tabs.currentMode == this.tabs.transform.copy) {
                    this.setActive(false, null, true);
                    this.tabs.currentMode = this.app._events.lastLogo = null;
                    let newM:ModelCustomImage = new ModelCustomImage(this.app, new DecorData(this.options.preview, this.options.jsonData, null, this.material), ()=> {
                        newM.setActive(true);
                        this.app.decorations.push(newM);
                        newM.updateMaterial(this.material, uv, ev);
                        this.app._events.lastLogo = newM;
                    });
                }
            }


        } else {
            this.activeArea.uv = center;
        }

        if (this.locked)return;
        this.locked = this.hasToBeLocked;
        for (let canvasEdit in this.canvasList) {
            let canvasCurEdit:any = this.canvasList[canvasEdit];
            if (!canvasCurEdit || !this.material[canvasEdit])continue;
            let ctx:any = canvasCurEdit.cntx;
            if (this.material[this.MAT_PREFIX + canvasEdit] && this.material[this.MAT_PREFIX + canvasEdit].image) {
                ctx.drawImage(this.material[this.MAT_PREFIX + canvasEdit].image, 0, 0);
            } else {
                ctx.fillStyle = '#ffffff';
                ctx.fillRect(0, 0, canvasCurEdit.width, canvasCurEdit.height);
            }
            for (let i = 0, logs:Array<ModelCustomImage> = this.material.logos; i < logs.length; i++) {

                ctx.save();

                if (logs[i].id == this.id) {

                    this.activeArea.update(center, canvasCurEdit, 0, 0, this.options.preview.width * this.scale, this.options.preview.height * this.scale, this.angle);
                    //if (i < logs.length - 1) {
                    //    item = i;
                    //    hasRedraw = logs[i];
                    //    ctx.restore();
                    //    continue;
                    //}
                }

                ctx.translate(logs[i].activeArea.center_x, logs[i].activeArea.center_y);
                ctx.rotate(logs[i].angle);
                if (logs[i].mapsText[canvasEdit]) {
                    ctx.drawImage(logs[i].mapsText[canvasEdit], -logs[i].activeArea.width / 2, -logs[i].activeArea.height / 2, logs[i].activeArea.width, logs[i].activeArea.height);
                }
                ctx.restore();
                if (canvasEdit == this.mapsUpdate[0]) logs[i].redrawTabs(ctx, canvasCurEdit.width);
            }
            //if (hasRedraw) {
            //    ctx.save();
            //    ctx.translate(hasRedraw.activeArea.center_x, hasRedraw.activeArea.center_y);
            //    ctx.rotate(hasRedraw.angle);
            //    ctx.drawImage(hasRedraw.options.preview, -hasRedraw.activeArea.width / 2, -hasRedraw.activeArea.height / 2, hasRedraw.activeArea.width, hasRedraw.activeArea.height);
            //    ctx.restore();
            //    if (canvasEdit == this.mapsUpdate[0]) {
            //        hasRedraw.redrawTabs(ctx,canvasCurEdit.width);
            //        this.material.logos.push(this.material.logos.splice(item, 1)[0]);
            //    }
            //}

            this.lastEv = ev;
            this.texture[canvasEdit].image = canvasCurEdit;
            this.texture[canvasEdit].needsUpdate = true;
            canvasCurEdit.hasChanges = true;

        }
        this.app._animation.play();

    }


    private redrawTabs(ctx, size) {
        if (this.active) {
            let _ar:ActiveArea = this.activeArea;
            ctx.strokeRect(_ar.left_top_pos_x, _ar.left_top_y, _ar.border_width, _ar.border_height);
            this.tabs.update(ctx, this.locked, _ar.left_bottom_pos_x, _ar.left_top_y, size);
        }
    }

}
class ModeEdit {
    image:any;
    left_top_x:number;
    left_top_y:number;
    size:number;

    constructor(img:string = null) {
        this.image = img;
    }
}
class TransformMode {
    //translate:ModeEdit;
    rotate:ModeEdit;
    pin:ModeEdit;
    resize:ModeEdit;
    copy:ModeEdit;
    drop:ModeEdit;
}
class TabEdit {
    static size:number = 32;
    private transforms = ['resize', 'pin', 'rotate', 'copy', 'drop'];
    transform:TransformMode;
    currentMode:ModeEdit;
    private id:number = Date.now();

    constructor() {
        this.transform = new TransformMode();
        let imageLoader = new THREE.ImageLoader(new THREE.LoadingManager());
        this.transforms.forEach((mode)=> {

            this.transform[mode] = new ModeEdit(imageLoader.load(Config.UI_STORAGE + mode + ".png"));
        });
    }

    update(cntx, locked, x, y, size:number = TabEdit.size * 2) {
        let _s = size / TabEdit.size;
        for (let key = 0; key < this.transforms.length; key++) {
            let cur = this.transform[this.transforms[key]];
            if (!cur)continue;
            cur.left_top_x = x;
            cur.left_top_y = y + _s * key;
            cur.size = _s;
            if (locked) {
                if (key == 1) {
                    if (cur == this.currentMode)cntx.fillRect(cur.left_top_x, cur.left_top_y, _s, _s);
                    cntx.drawImage(cur.image, cur.left_top_x, cur.left_top_y, _s, _s);
                }
            } else {
                if (cur == this.currentMode)cntx.fillRect(cur.left_top_x, cur.left_top_y, _s, _s);
                cntx.drawImage(cur.image, cur.left_top_x, cur.left_top_y, _s, _s);
            }
        }
    }

    selectMode(uvX, uvY) {
        this.currentMode = null;
        for (let f = 0; f < this.transforms.length; f++) {
            let cur = this.transform[this.transforms[f]];
            if (
                uvX > cur.left_top_x && uvX < cur.left_top_x + cur.size &&
                uvY > cur.left_top_y && uvY < cur.left_top_y + cur.size
            ) {
                return this.currentMode = this.transform[this.transforms[f]];

            }
        }
    }
}
class ActiveArea {
    prevCenter:any = {x: 0.5, y: 0.5};
    uv:any = {x: 0.5, y: 0.5};
    lastUv:any = {x: 0.5, y: 0.5};
    lastOffsetUv:any = {x: 0, y: 0};
    offsetX:number = 0;
    offsetY:number = 0;
    _offsetX:number = 0;
    _offsetY:number = 0;
    center_x:number = 512;
    center_y:number = 512;
    width:number = 0;
    height:number = 0;
    left_top_pos_x:number = 0;
    left_top_pos_y:number = 0;
    left_bottom_pos_x:number = 0;
    left_bottom_pos_y:number = 0;
    left_top_x:number = 0;
    left_top_y:number = 0;
    left_bottom_x:number = 0;
    left_bottom_y:number = 0;

    border_left_top_x:number = 0;
    border_left_top_y:number = 0;

    border_right_bottom_x:number = 0;
    border_right_bottom_y:number = 0;

    border_width:number = 0;
    border_height:number = 0;


    constructor() {

    }

    updateOffsets(uvs, area, noneedRefreshArea) {

        let uv = uvs || this.lastUv,
            offsetUvX = uv.x - this.prevCenter.x,
            offsetUvY = (uv.y - this.prevCenter.y);

        this._offsetX = (offsetUvX) * area.width;
        this._offsetY = (offsetUvY) * area.height * -1;
        if (noneedRefreshArea)return;
        this.prevCenter = {x: uv.x - this.lastOffsetUv.x, y: uv.y - this.lastOffsetUv.y};
        this.lastOffsetUv.x = offsetUvX;
        this.lastOffsetUv.y = offsetUvY;

    }

    update(center, canvas, offsetX:number = 0, offsetY:number = 0, width:number = 0, height:number = 0, angle:number = 0) {


        this.offsetX = this._offsetX;
        this.offsetY = this._offsetY;
        //
        this.center_x = ( center.x  ) * canvas.width - this.offsetX;
        this.center_y = canvas.height - ( center.y ) * canvas.height - this.offsetY;
        //
        this.width = width;
        this.height = height;
        //
        let nw = Math.abs(width * Math.cos(angle)) + Math.abs(height * Math.sin(angle)),
            nh = Math.abs(width * Math.sin(angle)) + Math.abs(height * Math.cos(angle));
        //
        this.left_top_pos_x = this.center_x - width / 2 - (nw - width) / 2;
        this.left_top_pos_y = canvas.height - this.center_y - height / 2 - (nh - height) / 2;

        this.left_top_x = (this.center_x ) / canvas.width;
        this.left_top_y = this.center_y - height / 2 - (nh - height) / 2;
        this.left_bottom_x = (this.center_x + this.width) / canvas.width;
        this.left_bottom_y = (this.center_y + this.height) / canvas.height;
        //
        this.border_width = nw;
        this.border_height = nh;
        //
        this.left_bottom_pos_x = this.center_x + nw / 2;
        this.left_bottom_pos_y = canvas.height - this.center_y + nh / 2;

        this.lastUv = center;
    }


    updateBorder() {
        this.border_right_bottom_x = this.border_left_top_x + this.border_width;
        this.border_right_bottom_y = this.border_left_top_y + this.border_height - TabEdit.size;
    }
}