import {MApp} from "./MApp";
import {GLMain} from "./GLMain";
import {ModelCustomImage} from "./ModelCustomImage";
import {DecorData} from "../../directives";

declare var THREE:any, Pace:any;
/*
 * MEvents - events controls on renderer target
 * @lastLogo - last selected decoration
 * @lastSelectedMesh - last selected detail of model
 * @lastInter - last selected details data
 * @keyCode - all key active codes
 * */
export class MEvents extends GLMain {
    private  EVENTS_NAME:any;
    private mouse:MMouse;
    private raycaster:any;
    private canEdit:boolean = true;
    private pathOnMove:number = 50;
    private lastEv:any;
    lastLogo:ModelCustomImage;
    lastSelectedMesh:any;
    lastInter:any;
    curDrag:any;
    keyCode:Array<number> = [];


    constructor(private main:MApp) {
        super();
        let
            _self = this,
            elem = main.gl.domElement,
            handler = (elem.addEventListener || elem.attachEvent).bind(elem);
        this.EVENTS_NAME = this.ENTITY.Config.EVENTS_NAME;
        this.mouse = new MMouse(main);
        this.raycaster = new THREE.Raycaster();

        handler(this.EVENTS_NAME.MOUSE_DOWN, (e)=>this.onMouseDown(e));
        handler(this.EVENTS_NAME.MOUSE_UP, (e)=>this.onMouseUp(e));
        handler(this.EVENTS_NAME.MOUSE_MOVE, (e)=>this.onMouseMove(e));
        handler(this.EVENTS_NAME.DB_CLICK, (e)=>this.onDbClick(e));
        handler(this.EVENTS_NAME.SELECT_START, main.onEventPrevent);
        handler("webkitfullscreenchange", (e)=>this.onfullscreenchange(e));
        handler("mozfullscreenchange", (e)=>this.onfullscreenchange(e));
        handler("fullscreenchange", (e)=>this.onfullscreenchange(e));
        handler(this.EVENTS_NAME.MOUSE_OUT, (e)=>this.onMouseOut(e));
        handler("drop", (e)=>this.onDrop(e));
        handler("dragover", (e)=> this.main.onEventPrevent(e));

        window.addEventListener('resize', ()=>this.onWindowResize());

        window.addEventListener('keydown', function (event:any) {

            let control:any = main.transformControls;
            _self.keyCode.push(event.keyCode);
            switch (event.keyCode) {

                case 81: // Q
                    control.setSpace(control.space === "local" ? "world" : "local");
                    break;

                case 16: // SHIFT
                    main.controls.enabled = false;

                case 17: // Ctrl


                    //if (main.transformControls)main.transformControls.setTranslationSnap(100);
                    //if (main.transformControls)main.transformControls.setRotationSnap(THREE.Math.degToRad(15));
                    break;

                case 87: // W
                    if (main.transformControls)main.transformControls.setMode("translate");
                    break;

                case 69: // E
                    if (main.transformControls)main.transformControls.setMode("rotate");
                    break;

                case 82: // R
                    if (main.transformControls)main.transformControls.setMode("scale");
                    break;

                case 187:
                case 107: // +, =, num+
                    if (main.transformControls)main.transformControls.setSize(main.transformControls.size + 0.1);
                    break;

                case 189:
                case 109: // -, _, num-
                    if (main.transformControls)main.transformControls.setSize(Math.max(main.transformControls.size - 0.1, 0.1));
                    break;
            }

        });

        window.addEventListener('keyup', function (event:any) {
            let control:any = main.transformControls;
            _self.keyCode = [];
            switch (event.keyCode) {

                case 17: // Ctrl
                    //control.setTranslationSnap(1);
                    //control.setRotationSnap(1);
                    break;
                case 16: // SHIFT
                    main.controls.enabled = true;
                    break;
            }
        });

        this.onWindowResize();
    }

    onWindowResize() {
        let app = this.main,
            _w = app._W(),
            _h = app._H(),
            _px = 'px';

        app.camera.aspect = _w / _h;
        app.camera.updateProjectionMatrix();
        app.gl.setSize(_w, _h);
        //app._container.style.height = _h + _px;
        if (app._animation)app._animation.play();
        if (app.rs_camera)app.rs_camera.helper.Update_RS_Camera(_w, _h, app.camera, app.controls);
    }

    private onDrop(e) {
        this.main.onEventPrevent(e);
        let data = this.curDrag;

        if (data instanceof ModelCustomImage) {
            this.onSelected(e, (intersects)=> {
                if (intersects.length) {
                    if (this.lastLogo)this.lastLogo.setActive(false, null, true);
                    let newM:ModelCustomImage = new ModelCustomImage(this.main, new DecorData(data.options.preview, data.options.jsonData, null, data.material), ()=> {
                        newM.activeArea.uv = newM.activeArea.lastUv =newM.activeArea.prevCenter = intersects[0].uv;
                        newM.setActive(true);
                        this.main.decorations.push(newM);
                        newM.updateMaterial(intersects[0].object.material, intersects[0].uv);
                        this.lastLogo = newM;
                    });


                }
            });
        }
    }

    onMouseUp(ev:any, acc:any = false) {
        this.main.onEventPrevent(ev);
        this.mouse.down = this.lastSelectedMesh = null;
        this.canEdit = !this.canEdit;
        this.main.controls.enabled = true;
        if (this.main._animation)this.main._animation.play();
        //


        if (this.lastLogo) {
            this.lastLogo.updateArea(null);
        }else{
            this.onSelected(ev, (intersects)=> {

                if (intersects.length) {

                    if (!this.main.controls.enabled) {
                    }
                    if (this.keyCode.indexOf(17) > -1) {
                        this.main._datGui.onSelect(intersects[0].object);
                    }/*else if(intersects[0].object.canTransform){
                     this.main.transformControls.attach(intersects[0].object);
                     }*/ else {
                        if (this.main.main.options.component.acc /*&& !this.mouse.hasMove*/) {
                            this.main.main.options.component.acc.toggle('acc-row-' + intersects[0].object.userData.name[intersects[0].object.userData.name.length - 1]);
                            this.main.main.options.component.curModel = intersects[0].object;
                        }

                    }
                }

            });
        }

    }

    private onMouseMove(ev:any) {
        this.mouse.hasMove = true;
        this.lastSelectedMesh = null;
        if (this.main._animation)this.main._animation.play();
        if (!this.main.controls.enabled) {
            this.onSelected(ev, (inters)=> {
                if (inters[0]) {
                    if (this.lastLogo) {
                        this.lastLogo.updateMaterial(inters[0].object.material, inters[0].uv, ev);
                    }
                    this.lastSelectedMesh = inters[0].object;
                } else {
                    if (this.lastLogo) {
                        this.lastLogo.updateMaterial(null, null, ev);
                    }
                }
            });
        }
    }

    private onSelected(ev, callback) {
        let intersectList = this.inter(ev);
        if (intersectList && intersectList[0]) {
            if (intersectList[0].object.name.match(this.ENTITY.Config.IGNORE))return;
            this.lastInter = intersectList[0];
        }
        callback(intersectList);
    }

    private onMouseDown(ev:Event) {
        this.mouse.hasMove = this.keyCode.indexOf(17) > -1;
        this.mouse.down = ev;
        this.lastEv = this.lastSelectedMesh = false;
        if (this.main.controls.enabled) {
            this.onSelected(ev, (inters)=> {
                let _inter = inters[0];
                if (_inter) {
                    this.lastSelectedMesh = _inter.object;

                    let curLogo = this.isLogoSelected(_inter.uv,_inter.object.material.logos);
                    if (this.lastLogo && ((curLogo && curLogo.id != this.lastLogo.id) || !curLogo))this.lastLogo.setActive(false, null, true);
                    if (curLogo) {
                        curLogo.setActive(true, _inter.uv);
                        this.main.controls.enabled = false;
                    }

                    this.lastLogo = curLogo;
                }
            });
        }
    }

    private isLogoSelected(uv,ar) {
        let decots = ar ||this.main.decorations;
        for (let i = 0, decor:Array<ModelCustomImage> =decots; i < decor.length; i++) {
            if (decor[i].isSelected(uv)) {
                return decor[i];
            }
        }
    }

    private onMouseOut(ev:any) {
        if (this.mouse.down)this.onMouseUp(ev);
    }

    toggleFullscreen() {
        //let _d:any = document;
        //if(this.main.gl.domElement.parentNode.isFullScreen){
        //    this.cancelFullscreen();
        //}else{
        this.launchFullScreen(this.main.gl.domElement.parentNode);
        //}
    }

    //Запустить отображение в полноэкранном режиме
    launchFullScreen(element) {
        if (element.requestFullScreen) {
            element.requestFullScreen();
        } else if (element.mozRequestFullScreen) {
            element.mozRequestFullScreen();
        } else if (element.webkitRequestFullScreen) {
            element.webkitRequestFullScreen();
        }
    }

// Выход из полноэкранного режима
    cancelFullscreen() {
        let _d:any = document;
        if (_d.cancelFullScreen) {
            _d.cancelFullScreen();
        } else if (_d.mozCancelFullScreen) {
            _d.mozCancelFullScreen();
        } else if (_d.webkitCancelFullScreen) {
            _d.webkitCancelFullScreen();
        }
    }

    onfullscreenchange(e) {
        let _d:any = document;
        let fullscreenElement =
            _d.fullscreenElement ||
            _d.mozFullscreenElement ||
            _d.webkitFullscreenElement;
        let fullscreenEnabled =
            _d.fullscreenEnabled ||
            _d.mozFullscreenEnabled ||
            _d.webkitFullscreenEnabled;
        console.log('fullscreenEnabled = ' + fullscreenEnabled, ',  fullscreenElement = ', fullscreenElement, ',  e = ', e);
    }

    private onDbClick(e:any) {
        if (this.canEdit) {
            this.main.controls.target.copy(this.main.scene.position);
            this.main.controls.update();
        }
        this.main.onEventPrevent(e);
    }

    onCntxMenu(event) {
        event.preventDefault();
        return false;
    }

    inter(ev:any, arg:any = null) {
        var _self = this,
            elements = arg && arg.childs ? arg.childs : [_self.main.model];

        if (!elements)return false;
        if (arg && arg.position) {
            var direction = new THREE.Vector3().subVectors(arg.target, arg.position);
            _self.raycaster.set(arg.position, direction.clone().normalize());
        } else {
            let
                mouseVector = _self.mouse.interPoint(ev);
            _self.raycaster.setFromCamera(mouseVector, _self.main.camera);
        }

        return _self.raycaster.intersectObjects(elements, true);
    }
}
export class MMouse {
    main:MApp;
    isDown:boolean;
    hasMove:boolean;
    down:any;


    constructor(main) {
        this.isDown = false;
        this.main = main;
    }

    interPoint(ev) {
        let _slider = this.main.gl.domElement,
            rect = _slider.getBoundingClientRect(),
            canvasW = _slider.clientWidth,
            canvasH = _slider.clientHeight,

            _x = (ev ? ev.clientX : canvasW / 2) - rect.left,
            _y = (ev ? ev.clientY : canvasH / 2) - rect.top
            ;

        if (ev && ev.touches) {
            let firstFing = ev.touches.length ? ev.touches[0] : ev.changedTouches[0];
            _x = firstFing.clientX;
            _y = firstFing.clientY;
        }
        return new THREE.Vector2(( (_x ) / canvasW) * 2 - 1, -( (_y ) / canvasH) * 2 + 1);
    }

    cumulativeOffset(element) {
        var top = 0, left = 0;
        do {
            top += element.offsetTop || 0;
            left += element.offsetLeft || 0;
            element = element.offsetParent;
        } while (element);

        return {
            offsetLeft: top,
            offsetTop: left
        };
    }
}
export class MAnimation {
    private canAnimate:boolean = false;
    private isStop:boolean = false;
    private lastUpdate:number = Date.now();
    private maxTimeUpdate:number = 3000;
    private id:number = Date.now();
    private animations:Array<Function> = [];
    private lastIter:number = 0;
    private app:MApp;

    constructor(main) {
        this.app = main;
        this.play();
        setTimeout(()=> {
            this.animate();
        }, 100);


    }

    add(callback:Function) {
        this.animations.push(callback);
    }

    animate() {
        if (!this.app.gl.domElement.width || this.isStop)return;
        for (let i = 0; i < this.animations.length; i++) {
            this.animations[i]();
        }

        if (this.canAnimate) {
            this.canAnimate = this.lastUpdate > Date.now();
            if (!this.canAnimate || this.lastIter > 2)this.lastIter = 0;
            this.app.render();
        }
        /* if (this.app._container.clientWidth != this.app._container.lastClientWidth) {
         this.app._container.lastClientWidth = this.app._container.clientWidth;
         this.app._events.onWindowResize();
         }*/
        requestAnimationFrame(() => {
            this.animate();
        });


    }

    play(flag:boolean = true) {
        this.lastUpdate = Date.now() + ( this.maxTimeUpdate);
        if (this.canAnimate) return;
        this.canAnimate = flag || !Pace.running;
        //this.animate();
    }

    stop() {
        this.isStop = true;
        this.canAnimate = false;
        this.lastIter = 0;
    }
}