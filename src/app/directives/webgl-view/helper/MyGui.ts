import {MApp} from "./MApp";
import {GLMain} from "./GLMain";
import {Storage} from '../../../services/services';
import {ModelCustomImage} from './ModelCustomImage';

declare var THREE:any, dat:any;
/*
* MyGui - ui controls (on top right drop-down menu by default)
* */
export class MyGui extends GLMain {
    private gui:any;
    private params:any;
    private curMat:any;
    private hasMaterialEd:boolean;
    custom:any;

    constructor(private app:MApp) {
        super();
        let

            gui = this.gui = new dat.GUI();
        gui.close();


        let lights = gui.addFolder('Lights'),
            par = ['position_x_y_z', 'intensity', 'distance', 'decay', 'color', 'angle', 'penumbra', 'visible','skyColor','groundColor'];
        app.lights.children.forEach((light, key)=> {
            let lightsF = lights.addFolder(light.type + key);
            par.forEach((f)=> {
                let _f = f.split("_");
                if (_f.length < 2 && (typeof light[f] == 'undefined'))return;
                if (_f.length > 1) {
                    let _p = lightsF.addFolder(_f[0]);
                    for (let i = 1; i < _f.length; i++) {
                        _p.add(light[_f[0]], _f[i]).listen().min(-1000).max(1000).onChange(()=> {
                            this.onAnyWay();
                        });
                    }
                } else if (f.toLowerCase().match('color')) {
                    light['def' + f] = "#" + light[f].getHexString();
                    lightsF.addColor(light, 'def' + f).onChange(()=> {
                        light[f].setHex('0x' + light['def' + f].substr(1));
                        this.onAnyWay();
                    });
                } else if (typeof light[f] == 'number') {
                    lightsF.add(light, f).listen().min(0).max(100).onChange(()=> {
                        this.onAnyWay();
                    });
                } else {
                    lightsF.add(light, f).listen().onChange(()=> {
                        this.onAnyWay();
                    })
                }
            });
        });

        //gui.add(custom, 'renderWithRoom');
    }

    onSelect(mesh:any) {
        this.addMaterialEdit();
        this.curMat = mesh.material;
        this.curMat.needsUpdate = true;

        for (let fied in this.params) {
            if (fied == 'color') {
                this.params[fied] = this.curMat.color.getHex();
            } else if (fied == 'maps') {

            } else {
                this.params[fied] = this.curMat[fied];
            }
        }
        this.params.name = mesh.name;

        for (var i in this.gui.__controllers) {
            this.gui.__controllers[i].updateDisplay();
        }
        this.gui.open();
    }
    private addMaterialEdit(){
        if(this.hasMaterialEd)return;
        this.hasMaterialEd = true;
        let  gui = this.gui,_self = this,app = this.app;
        this.params = {
            color: 0xffffff,
            name: 'none',
            reflectivity: 1,
            clearCoat: 0,
            //refractionRatio: 1,
            envMapIntensity: 1,
            //clearCoat: 1,
            clearCoatRoughness: 1,
            metalness: 1,
            roughness: 1,
            opacity: 1,
            bumpScale: 1,
            displacementScale: 1,
            maps: {
                normalMap: {
                    normalScale_x: 1,
                    normalScale_y: 1,
                    repeat_x: 1,
                    repeat_Y: 1,
                    visible: true,
                },
                map: {
                    repeat_x: 1,
                    repeat_y: 1,
                    offset_x: 0,
                    offset_y: 0,
                    visible: true,
                }, metalnessMap: {
                    repeat_x: 1,
                    repeat_y: 1,
                    visible: true,
                }, roughnessMap: {
                    repeat_x: 1,
                    repeat_y: 1,
                    offset_x: 0,
                    offset_y: 0,
                    visible: true,
                }, bumpMap: {
                    repeat_x: 1,
                    repeat_y: 1,
                    offset_x: 0,
                    offset_y: 0,
                    visible: true
                }, displacementMap: {
                    repeat_x: 1,
                    repeat_y: 1,
                    offset_x: 0,
                    offset_y: 0,
                    displacementScale: 0,
                    displacementBias: 0,
                    visible: true
                },
                envMap: {
                    list: app.envMaps.map((map, key)=> {
                        return key
                    }),
                    visible: true
                }
            }
        }
        let custom = this.custom = {
            help: function () {
                alert('Hold Ctrl and click on object to select material for edit. Mouse Right click and move/ PAN to move the element');
            },
            addText: function () {
                //_self.app.decorations.push(new ModelCustomImage(_self.app, {text: prompt("Please input text", 'Hello U')}));
            },
            renderWithRoom: false
        }
        for (let fied in this.params) {
            if (fied == 'color') {
                gui.addColor(this.params, fied).listen().onChange(()=>this.onChange());
            } else if (fied == 'maps') {
                let mapsFolder = gui.addFolder('maps');
                for (let mapsF in this.params[fied]) {
                    let mapFolder = mapsFolder.addFolder(mapsF);
                    for (let mapFields in this.params[fied][mapsF]) {
                        let _d;
                        if (this.params[fied][mapsF][mapFields] instanceof Array) {
                            _d = mapFolder.add(this.params[fied][mapsF], mapFields, this.params[fied][mapsF][mapFields]).listen();
                        } else {
                            _d = mapFolder.add(this.params[fied][mapsF], mapFields).listen();
                            if (typeof this.params[fied][mapsF][mapFields] == 'number') _d = _d.min(-10).max(10).step(0.01);
                        }

                        _d.onChange((val)=> {
                            if (!this.curMat)return;
                            let _s = mapFields.split("_"),
                                curVal = this.params[fied][mapsF][mapFields];
                            if (mapFields.match('normalS')) {
                                this.curMat[_s[0]][_s[1]] = curVal;
                            } else if (mapFields == 'visible') {
                                if (!this.curMat["_" + mapsF])this.curMat["_" + mapsF] = this.curMat[mapsF];
                                this.curMat[mapsF] = this.curMat[curVal ? "_" + mapsF : "__NONE__"];
                            } else if (mapFields == 'list') {
                                this.curMat[mapsF] = app.envMaps[val].maps;
                            } else {
                                this.curMat[mapsF][_s[0]][_s[1]] = curVal;
                            }
                            this.curMat.needsUpdate = true;
                            this.app._animation.play();
                        });
                    }
                }

            } else {
                let _d = gui.add(this.params, fied).listen();
                if (fied != 'name')_d.min(-10).max(10).step(0.01).onChange(()=>this.onChange());
            }
        }
        gui.add(custom, 'help');
        //gui.add(custom, 'addText');
    }

    private onChange() {
        if (!this.curMat)return;
        for (let fied in this.params) {
            if (fied == 'color') {
                this.curMat[fied].setHex(this.params[fied]);
            } else if (fied == 'maps') {

            } else {
                this.curMat[fied] = this.params[fied] + 0.0;
            }
        }
        this.curMat.needsUpdate = true;
        this.onAnyWay();
    }

    private onAnyWay() {
        this.app._animation.play();
    }

    destroy() {
        this.gui.destroy();
    }
}