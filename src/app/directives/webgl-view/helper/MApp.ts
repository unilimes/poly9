import {GLMain} from "./GLMain";
import {MyGui} from "./MyGui";
import {WebglView} from "../webgl.view";
import {MEvents,MAnimation} from "./MEvents";
import {ModelCustomImage} from "./ModelCustomImage";
import {Command} from '../../../services/services';

declare var THREE:any, TWEEN:any, Pace:any, rsdemo, Matrix4x4:any;
/*
 * MApp - threejs scene build control
 * @scene - threejs scene;
 * @model - object3d loaded model;
 * @camera - threejs perspective camera;
 * @rs_camera - reality server camera;
 * @gl - threejs renderer, WebglRender;
 * @_objLoad - threejs model loader;
 * @lights - threejs container for lights;
 * @screen - render size;
 * @innerModel - if has model from parent;
 * @_container - render dom elemnt parent;
 * @envMaps - list of eviroment cube maps;
 * */
export class MApp extends GLMain {
    isMobile:boolean = false;
    scene:any;
    anisotropy:number;
    model:any;
    camera:any;
    rs_camera:any;
    gl:any;
    _datGui:MyGui;
    _objLoad:any;
    spotLight:any;
    lights:any;
    screen:any = {};
    main:WebglView;
    loader:any;
    innerModel:any;
    _events:MEvents;
    _animation:MAnimation;
    controls:any;
    transformControls:any;
    _container:any;
    _preloaderStatus:any;
    allLoad:boolean = false;
    private sun:any;
    envMaps:Array<any> = [];
    decorations:Array<ModelCustomImage> = [];

    constructor(main:WebglView) {
        super();
        this.main = main;
        this.isMobile = this.deviceCheck();

        this.scene = new THREE.Scene();
        this.model = new THREE.Object3D();
        this.model.name = 'model' + Date.now();
        this.scene.add(this.model);
        let renderer = this.gl = new THREE.WebGLRenderer({
                canvas: this.main.canvasTarget['nativeElement'],
                antialias: true,
                alpha: true,
                clearAlpha: true,
                sortObjects: false,
                preserveDrawingBuffer: true
            }),
            SCREEN_WIDTH = this.screen.width = 720,
            SCREEN_HEIGHT = this.screen.height = 405,
            _self = this;
        renderer.domElement.className = 'gl-view';

        this._preloaderStatus = document.querySelector('.preloader-data.preloader-status') || {style: {}};
        renderer.setClearColor(0xffffff, 0);
        renderer.setPixelRatio(window.devicePixelRatio);
        renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
        renderer.shadowMap.enabled = !!main.options.shadow;
        renderer.shadowMap.type = THREE.PCFSoftShadowMap;
        renderer.gammaInput = renderer.gammaOutput = renderer.shadowMap.enabled;

        this.anisotropy = renderer.getMaxAnisotropy();
        if (main.options.camponent)main.options.camponent.anisotropy = this.anisotropy;
        this.camera = new THREE.PerspectiveCamera(45, SCREEN_WIDTH / SCREEN_HEIGHT, 1, 200000 * 1000);

        //this.controls = {update:()=>{}};
        this.controls = new THREE.OOrbitControls(this.camera, renderer.domElement);
        //this.controls.constraint.smoothZoom = true;
        //this.controls.constraint.zoomDampingFactor = 0.2;
        this.controls.constraint.smoothZoomSpeed = 5.0;
        this.controls.device = this.controls.enableDamping = false;
        this.controls.inverse = false;
        //this.controls.enabled = !!main.options.controls;
        this.controls.addEventListener('change', ()=> {
            this._animation.play();
            if (this.rs_camera)this.rs_camera.helper.Update_RS_Camera(0, 0, this.camera, this.controls);
            //console.log(this.camera.noChange);
        });
        //controls.addEventListener( 'change', render_this );
        //controls.addEventListener( 'end', move_end );
        //controls.addEventListener( 'start', begin_moving );
        //controls.addEventListener( 'auto', auto_moving );
        //

        this.transformControls = new THREE.TransformControls(this.camera, renderer.domElement);
        //this.transformControls.visible  = false;
        this.transformControls.addEventListener('change', ()=> {
            this._animation.play();
            //this.rs_camera.helper.Update_RS_Camera(0, 0, this.camera, this.controls);
            this.main.options.component.commands = this.updateSceneMeshMatrix(!!this.innerModel);
            //this.transformControls.object.position.copy(this.transformControls.object.parent.position);
            //this.transformControls.object.parent.position.set(0, 0, 0);
            //this.transformControls.object.position0 = this.transformControls.object.position.clone();
        });
        this.scene.add(this.transformControls);

        this.camera.positionDef = new THREE.Vector3(0, 25, 150);
        this.camera.position.copy(this.camera.positionDef);
        this.camera.lookAt(this.scene.position);
        this.camera.updateProjectionMatrix();


        this.scene.name = 'Scene';
        this.camera.name = 'Camera';
        this.scene.add(this.camera);


        this.lights = new THREE.Object3D();

        this.model.add(this.lights);
        let hemisphere = new THREE.HemisphereLight(0xffffff, 0x000000, 0.09);
        hemisphere.name = "Hemisphere";
        hemisphere.position.set(0, 100, 0);
        this.lights.add(hemisphere);


        var ambient = new THREE.AmbientLight(0xffffff, 0.81);
        this.lights.add(ambient);

        let spotLight = this.spotLight = new THREE.SpotLight(0xffffff, 0.41);
        spotLight.position.set(15, 40, 35);
        spotLight.angle = Math.PI / 2;
        spotLight.penumbra = 0.05;
        spotLight.decay = 2;
        spotLight.distance = this.camera.far;

        spotLight.castShadow = true;
        spotLight.shadow.mapSize.width = 1024;
        spotLight.shadow.mapSize.height = 1024;
        spotLight.shadow.camera.near = 10;
        spotLight.shadow.camera.far = this.camera.far;
        spotLight.visible = false;
        this.lights.add(spotLight);

        let directionalLight = new THREE.DirectionalLight(0xffffff, 0.5);
        directionalLight.position.set(0.4, 0.9, 0, 4);
        this.lights.add(directionalLight);
        let directionalLight1 = new THREE.DirectionalLight(0xffffff, 0.5);
        directionalLight1.position.set(-0.4, 0.9, -0, 4);
        this.lights.add(directionalLight);
        this.lights.add(directionalLight1);
        directionalLight.target = directionalLight1.target = this.model;
        //let  lightHelper = new THREE.SpotLightHelper( spotLight );
        //this.scene.add( lightHelper );
        //
        //let shadowCameraHelper = new THREE.CameraHelper( spotLight.shadow.camera );
        //this.scene.add( shadowCameraHelper );

        let sphere = new THREE.SphereGeometry(0.5, 16, 8),
            light1 = new THREE.PointLight(0xffffff, 2, 1024),
            light2 = new THREE.PointLight(0x0040ff, 2, 1024);
        //light1.add(new THREE.Mesh(sphere, new THREE.MeshBasicMaterial({color: 0xff0040})));
        this.lights.add(light1);
        //light2.add(new THREE.Mesh(sphere, new THREE.MeshBasicMaterial({color: 0x0040ff})));
        this.lights.add(light2);
        light1.position.set(50, 0, 0);
        light2.position.set(-50, 0, 0);
        light1.visible = light2.visible = spotLight.visible = false;
        if (this.main.options.helper)this.scene.add(new THREE.AxisHelper(500));


        /*let imagePrefix = "./assets/images/skyboxsun25degtest/",
         directions = ["skyrender0001", "skyrender0004", "skyrender0003", "skyrender0006", "skyrender0005", "skyrender0002"],
         imageSuffix:string = ".bmp",
         _loader = new THREE.CubeTextureLoader();
         //this.scene.background = _loader.load(directions.map((e:string)=>{return imagePrefix+e +imageSuffix;}));
         //this.scene.fog = new THREE.Fog(0xffffff, 1, 100);
         //this.scene.fog.color.setHSL(0.6, 0, 1);

         var skyGeometry = new THREE.CubeGeometry( 5000, 5000, 5000 );

         var materialArray = [];
         for (var i = 0; i < 6; i++)
         materialArray.push( new THREE.MeshBasicMaterial({
         map: THREE.ImageUtils.loadTexture( imagePrefix + directions[i] + imageSuffix ),
         side: THREE.BackSide
         }));
         var skyMaterial = new THREE.MeshFaceMaterial( materialArray );
         var skyBox = new THREE.Mesh( skyGeometry, skyMaterial );


         this.scene.add( skyBox );
         var  ss = 1.41;
         skyBox.scale.set(ss,ss,ss);
         skyBox.rotation.y = -Math.PI/2;
         */
        // Add Sky Mesh
        this.sun = new THREE.Object3D();
        let sky = new THREE.Sky();
        this.scene.add(this.sun);
        this.sun.add(sky.mesh);


        //var skyGeo = new THREE.SphereBufferGeometry( 4500, 32, 15 );
        //var skyMesh = new THREE.Mesh( skyGeo, new THREE.MeshBasicMaterial({side:2,color: 0x00ffff}) );
        //this.scene.add( skyMesh );
        //console.log(sky.mesh);
        //sky.mesh.material = new THREE.MeshBasicMaterial({side:2,color: 0x00ffff})
        // Add Sun Helper
        let sunSphere = new THREE.Mesh(
            new THREE.SphereBufferGeometry(20000, 16, 8),
            new THREE.MeshBasicMaterial({color: 0x00ffff})
        );
        //sunSphere.position.y = - 700000;
        //sunSphere.visible = false;
        this.sun.add(sunSphere);

        /// GUI
        let distance = 10000,
            effectController = {
                turbidity: 10.2,
                rayleigh: 0.52,
                mieCoefficient: 0.005,
                mieDirectionalG: 0.8,
                luminance: 0.51,
                inclination: 0.49, // elevation / inclination
                azimuth: 0.25, // Facing front,
                sun: !true
            };

        let uniforms = sky.uniforms;
        uniforms.turbidity.value = effectController.turbidity;
        uniforms.rayleigh.value = effectController.rayleigh;
        uniforms.luminance.value = effectController.luminance;
        uniforms.mieCoefficient.value = effectController.mieCoefficient;
        uniforms.mieDirectionalG.value = effectController.mieDirectionalG;

        var theta = Math.PI * ( effectController.inclination - 0.5 );
        var phi = 2 * Math.PI * ( effectController.azimuth - 0.5 );

        sunSphere.position.x = distance * Math.cos(phi);
        sunSphere.position.y = distance * Math.sin(phi) * Math.sin(theta);
        sunSphere.position.z = distance * Math.sin(phi) * Math.cos(theta);
        //sunSphere.visible = effectController.sun;

        sky.uniforms.sunPosition.value.copy(sunSphere.position);

        /*---------env maps--------*/
        var imgSrc = "./assets/images/env/",
            envLoader = new THREE.CubeTextureLoader(),
            envs:any = this.envMaps =
                [
                    {url: 'sb_frozen/frozen_', type: '.png', dirs: ['ft', 'bk', 'up', 'dn', 'rt', 'lf']},
                    {url: 'studiobox/', type: '.png', dirs: ['px', 'nx', 'py', 'ny', 'pz', 'nz']},
                    {url: 'skybox/', type: '.jpg', dirs: ['posx', 'negx', 'posy', 'negy', 'posz', 'negz']},
                    {url: 'bridge/', type: '.jpg', dirs: ['posx', 'negx', 'posy', 'negy', 'posz', 'negz']}
                ];
        for (let key = 0; key < envs.length; key++) {
            let e:any = envs[key],
                src = imgSrc + e.url,
                fType = e.type,
                directions = e.dirs,
                urls = [];
            for (let i = 0; i < 6; i++) {
                let url = src + directions[i] + fType;
                urls.push(url);
            }
            var textureCube = envLoader.load(urls);
            textureCube.format = THREE.RGBFormat;
            textureCube.mapping = THREE.CubeReflectionMapping;
            e.maps = textureCube;
            e.name = key;
        }

        let hdrCubeRenderTarget:any;
        let genCubeUrls = (prefix:string, postfix:string)=> {
            return [
                prefix + 'px' + postfix, prefix + 'nx' + postfix,
                prefix + 'py' + postfix, prefix + 'ny' + postfix,
                prefix + 'pz' + postfix, prefix + 'nz' + postfix
            ];
        };


        THREE.Mesh.prototype.getScreenPst = function () {
            let mesh:any = this,
                m = _self.gl.domElement,
                offset = _self._offset(),
                width = m.clientWidth, height = m.clientHeight,
                widthHalf = width / 2, heightHalf = height / 2,
                position:any = new THREE.Vector3();

            mesh.updateMatrixWorld();
            mesh.updateMatrix();
            mesh.geometry.computeBoundingBox();
            mesh.geometry.computeBoundingSphere();

            let boundingBox = mesh.geometry.boundingBox;
            position.subVectors(boundingBox.max, boundingBox.min);
            position.multiplyScalar(0.5);
            position.add(boundingBox.min);

            position.project(_self.camera);
            position.x = ( position.x * widthHalf ) + widthHalf + offset.left;
            position.y = -( position.y * heightHalf ) + heightHalf + offset.top;
            mesh.onscreenParams = position;
            mesh.onscreenOffset = offset;
        };
        THREE.Mesh.prototype.getMatrixR = function (isInner) {
            let
                flag = isInner || this.canTransform,
                mesh:any = this,
                axis = ['x', 'y', 'z', 'w'],
                curHor = 0,
                curVer = 0,
                translate:any = new Matrix4x4(),
                rotate:any = new Matrix4x4(),
                rotation:any = [new Matrix4x4(), new Matrix4x4(), new Matrix4x4()],
                scale:any = new Matrix4x4(),
                genPst = mesh.parent.position.clone().add(mesh.position);

            translate.set_translation_elements(genPst.x, genPst.y, genPst.z);
            rotation[0].set_rotation(new THREE.Vector3(-1, 0, 0), mesh.rotation.x);
            rotation[1].set_rotation(new THREE.Vector3(0, -1, 0), mesh.rotation.y);
            rotation[2].set_rotation(new THREE.Vector3(0, 0, -1), mesh.rotation.z);
            scale.set_scaling(mesh.scale.x, mesh.scale.y, mesh.scale.z);
            return translate.multiply(rotation[0].multiply(rotation[1]).multiply(rotation[2])).multiply(scale);
        };
        this.onFinishLoadTemplates();
    }

    setBackground(img) {
        this.main.backGround = img;
        this.sun.visible = false;
    }

    private onFinishLoadTemplates() {
        this.loadModel(()=> {
            let main = this.main,
                _opt = main.options,
                parentCanvas = this._container = main.projCnt['nativeElement'],
                onFinish = ()=> {
                    this._animation.play();
                    this.allLoad = true;
                    main.preloader.fade().onUpdatePreloaderStatus(0);
                    if (main.options.onFinish)main.options.onFinish(this.model);
                    this.zoomCamera();
                    if (_opt.component) {
                        _opt.component.mainModel = this.model;
                        if (_opt.component.innerElement) {
                            this.innerModel = new THREE.Object3D();
                            this.innerModel.userData.refresh = ()=> {
                                let _main = _opt.component.innerElement;
                                this.innerModel.traverse((child)=> {
                                    if (child.type == this.ENTITY.Config.MODELS.TYPES.MESH) {
                                        _main.traverse((mainChild)=> {
                                            if (mainChild.type == this.ENTITY.Config.MODELS.TYPES.MESH && mainChild.userData.name == child.userData.name) {
                                                child.material = mainChild.material;
                                                return false;
                                            }
                                        });
                                    }
                                });

                            };
                            //_opt.component.innerElement.parent0 = _opt.component.innerElement.parent;
                            this.innerModel.add(_opt.component.innerElement.clone());
                            this.model.add(this.innerModel);
                            this.transformControls.attach(this.innerModel.children[0]);
                            this.innerModel.category = this.ENTITY.Config.MODELS.CATEGORY.INNER;
                        } else {
                            if (!this._datGui)this._datGui = new MyGui(this);
                        }
                        if (_opt.component.roomModal) {
                            _opt.component.roomModal.innerElement = this.model;
                        }
                    }
                };
            parentCanvas.appendChild(this.gl.domElement);
            this._events = new MEvents(this);
            this._animation = new MAnimation(this);

            let _inter = setTimeout(()=> {
                Pace.stop();
                onFinish();
            }, 2000);
            Pace.once('done', (e)=> {
                clearTimeout(_inter);
                onFinish();
            });
        });
    }

    private  deviceCheck() {
        var check = false;
        (function (a) {
            if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true;
        })(navigator.userAgent || navigator.vendor || window['opera']);
        return check;
    }

    loadModel(callback:Function = (t)=> {
        console.log("load was finished succesed");
    }) {

        if (this.main.options.modelName) {
            if (this.main.options.modelName.match('.obj')) {
                let loader = this._objLoad || new THREE.OBJLoader();
                this._objLoad = loader;
                loader.load(this.main.options.modelName, (object)=> {
                    object.userData.url = this.main.options.modelName;
                    callback(this._onLoadModel(object));
                    this.model.userData.url = object.userData.url;
                }, (e)=>this.onProgress(e), (e)=> {
                    callback();
                    this.onError(e)
                });
            } else {
                callback();
            }
        } else {
            callback();
        }
    }

    private onError(xhr) {
        console.error(xhr);
    }

    private onProgress(xhr) {
        if (xhr.lengthComputable) {
            this.main.preloader.onUpdatePreloaderStatus(xhr.loaded / xhr.total);
        }
    }

    private _onLoadModel(object) {
        let
            params = this.main.options.params,
            hasToadd,
            objBbox = new THREE.Box3().setFromObject(object),
            bboxCenter = objBbox.getCenter().clone(),
            _material = new THREE.MeshPhysicalMaterial({
                color:new THREE.Color(1,1,1),
                transparent: true,
                side: THREE.DoubleSide,
                envMap: this.scene.background,
                displacementScale:1
            }), key = 0,

            difference = this.main.options.curModel.model.scale || 1,//new THREE.Vector3(1,1,1),
            commands = [],
            _opt = this.main.options,
            _componet = _opt.component;
        bboxCenter.multiplyScalar(-1);

        if (params) {
            if (this.model.userData.url && this.model.userData.url.match('barware_s11624.obj')) {
                this.model.children.splice(3, 4).forEach((meshes)=> {
                    commands.push(
                        new Command(Command.REQUEST.INSTANCE.DETACH, {
                            instance_name: meshes.userData.originName
                        })
                    );
                });
            } else if (this.model.isOld) {
                commands.push(
                    new Command(Command.REQUEST.INSTANCE.DETACH, {
                        instance_name: this.model.children.pop().userData.originName
                    })
                );
            }
            this.model.isOld = true;
            let _helper = new THREE.BoxHelper(object),
                radius = _helper.geometry.boundingSphere.radius;
            difference = this.model.radius / radius;
            bboxCenter = this.model.translateO;
        } else {
            this.model.traverse((child)=> {
                if (child.type == this.ENTITY.Config.MODELS.TYPES.MESH) {
                    commands.push(
                        new Command(Command.REQUEST.INSTANCE.DETACH, {
                            instance_name: child.userData.originName
                        })
                    );
                }
            });
            this.model.children.splice(1);
            this.model.isOld = this.model.radius = false;
            this.model.translateO = bboxCenter.clone();
        }

        let _childs = [];
        object.traverse((child)=> {
            if (child.type == this.ENTITY.Config.MODELS.TYPES.MESH) {
                //child.geometry.translate(bboxCenter.x, bboxCenter.y, bboxCenter.z);
                child.userData.originName = child.name;
                child.name = child.userData.name = (!params ? 'Mesh part :' : 'Custom part :') + (this.model.children.length + key++);
                child.receiveShadow = child.castShadow = true;
                child.material = _material.clone();
                child.material.name = "exMaterial"+  this.ENTITY.Config.randomstr()
                _childs.push(child);
                if (params) {
                    child.canTransform = true;
                    let _sc = this.model.children[1].scale.x;
                    child.scale.set(params.scale.x * _sc, params.scale.y * _sc, params.scale.z * _sc);
                    child.position.set((params.position.x + bboxCenter.x) * _sc, (params.position.y + bboxCenter.y) * _sc, (params.position.z + bboxCenter.z) * _sc);
                    child.rotation.x = THREE.Math.degToRad(params.rotation.x);
                    child.rotation.y = THREE.Math.degToRad(params.rotation.y);
                    child.rotation.z = THREE.Math.degToRad(params.rotation.z);
                    child.params = params;

                } else {
                    child.position.set(bboxCenter.x * difference, bboxCenter.y * difference, bboxCenter.z * difference);
                    child.scale.multiplyScalar(difference);
                }

                if (_componet)_componet.showGlyphicon[key] = true;
            }
        });
        //this.transformControls.detach();
        _childs.forEach((el)=> {

            //if (params && el.canTransform) {
            //    this.scene.remove(this.transformControls);
            //    this.transformControls.attach(el);
            //    this.model.add(this.transformControls.object);
            //    this.scene.add(this.transformControls);
            //} else {
                this.model.add(el);
            //}
        })
        //if (params  ) this.transformControls.attach(this.model);
        _material.needsUpdate = true;
        //objBbox.setFromObject(object); // Update the bounding box
        object.name += 'Model';
        return commands;
    }



    setNewPst(nPosition:any = this.scene.position) {
        //this.model.traverse((child)=>{
        //   if(child.type == this.ENTITY.Config.MODELS.TYPES.MESH){
        //       if(!child.position0)child.position0 = child.position.clone();
        //       child.position.copy(child.position0.clone().add(nPosition));
        //   }
        //});
        this.model.position.copy(nPosition);
        this.controls.target.copy(nPosition);
        this.zoomCamera(null, nPosition);
        return this.updateSceneMeshMatrix();
    }

    updateSceneMeshMatrix(onlyInner:boolean=false) {
        let commands = [],
            curStorage = onlyInner ? this.innerModel : this.model;
        if (curStorage) {
            curStorage.traverse((child)=> {
                if (child.type == this.ENTITY.Config.MODELS.TYPES.MESH) {
                    commands.push(
                        new Command(Command.REQUEST.INSTANCE.WORLD_TO_OBJ, {
                            "instance_name": child.userData.originName,
                            "transform": child.getMatrixR(onlyInner)
                        })
                    );
                }
            });
        }

        return commands;
    }
    zoomCamera(model:any = null, nPosition:any = null) {
        if (!model && !this.model.children.length)return;
        if (!nPosition)nPosition = this.model.position;
        let radius = this.model.radius;
        if (!radius) {
            let boxHelper = new THREE.BoxHelper(model || this.model);
            radius = boxHelper.geometry.boundingSphere.radius;
            this.model.boxHelper = boxHelper;
            boxHelper.geometry.computeBoundingBox();
        }

        let sc = this.main.options.component.innerElement ? 0.3 : 1.8,
            newPst = new THREE.Vector3(nPosition.x + radius * sc, nPosition.y, nPosition.z + radius * sc);

        this.model.radius = radius;
        this.spotLight.position.copy(newPst.clone().addScaledVector(newPst.clone().sub(nPosition).normalize(), radius));
        //this.spotLight.lookAt(nPosition);
        //this.spotLight.position.y += Math.abs(this.spotLight.position.y*1);
        this.move([{
            onUpdate: (delta)=> {
                this.camera.updateProjectionMatrix();
                this.camera.position.lerp(newPst, delta);
            }
        }]);
    }

    private move(arg) {
        let moveObj = this.camera;

        if (!this.controls.enabled)return;
        let duration = 900, tween;
        tween = new TWEEN.Tween({delta: 0}).to({delta: 1}, duration)
            //.easing(TWEEN.Easing.Cubic.Out)
            .onStart(()=> {
                this.controls.enabled = false;
                this._animation.play();
            })
            .onUpdate(function () {
                for (let i = 0; i < arg.length; i++) {
                    //arg.field[i].obj[arg.field[i].fieldName].lerp(arg.field[i].value, this.delta);
                    arg[i].onUpdate(this.delta);
                }
            })
            .onComplete(()=> {
                tween.stop();
                tween = null;
                this.controls.enabled = true;
            })
            .start();
    }

    private setCenterPoint(object) {
        let middle = new THREE.Vector3(),
            mesh = new THREE.BoxHelper(object);
        let geometry = mesh.geometry;

        geometry.computeBoundingBox();

        middle.x = (geometry.boundingBox.max.x + geometry.boundingBox.min.x) / 2;
        middle.y = (geometry.boundingBox.max.y + geometry.boundingBox.min.y) / 2;
        middle.z = (geometry.boundingBox.max.z + geometry.boundingBox.min.z) / 2;
        //
        object.position.copy(geometry.boundingBox.min.negate());
        //object.position.x +=geometry.boundingBox.min.x;
        //object.position.y +=geometry.boundingBox.min.y;
        //object.position.z +=geometry.boundingBox.min.z;
        this.scene.add(mesh);
        return middle;
    }

    _W() {
        return this._parent().clientWidth;
    }

    _H() {
        return this._parent().clientHeight;
    }

    _parent():HTMLElement {
        return this.main.projCnt['nativeElement'];
    }

    _offset() {
        return this.gl.domElement.getBoundingClientRect()
    }

    onEventPrevent(event) {
        event.preventDefault();
        return false;
    }

    render() {
        if (Pace.running) return;
        //if(this.controls.enabled){
        this.transformControls.update();
        this.controls.update();
        TWEEN.update();
        //}else{

        //}
        this.gl.render(this.scene, this.camera);
    }

}