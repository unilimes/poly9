import {Input,ViewChild,Component,OnInit,OnChanges,EventEmitter,Injectable} from '@angular/core';
import {Preloader} from '../directives';
import {MApp} from './helper/MApp';

declare var THREE:any, TWEEN:any, Pace:any, dat:any, rsdemo, Matrix4x4:any;


@Component({
    selector: 'app-project-webgl-view',
    templateUrl: './webgl.view.html',
    styleUrls: ['./webgl.view.sass']
})
export class WebglView implements OnInit,OnChanges {
    preview:string;
    _self:WebglView;
    @ViewChild("renderParent")
        renderParent:HTMLElement;
    @ViewChild("preloader")
        preloader:Preloader;
    @ViewChild("projCnt")
        projCnt:HTMLElement;
    @ViewChild("renderTarget")
        renderTarget:HTMLElement;
    @ViewChild("canvasTarget")
        canvasTarget:HTMLElement;
    @ViewChild("backGroundImg")
        backGroundImg:HTMLElement;
    @Input() options:any;

    app:MApp;
    private _id:number = Date.now();
    private inited:boolean = false;
    private isRender:boolean = false;
    canRender:boolean = false;
    backGround:string;

    constructor() {
        this._self = this;
    }

    ngOnChanges(changes) {

    }

    ngOnInit() {
        this.initWebgl();
    }

    initWebgl() {
        this.app = new MApp(this);
    }

    ngOnDestroy() {
        this.app._animation.stop();
        if (this.options && this.options.component.innerElement) {
            this.options.component.destroy();
        }
        if (this.app._datGui)this.app._datGui.destroy();
    }
}

