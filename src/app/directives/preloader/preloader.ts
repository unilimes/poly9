import {Input,ViewChild,Component,OnInit} from '@angular/core';
import {WebglView} from '../webgl-view/webgl.view';

declare var THREE:any;
/*
 * Preloader - preloader status bar
 * @preloader - container dom of component
 * @preloaderStatus -   dom for status
 * @webglView -   target component
 * */
@Component({
    selector: 'app-project-preloader',
    templateUrl: './preloader.html',
    styleUrls: ['./preloader.sass']
})
export class Preloader implements OnInit {

    @ViewChild("preloader")
        preloader:HTMLElement;
    @ViewChild("preloaderStatus")
        preloaderStatus:HTMLElement;
    @Input("_parent")
        webglView:WebglView;

    private isActive:boolean = true;
    private cntx:any;
    private _data:any = {};

    constructor() {
    }

    ngOnInit() {
        let _canvas = this.preloaderStatus['nativeElement'];
        this.cntx = _canvas.getContext('2d');
        _canvas.height = _canvas.width;
        this._data._W = _canvas.width;
        this._data._H = _canvas.height;
        this.cntx.lineWidth = 5;
        this.cntx.strokeStyle = '#6f2727';
        this.cntx.shadowOffsetX = 0;
        this.cntx.shadowOffsetY = 0;
        this.cntx.shadowBlur = 10;
        this.cntx.shadowColor = '#656565';
    }

    onPreloaderLoad() {
        this.preloader['nativeElement'].className += ' active';
    }

    onUpdatePreloaderStatus(value) {
        let context = this.cntx,
            centerX = this._data._W / 2,
            centerY = this._data._H / 2,
            radius = centerX - context.shadowBlur;

        context.clearRect(0, 0, this._data._W, this._data._H);
        context.beginPath();
        context.arc(centerX, centerY, radius, 0, 2 * Math.PI * value, false);
        context.stroke();
    }

    fade(show:boolean = false) {
        this.preloader['nativeElement'].className = show ? 'preloader-template active' : 'preloader-template';
        if (!show)this.webglView.app._animation.play();
        return this;
    }

}