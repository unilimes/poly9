import { Component, OnInit,ViewChild,Input} from '@angular/core';
import {document} from "ngx-bootstrap/utils/facade/browser";

import {MainComponent} from '../../components/main/main.component';
import {LandingComponent} from '../../components/landing/landing.component';
import {Config} from '../../entities/entities';
import {ModelCustomImage} from '../directives';
import {Http} from '@angular/http';

declare let THREE:any, fabric:any;
/*
 * Decor - dom controls for decorations
 * @targetCanvas - result view
 * @_parent - target component
 * @curEditDecor - target texture editor for model with cur decor
 * */
@Component({
    selector: 'decor-control',
    templateUrl: './decor.html',
    styleUrls: ['./decor.sass','./decor.css']
})
export class Decor implements OnInit {
    @ViewChild("targetCanvas")
        targetCanvas:any;
    @ViewChild("decorSet")
        decorSet:any;
    @ViewChild("fileUploader")
        fileUploader:any;
    @Input() _parent:any;
    private controller:any;
    private img:any;
    private inputedText:any;
    private inputText:string;
    private fileReader:any;
    private id:number = Date.now();
    private curEditDecor:ModelCustomImage;

    constructor(private http:Http) {
        this.fileReader = new FileReader();
    }

    ngOnInit() {
        this.controller = new fabric.Canvas(this.targetCanvas.nativeElement);
        this.controller.on("mouse:down",(e)=>{
            this.upload();
        });

    }

    private upload(){
        if(!this.img && !this.inputedText && !this.curEditDecor)this.fileUploader.nativeElement.click();
    }
    private ondragDropFile(e) {
        e.preventDefault();
        let dt = e.dataTransfer;
        if (dt.items) {
            for (let i=0; i < dt.items.length; i++) {
                if (dt.items[i].kind == "file") {
                    return this.parseUploadFiles([dt.items[i].getAsFile()]);
                }
            }
        }

    }

    private dragStart(event, decor:ModelCustomImage) {
        this._parent.webglView.app._events.curDrag = decor;
    }

    private ondragend(event, decor:ModelCustomImage) {
        this._parent.webglView.app._events.curDrag = null
    }

    private textHasChange() {
        this.inputedText = this.inputText ? new fabric.Text(this.inputText, {left: 0, top: 0}) : null;
        this.updateCanvas();
    }

    private removeImg() {
        this.img = null;
        this.updateCanvas();
    }

    private uploadImg(ev) {
        this.parseUploadFiles(ev.target.files);
    }

    private parseUploadFiles(files) {
        if (files &&   files.length) {
            let file = files[0];
            if (!file)return;
            if (!file.type.match('image'))return alert('please, upload only image');
            this.fileReader.onload = (e)=> {

                this.img = new Image();
                this.img.onload = ()=> {
                    this.controller.setWidth(this.decorSet.nativeElement.clientWidth);
                    this.controller.setHeight(this.decorSet.nativeElement.clientWidth * this.img.height / this.img.width);
                    this.img = new fabric.Image(this.img, {
                        left: 0,
                        top: 0,
                        width:  this.controller.getWidth(),
                        height:  this.controller.getHeight(),
                    });
                    this.updateCanvas();
                }
                this.img.src = e.target.result;
            }
            this.fileReader.readAsDataURL(file);
        }
    }

    private updateCanvas() {
        this.controller.clear();
        if (this.img) {
            this.controller.setWidth(this.decorSet.nativeElement.clientWidth);
            this.controller.setHeight(this.decorSet.nativeElement.clientWidth * this.img.height / this.img.width);
            this.controller.add(this.img);
        }
        if (this.inputedText) {
            this.controller.add(this.inputedText);
        }
        //if (!this.curEditDecor)this.add();
    }

    private add() {
        new DecorData(this.controller.toDataURL("image/png"), JSON.stringify(this.controller.toDatalessJSON()), (a:DecorData)=> {
            let decor:ModelCustomImage = this.curEditDecor = new ModelCustomImage(this._parent.webglView.app, a);
            this._parent.webglView.app.decorations.push(decor);
        }, this._parent.curModel.material);
    }

    private edit() {
        this.curEditDecor.options.update(this.controller.toDataURL("image/png"), JSON.stringify(this.controller.toDatalessJSON()), ()=> {
            this.curEditDecor.updateTextures()
        });
    }

    private select(decor:ModelCustomImage) {
        this.curEditDecor = decor;
        this.controller.clear();
        if (decor.options.jsonData) {
            this.controller.loadFromJSON(decor.options.jsonData, this.controller.renderAll.bind(this.controller));
            setTimeout(()=>{
                this.img =  this.inputedText = null;
                for(let i =0, list =this.controller._objects ;i<list.length;i++){
                    if(list[i].type =='image'){
                        this.img=list[i];
                    }else{
                        this.inputedText = list[i];
                    }
                }
                this.updateCanvas();
            },200);

        }
    }

    private dropItem(decor:ModelCustomImage) {
        if (this.curEditDecor && this.curEditDecor.id == decor.id) {
            this.controller.clear();
            this.img =  this.inputedText = this.curEditDecor = null;
        }
        decor.destroy();

    }

    private new() {
        this.controller.clear();
        this.curEditDecor = null;
    }
}
export class DecorData {
    jsonData:string;
    preview:any;
    material:any;

    constructor(prev:any, jsn:string, onFinish:Function = ()=> {
    }, material:any = null) {
        this.update(prev, jsn, onFinish, material);
        if (prev instanceof Image)this.preview = prev;
    }

    update(src:string = null, jsData:string = null, onFinish, material:any = null) {
        if (jsData)this.jsonData = jsData;
        if (material)this.material = material;
        if (src) {
            let img = new Image();
            img.onload = ()=> {
                this.preview = img;
                onFinish(this);
            }
            img.src = src;
        }
    }
}