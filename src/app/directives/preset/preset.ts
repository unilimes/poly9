import {Input,ViewChild,Component,OnInit,EventEmitter,Injectable} from '@angular/core';

import {GLMain} from "../webgl-view/helper/GLMain"
declare var THREE:any, TWEEN:any, Pace:any, dat:any, rsdemo, Matrix4x4:any;

/*
 * PreSet - vizualization room quad selected positions
 * @image - backgeound, default from options;
 * @optioins - inject options from parent;
 * */
@Component({
    selector: 'app-project-preset',
    template: '<div class="preset"><canvas #previewTarget></canvas></div>',
    styleUrls: ['./preset.sass']
})
export class PreSet extends GLMain implements OnInit {
    @ViewChild("previewTarget")
        previewTarget:HTMLElement;
    @Input() options:any;

    private _id:number = Date.now();
    private inited:boolean = false;
    private selectedSize:number = 80;
    private image:any;
    private offset:any;
    private context:any;
    private COLORS:any = {
        SELECTED: '#FF0000',
        DEF: '#FFFFFF'
    };
    canRender:boolean = false;
    backGround:string;

    constructor() {
        super();

    }


    ngOnInit() {
        let _c = this.previewTarget['nativeElement'];
        this.callback_storage.push({
            target: window,
            eventName: this.ENTITY.Config.EVENTS_NAME.RESIZE,
            handler: ()=> {
                this.setSize();
                this.drawImg();
                this.update();
                this.options._webgl.app.camera.noChange = false;
            }
        }, {
            target: _c,
            eventName: this.ENTITY.Config.EVENTS_NAME.MOUSE_MOVE,
            handler: (e)=> {
                this.onMouseMove(e);
            }
        }, {
            target: _c,
            eventName: this.ENTITY.Config.EVENTS_NAME.MOUSE_UP,
            handler: (e)=> {
                this.onMouseUp(e);
            }
        });




        if (!this.options || !this.options.room)return;


        let image = this.image = new Image(),
            _self:PreSet = this;
        image.onload = function () {
            _self.setSize();
            let cntx = _c.getContext('2d');
            _self.context = cntx;
            _self.drawImg();
            if (_self.options.onFinish)_self.options.onFinish(()=>{
                _self.callback_storage.forEach((data, key)=> {
                    if (data.eventName) {
                        data.target.addEventListener(data.eventName, data.handler);
                    }
                });
                _self.COLORS.CURRENT = _self.COLORS.DEF;
                cntx.lineWidth = 10;
                _self.update();
            });
        }
        image.src = this.options.room.preview;

    }

    ngOnDestroy() {
        this.callback_storage.forEach((data)=> {
            if (data.eventName) {
                data.target.removeEventListener(data.eventName, data.handler);
            }
        });
    }

    private setSize() {
        let _c = this.previewTarget['nativeElement'];
        _c.width = _c.parentNode.clientWidth|| _c.width ;
        _c.height = _c.parentNode.clientHeight|| _c.height;
    }

    private onMouseMove(ev) {
        document.body.style.cursor = '';
        for (let i = 0, _c = this.previewTarget['nativeElement'], _preData = this.options.room.preset; i < _preData.length; i++) {
            let preData = _preData[i];
            if (this.interPointer(ev, preData)) {
                document.body.style.cursor = 'pointer';
                preData.strokeStyle = this.COLORS.SELECTED;
                this.update(preData);
            }
            else if (preData.strokeStyle != this.COLORS.DEF) {
                preData.strokeStyle = this.COLORS.DEF;
                this.update(preData);
            }

        }
        //this.update();
    }

    private update(data:any = null) {
        if (!this.context)return;
        if (data) {
            this.redrawPointer(data);
        } else {
            this.options.room.preset.forEach((preData:any)=> {
                this.redrawPointer(preData);
            });
        }

    }

    private redrawPointer(data) {
        let _c = this.previewTarget['nativeElement'],
            image = this.image,
            arrowSize = 20,
            parAsp = _c.width/_c.height,
            canAsp = image.width/image.height,
            imgW = parAsp>canAsp?_c.width:  image.height*parAsp,
            imdHeight = parAsp>canAsp?image.width * _c.height / _c.width:  imgW*image.height/image.width,

            //imgW  = _c.width,// (_c.height * image.width / image.height),
            //imdHeight = _c.width * image.height / image.width,
            x0 = Math.floor(data.quad.x *imgW  - (imgW - _c.width )/2),
            y0 = Math.floor(data.quad.y * imdHeight- (imdHeight - _c.height )/2),
            x1 = x0 + this.selectedSize / 2,
            y1 = y0 + this.selectedSize / 2;

        this.context.strokeStyle = data.strokeStyle || this.COLORS.DEF;
        this.context.beginPath();
        this.context.moveTo(x1, y1 - arrowSize);
        this.context.lineTo(x1, y1 + arrowSize);
        this.context.stroke();
        this.context.beginPath();
        this.context.moveTo(x1 - arrowSize, y1);
        this.context.lineTo(x1 + arrowSize, y1);
        this.context.stroke();
        this.context.strokeRect(x0, y0, this.selectedSize, this.selectedSize);
        this.context.stroke();
    }

    private onMouseUp(ev) {
        for (let i = 0, _preData = this.options.room.preset; i < _preData.length; i++) {
            let preData = _preData[i];
            if (this.interPointer(ev, preData)) {
                return this.options.onSelected(preData);
            }
        }
    }

    private interPointer(ev, preData) {
        let _c = this.previewTarget['nativeElement'],
            image = this.image,
            arrowSize = 20,
            parAsp = _c.width/_c.height,
            canAsp = image.width/image.height,
            imgW = parAsp>canAsp?_c.width:  image.height*parAsp,
            imdHeight = parAsp>canAsp?image.width * _c.height / _c.width:  imgW*image.height/image.width,
            //imgW  =  _c.width,//(_c.height * image.width / image.height),
            //imdHeight = _c.width * image.height / image.width,
            x0 =  (preData.quad.x *imgW  - (imgW - _c.width )/2),
            y0 =  (preData.quad.y * imdHeight- (imdHeight - _c.height )/2) ;

        return (ev.clientX - this.offset.left > x0 && ev.clientX - this.offset.left < x0 + this.selectedSize &&
        ev.clientY - this.offset.top > y0 && ev.clientY - this.offset.top < y0 + this.selectedSize  );
    }

    private drawImg() {
        let image = this.image,
            _c = this.previewTarget['nativeElement'],
            cntx = this.context,
            parAsp = _c.width/_c.height,
            canAsp = image.width/image.height,
            imdWidth = parAsp>canAsp?_c.width:  image.height*parAsp,
            imdHeight = parAsp>canAsp?image.width * _c.height / _c.width:  imdWidth*image.height/image.width
            ;
        cntx.drawImage(image, _c.width / 2 - imdWidth / 2,_c.height / 2 - imdHeight / 2,  imdWidth, imdHeight);
        this.COLORS.CURRENT = this.COLORS.DEF;
        cntx.lineWidth = 10;
        this.offset = _c.getBoundingClientRect();
    }


}

