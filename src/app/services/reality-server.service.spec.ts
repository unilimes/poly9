import { TestBed, inject } from '@angular/core/testing';

import { RealityServerService } from './reality-server.service';

describe('RealityServerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RealityServerService]
    });
  });

  it('should be created', inject([RealityServerService], (service: RealityServerService) => {
    expect(service).toBeTruthy();
  }));
});
