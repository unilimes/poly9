import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Http, Response} from '@angular/http';

@Injectable()
export class DataService {

  constructor(public http: Http) {}

  getDataList(url: string, done: any){
    this.http.get(url, {}).subscribe((res: Response)=>{
      res = res.json();
      done(res);
    })
  }

}
