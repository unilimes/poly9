import { Injectable } from '@angular/core';

declare let localStorage;
@Injectable()
export class Storage {
    static   PREF:string = 'poly9:';

    static get(key:string) {
        return JSON.parse(localStorage.getItem(Storage.PREF + key)) || null;
    }

    static set(key:string, value:any) {
        localStorage.setItem(Storage.PREF + key, JSON.stringify(value));
    }

    static removeItem(key:string) {
        localStorage.removeItem(Storage.PREF + key);
    }
    static size():number {
        return localStorage.length;
    }
    static keyByIndex(i:number) {
        let key = localStorage.key(i);
        if(key.match(Storage.PREF))
                 return localStorage.key(i).split(Storage.PREF)[1]
        else  return null;
    }

}
