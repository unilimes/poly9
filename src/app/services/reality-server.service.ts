import { Injectable } from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import {Storage} from './services';
import * as ENTITY from '../entities/entities';

/*
*
* RealityServerService - service controls to remote render
* @SCENE - scope data info
* */
@Injectable()
export class RealityServerService {


    private curStRemItem:number = 0;
    private quality:number = 50;
    private renderLoop:LoopRender;
    RENDER_URL:string;
    SCENE:any;

    constructor(public http:Http) {
        this.RENDER_URL = ENTITY.Config.MODELS.URL.MAIN;
        this.SCENE = Command.SCENE;
        this.clearScopes(()=> {
            console.log("scopes are removed");
        });
    }

    initScope(room, options) {
        //this.SCENE.NAME = this.SCENE.NAME.split("_")[0] + "_" + room.scopeName.split("_")[1];
        this.curStRemItem = 0;
        //this.clearScopes(()=> {
        if (!room.scopeName)room.scopeName = room.modelUrl + ENTITY.Config.randomstr() + Date.now();
        if (!Storage.get(room.scopeName)) {
            this.checkScope(room, options, options.onSuccess);
        } else {
            //this.addRoom(options, ()=> {});
            options.onSuccess();
        }
        //});

    }

    private checkScope(room, options, next) {
        this.initCmnds([new Command(Command.REQUEST.IS_SCOPE, {scope_name: room.scopeName})], (error, res)=> {
            if (error || !res[0].result) {
                this.createScope(room, options, next);
            } else {
                Storage.set(room.scopeName, true);
                next();
            }
        });
    }

    private createScope(room, options, next) {
        let
            commands:any = [
                new Command(Command.REQUEST.CREATE_SCOPE, {scope_name: room.scopeName}),
                new Command(Command.REQUEST.USE_SCOPE, {scope_name: room.scopeName}),
                new Command(Command.REQUEST.SCENE.CREATE, {scene_name: this.SCENE.NAME}),
                /*---create group---*/
                new Command(Command.REQUEST.ELEMENT.CREATE, {
                    "element_name": this.SCENE.MAIN_GROUP,
                    "element_type": this.SCENE.TYPES.GROUP
                }),
                new Command(Command.REQUEST.SCENE.SET_ROOTFROUP, {
                    "scene_name": this.SCENE.NAME,
                    "group": this.SCENE.MAIN_GROUP
                }),
                /*----scene options----*/
                new Command(Command.REQUEST.ELEMENT.CREATE, {
                    element_name: this.SCENE.OPTIONS,
                    element_type: this.SCENE.TYPES.OPTIONS
                }),
                new Command(Command.REQUEST.SCENE.SET_OPTIONS, {
                    scene_name: this.SCENE.NAME,
                    options: this.SCENE.OPTIONS
                }),
                /*----scene options for material----*/
                //new Command(Command.REQUEST.SCENE.IMPORT_EL, {filename: "${shader}/material_examples/architectural.mdl"}),
                new Command(Command.REQUEST.SCENE.IMPORT_EL, {filename: "${shader}/nvidia/core_definitions.mdl"}),
                /*---create camera and add it to instance widch will be added to main group and transfrom along Z-axis ----*/
                new Command(Command.REQUEST.ELEMENT.CREATE, {
                    "element_name": this.SCENE.CAMERA.NAME,
                    "element_type": this.SCENE.TYPES.CAMERA
                }),
                new Command(Command.REQUEST.ELEMENT.CREATE, {
                    "element_name": this.SCENE.CAMERA.INSTANCE,
                    "element_type": this.SCENE.TYPES.INSTANCE
                }),
                new Command(Command.REQUEST.INSTANCE.ATTACH, {
                    "instance_name": this.SCENE.CAMERA.INSTANCE,
                    "item_name": this.SCENE.CAMERA.NAME
                }),
                new Command(Command.REQUEST.GROUP.ATTACH, {
                    "group_name": this.SCENE.MAIN_GROUP,
                    "item_name": this.SCENE.CAMERA.INSTANCE
                }),
                new Command(Command.REQUEST.SCENE.SET_CAMERA_INSTANCE, {
                    "scene_name": this.SCENE.NAME,
                    "camera_instance": this.SCENE.CAMERA.INSTANCE
                }),

                /*----TEST MODEL--
                 {
                 "jsonrpc": "2.0", "method": "instance_set_world_to_obj", "params": {
                 "instance_name": this.SCENE.CAMERA.INSTANCE,
                 "transform": {
                 "xx": -1.0, "xy": 0.0, "xz": 0.0, "xw": 0.0,
                 "yx": 0.0, "yy": 1.0, "yz": 0.20395425411200102, "yw": 0.0,
                 "zx": 0.0, "zy": 0.20395425411200102, "zz": -1.0, "zw": 0.0,
                 "wx": 8.0, "wy": -5.0, "wz": -25.0, "ww": 1.0
                 }
                 }, "id": 11
                 },
                 {
                 "jsonrpc": "2.0", "method": "generate_extrusion", "params": {
                 "name": "exObject",
                 "profile": [
                 {"x": 0.0, "y": 0.0},
                 {"x": 4.0, "y": 0.0},
                 {"x": 4.0, "y": 6.0},
                 {"x": 8.0, "y": 1.0},
                 {"x": 12.0, "y": 6.0},
                 {"x": 12.0, "y": 0.0},
                 {"x": 16.0, "y": 0.0},
                 {"x": 16.0, "y": 12.0},
                 {"x": 12.0, "y": 12.0},
                 {"x": 8.0, "y": 7.0},
                 {"x": 4.0, "y": 12.0},
                 {"x": 0.0, "y": 12.0}
                 ],
                 "length": 12.0
                 }, "id": 19
                 },
                 {
                 "jsonrpc": "2.0", "method": "element_set_attribute", "params": {
                 "element_name": "exObject",
                 "attribute_type": "Boolean",
                 "attribute_name": "visible",
                 "attribute_value": true,
                 "create": true
                 }, "id": 20
                 },
                 {
                 "jsonrpc": "2.0", "method": "create_element", "params": {
                 "element_name": "exObjectInstance",
                 "element_type": "Instance"
                 }, "id": 21
                 },
                 {
                 "jsonrpc": "2.0", "method": "instance_attach", "params": {
                 "instance_name": "exObjectInstance",
                 "item_name": "exObject"
                 }, "id": 22
                 },
                 {
                 "jsonrpc": "2.0", "method": "group_attach", "params": {
                 "group_name": "exRootGroup",
                 "item_name": "exObjectInstance"
                 }, "id": 23
                 },
                 {
                 "jsonrpc": "2.0", "method": "import_scene_elements", "params": {
                 "filename": "${shader}/material_examples/architectural.mdl"
                 }, "id": 24
                 },
                 {
                 "jsonrpc": "2.0", "method": "create_material_instance_from_definition", "params": {
                 "arguments": {
                 "diffuse": {"r": 0.8, "g": 0.1, "b": 0.25},
                 "reflectivity": 0.5,
                 "refl_gloss": 0.3
                 },
                 "material_definition_name": "mdl::material_examples::architectural::architectural",
                 "material_name": "exMaterial"
                 }, "id": 25
                 },

                 {
                 "jsonrpc": "2.0", "method": "instance_set_material", "params": {
                 "instance_name": "exObjectInstance",
                 "material_name": "exMaterial"
                 }, "id": 26
                 },
                 */                /*---set lights, we have to see the white picture----*/
                new Command(Command.REQUEST.SCENE.IMPORT_EL, {filename: "${shader}/base.mdl"}),
                new Command(Command.REQUEST.CREATE_FUNCTION_CALL_FROM_DEFINITION, {
                    "arguments": {
                        "multiplier": 0.1,
                        "rgb_unit_conversion": {
                            "r": 1.0,
                            "g": 1.0,
                            "b": 1.0
                        },
                        "sun_disk_intensity": 1.0,
                        "physically_scaled_sun": true,
                        "sun_direction": room.sun_dir || {
                            x: 0.3629442359553849,
                            y: 0.8168728919780351,
                            z: 0.4483192611724406
                        }//options.component.webglView.app.spotLight.position.clone().normalize()
                    },
                    "function_definition_name": "mdl::base::sun_and_sky(bool,float,color,float,float,float,float,float,color,color,float3,float,float,float,bool,int,bool)",
                    "function_name": this.SCENE.LIGHTS.SKY
                }),

                /*-----settting Tonemapping so the picture will be normal color---*/
                new Command(Command.REQUEST.ELEMENT.SET_ATTRS, {
                    "create": true,
                    "element_name": this.SCENE.CAMERA.NAME,
                    "attributes": {
                        //iray_internal_tonemapper: {
                        //    "type": this.SCENE.TYPES.BOOLEAN,
                        //    "value": true
                        //},

                        "tm_tonemapper": {
                            "type": this.SCENE.TYPES.STRING,
                            "value": "mia_exposure_photographic"
                        },
                        "mip_cm2_factor": {
                            "type": this.SCENE.TYPES.FLOAT,
                            "value": 1.0
                        },
                        "mip_film_iso": {
                            "type": this.SCENE.TYPES.FLOAT,
                            "value": 100.0
                        },
                        "mip_camera_shutter": {
                            "type": this.SCENE.TYPES.FLOAT,
                            "value": 250.0
                        },
                        "mip_f_number": {
                            "type": this.SCENE.TYPES.FLOAT,
                            "value": 8.0
                        },
                        "mip_gamma": {
                            "type": this.SCENE.TYPES.FLOAT,
                            "value": 2.2
                        }
                    }
                }),
                new Command(Command.REQUEST.ELEMENT.SET_ATTRS, {
                    "element_name": this.SCENE.OPTIONS,
                    "create": true,
                    "attributes": {
                        //"environment_dome_ground": {
                        //    "type": this.SCENE.TYPES.BOOLEAN,
                        //    "value": true
                        //},
                        //"environment_dome_ground_reflectivity": {
                        //    "type": this.SCENE.TYPES.COLOR,
                        //    "value": {"r": 0.4, "g": 0.4, "b": 0.4}
                        //},
                        //"environment_dome_ground_glossiness": {
                        //    "type": this.SCENE.TYPES.FLOAT,
                        //    "value": 1000.0
                        //},
                        //"environment_dome_ground_shadow_intensity": {
                        //    "type": this.SCENE.TYPES.FLOAT,
                        //    "value": 0.8
                        //},
                        "progressive_rendering_max_time": {
                            "type": this.SCENE.TYPES.SINT_32,
                            "value": 10000000
                        },
                        "face_back": {
                            "type": this.SCENE.TYPES.BOOLEAN,
                            "value": true
                        },
                        "face_front": {
                            "type": this.SCENE.TYPES.BOOLEAN,
                            "value": true
                        },
                        "filter": {
                            "type": this.SCENE.TYPES.SINT_32,
                            "value": 0
                        },
                        "finalgather_recv": {
                            "type": this.SCENE.TYPES.BOOLEAN,
                            "value": true
                        },
                        "progressive_rendering_max_samples": {
                            "type": this.SCENE.TYPES.SINT_32,
                            "value": 10000000
                        },
                        "radius": {
                            "type": this.SCENE.TYPES.FLOAT,
                            "value": 1
                        },
                        "reflection_recv": {
                            "type": this.SCENE.TYPES.BOOLEAN,
                            "value": true
                        },
                        "refraction_recv": {
                            "type": this.SCENE.TYPES.BOOLEAN,
                            "value": true
                        },
                        "shadow_recv": {
                            "type": this.SCENE.TYPES.BOOLEAN,
                            "value": true
                        },
                        "transparency_cast": {
                            "type": this.SCENE.TYPES.BOOLEAN,
                            "value": true
                        },
                        "transparency_recv": {
                            "type": this.SCENE.TYPES.BOOLEAN,
                            "value": true
                        },
                        //"progressive_rendering_filtering": {"type": "Boolean", value: false},
                        "environment_function": {"type": this.SCENE.TYPES.ATTR_REF, value: this.SCENE.LIGHTS.SKY},
                        //"environment_lighting_blur": {"type": this.SCENE.TYPES.BOOLEAN, value: true},
                        //"environment_lighting_resolution": {"type": this.SCENE.TYPES.SINT_32, value: 4096},
                        //"environment_function_intensity": {"type": this.SCENE.TYPES.FLOAT, value: 1},
                    }
                })
            ];

        if (options.hasRoom) {
            /*----load model----*/
            commands.push(
                new Command(Command.REQUEST.SCENE.IMPORT_EL, {
                    block: "true",
                    filename: ENTITY.Config.FILE.DIR.PREVIOUS + room.modelUrl
                }),
                new Command(Command.REQUEST.GROUP.ATTACH, {
                    group_name: this.SCENE.MAIN_GROUP,
                    item_name: this.SCENE.LOADED_GROUP
                }),
                new Command(Command.REQUEST.ELEMENT.SET_ATTRS, {
                    element_name: "Floor_inner",
                    attributes: {
                        refl_gloss: 100000,
                        refr_gloss: 100000
                    }
                }),
                new Command(Command.REQUEST.ELEMENT.SET_ATTRS, {
                    element_name: "glass",
                    attributes: {
                        refl_gloss: 100000,
                        refr_gloss: 100000
                    }
                })
            );


            //commands = [(new Command("import_scene", {
            //    "block": "true",
            //    "scene_name": "demo_scene",
            //    "filename": "scenes/meyemII/main.mi"
            //})),
            //    new Command(Command.REQUEST.INSTANCE.WORLD_TO_OBJ, {
            //        "instance_name": this.SCENE.CAMERA.INSTANCE,
            //        transform: {
            //            "xx": 0.9063078165054321,
            //            "xy": -0.2716537714004517,
            //            "xz": 0.3237443566322327,
            //            "xw": 0,
            //            "yx": -2.775557561562891e-17,
            //            "yy": 0.7660444378852844,
            //            "yz": 0.6427876353263855,
            //            "yw": 0,
            //            "zx": -0.4226182699203491,
            //            "zy": -0.5825634002685547,
            //            "zz": 0.6942720413208008,
            //            "zw": 0,
            //            "wx": -0.0005766593385487795,
            //            "wy": -0.01665955409407616,
            //            "wz": -0.2741777300834656,
            //            "ww": 1
            //        }
            //    }),
            //    {
            //        "name": "localize_element",
            //        "params": {"element_name": "mainCamera"}
            //    }, {
            //        "name": "localize_element",
            //        "params": {"element_name": "mainCameraShape"}
            //    }, {
            //        "name": "camera_set_aspect",
            //        "params": {"camera_name": "mainCameraShape", "aspect": 1.3513513513513513}
            //    }, {
            //        "name": "camera_set_resolution",
            //        "params": {"camera_name": "mainCameraShape", "resolution": {"x": 500, "y": 370}}
            //    }, {
            //        "name": "get_camera",
            //        "params": {"camera_name": "mainCameraShape"}
            //    }, {"name": "instance_get_world_to_obj", "params": {"instance_name": "mainCamera"}}
            //];
        }
        if (options.component._parent) {
            commands = commands.concat(options.component._parent.commads.list);
            options.component._parent.commads.list = [];
        } else {
            commands = commands.concat(options.component.commads.list);
            options.component.commads.list = [];
        }
        commands = commands.concat(this.updateCamera(options));
        this.SCENE.RENDER_CNTX_NAME = ENTITY.Config.randomstr();
        this.initCmnds([new Command('batch', {
            commands: commands.map((command:any)=> {
                command.name = command.method;
                return command
            })
        })], (error)=> {
            //if (!error) {
            //this.initCmnds([
            //    new Command(Command.REQUEST.USE_SCOPE, {scope_name: room.scopeName}),
            //    new Command('batch', {
            //        commands: options._webgl.app.updateSceneMeshMatrix().map((command:any)=> {
            //            command.name = command.method;
            //            return command
            //        })
            //    })
            //], (error)=> {
            Storage.set(room.scopeName, true);
            next();
            //});
            //}
        });
    }

    destroy(room, callback:Function = null) {
        if (room && Storage.get(room.scopeName)) {
            this.initCmnds([new Command('delete_scope', {scope_name: room.scopeName})], (error)=> {
                if (!error)Storage.removeItem(room.scopeName);
                if (callback)callback();
            });
        } else {
            if (callback)callback();
        }
        if (this.renderLoop) {
            this.renderLoop.onDestroy();
            this.renderLoop = null;
        }
    }

    private clearScopes(next) {
        if (this.curStRemItem < Storage.size()) {
            let key:any = Storage.keyByIndex(this.curStRemItem++);
            if (!key) {
                this.clearScopes(next);
            } else {
                this.initCmnds([new Command(Command.REQUEST.DEL_SCOPE, {scope_name: key})], (error)=> {
                    if (!error)Storage.removeItem(key);
                    this.clearScopes(next);
                });
            }
        } else {
            next();
        }
    }

    private updateCamera(options) {
        //if (options._webgl.app.camera.noChange)return [];
        //options._webgl.app.camera.noChange = true;

        return [
            new Command(Command.REQUEST.INSTANCE.WORLD_TO_OBJ, {
                "instance_name": this.SCENE.CAMERA.INSTANCE,
                "transform": options.self.curRoom.camera.m_world_to_obj
            }),
            new Command(Command.REQUEST.CAMERA.FOCAL, {
                "camera_name": this.SCENE.CAMERA.NAME,
                "focal": options.self.curRoom.camera.m_focal
            }),
            new Command(Command.REQUEST.CAMERA.RESOLUTION, {
                "camera_name": this.SCENE.CAMERA.NAME,
                "resolution": {
                    x: options.self.viewModal['nativeElement'].clientWidth,
                    y: options.self.viewModal['nativeElement'].clientHeight
                }
            }),
            new Command(Command.REQUEST.CAMERA.ASPECT, {
                "camera_name": this.SCENE.CAMERA.NAME,
                "aspect": options.self.viewModal['nativeElement'].clientWidth / options.self.viewModal['nativeElement'].clientHeight
            }),

            new Command(Command.REQUEST.CAMERA.APERTURE, {
                "camera_name": this.SCENE.CAMERA.NAME,
                "aperture": options.self.curRoom.camera.m_aperture
            }),
            new Command('camera_set_clip_max', {
                "camera_name": this.SCENE.CAMERA.NAME,
                "clip_max": options.self.curRoom.camera.far
            }),
        ];
    }

    setEnv() {
        return [
            new Command(Command.REQUEST.ELEMENT.SET_ATTRS, {
                "element_name": this.SCENE.OPTIONS,
                "attributes": {
                    "progressive_rendering_max_samples": {
                        "type": this.SCENE.TYPES.SINT_32,
                        "value": 10000000
                    }
                    //"progressive_rendering_filtering": {"type": "Boolean", value: false},
                    //"environment_function": {"type": this.SCENE.TYPES.ATTR_REF, value: this.SCENE.LIGHTS.SKY},
                    //"environment_lighting_blur": {"type": this.SCENE.TYPES.BOOLEAN, value: true},
                    //"environment_lighting_resolution": {"type": this.SCENE.TYPES.SINT_32, value: 4096},
                    //"environment_function_intensity": {"type": this.SCENE.TYPES.FLOAT, value: 1},
                },
                "create": true
            })
        ];
    }


    renderScene(room, options) {

        this.cancelRender();
        this.uploadCustomImg(options, ()=> {
            let commands = [new Command(Command.REQUEST.USE_SCOPE, {scope_name: room.scopeName})],
                parentCmnds = options.component._parent ? options.component._parent.commads : (options.component.commads || {}),
                updateImg = ()=> {
                    commands = [
                        new Command(Command.REQUEST.USE_SCOPE, {scope_name: room.scopeName}),
                        ...this.updateCamera(options),
                        ...this.setEnv(),
                        new Command(Command.REQUEST.SCENE.RENDER, {
                            "scene_name": this.SCENE.NAME,
                            "renderer": "iray",
                            //"canvas_name": null,
                            //"format": null,
                            "render_context_name": this.SCENE.RENDER_CNTX_NAME,
                            //"quality": 50,
                            //"render_context_options": {
                            //    //"batch_update_interval":{"type":"Float32","value":1000000.0},
                            //    "scheduler_mode": {
                            //        "type": this.SCENE.TYPES.STRING,
                            //        "value": "batch"
                            //    }
                            //}
                        })
                    ];
                    options.img._onload = ()=> {
                        if (this.renderLoop) {
                            this.renderLoop.startRender();
                            this.renderLoop.options = options;
                        }
                        else {
                            this.renderLoop = new LoopRender(this, options, room);
                        }
                    }
                    options.img.src = (Storage.get(room.scopeName) ? this.RENDER_URL + "?json_rpc_request=" + encodeURIComponent(JSON.stringify(commands)) + "&rid=" + Date.now() : '');
                };

            commands = commands.concat(options.component.commands);

            if (parentCmnds.list)commands = commands.concat(parentCmnds.list);
            if (parentCmnds.materials)for (let field in parentCmnds.materials) {
                if (parentCmnds.materials[field] instanceof Array) {
                    commands = commands.concat(parentCmnds.materials[field]);
                }
            }
            if (options.component._parent) {
                commands = commands.concat(options.component._parent.commands);
                options.component._parent.commands = [];
            }
            options.component.commands = [];
            parentCmnds.list = [];
            parentCmnds.materials = {};
            //commands = commands.concat(options._webgl.app.updateSceneMeshMatrix());
            //console.log("image update start");
            if (commands.length > 1) {
                this.initCmnds(commands, (error)=> {
                    updateImg();
                });
            } else {
                updateImg();
            }
        });


    }

    private uploadCustomImg(options, nextElem:Function) {
        let parent:any = options.self._parent,
            curModel = parent.mainModel, maps = ENTITY.Config.MAT_MAPS,
            prefix = "__b_",
            callbacks = [()=>setTimeout(nextElem, 100)],
            next = ()=> {
                if (callbacks.length)return callbacks.shift()()
            };
        if (curModel) {
            let _form = new FormData(),
                hasCustomText;


            curModel.traverse((child)=> {
                if (child.type == ENTITY.Config.MODELS.TYPES.MESH && child.material.logos && child.material.logos.length) {
                    for (let i = 0; i < maps.length; i++) {
                        if (i == 5 || i == 3)continue;
                        if (child.material[maps[i]] && child.material[maps[i]].image.hasChanges) {
                            for (let di = 0; di < child.material.logos.length; di++) {
                                child.material.logos[di].setActive();
                            }
                            child.material[maps[i]].image.hasChanges = false;
                            let imgFile = this.dataURItoBlob(child.material[maps[i]].image);
                            _form.append("textures[]", imgFile.file, (i == 0 ? prefix : '' ) + maps[i] + child.name + "." + imgFile.type);
                            hasCustomText = true;
                        }
                    }

                    //child.material.map.image.hasChanges = false;

                }
            });
            if (hasCustomText) {
                this.http.post(ENTITY.Config.MODELS.URL.DEV_APP, _form).subscribe((resp:any) => {
                    resp = resp.json();
                    if (resp.status) {
                        curModel.traverse((child)=> {
                            if (child.type == ENTITY.Config.MODELS.TYPES.MESH && child.material.logos && child.material.logos.length)
                                for (let i = 0; i < resp.data.length; i++) {
                                    if (resp.data[i].originName.match(child.name)) {
                                        let curText:any = resp.data.splice(i--, 1)[0],
                                            command:Command,
                                            matName = child.material.name;

                                        curText.newName = ENTITY.Config.CUR_PROJ + curText.newName;
                                        if (parent.commads.materials[child.userData.originName] && parent.commads.materials[child.userData.originName].length) {
                                            matName = parent.commads.materials[child.userData.originName][0].params.material_name;
                                        } else {
                                            if (matName.match("exMaterial")) {
                                                parent.commads.materials[child.userData.originName] = [];
                                            } else {
                                                matName = 'exMaterial' + ENTITY.Config.randomstr();
                                                parent.commads.materials[child.userData.originName] = [
                                                    new Command(Command.REQUEST.MATERIAL.CREATE_DEF,
                                                        {
                                                            "material_name": matName,
                                                            material_definition_name: "mdl::nvidia::core_definitions::flex_material",
                                                            "arguments": {},
                                                        }
                                                    ),
                                                    new Command(Command.REQUEST.ELEMENT.SET_ATTRS,
                                                        {
                                                            "element_name": matName,
                                                            "attributes": {
                                                                "is_metal": false,
                                                                "anisotropy": 0,
                                                                "reflection_roughness": 0.1,
                                                                "ior": 1.85,
                                                                "base_thickness": 0.0,
                                                                "diffuse_roughness": 0.15,
                                                                "thin_walled": false,
                                                                "transmission_roughness": 0.5
                                                            }
                                                        }
                                                    ),
                                                    new Command(Command.REQUEST.INSTANCE.SET_MATRL,
                                                        {
                                                            "instance_name": child.userData.originName,
                                                            "material_name": matName,
                                                            "override": true
                                                        }
                                                    )

                                                ];
                                            }

                                        }

                                        if (curText.originName.match(prefix + maps[0]))command = (new Command(Command.REQUEST.MATERIAL.DIFFUSE,
                                            {
                                                "material_name": matName,
                                                "argument_name": "base_color",
                                                "texture_name": curText.newName,
                                                "create_from_file": true,
                                                texture_options: {
                                                    "scaling": child.material.map.repeat,
                                                    "color_offset": child.material.map.offset
                                                }
                                            }
                                        )); else if (curText.originName.match(maps[1]) || curText.originName.match(maps[2]))command = (new Command(Command.REQUEST.MATERIAL.BUMP,
                                            {
                                                "material_name": matName,
                                                "argument_name": "normal",
                                                "texture_name": curText.newName,
                                                "texture_options": {
                                                    "bump_source": "mono_average",
                                                    "factor": child.material.bumpScale * 100
                                                },
                                                "create_from_file": true
                                            }
                                        )); else if (curText.originName.match(maps[4]))command = (new Command(Command.REQUEST.MATERIAL.DIFFUSE,
                                            {
                                                "material_name": matName,
                                                "argument_name": "reflectivity",
                                                "texture_name": curText.newName,
                                                "create_from_file": true,
                                                texture_options: {
                                                    "scaling": child.material.roughnessMap.repeat,
                                                    "color_offset": child.material.roughnessMap.offset
                                                }
                                            }
                                        ));


                                        if (command) {
                                            for (let di = 0, _listM = parent.commads.materials[child.userData.originName]; di < _listM.length; di++) {
                                                if (command.params.argument_name == _listM[di].params.argument_name)_listM.splice(di, 1);
                                            }
                                            parent.commads.materials[child.userData.originName].push(command);
                                        }
                                        //break;
                                    }

                                }
                        });
                    } else {
                        alert(resp.message);
                    }
                    next();
                }, ()=> {
                    next();
                    console.log("onerror");
                }, ()=> {
                    //setTimeout(next,1200) ;
                });
            } else {
                next();
            }
        }
    }

    private  dataURItoBlob(canvas) {

        let dataURI = canvas.toDataURL(),
            byteString = atob(dataURI.split(',')[1]),
            mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0],
            ab = new ArrayBuffer(byteString.length),
            ia = new Uint8Array(ab);

        // set the bytes of the buffer to the correct values
        for (let i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }

        return {file: new Blob([ab], {type: mimeString}), type: mimeString.split("/")[1]};
    }

    cancelRender() {
        if (this.renderLoop) this.renderLoop.startLoop(false);
    }

    initCmnds(commands:Array<Command>, onFinish) {
        this.http.post(this.RENDER_URL, commands).subscribe((res:any)=> {
            res = res.json();
            let error = '',
                hasInnerCmnds = [];
            for (let i = 0; i < res.length; i++) {
                if (res[i].error)error += ", " + res[i].error.message;
            }
            if (error) {
                console.warn(error);
            } else {
                for (let i = 0; i < commands.length; i++) {
                    if (commands[i].onFinishCmnds)hasInnerCmnds = hasInnerCmnds.concat(commands[i].onFinishCmnds(res[i]));
                }
            }

            if (hasInnerCmnds.length) {
                this.initCmnds([commands[0]].concat(hasInnerCmnds), onFinish);
            } else {
                onFinish(error, res);
            }

        }, (error)=> {
            console.log(error);
        }, ()=> {
            //onFinish("server is unavailable");
        });
    }

    resize(scopeName, options) {
        if (!Storage.get(scopeName))return alert('render remote is not working');
        let commands = [
            new Command(Command.REQUEST.USE_SCOPE, {scope_name: scopeName}),
            new Command(Command.REQUEST.CAMERA.RESOLUTION, {
                "element_name": this.SCENE.CAMERA,
                "resolution": {x: options.width, y: options.height}
            }),
        ];
        this.http.post(this.RENDER_URL, commands).subscribe((res:any)=> {
            console.log(res.json());
        });
    }


    public addScene(url:string, data:any, done:any) {
        let xhr = new XMLHttpRequest();
        xhr.withCredentials = true;

        xhr.addEventListener("readystatechange", function () {
            if (this.readyState === 4) {
                done(this.responseText)
            }
        });

        xhr.open("POST", `${url}`);
        xhr.setRequestHeader("content-type", "application/javascript");
        xhr.setRequestHeader("password", "2lmazQt6cqVXiJI");
        xhr.setRequestHeader("cache-control", "no-cache");
        xhr.setRequestHeader("postman-token", "c2905c99-6a20-f357-89e5-d6725367ddb9");

        xhr.send(data);
    }

    public getSceneRender(url:string, scope:string, scene:string, done:any) {

        let data:any = [
            {
                "jsonrpc": "2.0", "method": "use_scope", "params": {
                "scope_name": `${scope}`
            }, "id": 1
            },

            {
                "jsonrpc": "2.0", "method": "element_set_attributes", "params": {
                "element_name": "exOptions",
                "create": true,
                "attributes": {
                    "progressive_rendering_max_samples": {
                        "type": "Sint32",
                        "value": 100
                    },
                    "progressive_rendering_max_time": {
                        "type": "Sint32",
                        "value": 10
                    }
                }
            }, "id": 2
            },

            {
                "jsonrpc": "2.0", "method": "render", "params": {
                "scene_name": `${scene}`,
                "renderer": "iray",
                "render_context_options": {
                    "scheduler_mode": {
                        "type": "String",
                        "value": "batch"
                    }
                }
            }, "id": 3
            }
        ];

    }

}
/*
 * LoopRender - for best quality of render
 * @options - options for remote render
 * */
class LoopRender {
    options:any;
    private keep:boolean = false;
    private isNotPaused:boolean = false;
    private isAlive:boolean = false;
    private RENDER:any = {
        QUALITY: 50,
        TIME_UPDATE: 2000,
        NAME: ''
    }

    constructor(private main:RealityServerService, options:any, private room:any) {
        this.options = options;
        this.startRender();

    }

    startRender() {
        this.RENDER.NAME = ENTITY.Config.randomstr();
        this.main.initCmnds([
            new Command(Command.REQUEST.USE_SCOPE, {scope_name: this.room.scopeName}),
            new Command('render_loop_start', {
                render_loop_name: this.RENDER.NAME,
                scene_name: this.main.SCENE.NAME,
                timeout: 10,
                render_loop_handler_name: "default",
                render_loop_handler_parameters: ["renderer", "iray"]
            })], (er)=> {
            //if (!er) {
        this.isAlive = true;
        this.startLoop(true);
            //}

        });
    }

    onDestroy() {
        this.isAlive = false;
        this.startLoop(false, ()=> {
            this.keep = this.isNotPaused = false;
        });
    }

    startLoop(start, onFinish:Function = ()=> {
    }) {
        this.RENDER.QUALITY = 50;
        //this.main.initCmnds([
        //    new Command(Command.REQUEST.USE_SCOPE, {scope_name: this.room.scopeName}),
        //    //new Command('render_loop_cancel_render', {render_loop_name: this.RENDER.NAME}),
        //    new Command('render_loop_set_paused', {render_loop_name: this.RENDER.NAME, paused: !start}),
        //    new Command("render_loop_keep_alive", {render_loop_name: this.RENDER.NAME}),
        //], (error)=> {
        this.isNotPaused = start;
        this.keep = !start;
        if (this.isAlive) {
            if (start) {
                this.updateImg();
            } else {
                this.cancelRender();
            }
        } else {
            this.isAlive = false;
        }


        //});
    }

    private cancelRender() {
        this.isNotPaused = false;
        this.main.initCmnds([
            new Command(Command.REQUEST.USE_SCOPE, {scope_name: this.room.scopeName}),
            new Command('render_loop_cancel_render', {render_loop_name: this.RENDER.NAME}),
        ], (error)=> {

        });
    }

    private updateImg(last:Function =null) {

        if (!this.isAlive)return;
        if (this.isNotPaused && this.options.self.curRoom) {
            let commands = [
                new Command(Command.REQUEST.USE_SCOPE, {scope_name: this.room.scopeName}),
                //this.main.setEnv(),


            ];

            if(last){
                commands.push( new Command(Command.REQUEST.SCENE.RENDER, {
                    "scene_name": this.main.SCENE.NAME,
                    "renderer": null,
                    "canvas_name": null,
                    //"format": this.options.formatRenderImg,
                    "render_context_name": this.main.SCENE.RENDER_CNTX_NAME,
                    "quality": 100,
                    render_context_options:  {
                    //"batch_update_interval": {"type": "Float32", "value": 1000000.0},
                    "scheduler_mode": {
                        "type": this.main.SCENE.TYPES.STRING,
                        "value": "batch"
                    }
                }
                }));
            }else{
                commands.push(new Command("render_loop_get_last_render", {render_loop_name: this.RENDER.NAME}));
            }


            this.options.img._onload = ()=> {
                if(last)return last();
                setTimeout(()=> {

                    if (this.RENDER.QUALITY > 100) {

                        this.updateImg(()=>{
                            this.keep = true;
                            this.isNotPaused = false;
                        });
                        //this.keepAlive();
                    } /*else if(this.RENDER.QUALITY>60){

                     this.main.initCmnds([
                     new Command(Command.REQUEST.USE_SCOPE, {scope_name: this.room.scopeName}),
                     new Command("render_loop_get_last_render_result", {render_loop_name:this.RENDER.NAME}),

                     ],()=>{
                     this.RENDER.QUALITY = 40;
                     this.updateImg();
                     })
                     }*/ else {
                        this.updateImg();
                    }
                    this.RENDER.QUALITY +=2;
                }, this.RENDER.TIME_UPDATE * 0.5)
            }
            this.options.img.src = this.main.RENDER_URL + "?json_rpc_request=" + encodeURIComponent(JSON.stringify(commands)) + "&rid=" + Date.now();

        } else {
            this.startLoop(false);
        }


    }


    private keepAlive() {
        setTimeout(()=> {
            if (this.keep && this.isAlive) {
                this.main.initCmnds([
                    new Command(Command.REQUEST.USE_SCOPE, {scope_name: this.room.scopeName}),
                    new Command("render_loop_keep_alive", {render_loop_name: this.RENDER.NAME})
                ], (er)=> {
                    if (er) {
                        this.isAlive = false;
                    } else {
                        this.keepAlive();
                    }
                });
            }
        }, 2 * this.RENDER.TIME_UPDATE);
    }
}
/*
 * Command - command standard for reality server request
 * */
export class Command {
    static SCENE:any = {
        CNTX_TIME_OUT: 1,
        TYPES: {
            GROUP: 'Group',
            OPTIONS: 'Options',
            CAMERA: 'Camera',
            INSTANCE: 'Instance',
            ATTR_REF: 'Ref',
            STRING: 'String',
            FLOAT: 'Float32',
            SINT_32: 'Sint32',
            BOOLEAN: 'Boolean',
            COLOR: "Color",
        },
        LIGHTS: {
            SKY: 'exSunSky'
        },
        NAME: 'demo_scene_ex',
        OPTIONS: 'exOptions',
        MAIN_GROUP: 'exRootGroup',
        RENDER: {
            LOOP: "1111"
        },
        ROOM_GROUP: 'room_Group',
        LOADED_GROUP: 'obj_root_group_1',
        CAMERA: {
            NAME: 'exCamera',
            INSTANCE: 'exCameraInstance'
        }
    };
    static REQUEST:any = {
        CREATE_FUNCTION_CALL_FROM_DEFINITION: 'create_function_call_from_definition',
        MDL_ATTACH_FUNC_2_ARG: 'mdl_attach_function_to_argument',
        CREATE_SCOPE: 'create_scope',
        BATCH: 'batch',
        USE_SCOPE: 'use_scope',
        IS_SCOPE: 'scope_exists',
        DEL_SCOPE: 'delete_scope',
        MESH: {
            CREATE: 'generate_mesh'
        },
        MATERIAL: {
            CREATE_DEF: 'create_material_instance_from_definition',
            BUMP: "material_attach_bump_texture_to_argument",
            DIFFUSE: "material_attach_texture_to_argument"
        },
        ELEMENT: {
            CREATE: "create_element",
            SET_ATTR: "element_set_attribute",
            SET_ATTRS: "element_set_attributes",
            GET_ATTRS: "element_get_attributes",
        },
        SCENE: {
            IMPORT: "import_scene",
            IMPORT_EL: "import_scene_elements",
            CREATE: 'create_scene',
            SET_CAMERA_INSTANCE: 'scene_set_camera_instance',
            SET_OPTIONS: "scene_set_options",
            SET_ROOTFROUP: "scene_set_rootgroup",
            RENDER: 'render'
        },
        CAMERA: {
            ASPECT: "camera_set_aspect",
            RESOLUTION: "camera_set_resolution",
            FOCAL: "camera_set_focal",
            APERTURE: 'camera_set_aperture'
        },
        GROUP: {
            ATTACH: "group_attach",
            DATACH: "group_detach"
        },
        INSTANCE: {
            WORLD_TO_OBJ: "instance_set_world_to_obj",
            SET_TRANSFORMS: "instance_set_transforms",
            ATTACH: "instance_attach",
            DETACH: "instance_detach",
            SET_CAMERA: "scene_set_camera_instance",
            SET_MATRL: "instance_set_material",
            SETS_MATRL: "instances_set_material",
        },
        TEXTURE: {
            CREATE: 'create_texture'
        }
    }
    private jsonrpc:string = '2.0';
    public onFinishCmnds:Function;
    private method:string = '';
    params:any = {};
    private id:number;

    constructor(method, params, onFinishCmnds:Function = null, id:number = Date.now()) {
        this.method = method;
        this.params = params;
        this.id = id;
        if (onFinishCmnds)this.onFinishCmnds = onFinishCmnds;


    }
}
