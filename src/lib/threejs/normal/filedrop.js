NMO_FileDrop = new function () {
  var self = this;

  this.height_canvas = document.createElement("canvas");

  this.container_height = 300;

  this.initHeightMapWithImg = function(img,done,opt){
    var options = opt||{};
    self.height_image =img;
    self.height_canvas.width = img.width;
    self.height_canvas.height = img.height;
    var context = self.height_canvas.getContext('2d');

    context.drawImage(img, 0, 0, img.width, img.height);

    self.width = self.naturalWidth;
    self.height = self.naturalHeight;

    NMO_RenderNormalview.renderNormalview_init(); // init only when both types of images are loaded (init)
    NMO_AmbientOccMap.initAOshader();
    NMO_DisplacementMap.initDisplacementshader();
    NMO_SpecularMap.initSpecularshader();
    NMO_RenderNormalview.height_map.needsUpdate = true;

    NMO_NormalMap.setNormalSetting('strength', 20000.5*(options.normalScale||1), 'initial');
    NMO_NormalMap.setNormalSetting('level', 8.0, 'initial');
    NMO_NormalMap.setNormalSetting('blur_sharp', 0, 'initial');
    NMO_NormalMap.createNormalMap(); // height map was loaded... so create standard normal map!

    NMO_DisplacementMap.createDisplacementMap(-0.5);
    NMO_DisplacementMap.setDisplacementScale(0.0);

    NMO_AmbientOccMap.createAmbientOcclusionTexture();
    NMO_SpecularMap.createSpecularTexture();

    var res={map:img};
    (function getImg(listMaps){
      if(listMaps.length){
        var _img = new Image(),
            cur = listMaps.shift();
        _img.onload = function(){
          res[cur.n] = _img;
          getImg(listMaps);
        }
        _img.src = cur.v.toDataURL();
      }else{
        done(res);
      }


    })([{n:'normalMap',v:NMO_NormalMap.normal_canvas}, {n:'displacementMap',v:NMO_DisplacementMap.displacement_canvas}, {n:'metalnessMap',v:NMO_SpecularMap.specular_canvas}, {n:'roughnessMap',v:NMO_AmbientOccMap.ao_canvas}])
  }
  this.initHeightMap = function (url, done) {
    self.height_image = new Image();
    self.height_image.crossOrigin = "anonymous";
    self.height_image.onload = function () {
      var context = self.height_canvas.getContext('2d');

      context.drawImage(this, 0, 0, this.width, this.height, 0, 0, self.height_canvas.width, self.height_canvas.height);

      self.width = self.naturalWidth;
      self.height = self.naturalHeight;

      NMO_RenderNormalview.renderNormalview_init(); // init only when both types of images are loaded (init)
      NMO_AmbientOccMap.initAOshader();
      NMO_DisplacementMap.initDisplacementshader();
      NMO_SpecularMap.initSpecularshader();
      NMO_RenderNormalview.height_map.needsUpdate = true;

      NMO_NormalMap.setNormalSetting('strength', 2.5, 'initial');
      NMO_NormalMap.setNormalSetting('level', 8.0, 'initial');
      NMO_NormalMap.setNormalSetting('blur_sharp', 0, 'initial');
      NMO_NormalMap.createNormalMap(); // height map was loaded... so create standard normal map!

      NMO_DisplacementMap.createDisplacementMap(-0.5);
      NMO_DisplacementMap.setDisplacementScale(-0.3);

      NMO_AmbientOccMap.createAmbientOcclusionTexture();
      NMO_SpecularMap.createSpecularTexture();

      done(NMO_NormalMap.normal_canvas, NMO_DisplacementMap.displacement_canvas, NMO_SpecularMap.specular_canvas, NMO_AmbientOccMap.ao_canvas);
    };

    self.height_image.src = url;
  };
};
