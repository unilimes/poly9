/**
 * @author mrdoob / http://mrdoob.com/
 */

THREE.OBJExporter = function () {
};

THREE.OBJExporter.prototype = {

    constructor: THREE.OBJExporter,

    parse: function (object) {

        var output = '';

        var indexVertex = 0;
        var indexVertexUvs = 0;
        var indexNormals = 0;

        var vertex = new THREE.Vector3();
        var normal = new THREE.Vector3();
        var uv = new THREE.Vector2();

        var i, j, k, l, m, face = [];
        var materials = {};
        var mtlFileName = 'model'; // maybe this value can be passed as parameter
        output += 'mtllib ' + mtlFileName + '.mtl\n';

        var parseMesh = function (mesh) {

            var nbVertex = 0;
            var nbNormals = 0;
            var nbVertexUvs = 0;

            var material = mesh.material;
            var geometry = mesh.geometry;

            var normalMatrixWorld = new THREE.Matrix3();

            if (geometry instanceof THREE.Geometry) {

                geometry = new THREE.BufferGeometry().setFromObject(mesh);

            }

            if (geometry instanceof THREE.BufferGeometry) {

                // shortcuts
                var vertices = geometry.getAttribute('position');
                var normals = geometry.getAttribute('normal');
                var uvs = geometry.getAttribute('uv');
                var indices = geometry.getIndex();
                var round = 4;

                // name of the mesh object
                //output += 'o ' + mesh.name + '\n';
                output += '# ' + '\n';
                output += '# ' + mesh.name + '\n';
                output += '# ' + '\n';

                // name of the mesh material
                //if (mesh.material && mesh.material.name) {
                //    output += 'usemtl ' + mesh.material.name + '\n';
                //}

                // vertices

                if (vertices !== undefined) {

                    for (i = 0, l = vertices.count; i < l; i++, nbVertex++) {

                        vertex.x = vertices.getX(i).toFixed(round);
                        vertex.y = vertices.getY(i).toFixed(round);
                        vertex.z = vertices.getZ(i).toFixed(round);

                        // transfrom the vertex to world space
                        vertex.applyMatrix4(mesh.matrixWorld);

                        // transform the vertex to export format
                        output += 'v ' + vertex.x + ' ' + vertex.y + ' ' + vertex.z + '\n';

                    }

                }

                // uvs

                if (uvs !== undefined) {

                    for (i = 0, l = uvs.count; i < l; i++, nbVertexUvs++) {

                        uv.x = uvs.getX(i).toFixed(round);
                        uv.y = uvs.getY(i).toFixed(round);

                        // transform the uv to export format
                        output += 'vt ' + uv.x + ' ' + uv.y + ' 0.00000' + '\n';

                    }

                }

                // normals

                if (normals !== undefined) {

                    normalMatrixWorld.getNormalMatrix(mesh.matrixWorld);

                    for (i = 0, l = normals.count; i < l; i++, nbNormals++) {

                        normal.x = normals.getX(i).toFixed(round);
                        normal.y = normals.getY(i).toFixed(round);
                        normal.z = normals.getZ(i).toFixed(round);

                        // transfrom the normal to world space
                        normal.applyMatrix3(normalMatrixWorld);

                        // transform the normal to export format
                        output += 'vn ' + normal.x + ' ' + normal.y + ' ' + normal.z + '\n';

                    }

                }

                // material
                output += 'g  ' + mesh.name + '\n';


                if(material.type =='MultiMaterial'){
                    for (var n = 0; n < geometry.groups.length; n++) {
                        var _group = geometry.groups[n];
                        var _mat = material.materials[_group.materialIndex];

                        materials[ _mat.uuid] = _mat;
                        if (_mat.name !== '')
                            output += 'usemtl ' + _mat.name + '\n';
                        else
                            output += 'usemtl material' + _mat.id + '\n';
                        materials[_mat.uuid] = _mat;

                        // faces
                        if (indices !== null) {

                            for (i = _group.start, l = _group.count; i < l; i += 3) {

                                for (m = 0; m < 3; m++) {

                                    j = indices.getX(i + m) + 1;

                                    face[m] = ( indexVertex + j ) + '/' + ( uvs ? ( indexVertexUvs + j ) : '' ) + '/' + ( indexNormals + j );

                                }

                                // transform the face to export format
                                output += 'f ' + face.join(' ') + "\n";

                            }

                        } else {

                            for (i = _group.start, l = _group.count; i < l; i += 3) {

                                for (m = 0; m < 3; m++) {

                                    j = i + m + 1;

                                    face[m] = ( indexVertex + j ) + '/' + ( uvs ? ( indexVertexUvs + j ) : '' ) + '/' + ( indexNormals + j );

                                }

                                // transform the face to export format
                                output += 'f ' + face.join(' ') + "\n";

                            }

                        }


                    }
                }else{
                    if (material.name !== '')
                        output += 'usemtl ' + material.name + '\n';
                    else
                        output += 'usemtl material' + material.id + '\n';
                    materials[material.uuid] = material;
                    // faces
                    if (indices !== null) {

                        for (i = 0, l = indices.count; i < l; i += 3) {

                            for (m = 0; m < 3; m++) {

                                j = indices.getX(i + m) + 1;

                                face[m] = ( indexVertex + j ) + '/' + ( uvs ? ( indexVertexUvs + j ) : '' ) + '/' + ( indexNormals + j );

                            }

                            // transform the face to export format
                            output += 'f ' + face.join(' ') + "\n";

                        }

                    } else {

                        for (i = 0, l = vertices.count; i < l; i += 3) {

                            for (m = 0; m < 3; m++) {

                                j = i + m + 1;

                                face[m] = ( indexVertex + j ) + '/' + ( uvs ? ( indexVertexUvs + j ) : '' ) + '/' + ( indexNormals + j );

                            }

                            // transform the face to export format
                            output += 'f ' + face.join(' ') + "\n";

                        }

                    }
                }





            } else {

                console.warn('THREE.OBJExporter.parseMesh(): geometry type unsupported', geometry);

            }

            // update index
            indexVertex += nbVertex;
            indexVertexUvs += nbVertexUvs;
            indexNormals += nbNormals;

        };

        var parseLine = function (line) {

            var nbVertex = 0;

            var geometry = line.geometry;
            var type = line.type;

            if (geometry instanceof THREE.Geometry) {

                geometry = new THREE.BufferGeometry().setFromObject(line);

            }

            if (geometry instanceof THREE.BufferGeometry) {

                // shortcuts
                var vertices = geometry.getAttribute('position');
                var indices = geometry.getIndex();

                // name of the line object
                output += 'o ' + line.name + '\n';

                if (vertices !== undefined) {

                    for (i = 0, l = vertices.count; i < l; i++, nbVertex++) {

                        vertex.x = vertices.getX(i);
                        vertex.y = vertices.getY(i);
                        vertex.z = vertices.getZ(i);

                        // transfrom the vertex to world space
                        vertex.applyMatrix4(line.matrixWorld);

                        // transform the vertex to export format
                        output += 'v ' + vertex.x + ' ' + vertex.y + ' ' + vertex.z + '\n';

                    }

                }

                if (type === 'Line') {

                    output += 'l ';

                    for (j = 1, l = vertices.count; j <= l; j++) {

                        output += ( indexVertex + j ) + ' ';

                    }

                    output += '\n';

                }

                if (type === 'LineSegments') {

                    for (j = 1, k = j + 1, l = vertices.count; j < l; j += 2, k = j + 1) {

                        output += 'l ' + ( indexVertex + j ) + ' ' + ( indexVertex + k ) + '\n';

                    }

                }

            } else {

                console.warn('THREE.OBJExporter.parseLine(): geometry type unsupported', geometry);

            }

            // update index
            indexVertex += nbVertex;

        };

        object.traverse(function (child) {

            if (child instanceof THREE.Mesh) {

                parseMesh(child);

            }

           /* if (child instanceof THREE.Line) {

                parseLine(child);

            }
*/
        });

        // mtl output

        var images = [],
            items = 0,
            round = 4,
            mtlOutput = '',// "metalnessMap", "roughnessMap"
            maps = [
                {name: 'map', output: ['map_Ka', 'map_Kd']},
                {
                    name: 'normalMap',
                    output: ['map_normal'],
                    attr: [{name: 'normalScale_x', output: 'sX'}, {name: 'normalScale_y', output: 'sY'}]
                },
                {name: 'bumpMap', output: ['map_bump'], attr: [{name: 'bumpScale', output: 'bm'}]},
                {name: 'displacementMap', output: ['map_disp'], attr: [{name: 'displacementScale', output: 'dm'}]},
                {name: 'metalnessMap', output: ['map_metal'], attr: [{name: 'metalness', output: 'mm'}]},
                {name: 'roughnessMap', output: ['map_rough'], attr: [{name: 'roughness', output: 'rm'}]},
            ];

        var _img = [], _imgS = [];
        for (var key in materials) {
            var mat = materials[key];
            mtlOutput += '\n';
            if (mat.name !== '')
                mtlOutput += 'newmtl ' + mat.name + '\n';
            else
                mtlOutput += 'newmtl material' + mat.id + '\n';

            mtlOutput += '	Ns 10.0000\n';
            mtlOutput += '	Ni 1.5000\n';
            mtlOutput += '	d ' + (mat.opacity).toFixed(round) + '\n';
            mtlOutput += '	Tr ' + ( mat.opacity).toFixed(round) + '\n';
            mtlOutput += '	Tf 1.0000 1.0000 1.0000\n';
            mtlOutput += '	illum 2\n';
            mtlOutput += '	Ka ' + (mat.color.r).toFixed(round) + ' ' + (mat.color.g).toFixed(round) + ' ' + (mat.color.b).toFixed(round) + ' ' + '\n';
            mtlOutput += '	Kd ' + (mat.color.r).toFixed(round) + ' ' + (mat.color.g).toFixed(round) + ' ' + (mat.color.b).toFixed(round) + ' ' + '\n';
            mtlOutput += '	Ks 0.0000 0.0000 0.0000\n';
            mtlOutput += '	Ke 0.0000 0.0000 0.0000\n';

            for (var mapI = 0; mapI < maps.length; mapI++) {
                var curItem = maps[mapI],
                    _map = mat[curItem.name];
                if (_map && _map instanceof THREE.Texture) {
                    //var file = _map.image.currentSrc;
                    var file,
                        itemsss = _img.indexOf(_map.image.src);
                    if (itemsss > -1) {
                        //file = "maps\\" + _imgS[itemsss];
                        file = "maps " + _map.image.currentSrc;
                    } else {
                        images.push({name: "image" + items++ + (".png"), img: _map.image});
                        file = "maps " + _map.image.currentSrc;
                        _img.push(_map.image.src);
                        _imgS.push(images[images.length - 1].name);
                    }


                    for (var outI = 0; outI < curItem.output.length; outI++) {
                        mtlOutput += '	' + curItem.output[outI];
                        if (curItem.attr) {
                            for (var attrI = 0; attrI < curItem.attr.length; attrI++) {
                                var _s = curItem.attr[attrI].name.split("_"),
                                    _val = mat;
                                for (var _sI = 0; _sI < _s.length; _sI++) {
                                    _val = _val[_s[_sI]];
                                }
                                mtlOutput += " -" + curItem.attr[attrI].output + " " + _val;
                            }
                        }
                        mtlOutput += " " + file + '\n';
                    }
                }
            }
            /*if (mat.map && mat.map instanceof THREE.Texture) {

             var file = mat.map.image.currentSrc.slice( mat.map.image.currentSrc.slice.lastIndexOf("/"), mat.map.image.currentSrc.length - 1 );

             mtlOutput += 'map_Ka ' + file + '\n';
             mtlOutput += 'map_Kd ' + file + '\n';

             }*/

        }

        return {
            images: images,
            obj: output,
            mtl: mtlOutput
        };

    }

};
