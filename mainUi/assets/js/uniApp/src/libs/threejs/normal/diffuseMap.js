var diffuse_canvas = document.createElement("canvas");

var readDiffuseImage = function(imgFile){
  if(!imgFile.type.match(/image.*/)){
    return;
  }

  var reader = new FileReader();
  reader.onload = function(e){
    var data = e.target.result;
    if (imgFile.type == "image/targa"){
      var tga = new TGA();
      tga.load(new Uint8Array(data));
      data = tga.getDataURL('image/png');
    }
    loadDiffuseMap(data);
    document.getElementById('input_diffuse').disabled = false;
  };
  if (imgFile.type == "image/targa")
    reader.readAsArrayBuffer(imgFile);
  else
    reader.readAsDataURL(imgFile);
};

var loadDiffuseMap = function(source){
  diffuse_image = new Image();

  diffuse_image.onload = function(){
    var ctx_diffuse = diffuse_canvas.getContext("2d");
    diffuse_canvas.width = diffuse_image.width;
    diffuse_canvas.height = diffuse_image.height;
    ctx_diffuse.clearRect(0, 0, diffuse_image.width, diffuse_image.height);
    ctx_diffuse.drawImage(diffuse_image,0,0, diffuse_image.width, diffuse_image.height);
    NMO_RenderView.enableDiffuse();

    NMO_RenderView.diffuse_map.needsUpdate = true;
  };

  diffuse_image.src = source;
};
