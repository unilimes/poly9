import {Config} from "./entities/helpers/Config.js";
import {Preloader} from "./entities/preloader/glPreloader.js";
import {MHttp} from "./entities/helpers/Http.js";
import {MStorage} from "./entities/helpers/MStorage.js";

/**
 *static class for storage all utils in app
 */
export class Utils {

}
Utils.Config = Config;
Utils.Preloader = Preloader;
Utils.MHttp = MHttp;
Utils.MStorage = MStorage;