/**
 *@class zip structure
 *
 */
export class IZipStructure {

    /**
     *
     *@constructor
     *@param {Object} data - zip structure
     *@param {Object} [data.materials = {}] - materials store, from 'materials' folder
     *@param {Object} [data.files = {}] - anothers files store, from anothers folder which don`t match any
     *@param {Array<IPattern>} [data.decorations = []] - patterns decorations store, from 'decorations' folder
     *@param {Array<IProductModelPart>} [data.parts = []] - parts store, from 'parts' folder
     * */
    constructor(data) {
        data = data || {};
        this.materials= data.materials||{};
        this.decorations= data.decorations||[];
        this.parts= data.parts||[];
        this.files= data.files||{};
    }
}
