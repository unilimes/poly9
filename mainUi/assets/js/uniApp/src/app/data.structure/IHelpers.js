export class IVector3 {
    constructor(data) {
        data = data || {};
        this.x = data.x;
        this.y = data.y;
        this.z = data.z;
    }
}
