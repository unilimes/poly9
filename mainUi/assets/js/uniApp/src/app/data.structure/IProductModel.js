/**
 *@class model structure
 *
 */
export class IProductModel {

    /**
     *
     *@constructor
     *@param {Object} data - check {@link http://54.227.198.145:3009/assets/js/uniApp/src/app/data.structure/product.model.json|model} json
     *@param {Object} [data._id = null] - unique id
     *@param {Object} [data.model = IProductModelData] - model data
     * */
    constructor(data) {
        data = data || {};
        this._id = data.id;
        this.model = new IProductModelData(data.model);
    }
}
/**
 *@class model data structure
 *
 */
export class IProductModelData {

    /**
     *
     *@constructor
     *@param {Object} data - check {@link http://54.227.198.145:3009/assets/js/uniApp/src/app/data.structure/product.model.json|model} json
     *@param {string} [data.absUrl = null] - if set will load from remote server
     *@param {string} [data.absUrlRS = null] - if set will load from your content_root folder on RS server
     *@param {string} [data.name = null] - name for model, match to the name of obj file
     *@param {boolean} [data.hideEnv = false] - hide environment map or not for current model
     * */
    constructor(data) {
        data = data || {};
        this.absUrl = data.absUrl;
        this.absUrlRS = data.absUrlRS;
        this.name = data.name;
        this.hideEnv = data.hideEnv || false;
    }
}