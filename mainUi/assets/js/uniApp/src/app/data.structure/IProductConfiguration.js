/**
 * @class model configuration
 * */
export class IProductConfiguration {
    /**
     *@constructor
     *@param {Object} data -  {@link http://54.227.198.145:3009/assets/js/uniApp/src/app/data.structure/product.configuratioin.json|configuration} json
     *@param {string} data._id - unique _id
     *@param {string<IProductModel>} data.product_model_id - reference to model id
     *@param {Array<IProductModelPart>} [data.model_parts=[]] - list of reference to model parts
     *@param {string} data.preview - absolute path to image preview
     *@param {IMainSettins} data.main_settings - settings
     *
     * */
    constructor(data) {
        data = data || {};
        this._id = data._id;
        this.product_model_id = data.product_model_id;
        this.model_parts = data.model_parts||[];
        this.preview = data.preview;
        this.main_settings = data.main_settings;
    }
}
