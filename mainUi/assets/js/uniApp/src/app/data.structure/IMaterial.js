/**
 * @class material structure
 *
 */
export class IMaterial {
    /**
     *
     *@constructor
     *
     *@param {Object} data -  {@link http://54.227.198.145:3009/assets/js/uniApp/src/app/data.structure/material.json|material} json
     *@param {string} data._id - unique id
     *@param {string} data.name - name
     *@param {IMaterialMaps} data.maps - absolute pathes to maps
     *@param {IMaterialMaps} data.mapsRS - maps pathes on RS, warnign the pathes must be relative to your folder content_root on server
     *
     *@param {IMaterialSettings} data.settings, can have own default settings or the settings for configuration
     * */
    constructor(data) {
        data = data || {};
        this.name = data.name;
        this._id = data._id;
        this.maps = new IMaterialMaps(data.maps);
        this.mapsRS = new IMaterialMaps(data.mapsRS);
        this.settings = new IMaterialSettings(data.settings);
    }
}
/**
 * @class material maps
 * */
export class IMaterialMaps {
    /**
     * @constructor
     *@param {Object} data - absolute pathes to maps
     *@param {string} data.map - diffuse absolute path
     *@param {string} data.bumpMap - bump absolute path
     *@param {string} data.normalMap - normal absolute path, warning if set bump map will not use
     *@param {string} data.metalnessMap - metalness absolute path
     *@param {string} data.roughnessMap - roughness absolute path
     *@param {string} data.displacementMap - displacement absolute path
     *@param {string} data.envMap - enviremont cube map
     * */
    constructor(data) {
        let _d = data || {};
        this.map = _d.map;
        this.bumpMap = _d.bumpMap;
        this.normalMap = _d.normalMap;
        this.metalnessMap = _d.metalnessMap;
        this.roughnessMap = _d.roughnessMap;
        this.displacementMap = _d.displacementMap;
        this.alphaMap = _d.alphaMap;
        this.envMap = _d.envMap;
    }
}
/**
 * @class material settings structure
 *
 */
export class IMaterialSettings {
    /**
     *
     *@constructor
     *
     *
     *@param {Object} data - settings for   {@link https://threejs.org/docs/index.html#api/materials/MeshPhysicalMaterial|material}
     *@param {string} data._id - unique id
     *@param {string<IMaterial>} data.material_id - unique id
     *@param {string<IProductModelPart>} data.product_part_id - unique id
     *@param {string} [data.color='rgb(255,255,255)'] - diffuse color
     *@param {number} data.envMapIntensity - intensity of enviremont
     *@param {number} data.opacity - between 0 and 1
     *@param {number} data.reflectivity - reflection intensity
     *@param {number} data.metalness - metalness intensity between 0 and 1
     *@param {number} data.roughness - roughness intensity between 0 and 1
     *@param {number} data.bumpScale - if has bump map, will bump the map
     *@param {number} data.displacementScale - if has displacement map, will display the map, between 0 and 1
     * */
    constructor(data) {
        let _d = data || {};
        this._id = _d._id;
        this.material_id = _d.material_id;
        this.product_part_id = _d.product_part_id;
        this.color = _d.color instanceof THREE.Color? _d.color.getStyle(): _d.color;
        this.envMapIntensity = _d.envMapIntensity;
        this.opacity = _d.opacity;
        this.reflectivity = _d.reflectivity;
        this.roughness = _d.roughness;
        this.metalness = _d.metalness;
        this.bumpScale = _d.bumpScale;
        this.displacementScale = _d.displacementScale;
    }
}