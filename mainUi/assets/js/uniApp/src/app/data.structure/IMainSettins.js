import {IVector3} from './IHelpers.js';

/**
 * @class data structure for settings
 */
export class IMainSettins {
    /**
     *
     *@constructor
     *@param {Object} data - {@link http://54.227.198.145:3009/assets/js/uniApp/src/app/data.structure/main.settings.json|settings}  json
     *@param {Object} data.remote_render - settings for remote render(RS)
     *@param {number} [data.remote_render.image_size  = 2048] - settings for remote render(RS)
     *@param {IMainSettinsLocalRender} data.local_render - settings for local {@link https://threejs.org/docs/index.html#api/renderers/WebGLRenderer|render}
     *
     *@param {Object} data.controls.mouse - settings for mouse controls
     *@param {number} [data.controls.mouse.autoRotateSpeed = 0.1] - on default rotate speed
     *@param {number} [data.controls.mouse.rotateSpeed = 0.17] - on drag mouse by X rotate speed
     *@param {number} [data.controls.mouse.rotateSpeedUP = 0.065] - on drag mouse by Y rotate speed
     *@param {Object} data.pattern - settings for pattern
     *@param {number} [data.pattern.scaleSpeed = 0.01]- scale speed on edit pattern
     *@param {number} [data.pattern.rotateSpeed = 0.01] - rotate speed on edit pattern

     *
     */
    constructor(data) {
        data = data || {};
        this.remote_render = data.remote_render;
        this.local_render = new IMainSettinsLocalRender(data.local_render);
        this.controls = new IMainSettinsControls(data.controls);
        this.pattern = data.pattern;

    }
}
/**
 * @class data local render structure settings
 */
export class IMainSettinsLocalRender {
    /**
     *
     *@constructor
     *@param {Object} data - settings for local {@link https://threejs.org/docs/index.html#api/renderers/WebGLRenderer|render}
     *@param {Array<string>} [data.cubeMap = null] - if set default backGround will not use, list of images {@link https://www.google.com.ua/search?q=cubeMap+images&tbm=isch&source=iu&pf=m&ictx=1&fir=Rf8vnwVLFEp6YM%253A%252CF2wSt6vqNLibiM%252C_&usg=__AeNKBZookbjDYbRLFcXKn3KFEb0%3D&sa=X&ved=0ahUKEwix7e38sIvXAhXBF5oKHfT5Bh0Q9QEIKzAC#imgrc=Rf8vnwVLFEp6YM:|example}(6 item required) with absolute path
     *@param {string} [data.envMap = null] - link to environment map
     *@param {boolean} [data.showDimensionHelper = false] - show dimension box or not
     *@param {string} [data.hdr = null] - if set cubeMap will not use, absolute path to image,{@link https://www.google.com.ua/search?q=panorama+image&tbm=isch&source=iu&pf=m&ictx=1&fir=rCZ4DyXJmuhqCM%253A%252C47LuSNSoi55TzM%252C_&usg=__C3k3NrARKpPtop1PRAybSvEl-i8%3D&sa=X&ved=0ahUKEwiYi6CEsovXAhXkDpoKHZA0A4AQ9QEIKjAA#imgrc=rCZ4DyXJmuhqCM:|example}
     *@param {number} [data.pixelRatio = 1] - quality image render
     *@param {number} [data.blur = 0] - image blur
     *@param {boolean} [data.gammaInput = false] - are premultiplied gamma
     *@param {boolean} [data.gammaOutput = false] - outputted in premultiplied gamma
     *@param {boolean} [data.shadows = false] - enable shadows
     *@param {boolean} [data.antialias = true] - increase quality
     *@param {boolean} [data.alpha = true] - allow transparent in scene
     *@param {boolean} [data.preserveDrawingBuffer = true] -  if nned to take the screen shot, warning take more memory from device
     *@param {Array<IMainSettinsLocalLights>} data.lights - lights settings
     * */
    constructor(data) {
        data = data || {};
        this.cubeMap = data.cubeMap;
        this.envMap = data.envMap;
        this.showDimensionHelper = !!data.showDimensionHelper || false;
        this.hdr = data.hdr;
        this.pixelRatio = data.pixelRatio || window.devicePixelRatio;
        this.blur = data.blur || 0;
        this.gammaOutput = !!data.gammaOutput;
        this.gammaInput = !!data.gammaInput;
        this.shadows = !!data.shadows;
        this.antialias = !!data.antialias;
        this.alpha = !!data.alpha;
        //this.preserveDrawingBuffer = !!data.preserveDrawingBuffer;
        this.lights = data.lights ? data.lights.map((lights)=> {
            return new IMainSettinsLocalLights(lights)
        }) : [];
    }
}

/**
 * @class data controls structure settings
 */
export class IMainSettinsControls {
    /**
     *
     *@constructor
     *@param {Object} data - settings for local {@link https://threejs.org/docs/index.html#api/renderers/WebGLRenderer|render}
     *@param {boolean} [data.keys=false] - Keyboard Shortcuts, if true will work
     *@param {boolean} [data.contextMenu=false] - Right-click functions, if true will work
     *@param {HEX} [data.selectedColor='#b09e3c'] - color selected
     *@param {boolean} [data.highlightSelected=false] - highlight selected model/part or not
     * */
    constructor(data) {
        data = data || {};
        this.mouse = new IMainSettinsControlsMouse(data.mouse);
        this.keys = data.keys;
        this.contextMenu = data.contextMenu || false;
        this.selectedColor = data.selectedColor ;//|| '#b09e3c';
        this.highlightSelected = data.highlightSelected || false;

    }
}
/**
 * @class data controls mouse structure settings
 */
export class IMainSettinsControlsMouse {
    /**
     *
     *@constructor
     *@param {Object} data - settings for local controls mouse
     *@param {number} [data.autoRotateSpeed = 0.1] - auto rotate speed
     *@param {number} [data.rotateSpeed = 0.1693] - auto rotate speed
     *@param {number} [data.rotateSpeedUP = 0.06493] -   rotate speed on UP
     *@param {boolean} [data.lockRotation = false] -   rotate ability
     *@param {boolean} [data.lockZoom = false] -   zoom ability
     *@param {boolean} [data.lockPan = false] -   pan ability
     *@param {number} [data.minZoom = null] -   zoom limit
     *@param {number} [data.maxZoom = ] -   zoom limit
     * */
    constructor(data) {
        data = data || {};
        this.autoRotateSpeed = data.autoRotateSpeed || 0.1;
        this.rotateSpeed = typeof data.rotateSpeed =='number'?data.rotateSpeed: 0.1693;
        this.rotateSpeedUP = typeof data.rotateSpeedUP =='number'? data.rotateSpeedUP : 0.06493;
        this.lockRotation = typeof data.rotateSpeedUP =='boolean'? data.lockRotation:false;
        this.lockZoom =  typeof data.lockZoom =='boolean'? data.lockZoom:false;
        this.lockPan = typeof data.lockPan =='boolean'?  data.lockPan:false;
        this.minZoom = typeof data.minZoom =='number'?  data.minZoom:false;
        this.maxZoom = typeof data.minZoom =='number'?  data.minZoom:false;
    }
}


/**
 * @class data local {@link https://threejs.org/docs/index.html#api/lights/Light|lights} structure settings
 */
export class IMainSettinsLocalLights {
    /**
     *
     *@constructor
     *@param {Object} data - settings for local ligts
     *@param {IVector3} [data.position = null] - position XYZ
     *@param {string} [data.color = null] - color, rgb(255,0,0)
     *@param {number} [data.intensity = 0] -  intensity
     *@param {number} [data.angle = 0] -  angle
     *@param {number} [data.distance = 0] -  distance
     *@param {number} [data.decay = 0] -  decay
     *@param {number} [data.penumbra = 0] -  penumbra
     *@param {number} [data.type = 0] -  type
     * */
    constructor(data) {
        data = data || {};
        this.position = new IVector3(data.position);
        this.color = data.color instanceof THREE.Color ? data.color.getStyle() : data.color;
        this.intensity = data.intensity;
        this.angle = data.angle;
        this.distance = data.distance;
        this.decay = data.decay;
        this.penumbra = data.penumbra;
        this.type = data.type;
    }
}
