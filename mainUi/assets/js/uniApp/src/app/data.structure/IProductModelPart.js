/**
 *@class model parts structure
 *
 */
export class IProductModelPart {

    /**
     *
     *@constructor
     *@param {Object} data -  {@link http://54.227.198.145:3009/assets/js/uniApp/src/app/data.structure/product.model.part.json|part} json
     *@param {string} data._id - unique id
     *@param {string} [data.preview = null] - mesh part image preview
     *@param {string} [data.name = null] - mesh part name
     *@param {string} [data.absUrl = null] - if set will load from remote server
     *@param {string} [data.absUrlRS = null] - if set will load from your content_root folder on RS server
     *@param {string} [data.transform = [1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1]] - if set will apply {@link https://threejs.org/docs/index.html#api/math/Matrix4|matrix} translation in webgl area
     *@param {string<IMaterialSettings>} [data.material_settings_id = null] - if set will apply the settings of material for this part
     *@param {string<IProductConfiguration>} [data.product_conf_id = null] -  configuration part settings
     *@param {Array<IPattern>} [data.product_part_patterns = []] - if set will apply all patterns for this part
     *@param {string<IProductModelPart>} [data.product_part_id = null] - if set will try to find the part from parent absUrl
     *@param {string} [data.product_part_name = null] - if set will try find the part in the model product
     *@param {string<IProductModel>} [data.product_model_id = null] - if set will describe the part from product model
     * */
    constructor(data) {
        data = data || {};
        this._id = data._id;
        this.preview = data.preview;
        this.name = data.name;
        this.absUrl = data.absUrl;
        this.absUrlRS = data.absUrlRS;
        this.transform = data.transform || [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1];
        this.product_conf_id = data.product_conf_id;
        this.material_settings_id = data.material_settings_id;
        this.product_part_id = data.product_part_id;
        this.product_part_name = data.product_part_name;
        this.product_part_patterns = data.product_part_patterns || [];
        this.product_model_id = data.product_model_id;
    }
}