import {IRoomQuad}from "./IRoomQuad.js";
import {IRoom}from "./IRoom.js";
import {IMainSettins,IMainSettinsLocalRender,IMainSettinsLocalLights}from "./IMainSettins.js";
import {IMaterial,IMaterialMaps,IMaterialSettings}from "./IMaterial.js";
import {IPattern,IPatternSettings}from "./IPattern.js";
import {IProductModel}from "./IProductModel.js";
import {IProductModelPart}from "./IProductModelPart.js";
import {IProductConfiguration}from "./IProductConfiguration.js";
import {IZipStructure}from "./IZipStructure.js";

export class Interfaces{

}
Interfaces.IProductConfiguration = IProductConfiguration;
Interfaces.IProductModelPart = IProductModelPart;
Interfaces.IProductModel = IProductModel;
Interfaces.IPattern = IPattern;
Interfaces.IMaterial = IMaterial;
Interfaces.IPatternSettings = IPatternSettings;
Interfaces.IMaterialMaps = IMaterialMaps;
Interfaces.IMaterialSettings = IMaterialSettings;
Interfaces.IMainSettinsLocalLights = IMainSettinsLocalLights;
Interfaces.IMainSettinsLocalRender = IMainSettinsLocalRender;
Interfaces.IMainSettins = IMainSettins;
Interfaces.IRoom = IRoom;
Interfaces.IRoomQuad = IRoomQuad;
Interfaces.IZipStructure = IZipStructure;