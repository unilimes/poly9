/**
 * @class pattern structure
 *
 */
export class IPattern {
    /**
     *
     *@constructor
     *@param {Object} opt -  {@link http://54.227.198.145:3009/assets/js/uniApp/src/app/data.structure/pattern.json|pattern} json
     *@param {string} opt._id - unique id
     *@param {string<IProductModelPart>} opt.product_part_id - reference _id
     *@param {string} opt.image - absolute path or database64 for image
     *@param {IPatternSettings} opt.area - describe pattern transformation
     * */
    constructor(opt) {
        let data = opt || {};
        this._id = data._id;
        this.product_part_id = data.product_part_id;
        this.image = data.image;
        this.area = new IPatternSettings(data.area);
    }
}

/**
 * @class pattern settings structure
 *
 */
export class IPatternSettings {
    /**
     *
     *@constructor
     *@param {Object} data.area - describe pattern settings
     *@param {number} [data.embossIntensity=0] - emboss
     *@param {number} [data.angle=0] - angle
     *@param {number} [data.scale=1] - scale
     *@param {number} [data.center_x=0.5] - center by X for image on model part, between 0...1
     *@param {number} [data.center_y=0.5] - center by Y for image on model part, between 0...1
     * */
    constructor(data) {
        let _d = data || {};
        this.embossIntensity = _d.embossIntensity||0;
        this.angle = _d.angle ||0;
        this.scale = _d.scale||1;
        this.center_x = _d.center_x||0.5;
        this.center_y = _d.center_y||0.5;
    }
}