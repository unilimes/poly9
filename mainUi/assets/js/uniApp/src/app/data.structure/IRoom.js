import {IRoomQuad} from "./IRoomQuad.js"

/**
 * @class room structure,
 * please view {@link https://www.migenius.com/category/tutorials|link} to se RS configs, data types, command
 *
 */
export class IRoom {
    /**
     *
     *@constructor
     *
     *@param {Object} data -  {@link http://54.227.198.145:3009/assets/js/uniApp/src/app/data.structure/room.json|room} json
     *@param {string} data._id - unique id
     *@param {Object} [data.modelUrl=null] - pathe to model room, we actualy do not use it now, but who knows maybe Suhail will want to com back to the version when we could load our product model in the room
     *@param {string} data.modelUrl.absUrlRS - relative path from your content_root folder on RS server
     *@param {string} data.preview - image preview for room
     *@param {Object} data.camera - camera transformation for RS
     *@param {Object} data.camera.m_world_to_obj - {@link https://www.migenius.com/articles/3d-transformations-part-2-srt|transformation} on RS
     *@param {Object} data.camera.origin - camera position in webgl namespace, to calculate model rotation view
     *@param {number} [data.camera.origin.x=0] - x position
     *@param {number} [data.camera.origin.y=0] - y position
     *@param {number} [data.camera.origin.z=0] - z position
     *@param {number} [data.camera.m_focal = 0] - camera view focal
     *@param {number} [data.camera.m_aperture = 0] - camera view aperture
     *@param {number} [data.camera.far = 10000] - camera view far
     *
     *@param {Array<IRoomQuad>} data.preset - locations for product in the room
     *
     * */
    constructor(data) {
        if (data) {
            this._id = data._id;
            this.modelUrl = data.modelUrl;
            this.preview = data.preview;
            this.camera = data.camera;
            this.preset = data.preset;
            this.preset = data.preset.map((quad)=> {
                return new IRoomQuad(quad)
            });
        }
    }
}
