
/**
 *@param {string} data._id - unique _id
 *@param {Object} data.quad - position for pointer in room preview image, between 0..1
 *@param {number} data.quad.x - x position
 *@param {number} data.quad.y - y position
 *@param {Object} data.position  - position for model in RS
 *@param {number} data.position.x  - x
 *@param {number} data.position.y  - y
 *@param {number} data.position.z  - z
 *
 *
 * */
export class IRoomQuad {
    constructor(data) {
        if (data) {
            this._id = data._id;
            this.quad = data.quad;
            this.position = data.position;
        }
    }
}