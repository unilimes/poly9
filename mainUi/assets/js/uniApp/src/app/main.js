import {UniApp} from "./entities/UniApp.js";

/**
 * export main functionality in global namespace
 */
window.UniApp  = UniApp;
function GEL_PRODUCT_OB() {
    if (!APP) {
        return "error?message=no namespace defined";
    } else {
        return  APP.uploadModel();
    }
}
window.GEL_PRODUCT_OB = GEL_PRODUCT_OB;