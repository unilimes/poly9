import {Utils} from '../../utils.js';
import {ModelCustomImage} from '../viewer/ModelCustomImage.js';
/*
 * Decor - dom controls for decorations
 * @targetCanvas - result view
 * @_parent - target component
 * @curEditDecor - target texture editor for model with cur decor
 * */

export class Decor {

    constructor(app) {

        this.app = app;
        this.MODES = {IMAGE: 1, TEXT: 2};
        this.curEditDecor = this.inputText = this.inputedText = this.img = null;
        this.id = Date.now();
        this.decorSet = document.createElement('div');

        this.textColor = '#000000';
        this.textpadding = 12;
        this.textOutColor = '#ffffff';
        this.textOutStroke = this.textRotate = 0;
        this.fontSize = 86;
        this.fileReader = new FileReader();
        /*  this.decorSet.setAttribute("draggable", true);
         this.decorSet.className = 'decor-set panel-body custom-modal-body';
         this.decorSet.ondrop = (e)=> {
         this.ondragDropFile(e);
         };
         this.decorSet.ondragstart = (e)=> {
         return false;
         };
         this.decorSet.ondragover = (e)=> {
         this.ondragDropFile(e);
         };
         this.decorSet.innerHTML +=
         '<canvas></canvas>' +
         '<form class="form-horizontal">' +
         '   <div class="form-group">' +
         '       <label class="col-sm-2 control-label ">Text:</label>' +
         '       <div class="col-sm-10">' +
         '           <input type="text" class="form-control"   data-action="text-decor">' +
         '       </div>' +
         '   </div>' +
         '   <div class="form-group">' +
         '       <label class="control-label col-sm-2">Text color:</label>' +
         '       <div class="col-sm-10">' +
         '           <input type="color"  class="form-control"  data-action="text-color" value="' + this.textColor + '" >' +
         '       </div>' +
         '   </div>' +
         '   <div class="form-group">' +
         '       <label class="control-label col-sm-2">Font size:</label>' +
         '       <div class="col-sm-10">' +
         '           <select   class="form-control" data-action="font-size" value="' + this.fontSize + '" >' +
         '           </select>' +
         '       </div>' +
         '   </div>' +
         '   <div class="form-group">' +
         '<ul class="list-group">' +
         '   <li class="list-group-item">' +
         '       Tiling' +
         '       <div class="material-switch pull-right">' +
         '           <input id="someSwitchOptionDefault" data-action="tiling" type="checkbox"/>' +
         '           <label for="someSwitchOptionDefault" class="label-info"></label>' +
         '       </div>' +
         '   </li>' +
         '   <li class="list-group-item repeat-s">' +
         '       <div class="form-group">' +
         '           <label class="control-label col-sm-2">RepeatX:</label>' +
         '           <div class="range range-primary col-sm-10" >' +
         '               <input type="range" name="rangeRepeatX" min="1" max="10" value="1" data-action="repeatX" onchange="rangePrimaryRepeatX.value=value">' +
         '               <output id="rangePrimaryRepeatX">1</output>' +
         '           </div>' +
         '        </div>' +
         '   </li>' +
         '   <li class="list-group-item repeat-s">' +
         '       <div class="form-group">' +
         '           <label class="control-label col-sm-2">RepeatY:</label>' +
         '           <div class="range range-primary col-sm-10" >' +
         '               <input type="range" name="rangeRepeatY" min="1" max="10" value="1" data-action="repeatY" onchange="rangePrimaryRepeatY.value=value">' +
         '               <output id="rangePrimaryRepeatY">1</output>' +
         '           </div>' +
         '        </div>' +
         '   </li>' +
         '</ul>' +
         '    </div>' +
         '    <div class="fileinput fileinput-new" data-provides="fileinput">' +
         '       <input type="file" name="Choose file" value="Choose file" accept="image/!*"  />' +
         '       <button data-btn-type="removeImg" >Remove Img</button>' +
         '    </div>' +
         '    <button data-btn-type="add"  >Create</button>' +
         '    <button  data-btn-type="edit" >Save</button>' +
         '    <button   data-btn-type="new">New</button>' +
         '   </div>' +
         '</form>' +
         ' <div class="decor-list" ></div>';

         [].forEach.call(this.decorSet.querySelectorAll('input,select'), (node)=> {
         let handler = (node.addEventListener || node.attachEvent).bind(node);
         if (node.getAttribute('type') == 'file') {
         this.fileUploader = node;
         handler(Utils.Config.EVENTS_NAME.CHANGE, (e)=> {
         this.uploadImg(e)
         });
         } else {
         let attr = node.getAttribute('data-action');
         switch (attr) {
         case 'text-decor':
         {
         handler('blur', (e)=> {
         this.textHasChange(e, node.value);
         });
         break;
         }
         case 'repeatX':
         case 'repeatY':
         {
         this[attr] = node;
         break;
         }
         case 'tiling':
         {
         this.tiling = node;
         handler('click', (e)=> {
         [].forEach.call(this.decorSet.querySelectorAll('.list-group-item.repeat-s'), (el)=> {
         el.style.display = node.checked ? 'block' : '';
         });
         });
         break;
         }
         case 'text-color':
         {
         handler('change', (e)=> {
         this.textColor = node.value;
         this.textHasChange(e);
         });
         break;
         }
         case 'font-size':
         {

         for (let i = 8; i < 42; i += 2) {
         node.innerHTML += '<option value="' + i + '">' + i + '</option>';
         }
         handler('change', (e)=> {
         this.fontSize = node.value;
         this.textHasChange(e);
         });
         break;
         }
         }

         }
         });
         (this.decorSet.addEventListener || this.decorSet.attachEvent).bind(this.decorSet)(Utils.Config.EVENTS_NAME.CLICK, (e)=> {
         let act = e.target.getAttribute("data-btn-type");
         if (this[act])this[act](e);

         });
         this.targetCanvas = this.decorSet.querySelector('canvas');
         this.buttons = this.decorSet.querySelectorAll('button');
         this.decorList = this.decorSet.lastChild;
         let decor = this.decor = document.createElement('div');
         decor.className = 'decor';
         decor.appendChild(this.decorSet);

         this.container = document.getElementById('addpattern');*/
        if (!this.container) {


            let parent = this.container = document.createElement('div');
            this.container.className = "modal fade";
            document.body.appendChild(this.container);
            this.container.innerHTML =
                //'<div class="customBackground"   data-action="close"></div>' +
                //'<div class="customModal" >' +
                //'   <button type="button" class="close" data-dismiss="alert" data-action="close" aria-label="Close" >' +
                //'       <span aria-r-hidden="true" data-action="close" >&times;</span>' +
                //'   </button>' +
                //'   <div class="modal-data" >' +
                //'       <h5>Add Pattern</h5>' +
                //'       <div class="list-of-scene"></div>' +
                //'   </div>' +
                //'</div>';

                '<div class="modal-dialog modal-lg webgl-view-el">' +
                '   <div class="modal-content">' +
                '       <div class="modal-header">' +
                '           <button type="button" class="close" data-dismiss="modal">&times;</button>' +
                '           <h5 class="modal-title">Add Pattern</h5>' +
                '       </div>' +
                '   <div class="modal-body">' +
                '   <div class="form-group">' +
                '       <label class="radio-inline">' +
                '           <input type="radio" id="imageUpload" target="displayImage" mode="' + this.MODES.IMAGE + '" name="decorSettings" class="styled" checked>' +
                '       Upload Image </label>' +
                '       <label class="radio-inline">' +
                '           <input type="radio" id="textUpload" target="displayText"   mode="' + this.MODES.TEXT + '"name="decorSettings" class="styled">' +
                '       Add Text </label>' +
                '   </div>' +
                '   <hr>' +
                '   <div class="form-group region-style popuptoken" id="displayImage">' +
                '       <div class="panel-body padd2">' +
                '           <div class="col-one">' +
                '               <div class="selectimg float">' +
                '                   <form  action="/"  class="dropzone patternmodal" id="dropzone_single2"></form>' +
                '               </div>' +
                '           </div>' +
                '           <div class="row">' +
                '               <div class="col-sm-12 col-md-12 col-sm-12">' +
                '                   <div class="text-right"></div>' +
                '               </div>' +
                '           </div>' +
                '       </div>' +
                '   </div>' +

                '   <div class="form-group region-style popuptoken textModal" id="displayText" style="display:none;">' +
                '       <div class="panel-body padd2">' +
                '           <div class="row">' +
                '               <div class="col-sm-12 col-md-12 col-sm-12">' +
                '                   <div class="form-group">' +
                '                       <canvas class="text-danger-600 view-text" style="width: inherit;height: 40px" height="40""></canvas>' +
                '                   </div>' +
                '               </div>' +
                '           </div>' +
                '       <div class="row">' +
                '       <div class="col-sm-6 col-md-6 col-sm-12">' +
                '           <div class="form-group">' +
                '               <label>Text Content</label>' +
                '               <input type="text" data-action="text-decor" class="form-control" autofocus="true">' +
                '           </div>' +
                '       </div>' +
                '       <div class="col-sm-6 col-md-6 col-sm-12">' +
                '           <div class="form-group">' +
                '               <label>Font</label>' +
                '               <select name="select" data-action="font-family"  class="form-control"></select>' +
                '           </div>' +
                '       </div>' +
                '   </div>' +
                '   <div class="row">' +
                '       <div class="col-sm-6 col-md-6 col-sm-12">' +
                '           <div class="form-group">' +
                '               <label class="float">Font Color</label>' +
                '               <input type="color"  class="form-control"  data-action="text-color" value="' + this.textColor + '" >' +
                '           </div>' +
                '       </div>' +
                '       <div class="col-sm-6 col-md-6 col-sm-12">' +
                '           <div class="form-group">' +
                '               <label>Font Size</label>' +
                '               <input type="number" data-action="font-size"  class="form-control" min="10" value="' + this.fontSize + '">' +
                '           </div>' +
                '       </div>' +
                '   </div>' +
                '   <div class="row">' +
                '       <div class="col-sm-6 col-md-6 col-sm-12">' +
                '           <div class="form-group">' +
                '               <label class="float">Outlinefarbe</label>' +
                '               <input type="color"  class="form-control"  data-action="text-out-color" value="' + this.textOutColor + '" >' +
                '           </div>' +
                '       </div>' +
                '   <div class="col-sm-6 col-md-6 col-sm-12">' +
                '       <div class="form-group">' +
                '           <label>Outline Strength</label>' +
                '           <input type="number" data-action="text-out-stroke" class="form-control" value="' + this.textOutStroke + '">' +
                '       </div>' +
                '   </div>' +
                '</div>' +
                    //'<div class="row">'+
                    //    '<div class="col-sm-6 col-md-6 col-sm-12">'+
                    //        '<div class="form-group">'+
                    //            '<label>Level Order</label>'+
                    //            '<div class="btn-toolbar">'+
                    //                '<div class="btn-group">'+
                    //                    '<button type="button" class="btn btn-default legitRipple"><i class="icon-arrow-up8"></i></button>'+
                    //                    '<button type="button" class="btn btn-default legitRipple"><i class="icon-arrow-down8"></i></button>'+
                    //            '   </div>'+
                    //            '</div>'+
                    //        '</div>'+
                    //    '</div>'+
                    //    '<div class="col-sm-6 col-md-6 col-sm-12">'+
                    //        '<div class="form-group">'+
                    //         '<label>Fix Text</label>'+
                    //            '<div class="checkbox checkbox-switchery">'+
                    //                '<label>'+
                    //                 '<input type="checkbox" class="switchery" checked="checked">'+
                    //                '</label>'+
                    //            '</div>'+
                    //        '</div>'+
                    //    '</div>'+
                    //'</div>'+
                '<div class="row">' +
                '   <div class="col-sm-6 col-md-6 col-sm-12">' +
                '       <div class="form-group">' +
                '           <label>Rotate Text (Degree)</label>' +
                '           <input type="number" data-action="text-rotate" class="form-control" value="0">' +
                '       </div>' +
                '   </div>' +
                '   <div class="col-sm-6 col-md-6 col-sm-12">' +
                '       <div class="text-right">' +
                '           <label>Text Padding</label>' +
                '           <input type="number" data-action="text-padding" class="form-control" min="0" value="' + this.textpadding + '">' +
                '       </div>' +
                '   </div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<ul class="list-group tilings">' +
                '   <li class="list-group-item">' +
                '       Tiling' +
                '       <div class="material-switch pull-right">' +
                '           <input id="someSwitchOptionDefault" data-action="tiling" type="checkbox"/>' +
                '           <label for="someSwitchOptionDefault" class="label-info"></label>' +
                '       </div>' +
                '   </li>' +
                '   <li class="list-group-item repeat-s">' +
                '       <div class="form-group">' +
                '           <label class="control-label col-sm-2">RepeatX:</label>' +
                '           <div class="range range-primary col-sm-10" >' +
                '               <input type="range" name="rangeRepeatX" min="1" max="10" value="1" data-action="repeatX" onchange="rangePrimaryRepeatX.value=value">' +
                '               <output id="rangePrimaryRepeatX">1</output>' +
                '           </div>' +
                '        </div>' +
                '   </li>' +
                '   <li class="list-group-item repeat-s">' +
                '       <div class="form-group">' +
                '           <label class="control-label col-sm-2">RepeatY:</label>' +
                '           <div class="range range-primary col-sm-10" >' +
                '               <input type="range" name="rangeRepeatY" min="1" max="10" value="1" data-action="repeatY" onchange="rangePrimaryRepeatY.value=value">' +
                '               <output id="rangePrimaryRepeatY">1</output>' +
                '           </div>' +
                '        </div>' +
                '   </li>' +
                '</ul>' +
                '<ul class="list-group bosses">' +
                '   <li class="list-group-item">' +
                '       Emboss/Deboss' +
                '       <div class="material-switch pull-right">' +
                '           <input id="someSwitchEmboss" data-action="emboss" type="checkbox"/>' +
                '           <label for="someSwitchEmboss" class="label-info"></label>' +
                '       </div>' +
                '   </li>' +
                '   <li class="list-group-item repeat-s">' +
                '       <div class="form-group">' +
                '           <label class="control-label col-sm-2">Intensity:</label>' +
                '           <div class="range range-primary col-sm-10" >' +
                '               <input type="range" name="rangeRepeatX" min="-10" max="10" value="0" data-action="bossIntensity" onchange="rangePrimaryEmboss.value=value">' +
                '               <output id="rangePrimaryEmboss">0</output>' +
                '           </div>' +
                '        </div>' +
                '   </li>' +
                '</ul>' +

                '<button type="submit" class="btn btn-primary legitRipple">Add</button>' +
                '</div>' +
                '</div>' +
                '</div>'
            ;

            this.targetCanvas = this.container.querySelector('canvas');
            this.MODES.CURRENT = this.MODES.IMAGE;
            this.prevType = this.container.querySelector('#displayImage');
            jQuery('input[name="decorSettings"]').on('change', (e)=> {
                if (this.prevType)this.prevType.style.display = 'none';
                this.prevType = this.container.querySelector("#" + e.target.getAttribute('target'));
                this.MODES.CURRENT = e.target.getAttribute('mode');
                this.prevType.style.display = 'block';
            });
            this.container.querySelector('button[type="submit"]').addEventListener('click', (e)=> {
                this.add(e);
            });
            [].forEach.call(this.container.querySelectorAll('input,select'), (node)=> {
                let handler = (node.addEventListener || node.attachEvent).bind(node);
                if (node.getAttribute('type') == 'file') {
                    this.fileUploader = node;
                    handler(Utils.Config.EVENTS_NAME.CHANGE, (e)=> {
                        this.uploadImg(e)
                    });
                } else {
                    let attr = node.getAttribute('data-action');
                    switch (attr) {
                        case 'text-decor':
                        {
                            handler('input', (e)=> {
                                this.textHasChange(e, node.value);
                            });
                            break;
                        }
                        case 'bossIntensity':
                        {
                            this[attr] = node;
                            break;
                        }
                        case 'repeatX':
                        case 'repeatY':
                        {
                            this[attr] = node;
                            break;
                        }
                        case 'emboss':
                        {
                            this.emboss = node;
                            handler('click', (e)=> {
                                [].forEach.call(this.container.querySelectorAll('.bosses .list-group-item.repeat-s'), (el)=> {
                                    el.style.display = node.checked ? 'block' : '';
                                });
                            });
                            break;
                        }
                        case 'tiling':
                        {
                            this.tiling = node;
                            handler('click', (e)=> {
                                [].forEach.call(this.container.querySelectorAll('.tilings .list-group-item.repeat-s'), (el)=> {
                                    el.style.display = node.checked ? 'block' : '';
                                });
                            });
                            break;
                        }
                        case 'text-rotate':
                        {

                            handler('input', (e)=> {
                                this.textRotate = +node.value;
                                this.controller.wrapperEl.style.transform = "rotate(" + node.value + "deg)";
                            });
                            break;
                        }
                        case 'text-color':
                        {
                            handler('change', (e)=> {
                                this.textColor = node.value;
                                this.textHasChange(e);
                            });
                            break;
                        }
                        case 'text-padding':
                        {
                            handler('input', (e)=> {
                                this.textpadding = +node.value;
                                this.textHasChange(e);
                            });
                            break;
                        }
                        case 'text-out-color':
                        {
                            handler('change', (e)=> {
                                this.textOutColor = node.value;
                                this.textHasChange(e);
                            });
                            break;
                        }
                        case 'text-out-stroke':
                        {
                            handler('input', (e)=> {
                                this.textOutStroke = node.value;
                                this.textHasChange(e);
                            });
                            break;
                        }
                        case 'font-family':
                        {
                            let fonts = [
                                'Roboto',
                                'monospace',
                                "arial",
                                "helvetica",
                                "myriad pro",
                                "delicious",
                                "verdana",
                                "georgia",
                                "courier",
                                "comic sans ms",
                                "impact",
                                "monaco",
                                "optima",
                                "hoefler text",
                                "plaster",
                                "engagement",

                            ];


                            for (let i = 0; i < fonts.length; i++) {
                                node.innerHTML += '<option value="' + i + '">' + fonts[i] + '</option>';
                            }
                            handler('change', (e)=> {
                                this.fontFamily = fonts[node.value];
                                this.textHasChange(e);
                            });
                            break;
                        }
                        case 'font-size':
                        {

                            handler('input', (e)=> {
                                this.fontSize = node.value;
                                this.textHasChange(e);
                            });
                            break;
                        }
                    }

                }
            });

        }
        //parent.querySelector('.list-of-scene').appendChild(decor);
        this.container.setAttribute('data-dismiss', "modal");
        this.container.addEventListener('click', (e)=> {
            if (e.target.getAttribute('data-action') == 'close' || e.target.getAttribute('data-dismiss') == "modal")this.toggleModal();
        });

        this.controller = new fabric.Canvas(this.targetCanvas);
        this.controller.on("mouse:up", (e)=> {
            this.upload();
        });
        var myDropzone = this.myDropzone = new Dropzone(this.container.querySelector("form.dropzone"), {
            autoProcessQueue: false,
            acceptedFiles: 'image/*',
            dictDefaultMessage: 'Drop file to upload <span>or CLICK</span>',
            maxFiles: 1
        });
        myDropzone.on("addedfile", function (file) {
            if (this.fileTracker) {
                this.removeFile(this.fileTracker);
            }
            this.fileTracker = file;
            var reader = new FileReader();
            reader.onload = function (e) {
                myDropzone._image = e.target.result;
            };
            reader.readAsDataURL(file);
        });

    }

    onWindowResize() {
        //let _w = this.decor.parentNode.clientWidth;
        //if (!_w)return;
        //this.controller.setWidth(_w * 0.9);
        //this.controller.setHeight(this.decor.parentNode.clientHeight);
        //this.decorSet.style.height = 'inherit';
    }

    upload() {
        if (!this.img && !this.inputedText && !this.curEditDecor)this.fileUploader.click();
    }

    updateTemplate() {
        let list = this.app.decorations;
        if (list) {
            this.decorList.innerHTML = "";
            for (let i = 0; i < list.length; i++) {
                let
                    cur = list[i],
                    main = document.createElement('div');

                main.innerHTML +=
                    '<button type="button" class="btn btn-primary heading-btn decor-drop" data-action="drop"  ><i data-action="drop"  class="fa fa-remove"></i></button>' +
                    '<label class="image-checkbox prod-list">' +
                    '   <div class="thumbnail border-width ">' +
                    '       <div class="thumb"> <img src="' + cur.options.preview.src + '"> </div>' +
                    '       <div class="text-center">' +
                    '           <h6 class="no-margin">Leonardo Di Baggio</h6>' +
                    '       </div>' +
                    '   </div>' +
                    '   <input type="checkbox" name="image[]" value="">' +
                    '   <i class="fa fa-check hidden"></i>' +
                    '</label>';

                let parDiv = main,
                    button = parDiv.querySelector('button[data-action="drop"]');

                parDiv.className += 'col-md-3 col-sm-3 col-xs-3 padd2 ' + (cur.material ? '' : 'without-mat');
                parDiv.setAttribute('draggable', true);
                parDiv.setAttribute('pattern', 1);
                parDiv.ondragstart = (e)=> {
                    this.dragStart(e, cur);
                };
                parDiv.ondragend = (e)=> {
                    this.ondragend(e);
                };
                parDiv.addEventListener('click', (e)=> {
                    if (e.target.getAttribute('data-action') == 'drop') {
                        this.dropItem(cur);
                    } else {
                        this.select(cur);
                        this.toggleModal();
                    }
                });
                this.decorList.appendChild(parDiv);

            }
        }

    }

    toggleModal() {
        let hasCl = this.container.className.indexOf('in') > -1;
        if (hasCl) {
            this.container.className = this.container.className.replace(' in', "");

        } else {
            this.container.className += ' in';
            this.onWindowResize();
        }
        this.container.style.display = hasCl ? '' : 'block';
    }

    ondragDropFile(e) {
        e.preventDefault();
        let dt = e.dataTransfer;
        if (dt.items) {
            for (let i = 0; i < dt.items.length; i++) {
                if (dt.items[i].kind == "file") {
                    return this.parseUploadFiles([dt.items[i].getAsFile()]);
                }
            }
        }

    }

    dragStart(event, decor) {
        this.app._events.curDrag = {pattern:decor};
    }

    ondragend(event, decor) {
        this.app._events.curDrag = null;
        setTimeout(()=> {
            this.updateTemplate();
        }, 100);
    }

    textHasChange(e, txt) {
        let text = txt || (this.inputedText ? this.inputedText.text : null),
            options = {
                left: 0,
                top: 0,
                stroke: this.textOutColor,
                strokeWidth: this.textOutStroke,
                fill: this.textColor,
                fontSize: this.fontSize
            };
        if (this.fontFamily)options.fontFamily = this.fontFamily;
        if (this.inputedText) {
            ['left', 'top', 'width', 'height', 'scaleX', 'scaleY'].forEach((field)=> {
                options[field] = this.inputedText[field];
            });
        }
        this.inputedText = text ? new fabric.Text(text, options) : null;
        this.updateCanvas();
    }

    removeImg() {
        this.img = null;
        this.updateCanvas();
    }

    uploadImg(ev) {
        this.parseUploadFiles(ev.target.files);
    }

    parseUploadFiles(files) {
        if (files && files.length) {
            let file = files[0],
                item = 0,
                self = this;
            if (!file)return;
            if (!file.type.match('image'))return alert('please, upload only image');

            (function parseFiles() {
                if (item >= files.length)return;
                self.fileReader.onload = (e)=> {


                    //let sizeX = this.controller.getWidth(),
                    //    sizeY = this.controller.getHeight(),
                    //    _loadedImg = this.img = new Image();
                    let div = document.createElement('div');
                    div.className = 'dz-preview dz-processing dz-image-preview dz-success dz-complete';
                    div.innerHTML = '<div class="dz-image"><img data-dz-thumbnail="" alt="" ></div>' +
                        '<div class="dz-details">    <div class="dz-size"></div>    <div class="dz-filename"><span data-dz-name=""></span></div>  </div>';
                    let img = div.querySelector('img');
                    img.onload = ()=> {
                        //let parAsp = sizeX / sizeY,
                        //    canAsp = _loadedImg.width / _loadedImg.height,
                        //    imdWidth = parAsp > canAsp ? (canAsp > 1 ? sizeX : sizeY * canAsp) : (canAsp > 1 ? sizeX : _loadedImg.height * parAsp),
                        //    imdHeight = parAsp > canAsp ? (canAsp > 1 ? _loadedImg.width * sizeY / sizeX : imdWidth * _loadedImg.height / _loadedImg.width) : imdWidth * _loadedImg.height / _loadedImg.width;
                        //this.img = new fabric.Image(_loadedImg, {
                        //    left: sizeX / 2 - imdWidth / 2,
                        //    top: sizeY / 2 - imdHeight / 2,
                        //    width: imdWidth,
                        //    height: imdHeight,
                        //});
                        //this.updateCanvas();
                        parseFiles();
                    }
                    //this.img.src = e.target.result;
                    img.src = e.target.result;
                    img.setAttribute('alt', file.name);
                    div.querySelector('spa[data-dz-name]').innerText = file.name;
                }
                self.fileReader.readAsDataURL(files[item++]);
            })();

        }
    }

    updateCanvas() {
        this.controller.clear();
        if (this.img) {
            //this.controller.setWidth(this.decorSet.clientWidth);
            //this.controller.setHeight(this.decorSet.clientWidth * this.img.height / this.img.width);
            this.controller.add(this.img);
        }
        if (this.inputedText) {
            let padding = this.textpadding,
                _x = this.inputedText.width + 2 * padding,
                _y = this.inputedText.height + 2 * padding,
                square = new fabric.Rect({
                    width: _x,
                    height: _y,
                    fill: '#000000'
                });
            //this.controller.add(square);

            this.inputedText.left = this.inputedText.top = padding;
            this.controller.add(this.inputedText);
            this.controller.setWidth(_x);
            this.controller.setHeight(_y);

        }
        this.updateButtons();
        //if (!this.curEditDecor)this.add();
    }

    updateButtons() {
        /*for (let i = 0; i < this.buttons.length; i++) {
         this.buttons[i].className = '';
         if (this.curEditDecor) {
         if (['edit', 'new'].indexOf(this.buttons[i].getAttribute('data-btn-type')) > -1) {
         this.buttons[i].className = 'active';
         }
         } else {
         //if ((this.img || this.inputedText) && this.buttons[i].getAttribute('data-btn-type') == 'add')this.buttons[i].className = 'active';
         }
         if (this.img && this.buttons[i].getAttribute('data-btn-type') == 'removeImg')this.buttons[i].className = 'active';
         }*/
    }

    createDecor(imageData) {
        let self = this;
        return new DecorData(imageData, JSON.stringify(self.controller.toDatalessJSON()), (a)=> {
            //a.isTiling = this.tiling.checked;
            a.repeatX = self.repeatX.value;
            a.repeatY = self.repeatY.value;
            a.angle = self.textRotate;
            if (this.emboss.checked)a.embossIntensity = parseFloat(self.bossIntensity.value);
            let decor = self.curEditDecor = new ModelCustomImage(self.app, a);
            self.app.decorations.push(decor);
            self.updateTemplate();
            self.updateButtons();
        });
    }

    add(e) {
        let imageData, self = this;
        if (this.MODES.CURRENT == this.MODES.IMAGE) {
            if (this.myDropzone.files.length) {
                imageData = this.myDropzone._image;
                //this.myDropzone.removeAllFiles(true);
            } else {
                return alert('Upload some image');
            }
        } else {
            if (!this.inputedText) {
                return alert('Create some text');
            } else {
                imageData = this.controller.toDataURL("image/png");
            }
        }

        if (this.tiling.checked) {

            Utils.Config.getImage(imageData, (img)=> {
                let canvas = document.createElement('canvas'),
                    repeatX = self.repeatX.value,
                    repeatY = self.repeatY.value
                    ;
                canvas.width = img.width * repeatX;
                canvas.height = img.height * repeatY;
                let cntx = canvas.getContext('2d');
                for (let startX = 0; startX < repeatX; startX++) {
                    for (let startY = 0; startY < repeatY; startY++) {
                        cntx.drawImage(img, startX * img.width, startY * img.height);
                    }
                }
                self.createDecor(canvas.toDataURL());
            });
        } else {
            this.createDecor(imageData);
        }

        if (e)e.preventDefault();
    }

    edit(e) {
        this.curEditDecor.options.update(this.controller.toDataURL("image/png"), JSON.stringify(this.controller.toDatalessJSON()), ()=> {
            this.curEditDecor.setTiling({
                isTiling: this.tiling.checked,
                repeatY: this.repeatY.value,
                repeatX: this.repeatX.value
            });
            this.curEditDecor.updateTextures();
            this.updateTemplate();
            this.updateButtons();
        });
        if (e)e.preventDefault();
    }

    select(decor) {
        this.curEditDecor = decor;
        this.controller.clear();
        if (decor.options.jsonData) {
            this.controller.loadFromJSON(decor.options.jsonData, this.controller.renderAll.bind(this.controller));
            setTimeout(()=> {
                this.img = this.inputedText = null;
                for (let i = 0, list = this.controller._objects; i < list.length; i++) {
                    if (list[i].type == 'image') {
                        this.img = list[i];
                    } else {
                        this.inputedText = list[i];
                    }
                }
                this.updateCanvas();
                if (decor.options.isTiling) {
                    this.tiling.setAttribute('checked', true);
                } else {
                    this.tiling.removeAttribute('checked');
                }
            }, 200);

        }
    }

    dropItem(decor) {
        decor.destroy();
    }

    clearData(decor) {
        if (this.curEditDecor && this.curEditDecor.id == decor.id) {
            this.controller.clear();
            this.img = this.inputedText = this.curEditDecor = null;
        }
        this.updateTemplate();
        this.updateButtons();
    }

    new(e) {
        this.controller.clear();
        this.curEditDecor = this.img = this.inputedText = null;
        this.updateButtons();
        if (e)e.preventDefault();
    }
}
export class DecorData {

    constructor(prev, jsn, onFinish = ()=> {
    }, material = null) {
        this.update(prev, jsn, onFinish, material);
        if (prev instanceof Image)this.preview = prev;
    }

    update(src = null, jsData = null, onFinish = ()=> {
    }, material = null) {
        if (jsData)this.jsonData = jsData;
        if (material)this.material = material;
        if (src) {
            Utils.Config.getImage(src, (img)=> {
                this.preview = img;
                onFinish(this);
            });
        }
    }
}