import {GlViewer} from "../viewer/glViewer.js";
import {Utils} from "../../utils.js";
import {MHttp} from "../helpers/Http.js";
import {Command} from "../helpers/Command.js";
import {PreSet} from "../preset/preset.js";
import {Costumize} from "../Costumize.js";
import {Configuration,ProductPart,ProductPattern} from "../Configuration.js";
import {Interfaces} from "../../data.structure/interfaces.js"
import {ModelCustomImage} from "../viewer/ModelCustomImage.js"

/**
 *@class
 * --------WARNING  ---- this class only for TEST, in your app you will have your data on backend---------*/
export class TESTProductAction {
    constructor(app) {
        this.app = app;
        this.data = [];
        this.lastSelected = -1;
        this.configurations = [];
        this.SERVER_URL = /*'http://localhost:3009/';//*/Utils.Config.REMOTE_DATA;
        this.IMG_UPLOADS = this.SERVER_URL + "uploads/images/";
        this.elems = {
            save: document.createElement('button'),
            list: document.querySelector('.PROD_LIST_CONFS')
        }
        if (!this.elems.list) {
            this.elems.list = document.createElement('div');
            this.elems.list.className = 'row PROD_LIST_CONFS';
            document.body.appendChild(this.elems.list);
        }
        this.elems.save.className = "btn border-slate text-slate-800 btn-flat whiteback scren-shot-btn";
        this.elems.save.style.bottom = "190px";
        this.elems.save.innerText = "Save Configuration";
        this.elems.save.addEventListener('click', (e)=> {
            this.elems.save.style.display = 'none';
            this.saveCnfg({
                next: ()=> {
                    this.elems.save.style.display = '';
                    console.log("configuration was saved");
                }
            });
        });
        this.elems.list.onclick = ((e)=> {
            e.preventDefault();
            let curItem = e.target,
                confItem,
                action = curItem.getAttribute('data-action');
            if (curItem == e.currentTarget)return;
            if (action) {
                var _s = action.split("-");
                switch (_s[0]) {
                    case"drop":
                    {
                        try {
                            this.dropCnfg({
                                index: _s[1],
                                next: ()=> {
                                    console.log("configuration was droped");
                                }
                            });
                        } catch (e) {
                            console.log(e);
                        }

                        break;
                    }
                }
            } else {
                while (curItem && !(confItem = curItem.getAttribute('data-item'))) {
                    curItem = curItem.parentNode;
                }
                if (confItem) {
                    try {
                        this.selectCnf({
                            index: confItem - 1,
                            next: ()=> {
                                console.log("configuration was selected");
                            }
                        });
                    } catch (e) {
                        console.log(e);
                    }

                }
            }
        });
    }

    fillDBWithTestData() {
        let matList = [],
            self = this.app,
            prodList = [{
                model: {
                    url: 'http://54.227.198.145:3009/models/my_models/cups/barware_s11624/barware_s11624.obj',
                    name: 'barware_s11624'
                }
            }],
            partsList = [],
            serverUrl = this.SERVER_URL;
        for (let i = 0; i < self.materials.length; i++) {
            let curMat = self.materials[i],
                _mat = {
                    maps: {},
                    settings: {},
                    name: curMat.name,
                    preview: curMat.preview,
                };
            for (let d = 0; d < self.MAPS.length; d++) {
                _mat.maps[self.MAPS[d]] = curMat[self.MAPS[d]];
            }
            matList.push(_mat);
        }
        self.http.post(serverUrl + 'api/product/fillWithTestProduct', {list: prodList}, (resp)=> {
            let res = resp.json();
            if (res.status) {
                for (let i = 0; i < self.parts.length; i++) {
                    for (let d = 0, productId = res.data[i]._id; d < self.parts[i].parts.length; d++) {
                        let _curpart = self.parts[i].parts[d],
                            matrix = new THREE.Matrix4();
                        matrix.setPosition(_curpart.params.position);
                        matrix.scale(_curpart.params.scale);
                        matrix.makeRotationFromQuaternion(new THREE.Quaternion(
                            THREE.Math.degToRad(_curpart.params.rotation.x),
                            THREE.Math.degToRad(_curpart.params.rotation.y),
                            THREE.Math.degToRad(_curpart.params.rotation.z)
                        ));
                        partsList.push({
                            product_id: productId,
                            model: {
                                url: Utils.Config.REMOTE_DATA + Utils.Config.MODELS.URL.CUPS_DECOR + 514 + Utils.Config.DELIMETER + _curpart.name,
                                name: _curpart.name
                            },
                            transform: matrix.elements
                        });
                    }
                }
                self.http.post(serverUrl + 'api/parts/fillWithTestParts', {list: partsList}, (partResp)=> {
                    let _partResp = partResp.json();
                    if (_partResp.status) {
                        self.http.post(serverUrl + 'api/material/fillWithTestMaterials', {list: matList}, (matResp)=> {
                            let _matResp = matResp.json();
                            if (_matResp.status) {
                                console.log('Test data was saved');
                            } else {
                                console.log(matResp);
                            }
                        });
                    } else {
                        console.log(_partResp);
                    }

                });
            } else {
                console.log(res.message);
            }
        });
    }

    /**
     * load all configuratioins for product with model name = barware_s11624
     *
     * */
    loadProductData(opt) {
        let self = this.app,
            resp = this.data = [],
            PREF = this.IMG_UPLOADS;
        //return this.getConfiguratios();
        self.http.get(this.SERVER_URL + "api/product/barware_s11624", {}, (prodRes)=> {
            resp.push(prodRes.json());
            self.http.get(this.SERVER_URL + "api/parts/" + resp[0].data[0]._id, {}, (partsRes)=> {
                resp.push(partsRes.json());
                self.http.get(this.SERVER_URL + "api/material/", {}, (matRes)=> {
                    resp.push(matRes.json());
                    this.loadCnfg({
                        next: (d)=> {
                            resp.push(d);
                            if (opt.next)opt.next(resp);
                        },
                        prodModel: resp[0].data[0]
                    });
                });
            });

        });
    }

    /**
     * load configurations of product model
     * @param {Object} opt - opions
     * @param {IProductModel} opt.prodModel - your product model
     * @param {Function} opt.next - on finish load configurations
     *
     * */
    loadCnfg(opt) {

        if (!opt || !opt.prodModel)return console.error('missing data');
        let
            PREF = this.IMG_UPLOADS;

        this.app.http.get(this.SERVER_URL + "api/product_configuration/" + opt.prodModel._id, {}, (confRes)=> {
            let conf = confRes.json();
            if (conf.status) {
                this.configurations = conf.data.map((el)=> {
                    el.preview = PREF + el.preview;
                    return new Interfaces.IProductConfiguration(el);
                });
                this.updateHTML();
            } else {
                console.error(conf);
            }


            if (opt.next)opt.next(conf);
        });
    }


    /**
     * save configuration for current view
     *@param {Object} opt - settings
     *@param {IProductModel} [opt.prodModel = null] - your product model, by default will be use the product model wich you have loaded
     *@param {Function} opt.next - on finsh save cofiguration
     * */
    saveCnfg(opt) {

        let _self = this,
            app = _self.app,
            webGl = app.glViewer,
            conf = new Interfaces.IProductConfiguration({
                //product_model_id: _self.data[0].data[0]._id,
                preview: webGl.gl.domElement.toDataURL()
            }),
            modelsPart = [];

        if (!opt)return console.error("missing data");
        if (!opt.prodModel)opt.prodModel = this.app.productModel;
        if (!opt.prodModel)return console.error("missing  product model ");
        conf.product_model_id = opt.prodModel._id;

        app.http.post(this.SERVER_URL + "api/product_configuration/", conf, (resp)=> {
            let _conf = resp.json();
            if (_conf.status) {
                let _settings = app.extractMainSettings();
                conf._id = conf._isAll = _settings.product_conf_id = _conf.data._id;

                app.http.post(this.SERVER_URL + "api/product_main_settings/", _settings, (resp)=> {
                    let _mainSt = resp.json();
                    if (_mainSt.status) {
                        conf.main_settings = _mainSt.data;
                        webGl.model.traverse((child)=> {
                            if (child.type == Utils.Config.MODELS.TYPES.MESH) {
                                child.updateMatrix();
                                let partModel = new Interfaces.IProductModelPart({
                                    product_conf_id: _conf.data._id,
                                    transform: child.matrix.toArray()
                                });
                                conf.model_parts.push(partModel);

                                if (child.params) {
                                    partModel.product_part_id = child.params._id;
                                    //if (child.params.isReplacement) partModel.data.product_part_change = child.params.modelToChange.userData.originName;
                                }
                                partModel.product_part_name = child.userData.originParent + child.userData.originName;
                                modelsPart.push({
                                    data: partModel,
                                    onFinish: (part, next)=> {
                                        let saveMatSetting = ()=> {
                                            let _set = partModel.mat_settings = new Interfaces.IMaterialSettings(child.material);
                                            delete _set._id;
                                            partModel.mat_settings.product_part_id = partModel._id;
                                            if (child.material._mat && child.material._mat._id) {
                                                partModel.mat_settings.material_id = child.material._mat._id;
                                            }

                                            app.http.post(_self.SERVER_URL + "api/materil_setting/", partModel.mat_settings, (resp)=> {
                                                let _r = resp.json();
                                                if (_r.status) {
                                                    partModel.mat_settings._id = partModel.material_settings_id = _r.data._id;
                                                    if (next)next();
                                                } else {
                                                    console.log(_r);
                                                }
                                            });
                                        }
                                        if (child.params) partModel.product_part_id = child.params;
                                        if (child.material.logos) {
                                            partModel.product_part_patterns = [];
                                            for (let logos = 0; logos < child.material.logos.length; logos++) {
                                                let _l = child.material.logos[logos];
                                                let pattern = _l.getPattern(true);
                                                pattern.product_part_id = partModel._id;
                                                partModel.product_part_patterns.push(pattern);
                                            }
                                            let uploadPatternsC = 0;
                                            (function uploadPatterns() {
                                                let _l = child.material.logos[uploadPatternsC],
                                                    _patternSave = partModel.product_part_patterns[uploadPatternsC++];
                                                if (_patternSave) {
                                                    app.http.post(_self.SERVER_URL + "api/pattern/", _patternSave, (resp)=> {
                                                        let _r = resp.json();
                                                        if (_r.status) {
                                                            _patternSave._id = _r.data._id;
                                                            _patternSave.preview = _l.options.preview;
                                                            uploadPatterns();
                                                        } else {
                                                            console.log(_r);
                                                        }
                                                    });
                                                } else {
                                                    saveMatSetting();
                                                }
                                            })()

                                        } else {
                                            saveMatSetting();
                                        }
                                    }
                                });

                            }
                        });
                        let itemP = 0;
                        (function saveParts() {
                            if (itemP < modelsPart.length) {
                                let curPart = modelsPart[itemP++];
                                app.http.post(_self.SERVER_URL + "api/parts/", curPart.data, (resp)=> {
                                    let _r = resp.json();
                                    if (_r.status) {
                                        curPart.data._id = _r.data._id;
                                        curPart.onFinish(curPart.data, ()=> {
                                            saveParts();
                                        });
                                    } else {
                                        console.log(_r);
                                    }

                                });
                            } else {
                                _self.configurations.push(conf);
                                _self.lastSelected = _self.configurations.length - 1;
                                _self.updateHTML();
                                if (opt.next)opt.next();
                            }
                        })()
                    } else {
                        console.log(_mainSt);
                    }
                });

            } else {
                console.log(_conf);
            }
        });

        this.updateHTML();
    }

    /**
     * drop configuration
     *@param {Object} opt - settings
     *@param {number} opt.index - index in list of configurations, type APP._conf.configurations to see all of them
     *@param {Function} opt.next - on finish drop the configuration
     * */
    dropCnfg(opt) {
        if (!opt)return console.error("missing data");
        if (confirm('Are you sure to drop the configuration?')) {
            let index = opt.index;
            this.app.http.delete(this.SERVER_URL + "api/product_configuration/", {id: this.configurations[index]._id}, (res)=> {
                if (res.json().status) {
                    this.configurations.splice(index, 1);
                    this.updateHTML();
                } else {
                    console.log(res);
                }
                if (opt.next)opt.next();
            });
        } else {
            if (opt.next)opt.next();
        }
    }

    getConfiguratios() {
        this.app.http.get(this.SERVER_URL + "api/product_configuration/59e8697f5bb6391c99d17262", {}, (confRes)=> {
            console.log((confRes.json()));
        });
    }

    updateHTML() {
        this.elems.list.innerHTML = '';
        for (let i = 0; i < this.configurations.length; i++) {
            let _c = this.configurations[i],
                html = document.createElement('div');
            html.className = 'col-lg-3 col-sm-6';
            html.setAttribute('data-item', i + 1);
            html.innerHTML =
                '   <div class="panel something ">' +
                '       <div class="panel-body">' +
                '           <div class="thumb thumb-fixed"> <a> <img src="' + _c.preview + '" alt=""> </a>' +
                '               <div class="heart"><a href="#"><i class="fa fa-heart-o"></i></a><a href="#"><i class="fa fa-heart"></i></a></div>' +
                '           </div>' +
                '           <button data-action="drop-' + i + '">DROP</button>' +
                '       </div>' +
                '       <div class="panel-body panel-body-accent text-center pad15">' +
                '           <div class="row">' +
                '               <div class="col-sm-7">' +
                '                   <h5 class="text-semibold text-left suggest-title"><a href="#" class="text-default">Sleeve T Shirt</a></h5>' +
                '               </div>' +
                '               <div class="col-sm-5">' +
                '                   <h5 class="no-margin text-semibold item-box-price">$25.99</h5>' +
                '               </div>' +
                '           </div>' +
                '       </div>' +
                '       <div class="arrow-right"><span>New</span></div>' +
                '   </div>';
            this.elems.list.appendChild(html);
            _c.html = html;
        }
    }

    /**
     *select configuration from APP._conf.configurations list
     *@param {Object} opt - settings
     *@param {number} opt.index - item index from configurations list
     *@param {number} opt.next - on finish apply all data for configuration
     * */
    selectCnf(opt) {
        if (!opt)return console.error("missing data");
        let index = opt.index,
            selected = this.configurations[index],
            app = this.app,
            _matrix = new THREE.Matrix4(),
            updateView = ()=> {
                app.applyConfiguration({
                    next: ()=> {
                        if (opt.next)opt.next();
                    },
                    conf: selected
                });
                this.lastSelected = index;
            }
        if (this.lastSelected != index && selected) {
            if (selected._isAll) {
                updateView();
            } else {
                app.http.get(this.SERVER_URL + "api/product_configuration/settings/" + selected._id, null, (resp)=> {
                    let _res = resp.json();
                    if (_res.status) {
                        Object.assign(selected, _res.data);
                        selected.preview = this.IMG_UPLOADS + selected.preview;
                        selected._isAll = true;
                        selected.model_parts.forEach((_part)=> {
                            _part.mat_settings = new Interfaces.IMaterialSettings(_part.material_settings_id);
                            _part.product_part_patterns = _part.product_part_patterns.map((_pattern)=> {
                                _pattern.image = this.IMG_UPLOADS + _pattern.image;
                                return new Interfaces.IPattern(_pattern);
                            });
                        });
                        updateView();
                    } else {
                        console.log(_res);
                        if (opt.next)opt.next();
                    }
                })
            }

        }
    }

}