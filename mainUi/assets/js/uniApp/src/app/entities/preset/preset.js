import {GLMain} from"../viewer/glMain.js"
import {Utils} from"../../utils.js"
import {RealityServerService} from"../services/reality.service.js"

/**
 * @class PreSet - vizualization room quad selected positions
 * @image - backgeound, default from options;
 * @optioins - inject options from parent;
 * */

export class PreSet extends GLMain {

    constructor(app) {

        super();
        this.app = app;
        this.COLORS = {
            SELECTED: '#FF0000',
            DEF: '#FFFFFF'
        };
        this.selectedSize = 80;
        this.container = document.createElement('div');
        this.container.className = "webgl-view pop-up-modal";
        document.body.appendChild(this.container);
        if (!this.container)return console.error("the container for rooms list are not defined");

        this.container.innerHTML =
            '<div class="customBackground"   data-action="close"></div>' +
            '<div class="customModal" >' +
            '   <button type="button" class="close" data-dismiss="alert" data-action="close" aria-label="Close" >' +
            '       <span aria-r-hidden="true" data-action="close" >&times;</span>' +
            '   </button>' +
            '   <div  class="room-content r-hidden"  >' +
            '       <div class="modal-data position-select" >' +
            '           <h3><span class="back" data-action="back_to_rooms">back</span><span style="display: none" class="back" data-action="remote_render">, render</span>  Select Position  </h3>' +
            '           <div class="preset"><canvas></canvas></div>' +
            '       </div>' +
            '       <div class="modal-data render-preview r-hidden"  >' +
            '           <h3> Render Preview  <span  data-action="back_to_psts" class="r-hidden">back</span><div class="render-in-progress "  >please wait, image being rendered,</div><div class="render-status r-hidden"  > <div class="render-status-info"></div>%, </div></h3>' +
            '           <button type="button" class="render render-btn r-hidden" data-action="render_save"   >S</button>' +
            '           <div class="render-scene"></div>' +
            '       </div>' +
            '   </div>' +
            '   <div class="modal-data room-select" >' +
            '       <h3> Select a Room </h3>' +
            '       <div class="list-of-scene"></div>' +
            '   </div>' +
            '</div>';
        this.customModal = this.container.querySelector('.customModal');
        this.previewRoom = this.container.querySelector('.preset');
        this.previewTarget = this.previewRoom.querySelector('canvas');
        this.renderTarget = new Image();
        this.renderTarget.className = 'render-target';
        this.renderTarget.crossOrigin = '*';
        this.renderTarget.setAttribute('alt', 'Scene loading, please wait...');
        this.renderTarget.setAttribute('crossOrigin', '*');
        this.container.querySelector('.render-scene').appendChild(this.renderTarget);
        let render_status = this.container.querySelector('.render-status');
        let render_status_info = this.container.querySelector('.render-status-info');
        this.listRooms = this.container.querySelector('.list-of-scene');
        let saveBtn = this.container.querySelector('button[data-action="render_save"]');
        let back_to_psts = this.container.querySelector('span[data-action="back_to_psts"]');
        let renderProgress = this.container.querySelector('.render-in-progress');
        this.elements = {
            remote_render: this.container.querySelector('span[data-action="remote_render"]'),
            room_content: this.container.querySelector('.room-content'),
            position_select: this.container.querySelector('.modal-data.position-select'),
            render_preview: this.container.querySelector('.modal-data.render-preview'),
            room_select: this.container.querySelector('.modal-data.room-select')
        }
        let _self = this;


        this.updateListOfRoomsHTML();
        let saveImgData = this.saveImgData = document.createElement('canvas');
        this.container.addEventListener(Utils.Config.EVENTS_NAME.CLICK, (e)=> {
            let act = e.target.getAttribute('data-action');
            if (act) {
                switch (act) {
                    case "close":
                    {
                        this.toggleModal();
                        break;
                    }
                    case "back_to_rooms":
                    {
                        this.backToRooms();
                        break;
                    }
                    case "back_to_psts":
                    {
                        this.backToPst();
                        break;
                    }

                    case "render_save":
                    {
                        this.saveImg();
                        break;
                    }
                    case "remote_render":
                    {
                        this.render();
                        break;
                    }
                }
            } else if (e.target.getAttribute('data-item')) {
                if (this.selectRoom(app.rooms[e.target.getAttribute('data-item') - 1])) {
                    this.elements.room_select._display();
                    this.elements.position_select._display(true);
                    this.elements.room_content._display(true);
                }
            }
            e.preventDefault();
        });
        this.renderTarget.onStartLoad = function () {
            app.glViewer.preloader.prevParent = app.glViewer.preloader.preloader.parentNode;
            _self.customModal.appendChild(app.glViewer.preloader.preloader);
            app.glViewer.preloader.fade(true);
        }
        this.renderTarget.onprogress = function (val) {
            render_status.style.display = 'block';
            render_status_info.innerText = val;
            if (val > 99) {
                render_status.style.display = 'none';
                app.glViewer.preloader.prevParent.appendChild(app.glViewer.preloader.preloader);
                app.glViewer.preloader.fade();
            }
        }
        this.renderTarget.onload = function () {
            renderProgress.style.display = 'none';
            saveBtn.style.display = 'block';
            back_to_psts.style.display = 'block';

            saveImgData.width = this.width;
            saveImgData.height = this.height;
            let cntx = saveImgData.getContext('2d');
            cntx.drawImage(this, 0, 0, this.width, this.height);
            if (this._onload)this._onload();
        }
        this.renderTarget.onerror = function () {
            back_to_psts.style.display = 'block';
            saveBtn.style.display = 'none';
            console.error("can`t update image");
            if (this._onEror) {
                this._onEror();
            } else {
                app.glViewer.preloader.prevParent.appendChild(app.glViewer.preloader.preloader);
                app.glViewer.preloader.fade();
            }
        }
        this.renderTarget.onloadprogress = function (e) {
        };
        this.ngOnInit();
        this.realityServerService = new RealityServerService(this);
    }

    updateListOfRoomsHTML() {
        this.listRooms.innerHTML = '';
        this.app.rooms.forEach((room, item)=> {
            this.listRooms.innerHTML += '<div class="scene-preview" data-item="' + (item + 1) + '">' +
                '<img src="' + room.preview + '"data-item="' + (item + 1) + '"/>' +
                '</div>';
        });
        this.elements.room_select._display(true);
        this.elements.position_select._display();
        this.elements.room_content._display();
    }

    backToPst() {
        this.resetRoom();
        this.elements.render_preview._display();
        this.elements.position_select._display(true);
    }

    backToRooms() {
        this.resetRoom(true);
        this.elements.position_select._display();
        this.elements.render_preview._display();
        this.elements.room_select._display(true);
        this.callback_storage.forEach((data, key)=> {
            if (data.eventName) {
                data.target.removeEventListener(data.eventName, data.handler);
            }
        });
    }

    saveImg() {
        var link = document.createElement("a");
        link.setAttribute("href", this.getRemoteRenderedImage());
        link.setAttribute("download", "poly9.png");
        link.click();
    }

    /**
     * @return {string} base64 image data
     * */
    getRemoteRenderedImage() {
        return this.saveImgData.toDataURL();
    }

    /**
     *@param {Image}[img =this.renderTarget ] - image where will cantain the resul from RS
     */
    render(img, cnt) {
        let _img = img || this.renderTarget,
            _self = this;
        if (!_img.onload)_img.onload = function () {
            this['_onload']();
            _self.canRender = _self.canSave = true;
        }
        this.realityServerService.renderScene(this.curRoom, {
            onFinish: ()=> {
                this.resetRoom();
            },
            formatRenderImg: img ? "png" : null,
            component: this,
            img: _img
        });
        this.elements.position_select._display();
        this.elements.render_preview._display(true);
    }

    ngOnInit() {
        let _c = this.previewTarget;
        this.callback_storage.push({
            target: window,
            eventName: Utils.Config.EVENTS_NAME.RESIZE,
            handler: ()=> {
                this.setSize();
                this.drawImg();
                this.update();
            }
        }, {
            target: _c,
            eventName: Utils.Config.EVENTS_NAME.MOUSE_MOVE,
            handler: (e)=> {
                this.onMouseMove(e);
            }
        }, {
            target: _c,
            eventName: Utils.Config.EVENTS_NAME.MOUSE_UP,
            handler: (e)=> {
                this.onMouseUp(e);
            }
        });


    }

    ngOnDestroy() {
        this.callback_storage.forEach((data)=> {
            if (data.eventName) {
                data.target.removeEventListener(data.eventName, data.handler);
            }
        });
    }

    toggleModal() {
        if (this.container.className.indexOf('active') > -1) {
            this.container.className = this.container.className.replace(' active', "");
            this.realityServerService.cancelRender();
        } else {
            this.container.className += ' active';
        }
    }

    /**
     * @param {IRoom} room - room selection
     * */
    selectRoom(room) {
        if (!room)return;
        this.app.commands_model = [];
        let image = this.image = new Image(),
            _self = this;

        for (let i = 0, decor = this.app.glViewer.decorations; i < decor.length; i++) {
            if (decor[i].active) {
                decor[i].setActive(false);
            }
        }
        image.onload = function () {
            _self.setSize();
            let cntx = _self.previewTarget.getContext('2d');
            _self.context = cntx;
            _self.drawImg();
            //_self.selectScope(room,()=>{
            _self.callback_storage.forEach((data, key)=> {
                if (data.eventName) {
                    data.target.addEventListener(data.eventName, data.handler);
                }
            });
            _self.COLORS.CURRENT = _self.COLORS.DEF;
            cntx.lineWidth = 10;
            _self.curRoom = room;
            _self.update();
            //});

        }
        image.src = room.preview;
        return true;
    }

    selectScope(room, callback) {
        let hasRoom = 1;
        if (room.scopeName) {
            callback();
        } else {
            this.destroy(room, ()=> {
                this.realityServerService.initScope(room, {
                    hasRoom: hasRoom,
                    component: this,
                    onSuccess: ()=> {
                        callback();
                    }
                });
            });

        }
    }

    destroy(room, callback) {
        this.realityServerService.destroy(room || this.curRoom, callback);
    }

    setSize() {
        let _c = this.previewTarget;
        _c.width = _c.parentNode.clientWidth || _c.width;
        _c.height = _c.parentNode.clientHeight || _c.height;
    }


    onMouseMove(ev) {
        document.body.style.cursor = '';
        if (!this.curRoom)return;
        for (let i = 0, _c = this.previewTarget, _preData = this.curRoom.preset; i < _preData.length; i++) {
            let preData = _preData[i];
            if (this.interPointer(ev, preData)) {
                document.body.style.cursor = 'pointer';
                preData.strokeStyle = this.COLORS.SELECTED;
                this.update(preData);
            }
            else if (preData.strokeStyle != this.COLORS.DEF) {
                preData.strokeStyle = this.COLORS.DEF;
                this.update(preData);
            }

        }
        //this.update();
    }

    onMouseUp(ev) {
        for (let i = 0, _preData = this.curRoom.preset; i < _preData.length; i++) {
            let preData = _preData[i];
            if (this.interPointer(ev, preData)) {
                document.body.style.cursor = '';
                preData.isSelected = true;
                return this.selectQuad(preData);
            }
        }
    }

    /**
     *@param{IRoomQuad}preData - data location
     */
    selectQuad(preData) {
        this.app.commands.loadedModelsPst = this.app.glViewer.setNewPst(preData.position, this.curRoom.camera.origin);
        this.app.commands_model.push(...this.app.contactLocalCmds());
        this.curQuad = preData;
        this.elements.remote_render._display(true);
        this.drawImg();
        this.update();
    }

    update(data) {
        if (!this.context)return;
        if (data) {
            this.redrawPointer(data);
        } else {
            if (this.curRoom)this.curRoom.preset.forEach((preData)=> {
                this.redrawPointer(preData);
            });
        }
    }

    resetRoom(hard) {
        if (this.curRoom)this.curRoom.preset.forEach((preData)=> {
            preData.isSelected = false;
        });
        if (hard) {
            this.curRoom = null;
        } else {
            this.update();
        }
        this.app.commands_model = [];
        this.elements.remote_render._display();
    }

    redrawPointer(data) {
        if (data.isSelected)return;
        let _c = this.previewTarget,
            image = this.image,
            arrowSize = 20,
            parAsp = _c.width / _c.height,
            canAsp = image.width / image.height,
            imgW = parAsp > canAsp ? _c.width : image.height * parAsp,
            imdHeight = parAsp > canAsp ? image.width * _c.height / _c.width : imgW * image.height / image.width,
        //imgW  = _c.width,// (_c.height * image.width / image.height),
        //imdHeight = _c.width * image.height / image.width,
            x0 = Math.floor(data.quad.x * imgW - (imgW - _c.width ) / 2),
            y0 = Math.floor(data.quad.y * imdHeight - (imdHeight - _c.height ) / 2),
            x1 = x0 + this.selectedSize / 2,
            y1 = y0 + this.selectedSize / 2;

        this.context.strokeStyle = data.strokeStyle || this.COLORS.DEF;
        this.context.beginPath();
        this.context.moveTo(x1, y1 - arrowSize);
        this.context.lineTo(x1, y1 + arrowSize);
        this.context.stroke();
        this.context.beginPath();
        this.context.moveTo(x1 - arrowSize, y1);
        this.context.lineTo(x1 + arrowSize, y1);
        this.context.stroke();
        this.context.strokeRect(x0, y0, this.selectedSize, this.selectedSize);
        this.context.stroke();
    }


    interPointer(ev, preData) {
        if (preData.isSelected)return;
        let _c = this.previewTarget,
            image = this.image,
            arrowSize = 20,
            parAsp = _c.width / _c.height,
            canAsp = image.width / image.height,
            imgW = parAsp > canAsp ? _c.width : image.height * parAsp,
            imdHeight = parAsp > canAsp ? image.width * _c.height / _c.width : imgW * image.height / image.width,
        //imgW  =  _c.width,//(_c.height * image.width / image.height),
        //imdHeight = _c.width * image.height / image.width,
            x0 = (preData.quad.x * imgW - (imgW - _c.width ) / 2),
            y0 = (preData.quad.y * imdHeight - (imdHeight - _c.height ) / 2);

        return (ev.clientX - this.offset.left > x0 && ev.clientX - this.offset.left < x0 + this.selectedSize &&
        ev.clientY - this.offset.top > y0 && ev.clientY - this.offset.top < y0 + this.selectedSize  );
    }


    drawImg() {
        let image = this.image,
            _c = this.previewTarget,
            cntx = this.context,
            parAsp = _c.width / _c.height,
            canAsp = image.naturalWidth / image.naturalHeight,
            imdWidth = parAsp > canAsp ? _c.width : image.naturalHeight * parAsp,
            imdHeight = parAsp > canAsp ? image.naturalWidth * _c.height / _c.width : imdWidth * image.naturalHeight / image.naturalWidth
            ;
        cntx.clearRect(0, 0, _c.width, _c.height);
        cntx.drawImage(image, _c.width / 2 - imdWidth / 2, _c.height / 2 - imdHeight / 2, imdWidth, imdHeight);
        this.COLORS.CURRENT = this.COLORS.DEF;
        cntx.lineWidth = 10;
        this.offset = _c.getBoundingClientRect();
    }


}

