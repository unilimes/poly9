import {Utils} from "../../utils.js"
export class TransformControls {
    constructor(viewer) {
        let self = this;
        this.viewer = viewer;

        (function initTools() {
            let MODES = self.MODES = {
                CLOSE: 'close',
                XY_MOVE: 'xy-move',
                LEFT_TOP_S: 'left-top-scale',
                TOP_MID_S: 'top-middle-scale',
                RIGHT_TOP_S: 'right-top-scale',
                RIGHT_MID_S: 'right-middle-scale',
                RIGHT_BOTTOM_S: 'right-bottom-scale',
                BOT_MID_S: 'bottom-middle-scale',
                LEFT_MID_S: 'left-middle-scale',
                LEFT_BOTTOM_S: 'left-bottom-scale',
                Z_ROTATE: 'z-rotate',
                Y_ROTATE: 'y-rotate',
                X_ROTATE: 'x-rotate',
                Z_MOVE: 'z-move'
            }
            self.tools = new THREE.Object3D();
            self.tools.toolScale = new THREE.Object3D();


            let
                _min = this.mesh.geometry.min.clone(),
                _max = this.mesh.geometry.max.clone(),
                scaleElem = [
                {
                    name:'leftTop',position:this.mesh.geometry.max},'middelTop','rightTop','middleRight','rightBottom','middleBottom','leftBottom','middleRight'
            ];
            for (let i = 0; i <scaleElem.length; i++) {
                let obj = scaleElem[i],
                    scalePlane = new THREE.Mesh(new THREE.PlaneBufferGeometry(1,1),new THREE.MeshBasicMaterial({color:0xffffff}));


            }
        })()
    }

    attach(mesh) {
        if (mesh, type != Utils.Config.MODELS.TYPES.MESH) return console.error('Only mesh provided');
        this.mesh = mesh;
        this.mesh.add(this.tools);

    }

    detach(mesh) {
    }

    updateControlsTools() {
        this.mesh.geometry.updateBoundingBox();

    }

    toggle(show) {

        this.enabled = show;
    }
}