/**
 * Created by Админ on 06.11.2017.
 */
import {Utils} from "../../utils.js"
export class TransformControls {
    constructor(viewer) {
        this.main = viewer;
        let _self = this;

        (function initUI() {
            let container = _self.container = document.createElement('div'),
                MODES = {
                    CLOSE: 'close',
                    XY_MOVE: 'xy-move',
                    LEFT_TOP_S: 'left-top-scale',
                    TOP_MID_S: 'top-middle-scale',
                    RIGHT_TOP_S: 'right-top-scale',
                    RIGHT_MID_S: 'right-middle-scale',
                    RIGHT_BOTTOM_S: 'right-bottom-scale',
                    BOT_MID_S: 'bottom-middle-scale',
                    LEFT_MID_S: 'left-middle-scale',
                    LEFT_BOTTOM_S: 'left-bottom-scale',
                    Z_ROTATE: 'z-rotate',
                    Y_ROTATE: 'y-rotate',
                    X_ROTATE: 'x-rotate',
                    Z_MOVE: 'z-move'
                };
            container.className = 'transform-controls';
            container.innerHTML =
                '<h1 data-action="' + MODES.CLOSE + '">Close controls</h1>' +
                '<div class="info-action"></div>' +
                '<div class="main-transf">' +
                '   <div class="trnf-translate"  data-action="' + MODES.XY_MOVE + '"></div> ' +
                '   <div class="trnf-scale"> ' +
                '       <div class="border-scale" data-action="' + MODES.LEFT_TOP_S + '"></div>' +
                '       <div class="border-scale" data-action="' + MODES.TOP_MID_S + '"></div>' +
                '       <div class="border-scale" data-action="' + MODES.RIGHT_TOP_S + '"></div>' +
                '       <div class="border-scale" data-action="' + MODES.RIGHT_MID_S + '"></div>' +
                '       <div class="border-scale" data-action="' + MODES.RIGHT_BOTTOM_S + '"></div>' +
                '       <div class="border-scale" data-action="' + MODES.BOT_MID_S + '"></div>' +
                '       <div class="border-scale" data-action="' + MODES.LEFT_BOTTOM_S + '"></div>' +
                '       <div class="border-scale" data-action="' + MODES.LEFT_MID_S + '"></div>' +
                '   </div>' +
                '   <div class="trnf-rotate"> ' +
                '       <div class="z-rotate" data-action="' + MODES.Z_ROTATE + '"></div>' +
                '       <div class="x-rotate" data-action="' + MODES.X_ROTATE + '"></div>' +
                '       <div class="y-rotate" data-action="' + MODES.Y_ROTATE + '"></div>' +
                '   </div>' +
                '   <div class="trnf-move"> ' +
                '       <div class="z-move" data-action="' + MODES.Z_MOVE + '"></div>' +
                '   </div>' +
                '</div>'
            ;


            let handler = (container.addEventListener || container.attachEvent).bind(container),
                _action,
                startPoint = new THREE.Vector2(),
                quaternion,
                worldQuaternion = new THREE.Quaternion(),
                info = container.querySelector('.info-action'),

                tempparent,
                mouse = {
                    point: new THREE.Vector3()
                },
                meshPosition,
                meshScale,
                settings = _self.settings = {
                    angleSpeed: Math.PI,
                    moveZSpeed: 4,
                    scaleSpeed: 3
                },
                moveSpeed,
                movePoint = new THREE.Vector2(),
                cameraLastView,
                cameraLastFocus,
                getPoint = (ev)=> {

                    return {
                        x: (ev.touches ? ev.touches[0].pageX : ev.clientX),
                        y: (ev.touches ? ev.touches[0].pageY : ev.clientY)
                    }
                };
            handler(Utils.Config.EVENTS_NAME.MOUSE_DOWN, onMouseDown, false);
            handler(Utils.Config.EVENTS_NAME.MOUSE_MOVE, onMouseMove, false);
            handler(Utils.Config.EVENTS_NAME.MOUSE_UP, onMouseUp, false);

            handler(Utils.Config.EVENTS_NAME.TOUCH_START, onMouseDown, false);
            handler(Utils.Config.EVENTS_NAME.TOUCH_MOVE, onMouseMove, false);
            handler(Utils.Config.EVENTS_NAME.TOUCH_END, onMouseMove, false);

            handler(Utils.Config.EVENTS_NAME.MOUSE_OUT, (e)=> {
                if (!Utils.Config.isDescendant(container, e.relatedTarget)) {
                    return onMouseUp(e);
                }
            }, true);

            function onMouseUp(e) {
                if (_action) {
                    //e.preventDefault();
                    //e.stopPropagation();
                    tempparent.matrix.decompose(_self.mesh.position, new THREE.Quaternion(), new THREE.Vector3());
                    let _p = tempparent.parent;
                    tempparent.updateMatrixWorld();
                    tempparent.updateMatrix();
                    _self.mesh.updateMatrixWorld();
                    _self.mesh.updateMatrix();
                    let _cl = _self.mesh.matrix.clone();
                    //this.matrix.multiplyMatrices( matrix, this.matrix );
                    _cl.multiplyMatrices(tempparent.matrixWorld,_cl );
                    _p.remove(tempparent);
                    _p.add(_self.mesh);

                    //console.log(tempparent.rotation,tempparent.quaternion);
                    switch (_action) {
                        case  MODES.CLOSE :
                        {
                            _self.toggle();
                            _self.main.dimensionHelper.box.setFromObject(_self.main.model);
                            setTimeout(_self.main.updateDimensionLabels.bind(_self.main), 0);
                            break;
                        }
                        case MODES.Z_MOVE:
                        {
                            viewer.move({
                                duration: 500,
                                list: [
                                    {
                                        onUpdate: (delta)=> {
                                            viewer.camera.position.lerp(cameraLastView, delta);
                                            viewer.camera.lookAt(viewer.controls.target);
                                            viewer.camera.updateProjectionMatrix();
                                        }
                                    }
                                ]
                            });
                            break;
                        }
                        case MODES.XY_MOVE:
                        {
                            //_self.mesh.tempTransformPlane.position.x = _self.mesh.position.x;
                            //_self.mesh.tempTransformPlane.position.y = _self.mesh.position.y;
                            break;
                        }
                        case  MODES.TOP_MID_S:
                        case  MODES.BOT_MID_S:
                        case  MODES.RIGHT_MID_S:
                        case  MODES.LEFT_MID_S:
                        case  MODES.LEFT_TOP_S:
                        case  MODES.LEFT_BOTTOM_S:
                        case  MODES.RIGHT_TOP_S:
                        case  MODES.RIGHT_BOTTOM_S:
                        {

                            _cl.decompose(new THREE.Vector3(), new THREE.Quaternion(), _self.mesh.scale);
                            //_self.mesh.geometry.scale( tempparent.scale.x,tempparent.scale.y,tempparent.scale.z );
                            break;
                        }
                        case MODES.X_ROTATE:
                        case MODES.Y_ROTATE:
                        case MODES.Z_ROTATE:
                        {
                            _cl.decompose(new THREE.Vector3(), _self.mesh.quaternion, new THREE.Vector3());
                            break;
                        }
                    }

                    _action = mouse.meshPosition = false;
                    info.innerHTML = '';
                    hideContrls(true);
                }
                viewer.controls.enabled = true;
            }

            function onMouseDown(e) {
                let action = _action = e.target.getAttribute('data-action');
                if (action) {
                    viewer.controls.enabled = false;
                    //e.preventDefault();
                    //e.stopPropagation();
                    hideContrls();
                    e.target.style.display = '';
                    startPoint = getPoint(e);

                    tempparent = new THREE.Object3D();
                    tempparent.position.copy(_self.mesh.position);
                    worldQuaternion = tempparent.quaternion.clone();
                    quaternion = worldQuaternion.clone();//_self.mesh.quaternion.clone();
                    _self.mesh.position.set(0, 0, 0);
                    //_self.mesh.matrix.decompose(tempparent.position, tempparent.quaternion, tempparent.scale);
                    //new THREE.Matrix4().decompose(_self.mesh.position, _self.mesh.quaternion, _self.mesh.scale);
                    _self.mesh.parent.add(tempparent);
                    tempparent.add(_self.mesh);

                    switch (action) {
                        case  MODES.CLOSE :
                        {
                            break
                        }
                        case  MODES.Z_ROTATE:
                        {
                            quaternion._rotateBy = ( new THREE.Vector3(0, 0, -1));
                            break
                        }
                        case  MODES.X_ROTATE:
                        {

                            quaternion._rotateBy = ( new THREE.Vector3(1, 0, 0));
                            break
                        }
                        case  MODES.Y_ROTATE:
                        {

                            quaternion._rotateBy = ( new THREE.Vector3(0, 1, 0));
                            break
                        }
                        case  MODES.Z_MOVE:
                        {
                            moveSpeed = _self.mesh.parentRadius * 2;
                            meshPosition = tempparent.position.clone();
                            cameraLastView = viewer.camera.position.clone();
                            cameraLastFocus = viewer.controls.target.clone();
                            let moveTo = cameraLastView.clone().addScaledVector(new THREE.Vector3(0, 1, 0), moveSpeed);

                            viewer.move({
                                duration: 500,
                                list: [
                                    {
                                        onUpdate: (delta)=> {
                                            viewer.camera.position.lerp(moveTo, delta);
                                            viewer.camera.lookAt(viewer.controls.target);
                                            viewer.camera.updateProjectionMatrix();
                                        }
                                    }
                                ]
                            });
                            //jQuery(_self.domElems[_action]).draggable('enable');
                            break;
                        }
                        case  MODES.XY_MOVE:
                        {
                            e.target.style.display = 'none';
                            let inter = viewer._events.inter(e, {childs: [_self.mesh.tempTransformPlane]});
                            if (inter && inter[0]) {
                                inter = inter[0];
                                mouse.offset = inter.point.clone();
                            }
                            mouse.meshPosition = tempparent.position.clone();
                            break;
                        }
                        case  MODES.TOP_MID_S:
                        case  MODES.BOT_MID_S:
                        case  MODES.RIGHT_MID_S:
                        case  MODES.LEFT_MID_S:
                        case  MODES.LEFT_TOP_S:
                        case  MODES.LEFT_BOTTOM_S:
                        case  MODES.RIGHT_TOP_S:
                        case  MODES.RIGHT_BOTTOM_S:
                        {

                            meshScale = tempparent.scale.clone();
                            break;
                        }
                    }
                }
                info.innerHTML = action;
            }

            function onMouseMove(e) {
                //console.log(e.clientX,e.clientY);
                if (_action) {
                    //e.preventDefault();
                    //e.stopPropagation();
                    movePoint = getPoint(e);
                    switch (_action) {
                        case MODES.Y_ROTATE:
                        {

                            worldQuaternion.multiplyQuaternions(quaternion.clone(), worldQuaternion.setFromAxisAngle(quaternion._rotateBy, settings.angleSpeed * ((movePoint.x - startPoint.x) / container.clientWidth / 2)));
                            tempparent.quaternion.copy(worldQuaternion);

                            break
                        }
                        case MODES.Z_ROTATE:
                        {

                            worldQuaternion.multiplyQuaternions(quaternion.clone(), worldQuaternion.setFromAxisAngle(quaternion._rotateBy, settings.angleSpeed * ((movePoint.x - startPoint.x) / container.clientWidth / 2)));
                            tempparent.quaternion.copy(worldQuaternion);

                            break
                        }
                        case MODES.X_ROTATE:
                        {
                            worldQuaternion.multiplyQuaternions(quaternion.clone(), worldQuaternion.setFromAxisAngle(quaternion._rotateBy, settings.angleSpeed * ((movePoint.y - startPoint.y) / container.clientHeight / 2)));
                            tempparent.quaternion.copy(worldQuaternion);
                            break
                        }
                        case MODES.Z_MOVE:
                        {
                            tempparent.position.z = meshPosition.z + settings.moveZSpeed * moveSpeed * ((movePoint.y - startPoint.y) / container.clientHeight / 2);
                            //_self.domElems[_action].style.top =e.layerY+'px';
                            //console.log(e.layerY,e.layerX);
                            break;
                        }
                        case MODES.XY_MOVE:
                        {
                            let inter = viewer._events.inter(e, {childs: [_self.mesh.tempTransformPlane]});
                            if (inter && inter[0]) {
                                inter = inter[0];
                                mouse.point.copy(inter.point);
                                mouse.point.sub(mouse.offset);
                                //mouse.meshPosition.copy(inter.point);
                                //_self.mesh.position.x =  mouse.point.x;
                                //_self.mesh.position.y =  mouse.point.y;
                                //_self.mesh.position.copy(mouse.point);
                                //_self.mesh.position.add(mouse.point);
                                tempparent.position.copy(mouse.meshPosition.clone().add(mouse.point));
                            }
                            break;
                        }
                        case MODES.LEFT_TOP_S:
                        {
                            let _offset = -1 * settings.scaleSpeed * (((movePoint.y - startPoint.y) / container.clientHeight / 2) );
                            tempparent.scale.set(meshScale.x + _offset, meshScale.y + _offset, meshScale.z + _offset);
                            break;
                        }
                        case MODES.RIGHT_BOTTOM_S:
                        {
                            let _offset = settings.scaleSpeed * (((movePoint.y - startPoint.y) / container.clientHeight / 2) );
                            tempparent.scale.set(meshScale.x + _offset, meshScale.y + _offset, meshScale.z + _offset);
                            break;
                        }
                        case MODES.RIGHT_TOP_S:
                        {
                            let _offset = -1 * settings.scaleSpeed * (((movePoint.y - startPoint.y) / container.clientHeight / 2) );
                            tempparent.scale.set(meshScale.x + _offset, meshScale.y + _offset, meshScale.z + _offset);
                            break;
                        }
                        case MODES.LEFT_BOTTOM_S:
                        {
                            let _offset = settings.scaleSpeed * (((movePoint.y - startPoint.y) / container.clientHeight / 2) );
                            tempparent.scale.set(meshScale.x + _offset, meshScale.y + _offset, meshScale.z + _offset);
                            break;
                        }
                        case MODES.BOT_MID_S:
                        {
                            tempparent.scale.y = meshScale.y + settings.scaleSpeed * (((movePoint.y - startPoint.y) / container.clientHeight / 2) );
                            break;
                        }
                        case MODES.TOP_MID_S:
                        {
                            tempparent.scale.y = meshScale.y + -1 * settings.scaleSpeed * (((movePoint.y - startPoint.y) / container.clientHeight / 2) );
                            break;
                        }
                        case MODES.LEFT_MID_S:
                        {
                            tempparent.scale.x = meshScale.x + -1 * settings.scaleSpeed * (((movePoint.x - startPoint.x) / container.clientHeight / 2) );
                            break;
                        }
                        case MODES.RIGHT_MID_S:
                        {
                            tempparent.scale.x = meshScale.x + settings.scaleSpeed * (((movePoint.x - startPoint.x) / container.clientHeight / 2) );
                            break;
                        }
                    }
                    info.innerHTML = _action;
                    viewer.refresh();
                }
            }


            function hideContrls(flag) {

                let _display = flag ? '' : 'none';
                for (let node in _self.domElems) {
                    _self.domElems[node].style.display = _display;
                }
            }

            function addRadioBtnControls(){
               /* let container = '<div>' +
                    '<form>'
                    '<input type="radio" name="gender" value="male" checked> Male<br>'
                '<input type="radio" name="gender" value="female"> Female<br>'
                    '<input type="radio" name="gender" value="other"> Other'
                    </form> ' +
                    '' +
                    '' +
                    '</div>'*/
            }

            _self.domElems = {};
            (function selAtins(list) {
                for (let i = 0; i < list.length; i++) {
                    let _act = list[i].getAttribute('data-action');
                    if (_act == MODES.Z_MOVE || _act == MODES.X_ROTATE) {
                        jQuery(list[i]).draggable({
                            axis: "y", stop: function (event, ui) {
                                this.style.top = '';
                            }
                        });
                    } else if (_act == MODES.Z_ROTATE || _act == MODES.Y_ROTATE) {
                        jQuery(list[i]).draggable({
                            axis: "x", stop: function (event, ui) {
                                this.style.left = '';
                            }
                        });
                    }
                    if (_act)_self.domElems[_act] = list[i];
                    if (list[i].children)selAtins(list[i].children);
                }
            })(container.children);
            viewer.glcontainer.appendChild(container);
        })()
    }

    toggle(mesh) {

        let _mesh = this.mesh || mesh,
            isEnabled = this.container.className.match('active'),
            moveTo = isEnabled ? this.main.camera.lastView : mesh.position.clone().addScaledVector(new THREE.Vector3(0, 0, 1), mesh.parent.radius * 2),
            focusTo = isEnabled ? this.main.camera.lastFocus : mesh.position;
        if (mesh) {
            this.mesh = mesh;
            this.mesh.parentRadius = mesh.parent.radius;
        }
        this.main.move({
            duration: 500,
            onStart: ()=> {
                if (isEnabled) {
                    this.main.controls.mouseButtons.ORBIT = THREE.MOUSE.LEFT;
                    this.main.controls.mouseButtons.PAN = THREE.MOUSE.RIGHT;
                    this.container.className = this.container.classList[0];
                    //_mesh.tempTransformPlane.parent.remove(_mesh.tempTransformPlane);
                } else {
                    this.main.controls.mouseButtons.ORBIT = -1;
                    this.main.controls.mouseButtons.PAN = THREE.MOUSE.LEFT;
                    this.main.camera.lastView = this.main.camera.position.clone();
                    this.main.camera.lastFocus = this.main.controls.target.clone();
                }
            },
            onComplete: ()=> {
                if (isEnabled) {
                    //_mesh.plane.parent.add(_mesh);

                    this.main.model.traverse((child)=>{
                        if(child.type == Utils.Config.MODELS.TYPES.MESH){
                            if(child._lastMat)child.material = child._lastMat;
                        }
                    })
                } else {
                    _mesh.tempTransformPlane = new THREE.Mesh(new THREE.PlaneBufferGeometry(mesh.parentRadius * 3000, mesh.parentRadius * 2000, 1), new THREE.MeshBasicMaterial());
                    _mesh.tempTransformPlane.position.set(_mesh.position.x, _mesh.position.y, 0);
                    var _tempMat = new THREE.MeshBasicMaterial({wireframe:true, color:0x00007d});
                    this.main.model.traverse((child)=>{
                        if(child.type == Utils.Config.MODELS.TYPES.MESH && child.uuid !=mesh.uuid){
                            child._lastMat = child.material;
                            child.material = _tempMat;
                        }
                    })
                    //_mesh.parent.add(_mesh.tempTransformPlane);
                    //_mesh.plane.add(_mesh);
                    this.container.className += ' active';
                }
            },
            list: [
                {
                    onUpdate: (delta)=> {
                        this.main.camera.position.lerp(moveTo, delta);
                        this.main.controls.target.lerp(focusTo, delta);
                        this.main.camera.lookAt(this.main.controls.target);
                        this.main.camera.updateProjectionMatrix();
                    }
                }
            ]
        })

        this.enabled = !!mesh;

    }
}