import {GLMain}from '../viewer/glMain.js';
import {Utils}from '../../utils.js';
import {DecorData}from '../decoratioin/DecorData.js';
import {Interfaces}from '../../data.structure/interfaces.js';
/*
 * ModelCustomImage - model texture(s) control
 * @angle - rotation decor
 * @scale - size decor
 * @activeArea - current XY decor position
 * @options - decor data for decoration
 * */
export class ModelCustomImage extends GLMain {

    constructor(app, options, onFinish = ()=> {
    }, clone = null) {

        super();
        this.fontSize = 30;
        this.padding = 30;
        this.defSize = 1024;
        this.active = true;
        this.scale = 1;
        this.MAT_PREFIX = "_def";
        this.angle = 0;
        this.mapsUpdate = this.Utils.Config.MAT_MAPS;
        this.app = app;
        this.canvasList = {};
        this.texture = {};
        this.mapsText = {};
        this.activeArea = new ActiveArea();
        this.options = options;
        this.tabs = new TabEdit(this);
        this.updateActiveArea();

        let _emboss = options.embossIntensity;
        if (clone) {
            this.scale = clone.scale;
            this.angle = clone.angle;
        }
        if (options.angle)this.angle = options.angle;
        if (options.area) {
            if (options.area.angle)this.angle = options.area.angle;
            if (options.area.scale)this.scale = options.area.scale;
            if (options.area.embossIntensity)_emboss = options.area.embossIntensity;
        }
        NMO_FileDrop.initHeightMapWithImg(options.preview || options.image, (maps)=> {
            this.mapsText = maps;
            for (let i = 0; i < this.mapsUpdate.length; i++) {
                let canvas = this.canvasList[this.mapsUpdate[i]] = document.createElement("canvas");
                canvas.width = canvas.height = this.defSize;
                this.texture[this.mapsUpdate[i]] = new THREE.Texture();
                this.texture[this.mapsUpdate[i]].image = canvas;
                this.texture[this.mapsUpdate[i]].category = "custom";
                this.updateContext(canvas);
            }
            this.updateMaterial();
            onFinish(this);
            if (this.app._decor)this.app._decor.updateTemplate();
        }, {normalScale: _emboss});
    }

    getPattern(flag) {
        let _pattern = this._pattern;
        if (_pattern && !flag) {

        } else {
            _pattern = new Interfaces.IPattern();
            this._pattern = _pattern;
        }
        _pattern.image = this.options.preview.src;
        if (!_pattern.area)_pattern.area = new Interfaces.IPatternSettings();
        _pattern.area.angle = this.angle;
        _pattern.area.scale = this.scale;
        _pattern.area.center_x = this.activeArea.lastUv.x;
        _pattern.area.center_y = this.activeArea.lastUv.y;

        return _pattern;
    }

    onReSize() {
        //this.updateContext();
    }

    setTiling(opt) {
        if (!opt)return;
        this.options.isTiling = opt.isTiling;
        this.options.repeatX = opt.repeatX;
        this.options.repeatY = opt.repeatY;
    }

    updateActiveArea(uv) {
        if (uv)this.activeArea.uv = this.activeArea.lastUv = this.activeArea.prevCenter = uv;
    }

    updateContext(canvas) {
        canvas.cntx = canvas.getContext('2d');
        canvas.cntx.fillStyle = '#ffffff';
        canvas.cntx.strokeStyle = '#000000';
        canvas.cntx.lineWidth = 2;
    }

    dataURItoBlob() {
        let byteString = atob(this.canvas.toDataURL().split(',')[1]);
        let ab = new ArrayBuffer(byteString.length);
        let ia = new Uint8Array(ab);
        for (let i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }
        return new Blob([ab], {type: 'image/jpeg'});
    }

    getImgBlob() {
        let imageData = this.cntx.getImageData(0, 0, this.canvas.width, this.canvas.height).data,
            buffer = new Uint8Array(imageData.length);
        for (let index = 0; index < imageData.length; index++)
            buffer[index] = imageData[index];

        let imageBlob = new Blob(buffer);
        imageBlob.lastModifiedDate = new Date();
        imageBlob.name = this.ENTITY.Config.randomstr() + '.jpg';

        return imageBlob; //
    }

    destroy() {
        for (let d = 0; d < this.app.decorations.length; d++) {
            if (this.app.decorations[d].id == this.id) {
                if (this.app._events.lastLogo && this.app._events.lastLogo.id != this.id) {
                    this.app._events.lastLogo.setActive(false, null, true);
                }
                this.app._events.lastLogo = null;
                let droped = this.app.decorations.splice(d, 1)[0];
                droped.locked = droped.active = false;
                if (this.material) {
                    for (let i = 0; i < this.material.logos.length; i++) {
                        if (this.material.logos[i].id == this.id) {
                            this.material.logos.splice(i, 1);
                            break;
                        }
                    }
                }
                this.app._decor.clearData(droped);
                return droped.updateCanvas(null, null, true);
            }
        }
    }

    setActive(active = false, uv = null, noneedRefreshArea = false) {
        this.active = active;
        if (this.active && this.options.isTiling) this.active = false;
        if (!this.options.isTiling)this.updateArea(uv, noneedRefreshArea);
        this.updateCanvas(uv);
        if (!active && this.tabs.currentMode)this.tabs.currentMode = null;
    }

    updateArea(uv, noneedRefreshArea = false) {
        this.activeArea.updateOffsets(this.tabs.currentMode ? this.activeArea.uv : uv, this.canvasList[this.mapsUpdate[0]], noneedRefreshArea || this.hasToBeLocked);

    }

    updateMaterial(mat = null, uv = null, ev = null) {
        let material = mat,
            isSameMat;
        if (!material)material = this.material;
        if (!material)material = this.options.material;
        if (!material)return;
        if(material._isCashed){
            let _m = this.options.material = this.app._defMaterial();
            _m._mat = material._mat;
            _m.lastEditDecor = material.lastEditDecor;
           if(material._mat) ['envMap'].concat(Utils.Config.MAT_MAPS).forEach((el,key)=>{
                if(material[el]){
                    if(key ==0){
                        _m[el]=material[el];
                    }else{
                        _m[el]=new THREE.Texture(THREE.Cache.files[material._mat.maps[el]]);
                    }

                }

            });
            this.app._events.lastInter.object.material = material =_m;
            this.app.createToolTipInfo(this.app._events.lastInter.object);
        }
        if (this.material) {
            isSameMat = this.material.uuid == material.uuid;
            if (isSameMat) {

            } else {
                for (let i = 0, logs = this.material.logos; i < logs.length; i++) {
                    if (logs[i].id == this.id) {
                        logs.splice(i, 1);
                        break;
                    }
                }
                this.disableMapsOnEdit();
                this.updateMaterialMaps();
            }
        }

        if (isSameMat) {
            if (this.material.lastEditDecor.id != this.id) {
                this.material.lastEditDecor = this;
                for (let i = 0; i < this.mapsUpdate.length; i++) {
                    if (this.material[this.mapsUpdate[i]])this.material[this.mapsUpdate[i]] = this.texture[this.mapsUpdate[i]];
                }
            }
        } else {

            this.material = material;
            material.lastEditDecor = this;
            if (!material.logos)material.logos = [];
            material.logos.push(this);
            this.updateQuality();
            if (ev)this.disableMapsOnEdit(true);
        }
        this.material.needsUpdate = true;
        return this.updateCanvas(uv, ev);
    }

    disableMapsOnEdit(flag = false) {
        if (flag) {
            this.mapsUpdate.forEach((map, index)=> {
                if (index > 0) {
                    this.material['_temp' + map] = this.material[map];
                    this.material[map] = null;
                }
            })
        } else {
            this.mapsUpdate.forEach((map, index)=> {
                if (this.material['_temp' + map]) {
                    this.material[map] = this.texture[map];//['_temp' + map];
                    this.material['_temp' + map] = null;
                }
            });
            this.updateCanvas(null);
            this.material.needsUpdate = true;
        }
    }

    updateQuality() {
        let quality = this.app._datGui.decorParams.patternQuality;
        let material = this.material;
        for (let i = 0; i < this.mapsUpdate.length; i++) {
            let _m = this.mapsUpdate[i];
            if (i == 0 || material[_m] /*|| ((i==1 || i ==5) && this.options.bossIntensity)*/) {
                if (
                    !material[this.MAT_PREFIX + _m] &&
                    material[_m]
                    && !material[this.mapsUpdate[i]].category
                //&& material.logos.length < 2
                ) {
                    material[this.MAT_PREFIX + _m] = material[_m];
                }
                material[_m] = this.texture[_m];

                if (material[this.MAT_PREFIX + _m]) {
                    this.canvasList[_m].width = material[this.MAT_PREFIX + _m].image.width;
                    this.canvasList[_m].height = material[this.MAT_PREFIX + _m].image.height;
                    if (this.defSizeW < this.canvasList[_m].width || this.defSizeH < this.canvasList[_m].height) {
                        //this.defSizeW= this.canvasList[_m].width;
                        //this.defSizeH= this.canvasList[_m].height;
                        //this.canvasList[this.mapsUpdate[0]].width = this.defSizeW;
                        //this.canvasList[this.mapsUpdate[0]].height = this.defSizeH;
                        //this.updateContext(this.canvasList[this.mapsUpdate[0]]);
                    }

                } else {
                    //if(this.defSizeH){
                    //    this.canvasList[_m].height =this.defSizeH ;
                    //    this.canvasList[_m].width =this.defSizeW ;
                    //
                    //}else{
                    this.canvasList[_m].width = this.canvasList[_m].height = this.defSize;
                    //}

                }
                this.updateContext(this.canvasList[_m]);
            }
        }
    }

    updateMaterialMaps() {
        if (!this.material)return;
        if (this.material.logos.length) {
            let lastLogo = this.material.logos[this.material.logos.length - 1];
            for (let i = 0; i < this.mapsUpdate.length; i++) {
                if (this.material[this.mapsUpdate[i]])this.material[this.mapsUpdate[i]] = lastLogo.texture[this.mapsUpdate[i]];
            }
            lastLogo.updateCanvas();
        } else {
            for (let i = 0; i < this.mapsUpdate.length; i++) {
                if (this.material[this.mapsUpdate[i]])this.material[this.mapsUpdate[i]] = this.material[this.MAT_PREFIX + this.mapsUpdate[i]];
            }
        }
        this.material.needsUpdate = true;
    }

    isSelected(uv) {
        if (!this.material || this.options.isTiling)return false;
        let
            diffuseCanvas = this.canvasList[this.mapsUpdate[0]],
            clientX = uv.x * diffuseCanvas.width,
            clientY = uv.y * diffuseCanvas.height,
            isSelc = clientX > this.activeArea.left_top_pos_x - TabEdit.size &&
                clientX < this.activeArea.left_bottom_pos_x + diffuseCanvas.width / TabEdit.size &&
                clientY > this.activeArea.left_top_pos_y - TabEdit.size &&
                clientY < this.activeArea.left_bottom_pos_y + diffuseCanvas.height / TabEdit.size;
        if (isSelc && this.active) {
            this.tabs.selectMode(clientX, diffuseCanvas.height - clientY);
            if (this.tabs.currentMode == this.tabs.transform.pin) {
                this.tabs.currentMode = null;
                this.hasToBeLocked = !this.hasToBeLocked;
                if (!this.hasToBeLocked)this.locked = this.hasToBeLocked;
            } else if (this.tabs.currentMode == this.tabs.transform.drop && !this.tabs.isLocked) {
                return this.destroy();
            }
        }
        return isSelc;
    }

    updateTextures() {
        NMO_FileDrop.initHeightMapWithImg(this.options.preview, (maps)=> {
            this.mapsText = maps;
            this.updateMaterial();
        });
    }

    updateCanvas(uv = null, ev = null, hardReset = false) {

        let
            center = uv || this.activeArea.uv,
            hasRedraw, item,
            loked;


        if (this.tabs.currentMode) {
            center = this.activeArea.uv;
            if (ev && this.lastEv) {
                let dir = ev.x - this.lastEv.x;
                if (dir === 0)dir = ev.y - this.lastEv.y;

                let speed = this.app.camera._distanceToCenter / 129.9038131816173;

                if (this.tabs.currentMode == this.tabs.transform.rotate) {
                    this.angle += (dir) * (this.app._datGui.decorParams.rotateSpeed || 0.01) * speed;
                } else if (this.tabs.currentMode == this.tabs.transform.resize) {
                    this.scale += ( dir) * (this.app._datGui.decorParams.scaleSpeed || 0.01) * speed;
                    this.app.updateTabSizeInfo({
                        width: this.options.preview.width * this.scale,
                        height: this.options.preview.height * this.scale
                    });
                } else if (this.tabs.currentMode == this.tabs.transform.copy) {

                    this.disableMapsOnEdit();
                    this.active = false;
                    this.updateArea(uv);
                    let cA = this.activeArea.clone();
                    this.updateArea(uv, true);
                    this.tabs.currentMode = this.app._events.lastLogo = null;
                    new ModelCustomImage(this.app, new DecorData(this.options.preview, this.options.jsonData, null, this.material), (newModel)=> {
                        newModel.activeArea = cA;
                        this.app.decorations.push(newModel);
                        newModel.updateMaterial(this.material, uv);
                        this.app._events.lastLogo = newModel;
                        newModel.setActive(true);
                        newModel.disableMapsOnEdit(true);
                    }, this);
                    return;
                }
            }
        } else {
            this.activeArea.uv = center;
        }
        if ((this.locked && !hardReset) || !this.material)return;
        this.locked = this.hasToBeLocked;
        for (let canvasEdit in this.canvasList) {
            let canvasCurEdit = this.canvasList[canvasEdit];
            if (!canvasCurEdit || !this.material[canvasEdit])continue;
            let ctx = canvasCurEdit.cntx;
            if (this.material[this.MAT_PREFIX + canvasEdit] && this.material[this.MAT_PREFIX + canvasEdit].image) {
                ctx.drawImage(this.material[this.MAT_PREFIX + canvasEdit].image, 0, 0);
            } else {
                if (canvasEdit == this.mapsUpdate[1]) {
                    ctx.fillStyle = '#7f7f7f'
                } else if (canvasEdit == this.mapsUpdate[5]) {
                    ctx.fillStyle = '#000000';
                } else {
                    ctx.fillStyle = '#ffffff';
                }

                ctx.fillRect(0, 0, canvasCurEdit.width, canvasCurEdit.height);
            }
            for (let i = 0, logs = this.material.logos; i < logs.length; i++) {
                ctx.save();
                if (logs[i].id == this.id) {

                    this.activeArea.update(center, canvasCurEdit, 0, 0, this.options.preview.width * this.scale, this.options.preview.height * this.scale, this.angle);
                    //if (i < logs.length - 1) {
                    //    item = i;
                    //    hasRedraw = logs[i];
                    //    ctx.restore();
                    //    continue;
                    //}
                }
                if (logs[i].mapsText[canvasEdit]) {
                    if (logs[i].options.isTiling) {

                        if (!logs[i].activeArea._width) {
                            logs[i].activeArea._width = logs[i].activeArea.width;
                            logs[i].activeArea.width = logs[i].activeArea.width * (logs[i].options.repeatX || 1);
                        }
                        if (!logs[i].activeArea._height) {
                            logs[i].activeArea._height = logs[i].activeArea.height;
                            logs[i].activeArea.height = logs[i].activeArea.height * (logs[i].options.repeatY || 1);
                        }
                        for (let startX = 0,
                                 delByX = logs[i].options.repeatX || 1,
                                 delByY = logs[i].options.repeatY || 1,
                                 imgDraw = logs[i].mapsText[canvasEdit],

                                 sizeX = logs[i].activeArea._width,
                                 sizeY = logs[i].activeArea._height,
                                 asp = sizeX / sizeY,
                                 imgW = canvasCurEdit.width / delByX,
                                 imgH = canvasCurEdit.height / delByY;
                             startX < delByX; startX++) {
                            for (let startY = 0; startY < delByY; startY++) {
                                ctx.drawImage(imgDraw, startX * sizeX, startY * sizeY);
                            }
                        }

                    } else {
                        ctx.translate(logs[i].activeArea.center_x, logs[i].activeArea.center_y);
                        ctx.rotate(logs[i].angle);
                        ctx.drawImage(logs[i].mapsText[canvasEdit], -logs[i].activeArea.width / 2, -logs[i].activeArea.height / 2, logs[i].activeArea.width, logs[i].activeArea.height);
                    }

                }
                ctx.restore();
                if (canvasEdit == this.mapsUpdate[0]) logs[i].redrawTabs(ctx, canvasCurEdit.width);
            }
            //if (hasRedraw) {
            //    ctx.save();
            //    ctx.translate(hasRedraw.activeArea.center_x, hasRedraw.activeArea.center_y);
            //    ctx.rotate(hasRedraw.angle);
            //    ctx.drawImage(hasRedraw.options.preview, -hasRedraw.activeArea.width / 2, -hasRedraw.activeArea.height / 2, hasRedraw.activeArea.width, hasRedraw.activeArea.height);
            //    ctx.restore();
            //    if (canvasEdit == this.mapsUpdate[0]) {
            //        hasRedraw.redrawTabs(ctx,canvasCurEdit.width);
            //        this.material.logos.push(this.material.logos.splice(item, 1)[0]);
            //    }
            //}

            this.lastEv = ev;
            this.texture[canvasEdit].image = canvasCurEdit;
            this.texture[canvasEdit].needsUpdate = true;
            canvasCurEdit.hasChanges = true;

        }
        this.app.refresh();
        if (hardReset)this.updateMaterialMaps();

    }


    redrawTabs(ctx, size) {
        if (this.active) {
            let _ar = this.activeArea;
            ctx.strokeRect(_ar.left_top_pos_x, _ar.left_top_y, _ar.border_width, _ar.border_height);
            this.tabs.update(ctx, this.locked, size);
        }
    }

}
class ModeEdit {

    constructor(img = null) {
        this.image = img;
        this.left_top_x = 0;
        this.left_top_y = 0;
        this.size = 0;
    }
}
class TransformMode {
    constructor() {
        this.rotate = this.pin = this.resize = this.copy = this.drop = null;
    }
}

class TabEdit {
    constructor(main) {

        let pst = this._pst = ['left_top_pos_x', 'left_top_y', 'border_width', 'border_height', 'middle'];
        this.transforms = [
            {name: 'pin', x: pst[0], y: pst[1]},
            {name: 'drop', x: pst[0], y: pst[3]},
            {name: 'rotate', x: pst[2], y: pst[1]},
            {name: 'copy', x: pst[4], y: pst[3]},
            {name: 'resize', x: pst[2], y: pst[3]}
        ];
        this.transform = new TransformMode();
        this.currentMode = null;
        this.id = Date.now();
        this.main = main;
        let imageLoader = new THREE.ImageLoader(new THREE.LoadingManager());
        imageLoader.setCrossOrigin('anonymous');

        this.transforms.forEach((mode)=> {
            this.transform[mode.name] = new ModeEdit(imageLoader.load(Utils.Config.REMOTE_DATA + Utils.Config.UI_STORAGE + mode.name + ".png"));
        });

    }


    update(cntx, locked, size = TabEdit.size * 2) {
        let _s = size / TabEdit.size,
            _ar = this.main.activeArea;
        this.isLocked = locked;
        for (let key = 0; key < this.transforms.length; key++) {
            let _trsans = this.transforms[key],
                cur = this.transform[_trsans.name];
            if (!cur)continue;

            for (let d = 0, arrs = ['left_top_x', 'left_top_y']; d < 2; d++) {
                let _curI = 'x';
                if (d == 1) {
                    _curI = 'y';
                }
                switch (_trsans[_curI]) {
                    case this._pst[4]:
                    {
                        cur[arrs[d]] = _ar[this._pst[d == 0 ? 0 : 1]] + _ar[this._pst[d == 0 ? 2 : 3]] / 2 - TabEdit.size / 2;
                        break;
                    }
                    case this._pst[0]:
                    case this._pst[1]:
                    {
                        cur[arrs[d]] = _ar[_trsans[_curI]] - _s;
                        break;
                    }
                    case this._pst[2]:
                    {
                        cur[arrs[d]] = _ar[this._pst[0]] + _ar[_trsans[_curI]];
                        break;
                    }
                    case this._pst[3]:
                    {
                        cur[arrs[d]] = _ar[this._pst[1]] + _ar[_trsans[_curI]];
                        break;
                    }
                }
            }
            cur.size = _s;
            if (locked) {
                if (key == 0) {
                    if (cur == this.currentMode)cntx.fillRect(cur.left_top_x, cur.left_top_y, _s, _s);
                    cntx.drawImage(cur.image, cur.left_top_x, cur.left_top_y, _s, _s);
                }
            } else {
                if (cur == this.currentMode)cntx.fillRect(cur.left_top_x, cur.left_top_y, _s, _s);
                cntx.drawImage(cur.image, cur.left_top_x, cur.left_top_y, _s, _s);
            }
        }
    }

    selectMode(uvX, uvY) {
        this.currentMode = null;
        for (let f = 0; f < this.transforms.length; f++) {
            let cur = this.transform[this.transforms[f].name];
            if (
                uvX > cur.left_top_x && uvX < cur.left_top_x + cur.size &&
                uvY > cur.left_top_y && uvY < cur.left_top_y + cur.size
            ) {
                return this.currentMode = this.transform[this.transforms[f].name];

            }
        }
    }
}
TabEdit.size = 32;
class ActiveArea extends GLMain {


    constructor() {
        super();
        this.prevCenter = {x: 0.5, y: 0.5};
        this.uv = {x: 0.5, y: 0.5};
        this.lastUv = {x: 0.5, y: 0.5};
        this.lastOffsetUv = {x: 0, y: 0};
        this.offsetY = this.offsetX = this._offsetX = this._offsetY = this.width = this.height = this.left_top_pos_x =
            this.left_top_pos_y = this.left_bottom_pos_x = this.left_bottom_pos_y = this.left_top_x =
                this.left_top_y = this.left_bottom_x = this.left_bottom_y = this.border_left_top_x =
                    this.border_left_top_y = this.border_right_bottom_x = this.border_right_bottom_y = this.border_width = this.border_height = 0;
    }

    updateOffsets(uvs, area, noneedRefreshArea) {

        let uv = uvs || this.lastUv,
            offsetUvX = uv.x - this.prevCenter.x,
            offsetUvY = (uv.y - this.prevCenter.y);

        this._offsetX = (offsetUvX) * area.width;
        this._offsetY = (offsetUvY) * area.height * -1;
        if (noneedRefreshArea)return;
        this.prevCenter = {x: uv.x - this.lastOffsetUv.x, y: uv.y - this.lastOffsetUv.y};
        this.lastOffsetUv.x = offsetUvX;
        this.lastOffsetUv.y = offsetUvY;

    }

    update(center, canvas, offsetX = 0, offsetY = 0, width = 0, height = 0, angle = 0) {


        this.offsetX = this._offsetX;
        this.offsetY = this._offsetY;
        //
        this.center_x = ( center.x  ) * canvas.width - this.offsetX;
        this.center_y = canvas.height - ( center.y ) * canvas.height - this.offsetY;
        //
        this.width = width;
        this.height = height;
        //
        let nw = Math.abs(width * Math.cos(angle)) + Math.abs(height * Math.sin(angle)),
            nh = Math.abs(width * Math.sin(angle)) + Math.abs(height * Math.cos(angle));
        //
        this.left_top_pos_x = this.center_x - width / 2 - (nw - width) / 2;
        this.left_top_pos_y = canvas.height - this.center_y - height / 2 - (nh - height) / 2;

        this.left_top_x = (this.center_x ) / canvas.width;
        this.left_top_y = this.center_y - height / 2 - (nh - height) / 2;
        this.left_bottom_x = (this.center_x + this.width) / canvas.width;
        this.left_bottom_y = (this.center_y + this.height) / canvas.height;
        //
        this.border_width = nw;
        this.border_height = nh;
        //
        this.left_bottom_pos_x = this.center_x + nw / 2;
        this.left_bottom_pos_y = canvas.height - this.center_y + nh / 2;

        this.lastUv = center;
    }


    updateBorder() {
        this.border_right_bottom_x = this.border_left_top_x + this.border_width;
        this.border_right_bottom_y = this.border_left_top_y + this.border_height - TabEdit.size;
    }
}