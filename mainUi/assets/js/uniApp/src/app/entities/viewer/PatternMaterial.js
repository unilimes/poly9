export class PatternMaterial extends THREE.ShaderMaterial {
    constructor(opt) {
        let config = opt || {};

        let customShader = {
            fragm: '' +
            'vec4 patternColor = texture2D( patternMap, vUv );' +
            'patternColor = mapTexelToLinear( patternColor );' +
            'gl_FragColor = vec4( mix( gl_FragColor.rgb, patternColor.rgb, patternColor.a )*outgoingLight , gl_FragColor.a );\n'
            //'gl_FragColor =   gl_FragColor * patternColor;\n'
        }
        let lambertShader = THREE.ShaderLib.physical;
        let getShader = (shaderStr)=> {
            return shaderStr.replace(/#include\s+\<(\S+)\>/gi, function (match, p1) {
                var chunk = THREE.ShaderChunk[p1];
                if (p1 == 'map_pars_fragment')chunk += 'uniform sampler2D patternMap;\n';
                if (p1 == 'dithering_fragment')chunk += customShader.fragm;
                return chunk ? chunk : "";
            });
        }
        let uniforms = lambertShader.uniforms;
        let defines = {
            STANDARD: true,
            //USE_MAP: '',
            //USE_NORMALMAP: '',
            //USE_METALNESSMAP: '',
        };

        config.defines = defines;
        config.vertexShader = getShader(lambertShader.vertexShader);
        config.fragmentShader = getShader(lambertShader.fragmentShader);
        config.fog = false;
        config.lights = true;
        config.uniforms = uniforms;
        config.extensions = {
            derivatives: true,
            drawBuffers: true,
            fragDepth: true,
            shaderTextureLOD: true
        };
        super(config);
        this.color = config.color || new THREE.Color(1, 1, 1);
        this.displacementScale = config.displacementScale || 0;
        this.displacementBias = config.displacementBias || 0;
        this.uniforms.patternMap = {value: null};
    }

    onBeforeCompile() {
        super.onBeforeCompile();
        this.uniforms.map.value = this.map;
        this.uniforms.normalMap.value = this.normalMap;
        this.uniforms.metalnessMap.value = this.metalnessMap;
        this.uniforms.envMap.value = this.envMap;
        this.uniforms.envMap.value = this.color;
        this.uniforms.patternMap.value = this.patternMap;
    }

}