/**
 * Created by edinorog on 20.10.17.
 */
export class CMain {
    constructor(data) {
        this.data = data;
    }

}

export class Configuration extends CMain {
    parse(data) {
        this.data = data;
    }
}
export class ProductPart extends CMain {
}
export class ProductPattern extends CMain {
}