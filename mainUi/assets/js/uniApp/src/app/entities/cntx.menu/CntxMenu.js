import {GLMain} from "../viewer/glMain.js";

export class CntxMenu extends GLMain {
    constructor(main) {
        super();

        let menu = this.container = document.createElement('div'),
            handler = (menu.addEventListener || menu.attachEvent).bind(menu),
            EV_NAME = this.Utils.Config.EVENTS_NAME,
            items = [{name: 'Volume'}, {name: 'Edit Material'}, {name: 'Transform'}],
            html = '<ul cntx="1">';
        menu.className = 'cntx-menu';
        for (let i = 0; i < items.length; i++) {
            html += '<li cntx="1" item="' + i + '">' + items[i].name + "</li>";
        }
        html += "</ul>";
        menu.innerHTML = html;
        main.container.appendChild(menu);
        menu.setAttribute('cntx', 1);
        handler(EV_NAME.MOUSE_OUT, (e)=> {
            if (!e.toElement.getAttribute('cntx'))this.hide();
        });
        handler(EV_NAME.CLICK, (e)=> {
            this.Utils.Config.onEventPrevent(e);
            if (e.target.localName == 'li') {
                let _a = +e.target.getAttribute('item');
                switch (_a) {
                    case 0:
                    {
                        if(main.main.component){
                            alert(main.main.component.getVolume(this.mesh));
                        }else{
                            alert(main.main.getVolume(this.mesh));
                        }

                        break;
                    }
                    case 1:
                    {
                        main._datGui.onSelect(this.mesh);
                        break;
                    }
                    case 2:
                    {
                        main._transformUI.toggle(this.mesh);
                        //main._events.onTransformModel(this.mesh);
                        break;
                    }
                }
                this.hide();
            }

        });


        this.enabled = true;
    }

    toggle(_mesh, ev) {
        if(!this.enabled && _mesh)return;
        this.mesh = null;
        if (_mesh) {
            let mesh = _mesh.object || _mesh;
            if (mesh.type == this.Utils.Config.MODELS.TYPES.MESH) {
                this.mesh = mesh;
            }
        }


        let menu = this.container;
        if (ev) {
            menu.style.left = ev.x + 'px';
            menu.style.top = ev.y + 'px';
        }
        if (menu.className.match('active')) {
            this.hide();
        } else {
            this.show();
        }
    }

    hide() {
        this.container.className = 'cntx-menu';
    }

    show() {
        this.container.className = 'cntx-menu active';
    }
}