import {Utils} from "../utils.js";
import {Decor} from "./decoratioin/DecorData.js";

/**
 * Class representing an ui interface.
 */
export class Costumize {
    constructor(main) {
        this.onFinishCalls = [];
        let modalContainer = document.querySelector('.modal-body.modal-pad');
        if (!modalContainer) return console.error("no html container for customize found");
        this.htmlTemplate = {
            modalContainer: modalContainer,
            tabsHeaderContainer: modalContainer.querySelector('.nav.nav-tabs.nav-tabs-top>.model-parts'),
            tabsBodiesContainer: modalContainer.querySelector('.tab-content>.model-parts'),
            lastHeaderActive: modalContainer.querySelector('.nav.nav-tabs.nav-tabs-top>li.active'),
            lastBodyActive: modalContainer.querySelector('#TABS_CNT>.tab-pane.active')
        };
        this.htmlTemplate.tabsHeaderContainer.className += ' nav nav-tabs nav-tabs-top';
        this.htmlTemplate.tabsBodiesContainer.className += ' tab-content';
        this.main = main;
        this.addAccTemplate();
        this.updateListOfParts();
        let _tabs = modalContainer.querySelector('.nav.nav-tabs.nav-tabs-top');
        if (_tabs)_tabs.addEventListener('click', (e)=> {
            this.onTabSelect(e);
        });
        setTimeout(()=> {
            while (this.onFinishCalls.length)this.onFinishCalls.pop()();
            delete this.onFinishCalls;
        })
    }

    /**
     * set active tab.
     * @param {Event} e - event from caller.
     */
    onTabSelect(e) {
        let isSel = e.target.getAttribute('data-toggle');
        if (isSel == 'dropdown') {
            this.lastClicked = e.target.parentNode;
            this.lastClicked.clicked = true;
            return
        }
        if (isSel) {
            let _id = e.target.getAttribute('href');
            if (this.htmlTemplate.lastBodyActive)this.htmlTemplate.lastBodyActive.className = this.htmlTemplate.lastBodyActive.className.replace('active', '');
            if (this.htmlTemplate.lastHeaderActive)this.htmlTemplate.lastHeaderActive.className = this.htmlTemplate.lastHeaderActive.className.replace('active', '');
            e.target.parentNode.className += ' active';
            this.htmlTemplate.lastHeaderActive = e.target.parentNode;
            this.htmlTemplate.lastBodyActive = this.htmlTemplate.tabsBodiesContainer.querySelector(_id) || this.htmlTemplate.modalContainer.querySelector(_id);
            if (this.htmlTemplate.lastBodyActive)this.htmlTemplate.lastBodyActive.className += ' active';
            if (this.lastClicked) {
                if (this.lastClicked.clicked && e.target.getAttribute('isSubList')) {
                    if (!this.lastClicked.className.match('active'))this.lastClicked.className += ' active';
                } else {
                    this.lastClicked.className = this.lastClicked.className.replace(' active', '');
                }

            }
            if (e.target.getAttribute('isModel')) {
                if (this.htmlTemplate.lastBodyActive)this.htmlTemplate.lastBodyActive.appendChild(this.htmlTemplate.materials);
                this.main.curModel = this.main.glViewer.model.parts[_id.split('#model_part_')[1]];
            }
        }
        if (this.lastClicked)this.lastClicked.clicked = false;

        if (e.target.getAttribute('data-tab-item'))this.lastTabSelected = e.target.getAttribute('data-tab-item');
    }

    onModelClick(modelId) {
        let pref = '#model_part_',
            _id = pref + modelId,
            target = this.htmlTemplate.tabsHeaderContainer.querySelector('a[href="' + _id + '"]');
        this.htmlTemplate.lastBodyActive.className = this.htmlTemplate.lastBodyActive.className.replace('active', '');
        this.htmlTemplate.lastHeaderActive.className = this.htmlTemplate.lastHeaderActive.className.replace('active', '');
        target.parentNode.className += ' active';
        this.htmlTemplate.lastHeaderActive = target.parentNode;
        this.htmlTemplate.lastBodyActive = this.htmlTemplate.tabsBodiesContainer.querySelector(_id);
        this.htmlTemplate.lastBodyActive.className += ' active';

        if (target.getAttribute('isModel')) {
            this.htmlTemplate.lastBodyActive.appendChild(this.htmlTemplate.materials);
            this.main.curModel = this.main.glViewer.model.parts[modelId];
        }
        let dropdown = this.htmlTemplate.tabsHeaderContainer.querySelector('.dropdown');
        if (target.getAttribute('isSubList')) {
            if (!dropdown.className.match('active'))dropdown.className += ' active';
        } else if (dropdown)dropdown.className = dropdown.className.replace(' active', "");
    }

    updateTemplateFinishes() {
        this._matsList.innerHTML = "";
        let items = 0,
            self = this;
        (function loadImage(list) {
            if (list.length) {
                let iMag = list.shift(),
                    div = document.createElement('div'),
                    i = items++;
                div.className = "col-md-3 col-sm-4 col-xs-4 padd2";
                div.setAttribute("matItem", i);
                div.setAttribute("matName", iMag.name.toLowerCase());
                div.innerHTML +=
                    '   <label class="image-checkbox prod-list">' +
                    '       <div class="thumbnail border-width  ">' +
                    '           <div class="thumb"> ' +
                    '               <img src="' + (Utils.Config.DEFAULT_IMAGE) + '" data-img-mat-preview="' + i + '" alt="' + iMag.name + '> ' +
                    '           </div>' +
                    '           <div class="text-center">' +
                    '               <h6 class="no-margin">' + iMag.name + '</h6>' +
                    '           </div>' +
                    '       </div>' +
                    '       <input type="checkbox" name="image[]" value="">' +
                    '       <i class="fa fa-check hidden"></i>' +
                    '   </label>';

                self._matsList.appendChild(div);
                let image = div.querySelector('img[data-img-mat-preview="' + i + '"]'),
                    src = iMag.preview;

                Utils.Config.loadFile({
                    url: src,
                    onSuccess: (blobUrl)=> {
                        //console.log(image);
                        image.src = blobUrl;
                        if (src.length < 100)Utils.Config.LOADED.IMAGES[src] = blobUrl;
                        loadImage(list);
                    },onError:()=>{loadImage(list);}
                });
            } else {

            }
        })(this.main.materials.concat([]))

    }

    FinishesTemplate() {
        if (this._matsList)return this.updateTemplateFinishes();
        let _matsEdit = document.createElement('div'),
            matItems = this.main.materials,
            self = this,
            curAccId = 'myCarousel' + this.accItems;
        _matsEdit.innerHTML +=
            '<div class="panel-body padd10">' +
            '   <div class="row">' +
            '       <div class="col-sm-12">' +
            '           <div class="form-group nomargin">' +
            '               <label>Finishes(' + matItems.length + ')</label>' +
            '           </div>' +
            '       </div>' +
            '       <div class="col-sm-6">' +
            '           <div class="has-feedback has-feedback-left form-group nomargin">' +
            '               <input class="form-control" placeholder="Search..." type="search">' +
            '               <div class="form-control-feedback"> <i class="icon-search4 text-size-small text-muted"></i> </div>' +
            '           </div>' +
            '       </div>' +
            '   </div>' +
            '   <span class="sep10"></span>' +
            '</div>' +
            '<div class="panel-body accordionheight padd10"></div>';
        let _matsList = _matsEdit.querySelector('.panel-body.accordionheight.padd10'),
            search = _matsEdit.querySelector('input[type="search"]');
        _matsList.innerHTML = '<div class="item active"></div>';
        _matsList = this._matsList = _matsList.firstChild;
        this.updateTemplateFinishes();
        _matsList.addEventListener('click', (e)=>this.selectMat(e));
        search.oninput = function (e) {
            let inputed = this.value || '';
            inputed = inputed.toLowerCase();
            [].forEach.call(_matsList.childNodes, function (el) {
                let matNames = el.getAttribute('matName');
                if (matNames)el.style.display = matNames.match(inputed) ? '' : 'none';
            })
        }
        return _matsEdit;

    }

    ShapeTemplate() {
        if (!this._shapes)this._shapes = new Shapes(this);
        let partsTemplate = document.createElement('div'),
            _parts = this.main.parts;
        //partsTemplate.className = 'panel-body';

        partsTemplate.innerHTML +=

            '<div class="panel-body padd10">' +
            '   <div class="row">' +
            '       <h3 class="slectedtitle"><a href="#"  class="addpattern tab-edit"><i class="icon-plus2"></i> Add</a></h3>' +
            '   </div>' +
            '   <div class="row">' +
            '       <div class="col-sm-6">' +
            '           <div class="has-feedback has-feedback-left form-group nomargin">' +
            '               <input class="form-control" placeholder="Search..." type="search">' +
            '               <div class="form-control-feedback"> <i class="icon-search4 text-size-small text-muted"></i> </div>' +
            '           </div>' +
            '       </div>' +
            '       <div class="col-sm-6" >' +
            '           <div class="form-group" style="padding: 16px;">' +
            '               <label class="checkbox-inline checkbox-right col-sm-12 "  >' +
            '                   <div style="float: left;margin: 0 10px;">Will be replace </div>' +
            '                   <div class="material-switch  ">' +
            '                       <input id="someSwitchOptionDefault2" action="isReplace"   type="checkbox"/>' +
            '                       <label for="someSwitchOptionDefault2" class="label-info"></label>' +
            '                   </div>' +
            '               </label>' +
            '           </div>' +
            '       </div>' +
            '   </div>' +
            '   <span class="sep10"></span>' +
            '</div>' +
            '<div class="panel-body accordionheight padd10"></div>';
        let _partsList = partsTemplate.lastChild,
            search = partsTemplate.querySelector('input[type="search"]');
        _partsList.innerHTML = '<div class="item active"></div>';
        _partsList = _partsList.firstChild;

        partsTemplate.querySelector('a').addEventListener('click', (e)=> {
            this._shapes.toggleModal();
        });
        for (let i = 0, items = _parts; i < items.length; i++) {
            let partName = items[i].name;
            _partsList.innerHTML +=
                '<div class="col-md-3 col-sm-4 col-xs-4 padd2" partItem="' + i + '" parName="' + partName + '">' +
                '   <label class="image-checkbox prod-list">' +
                '       <div class="thumbnail border-width  ">' +
                '           <div class="thumb"> ' +
                '               <img src="' + (Utils.Config.DEFAULT_IMAGE) + '" alt="' + partName + '"> ' +
                '           </div>' +
                '           <div class="text-center">' +
                '               <h6 class="no-margin">' + partName + '</h6>' +
                '           </div>' +
                '       </div>' +
                '       <input type="checkbox" name="parts_radio_items" value="">' +
                '       <i class="fa fa-check hidden"></i>' +
                '   </label>' +
                '</div>';
        }
        _partsList.addEventListener('click', (e)=> {
            e.preventDefault();

            let curItem = e.target,
                partItem;
            if (curItem == e.currentTarget)return;
            while (curItem && !(partItem = curItem.getAttribute('partItem'))) {
                curItem = curItem.parentNode;
            }
            if (partItem) {
                let classes = ' image-checkbox-checked';
                if (this.lastPartSelected)this.lastPartSelected.className = this.lastPartSelected.className.replace(classes, '');
                this.lastPartSelected = curItem.children[0];
                curItem.children[0].className += classes;
                let replacement = partsTemplate.querySelector('input[action="isReplace"]'),
                    _part = _parts[partItem];
                _part.transform = null;
                this.main.applyPart({
                    part: _part, isReplacement: replacement.checked, next: (meshes)=> {
                        if (!meshes || !meshes.length)return;
                        let _mesh = meshes[0];
                        if (replacement.checked) {
                            if (this.main.curModel) {
                                _mesh.material = this.main.curModel.material;
                                this.main.glViewer.createToolTipInfo(_mesh);
                                this.main.glViewer.model.parts[this.main.curModel.uuid] = null;
                            }
                            this.main.curModel = meshes[0];
                            this.main.glViewer.model.parts[this.main.curModel.uuid] = _mesh;
                            //
                            //let _a = this.htmlTemplate.lastHeaderActive.querySelector('a');
                            //_a.setAttribute("href", '#model_part_' + meshes[0].uuid);
                            //_a.innerText = meshes[0].name;
                            //this.htmlTemplate.lastBodyActive.id = 'model_part_' + _mesh.uuid;
                        } else {
                            //this.updateListOfParts();

                        }
                        this.updateListOfParts();
                    }
                });
            }

        });
        search.oninput = function (e) {
            let inputed = this.value || '';
            inputed = inputed.toLowerCase();
            [].forEach.call(_partsList.childNodes, function (el) {
                let matNames = el.getAttribute('parName');
                if (matNames)el.style.display = matNames.match(inputed) ? '' : 'none';
            })
        }

        return partsTemplate;
    }

    PatternTemplate() {

        if (!this._decor)this._decor = this.main.glViewer._decor = new Decor(this.main.glViewer);

        let el = document.createElement('div'),
            curAccId = 'myCarousel' + this.accItems,
            caruselid = 'car' + Date.now(),
            decorid = 'DECOR_' + Date.now()
            ;
        el.innerHTML +=
            '<h3 class="slectedtitle">' +
            '   <label class="checkbox-inline checkbox-right">' +
            '       <input type="checkbox" class="styled">' +
            '           Repeat ' +
            '   </label>' +
            '   <a href="#" data-toggle="modal"  class="addpattern tab-edit"><i class="icon-plus2"></i> Add Pattern</a>' +
            '</h3>' +
            '<div class="panel-body padd5 float">' +
            '   <div class="col-sm-12">' +
            '       <div class="has-feedback has-feedback-left form-group nomargin">' +
            '           <input class="form-control" placeholder="Search..." type="search">' +
            '           <div class="form-control-feedback"> <i class="icon-search4 text-size-small text-muted"></i> </div>' +
            '       </div>' +
            '   </div>' +
            '</div>' +
            '<h3 class="slectedtitle">User Pattern</h3>' +
            '<div class="panel-body accordionheight padd10 float user-pattern"></div>' +
            '<h3 class="slectedtitle">Library Pattern</h3>' +
            '<div class="panel-body accordionheight padd10 float library-pattern"></div>'
        ;

        el.querySelector('a').addEventListener('click', (e)=> {
            this._decor.toggleModal();
        });
        this._decor.decorList = el.querySelector('.user-pattern');

        return el;
    }

    addNewTab(_mesh) {
        let li = document.createElement('li'),
            div = document.createElement('div');
        div.className = 'tab-pane';
        div.id = 'model_part_' + _mesh.uuid;
        li.innerHTML += "<a href=\"#model_part_" + _mesh.uuid + "\" data-toggle=\"model_part_" + _mesh.uuid + "\" isModel=\"1\" " + (this.htmlTemplate.headerParent.className.match("sub-list-h") ? 'isSubList="1"' : '') + ">" + _mesh.name + "</a>";
        this.htmlTemplate.headerParent.appendChild(li);
        this.htmlTemplate.tabsBodiesContainer.appendChild(div);
    }

    addAccTemplate() {

        let _matsEdit = this.htmlTemplate.materials = document.createElement('div'),
            _mainId = 'accordion-control-right',
            accordions = ['Shape', 'Finishes', 'Pattern'];
        _matsEdit.className = "panel-body custom-modal-body";
        _matsEdit.innerHTML = '<div class="panel-group panel-group-control panel-group-control-right content-group-lg" id="' + _mainId + '"></div>';
        let accord = _matsEdit.querySelector('#' + _mainId);
        accordions.forEach((acc, index)=> {
            let elAcc = document.createElement('div');
            elAcc.className = 'panel panel-white';
            accord.appendChild(elAcc);
            elAcc.innerHTML +=
                '<div class="panel-heading padd10">' +
                '<h6 class="panel-title">' +
                '<a data-toggle="collapse" data-parent="#' + _mainId + '" href="#' + (acc + index) + '" aria-expanded="false" class="collapsed">' + acc + '</a>' +
                '</h6>' +
                '</div>' +
                '<div id="' + (acc + index) + '" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;"></div>';
            elAcc.querySelector('#' + (acc + index)).appendChild(this[acc + 'Template']());
            this.accItems = index + 1;
        });


    }

    selectMat(e) {
        e.preventDefault();
        let curItem = e.target,
            matItem;
        if (curItem == e.currentTarget)return;
        while (curItem && !(matItem = curItem.getAttribute('matItem'))) {
            curItem = curItem.parentNode;
        }
        if (matItem) {
            let classes = ' image-checkbox-checked';
            if (this.lastmatSelected)this.lastmatSelected.className = this.lastmatSelected.className.replace(classes, '');
            this.lastmatSelected = curItem.children[0];
            curItem.children[0].className += classes;
            this.main.applyMatterial(
                {
                    index: matItem
                }
            )
        }
    }

    updateListOfParts() {
        let countOfParts = 0,
            hasMore = true,
            headerParent = this.htmlTemplate.tabsHeaderContainer,
            tabsBodiesContainer = this.htmlTemplate.tabsBodiesContainer;
        headerParent.innerHTML = '';
        tabsBodiesContainer.innerHTML = '';
        this.main.glViewer.model.parts = {};
        this.main.glViewer.model.traverse((child)=> {
            if (child.type == Utils.Config.MODELS.TYPES.MESH) {
                this.main.glViewer.model.parts[child.uuid] = child;
                tabsBodiesContainer.innerHTML += "<div class=\"tab-pane\" id=\"model_part_" + child.uuid + "\"></div>";
                if (countOfParts++ > 2 && hasMore) {
                    hasMore = false;
                    headerParent.innerHTML += " <li class=\"dropdown\">" +
                        "<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">More<span class=\"caret\"></span></a>" +
                        "<ul class=\"dropdown-menu dropdown-menu-right sub-list-h\"></ul></li>";
                    headerParent = headerParent.querySelector('ul');
                }
                headerParent.innerHTML += "<li><a href=\"#model_part_" + child.uuid + "\" data-toggle=\"model_part_" + child.uuid + "\" isModel=\"1\" " + (hasMore ? '' : 'isSubList="1"') + ">" + child._name + "</a></li>";
            }
        });
        this.htmlTemplate.headerParent = headerParent;
        this.onTabSelect({target: this.htmlTemplate.tabsHeaderContainer.children[0].children[0]});
    }
}

export class Shapes {
    constructor(costumize) {
        let parent = this.container = document.createElement('div');
        this.container.className = "modal fade";
        document.body.appendChild(this.container);
        this.container.innerHTML =
            '<div class="modal-dialog modal-lg webgl-view-el">' +
            '   <div class="modal-content">' +
            '       <div class="modal-header">' +
            '           <button type="button" class="close" data-dismiss="modal">&times;</button>' +
            '           <h5 class="modal-title">Add Shape</h5>' +
            '       </div>' +
            '       <div class="modal-body">' +
            '           <hr>' +
            '           <div class="form-group region-style popuptoken" id="displayImage">' +
            '               <div class="panel-body padd2">' +
            '                   <div class="col-one">' +
            '                       <div class="selectimg float">' +
            '                           <form action="/" class="dropzone patternmodal" id="dropzone_single1"></form>' +
            '                       </div>' +
            '                   </div>' +
            '               </div>' +
            '           </div>' +
            '           <button type="submit" class="btn btn-primary legitRipple">Add</button>' +
            '        </div>' +
            '    </div>' +
            '</div>';

        this.container.setAttribute('data-dismiss', "modal");
        this.container.addEventListener('click', (e)=> {
            if (e.target.getAttribute('data-action') == 'close' || e.target.getAttribute('data-dismiss') == "modal")this.toggleModal();
        });
        var myDropzone = this.myDropzone = new Dropzone(this.container.querySelector("form.dropzone"), {
            autoProcessQueue: false,
            acceptedFiles: '.obj',
            dictDefaultMessage: 'Drop file to upload <span>or CLICK</span>',
            maxFiles: 1
        });
        myDropzone.on("addedfile", function (file) {
            if (this.fileTracker) {
                this.removeFile(this.fileTracker);
            }
            this.fileTracker = file;
            var reader = new FileReader();
            reader.onload = function (e) {
                myDropzone.obj = e.target.result;
            };
            reader.readAsText(file)
        });
        this.container.querySelector('button[type="submit"]').addEventListener('click', (e)=> {
            let _listChild = costumize.main.glViewer._onUploadModel(myDropzone.obj);
            costumize.updateListOfParts();
            this.toggleModal();
        });
    }

    toggleModal() {
        let hasCl = this.container.className.indexOf('in') > -1;
        if (hasCl) {
            this.container.className = this.container.className.replace(' in', "");
        } else {
            this.container.className += ' in';
        }
        this.container.style.display = hasCl ? '' : 'block';
    }
}