import {GlViewer} from "./viewer/glViewer.js";
import {Utils} from "../utils.js";
import {MHttp} from "./helpers/Http.js";
import {Command} from "./helpers/Command.js";
import {PreSet} from "./preset/preset.js";
import {Costumize} from "./Costumize.js";
import {Configuration,ProductPart,ProductPattern} from "./Configuration.js";
import {Interfaces} from "../data.structure/interfaces.js"
import {ModelCustomImage} from "./viewer/ModelCustomImage.js"
import {TESTProductAction} from "./configuration/index.js"
import async from 'async'

/**
 * Class representing a main action.
 */
export class UniApp {
    /**
     *@constructor
     *
     */
    constructor(opt) {

        THREE.Mesh.prototype.getProductMaterial = function (hard) {

            if (!this.material) return new Error('can`t extract the material');
            let mesh = this,
                _matr = this.material._mat;
            if (_matr && !hard) {

            } else {
                let _mat = _matr = new Interfaces.IMaterial();
                _mat.name = mesh.material.name;
                _mat.maps = new Interfaces.IMaterialMaps();
                _mat.mapsRS = new Interfaces.IMaterialMaps();

            }

            _matr.settings = new Interfaces.IMaterialSettings(mesh.material);
            if (this.material.envMap && mesh.material.envMap.image instanceof Array) {
                _matr.maps.envMap = mesh.material.envMap.image.map((map)=> { return map.src; });
            }

            return mesh;

        };

        THREE.Mesh.prototype.applyMatSettings = function (settings) {
            if (!settings)return console.warn('no settings are selected');
            let material = this.material;
            for (let setting in  settings) {
                let val = ( settings[setting]);
                if (settings.hasOwnProperty(setting) && !Utils.Config.isUndefined(val)) {
                    if (setting == 'color') {
                        if (!Utils.Config.isUndefined(val)) {
                            if (!val.match) {
                                material[setting].setHex(val);
                            } else if (val.match('#')) {
                                material[setting].set(val);
                            } else if (val.match('rgb')) {
                                material[setting].setStyle(val);
                            } else {
                                material[setting].setHex(val);
                            }

                        }
                    } else {
                        val = parseFloat(val);
                        if (Utils.Config.isNumber(val))material[setting] = val;
                    }
                }
            }
            material.needsUpdate = true;
        };
        THREE.Mesh.prototype.middle = function () {
            this.updateMatrix();
            this.updateMatrixWorld();
            var middle = new THREE.Vector3();
            var geometry = this.geometry;
            geometry.computeBoundingBox();
            geometry.computeBoundingSphere();

            middle.x = (geometry.boundingBox.max.x + geometry.boundingBox.min.x) / 2;
            middle.y = (geometry.boundingBox.max.y + geometry.boundingBox.min.y) / 2;
            middle.z = (geometry.boundingBox.max.z + geometry.boundingBox.min.z) / 2;
            this.middleV = middle;


            var devide = new THREE.Vector3();
            if (this.parent) {
                this.parent.updateMatrixWorld();
                devide.copy(this.parent.position).negate();
            } else {
                this.updateMatrixWorld();
            }
            var vector = new THREE.Vector3();
            vector.setFromMatrixPosition(this.matrixWorld);
            this._pst = vector.clone();
            vector.add(devide);
            this.middleV = vector;

        };
        THREE.Mesh.prototype.getMatrixR = function (isInner) {
            let

                mesh = this,
                translate = new Matrix4x4(),
                rotation = [new Matrix4x4(), new Matrix4x4(), new Matrix4x4()],
                scale = new Matrix4x4(),
                genPst = mesh.parent.position.clone();//.add(mesh.position);

            translate.set_translation_elements(genPst.x, genPst.y, genPst.z);
            //rotation[0].set_rotation(new THREE.Vector3(-1, 0, 0), mesh.rotation.x);
            rotation[1].set_rotation(new THREE.Vector3(0, -1, 0), /*mesh.rotation.y +*/ mesh.parent.angleRadians ? mesh.parent.angleRadians : 0);
            //rotation[2].set_rotation(new THREE.Vector3(0, 0, -1), mesh.rotation.z);
            scale.set_scaling(mesh.scale.x, mesh.scale.y, mesh.scale.z);
            return translate.multiply(rotation[0].multiply(rotation[1]).multiply(rotation[2])).multiply(scale);
        };
        THREE.Mesh.prototype.getVolume = function () {
            if (!this._geo) {
                this._geo = new THREE.Geometry();
                this._geo.fromBufferGeometry(this.geometry);
            }

            let triangles = this._geo.faces,
                _vert = this._geo.vertices,
                square = 0,
                volume = 0;
            for (let i = 0; i < triangles.length; i++) {
                let _triangle = triangles[i],
                    v1 = _vert[_triangle.a],
                    v2 = _vert[_triangle.b],
                    v3 = _vert[_triangle.c];
                square += Utils.Config.getSquareOfTriangle(v1, v2, v3);
                volume += Utils.Config.getVolumeOfTriangle(v1, v2, v3);
            }
            let boxHelper = new THREE.BoxHelper(this),
                box = new THREE.Box3().setFromObject(this);
            //boxHelper.geometry.computeBoundingBox();
            boxHelper.geometry.computeBoundingSphere();
            boxHelper.square = square;
            boxHelper.volume = volume;
            boxHelper._size = box.size();
            return boxHelper;
        }
        THREE.Object3D.prototype.getChildByOriginName = function (name) {
            let res;
            this.traverse((child)=> {
                if (child.userData.originName == name || child.name == name)return res = child;
            })
            return res;
        }
        THREE.Loader.Handlers.add(/\.dds$/i, new THREE.DDSLoader());
        Node.prototype._display = function (disp) {
            this.style.display = disp ? 'block' : 'none';
        };
        /*Image.prototype.load = function (url) {
         var thisImg = this;
         var xmlHTTP = new XMLHttpRequest();
         xmlHTTP.open('GET', url, true);
         xmlHTTP.responseType = 'arraybuffer';
         xmlHTTP.onload = function (e) {
         var blob = new Blob([this.response]);
         thisImg.src = window.URL.createObjectURL(blob);
         };
         xmlHTTP.onprogress = function (e) {
         thisImg.completedPercentage = parseInt((e.loaded / e.total) * 100);
         if (thisImg.onprogress)thisImg.onprogress(thisImg.completedPercentage);
         };
         xmlHTTP.onloadstart = function () {
         thisImg.completedPercentage = 0;
         if (thisImg.onStartLoad)thisImg.onStartLoad();
         };
         xmlHTTP.send();
         };
         window.addEventListener("error", function (e) {
         if (e && e.target && e.target.nodeName && e.target.nodeName.toLowerCase() == "img") {
         e.target.src = Utils.Config.DEFAULT_IMAGE;
         }
         }, true);*/

        //var tempCanvas = document.createElement('canvas');
        document.body.addEventListener("load", function (e) {
            if (e && e.target && e.target.nodeName && e.target.nodeName.toLowerCase() == "img") {

                Utils.Config.LOADED.IMAGES[e.target.src] = e.target;
                THREE.Cache.add(e.target.src, e.target);
                /*tempCanvas.width = e.target.naturalWidth;
                 tempCanvas.height = e.target.naturalHeight;
                 let tempCntx = tempCanvas.getContext('2d');
                 tempCntx.drawImage(e.target, 0, 0,tempCanvas.width,tempCanvas.height);
                 tempCanvas.toBlob((blobData)=> {
                 Utils.Config.LOADED.IMAGES[e.target.src] = window.URL.createObjectURL(blobData);
                 });*/
            }
        }, true);
        THREE.ImageUtils.crossOrigin = '';
        Image.prototype.completedPercentage = 0;
        //Image.prototype.crossOrigin = 'anonymous';
        String.prototype.json = function () {
            if (this.length < 2)return this;
            return JSON.parse(this)
        };

        this.materials = [];
        this.urlParams = {};
        this.options = {};
        this.commands = {
            loadedModels: [],
            loadedModelsPst: [],
            loadedModelsDetail: [],
            materials: {}
        };

        let urlObj = location.href.split("?")[1];
        if (urlObj)urlObj = urlObj.split("&");
        if (urlObj) {
            for (let i = 0; i < urlObj.length; i++) {
                let _obj = urlObj[i].split("=");
                if (_obj.length > 1) {
                    this.urlParams[_obj[0]] = _obj[1];
                }
            }
        }
        if (urlObj && urlObj.length > 1 && location.href.match("model")) {
            for (let i = 0; i < urlObj.length; i++) {
                if (urlObj[i].match("model")) {
                    urlObj = urlObj[i].split("=")[1];
                    urlObj = urlObj.split(".")[0] + "/" + urlObj;
                    break;
                }
            }

        } else {
            urlObj = "01cccuTReRgODMLQwucA_DSCN0142.obj";
        }
        this.curMatCategories = [];//list of materials of selected category
        this.curMats = [];//list of materials of selected category
        this.matCategories = ["All"];//lis
        this.applyRoomsList([
            {
                _id: "room_0",
                modelUrl: Utils.Config.REALITY_DOMAIN_REF + Utils.Config.MODELS.URL.ROOMS + 'room/room.obj',
                preview: Utils.Config.REMOTE_DATA + Utils.Config.FILE.DIR.PROJECT_ROOM_PREVIEW + 'room-img-07-480x320.jpg',
                camera: {
                    m_world_to_obj: {
                        "ww": 1,
                        "wz": -2481.6921481040126,
                        "wy": 866.61129992492,
                        "wx": 130.08310644299536,
                        "zw": 0,
                        "zz": 0.32192535988403675,
                        "zy": -0.11032174072771123,
                        "zx": -0.9403154663124185,
                        "yw": 0,
                        "yz": 0.3241858317821754,
                        "yy": 0.9459934177739817,
                        "yx": 0,
                        "xw": 0,
                        "xz": 0.8895322417626201,
                        "xy": -0.3048369515841356,
                        "xx": 0.3403040167462895
                    },
                    origin: {x: 2172.296006717376, y: -692.6450999905709, z: 1314.57426658189},
                    m_focal: 50,
                    m_aperture: 100,
                    far: 10000,
                },
                preset: [
                    {
                        _id: "quad_0",
                        position: {"x": 1234, "y": -730, "z": -911},
                        quad: {
                            x: 0.9,
                            y: 0.55
                        }
                    }, {
                        _id: "quad_1",
                        position: {"x": 937, "y": -790, "z": 651},
                        quad: {
                            x: 0.4,
                            y: 0.7
                        }
                    }]
            },
            {
                _id: "room_0",
                modelUrl: Utils.Config.REALITY_DOMAIN_REF + Utils.Config.MODELS.URL.ROOMS + 'Inner_Apt145_AptType016/Inner_Apt145_AptType016.obj',
                preview: Utils.Config.REMOTE_DATA + Utils.Config.FILE.DIR.PROJECT_ROOM_PREVIEW + 'room-img-07-480x320.jpg',
                camera: {
                    m_world_to_obj: {
                        "ww": 1,
                        "wx": -203,
                        "wy": 133,
                        "wz": -320.52256482150733,
                        "xw": 0,
                        "xx": -0.95495090056124139,
                        "xy": 0.3624786312088406,
                        "xz": -0.7527609811007884,
                        "yw": 0,
                        "yx": -6.845793075767685e-8,
                        "yy": 0.9009837975107693,
                        "yz": 0.43385273610185826,
                        "zw": 0,
                        "zx": 0.8354877932985325,
                        "zy": 0.23840603713001626,
                        "zz": -0.49509868582850214,
                    },
                    m_focal: 50,
                    m_aperture: 100,
                    far: 10000,
                },
                sun_dir: {"x": -0.743629442359553849, "y": 0.2301168728919780351, "z": -0.024483192611724406},
                preset: [
                    {
                        _id: "quad_0",
                        position: {"x": 1234, "y": -730, "z": -911},
                        quad: {
                            x: 0.9,
                            y: 0.55
                        }
                    }, {
                        _id: "quad_1",
                        position: {"x": 937, "y": -790, "z": 651},
                        quad: {
                            x: 0.4,
                            y: 0.7
                        }
                    }]
            }
        ]);
        this.http = new MHttp();
        this.glViewer = new GlViewer(opt, {
            component: this,
            options: {}
        });
        this.glViewer.options = this.options;
    }

    contactLocalCmds() {
        let _cmds = this.commands,
            result = [],
            cmndsOrder = [
                "loadedModels",
                "materials",
                "loadedModelsDetail",
                "loadedModelsPst"
            ];
        for (let i = 0; i < cmndsOrder.length; i++) {
            let field = cmndsOrder[i];
            if (_cmds.hasOwnProperty(field)) {
                if (_cmds[field] instanceof Array) {
                    result = result.concat(_cmds[field]);
                    //_cmds[field] = [];
                } else {
                    for (let subField in _cmds[field]) {
                        if (_cmds.hasOwnProperty(field)) {
                            if (_cmds[field][subField] instanceof Array) {
                                result = result.concat(_cmds[field][subField]);
                                //delete _cmds[field][subField];
                            }
                        }
                    }
                }

            }
        }

        return result;
    }

    _uploadScene() {
        let result;
        try {
            let _opt = this.glViewer._onExportModel();
            result = (this.http.formData(_opt.form));
        } catch (e) {
        } finally {
            return result.json();
        }
    }

    /**
     * sync load and parse files from zip, see structure of zip https://prnt.sc/iia1e9
     * @param {Object} opt -  arguments for load zip
     * @param {string} [opt.url = null] -  absolute zip file path
     * @return {IZipStructure}  - default parsed structure
     * */
    loadZip(opt) {
        if (!opt || !opt.url)return console.error('Arguments not provided');
        var zip = new JSZip();
        return zip.loadAsync(
            new Promise(function (resolve, reject) {
                JSZipUtils.getBinaryContent(opt.url, function (err, data) {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(data);

                    }
                });
            })
        ).then(function (zip) {

            let result = new Interfaces.IZipStructure();

            (function parseZip() {
                for (let field in zip.files) {

                    if (zip.files.hasOwnProperty(field) && zip.files[field]) {
                        let _fileOriginName = zip.files[field].name,
                            _fileName = (_fileOriginName + "").toLowerCase(),
                            next = ()=> {
                                zip.files[field] = null;
                                delete zip.files[field];
                                parseZip();
                            },
                            error = ()=> {
                                console.warn(_fileOriginName + ' Was not loaded');
                                next();
                            };


                        if (_fileName.match(Utils.Config.ZIP_DIR.MATERIALS)) {
                            let dirName = _fileName.split("/")[1];
                            if (!dirName)return error();

                            if (!result.materials[dirName]) {
                                result.materials[dirName] = (new Interfaces.IMaterial({name: dirName}));
                            }

                            for (let i = 0; i < Utils.Config.ZIP_DIR.MAT_MAPS.length; i++) {
                                let _map = Utils.Config.ZIP_DIR.MAT_MAPS[i],
                                    __map = Utils.Config.MAT_MAPS[i];

                                for (let di = 0; di < _map.length; di++) {
                                    if (_fileName.match(_map[di])) {

                                        return loadFile(_fileOriginName, function (blob) {
                                            result.materials[dirName].maps[__map] = URL.createObjectURL(blob);
                                            next();
                                        }, error);


                                    }
                                }
                            }
                        } else if (_fileName.match(Utils.Config.ZIP_DIR.DECORATIONS)) {
                            let dec = new Interfaces.IPattern();
                            return loadFile(_fileOriginName, function (blob) {
                                result.decorations.push(dec);
                                dec.image = URL.createObjectURL(blob);
                                next();
                            }, error);
                        } else if (_fileName.match(Utils.Config.ZIP_DIR.PARTS)) {
                            let part = new Interfaces.IProductModelPart();
                            return loadFile(_fileOriginName, function (blob) {
                                result.parts.push(part);
                                part.absUrl = URL.createObjectURL(blob);
                                part.originName = _fileName;
                                next();
                            }, error);
                        } else {

                            return loadFile(_fileOriginName, function (blob) {
                                result.files[_fileOriginName] = URL.createObjectURL(blob);
                                next();
                            }, error);
                        }
                    }
                }
                if (opt.next)opt.next(result);
                return result;
            })()

            function loadFile(_fileName, next, error) {
                let _f = zip.file(_fileName);
                if (!_f || _f.dir)return error();
                _f.async("blob").then(next).catch(error);
            }

        }).catch((error)=> {
            console.warn(error);
        });
    }

    /**
     * sync upload current models in obj/mtl to remote server
     * @return {string} - string format for obj/mtl files pathes
     * */
    uploadModel() {
        let responce = "", result;
        try {
            let _opt = this.glViewer._onExportModel();
            result = this._uploadScene();
            if (result.status) {
                for (let i = 0; i < result.data.length; i++) {
                    if (result.data[i].originName.match('.obj')) {
                        responce += "data?file=" + Utils.Config.REMOTE_DATA + result.data[i].newName;
                    }
                }
                return responce;
            } else {
                return "error?message=nothing to upload";
            }
        } catch (e) {
            return "error?message=cannot upload";
        }
    }

    loadModel(opt) {
        if (!opt)opt = {};
        let urlObj = /*"01cccuTReRgODMLQwucA_DSCN0142";//*/"Latern_1";
        urlObj = urlObj + "/" + urlObj + ".obj";
        this.commands = {
            loadedModels: [],
            loadedModelsPst: [],
            loadedModelsDetail: [],
            materials: {}
        };
        this.options = {
            modelName: opt.url,
            modelTransform: {
                "scale": 145,
                "position": {
                    "room_0": {
                        "quad_0": {"x": 1234, "y": -730, "z": -911},
                        "quad_1": {"x": 937, "y": -790, "z": 651}
                    }
                }
            },
            onFinish: (commands)=> {
                if (opt.next)opt.next();
                /* this.http.get( Utils.Config.REMOTE_DATA + 'materials.json', null, (res)=> {
                 let materials = this.materials = res.json();
                 this.MAPS = Utils.Config.MAT_MAPS.concat([]);
                 let parameters = {
                 "map": "Material Diffuse Image Path",
                 "metalnessMap": "Material Metalness Image Path",
                 "roughnessMap": "Material Roughness Image Path",
                 "normalMap": "Material Normal Image Path",
                 "displacementMap": "Material Displacement Image Path",
                 "bumpMap": "Material Cavity Image Path"
                 }, parametersAlternative = {
                 "map": "Thumbnail",
                 "metalnessMap": "Material Metalness",
                 "roughnessMap": "Material Roughness",
                 "normalMap": "Material Normal",
                 "displacementMap": "Material Displacement",
                 "bumpMap": "Material Cavity"
                 };
                 for (let i = 0; i < materials.length; i++) {
                 if (this.matCategories.indexOf(materials[i].Category) < 0) {
                 this.matCategories.push(materials[i].Category);
                 }
                 materials[i].name = materials[i].name || materials[i]['Material name'];
                 materials[i].id = i;
                 for (let d = 0; d < this.MAPS.length; d++) {
                 materials[i][this.MAPS[d]] = materials[i][(parameters[this.MAPS[d]] || parametersAlternative[this.MAPS[d]])];
                 if (d == 0)materials[i].preview = materials[i][this.MAPS[d]];
                 }
                 }
                 this.selectedMatCat = this.matCategories[0];
                 this.filter(null, Utils.Config.MODELS.TYPES.MATERIAL);

                 this.http.get(Utils.Config.MODELS.URL.MAIN + './parts.json', null, (parts)=> {
                 this.parts = parts.json();
                 this._preset = new PreSet(this);
                 this.commands.loadedModels = [
                 new Command(Command.REQUEST.SCENE.IMPORT_EL, {
                 block: "true",
                 filename: Utils.Config.FILE.DIR.PREVIOUS + Utils.Config.MODELS.URL.CUPS + urlObj
                 }),
                 new Command(Command.REQUEST.GROUP.ATTACH, {
                 group_name: Command.SCENE.MAIN_GROUP,
                 item_name: Command.SCENE.LOADED_GROUP
                 })
                 ];
                 this.commands.loadedModelsPst = [];
                 this.commands.loadedModelsDetail = [];
                 this.commands.materials = {};
                 this.customize = new Costumize(this);
                 });
                 }); */
            }
        };
        if (!this.options.modelName)this.options.modelName = Utils.Config.REMOTE_DATA + Utils.Config.MODELS.URL.CUPS + urlObj;

        this.glViewer.options = this.options;
        this.dispoce();
        this.glViewer.loadModel(()=> {
            opt.next();
            this.commands.loadedModels = [
                new Command(Command.REQUEST.SCENE.IMPORT_EL, {
                    block: "true",
                    filename: opt.url ? opt.url : Utils.Config.REALITY_DOMAIN_REF + Utils.Config.MODELS.URL.CUPS + urlObj
                })
            ];
            this.commands.loadedModelsPst = [];
            this.commands.loadedModelsDetail = [];
            this.commands.materials = {};
            this.commands_model = [];
        });
    }

    loadMaterials(opt) {
        if (opt.materials) {
            let materials = this.materials = [],
                _mat = opt.materials,
                matches = [ ];
            this.MAPS = Utils.Config.MAT_MAPS.concat([]);
            for (let f in _mat) {
                if (_mat.hasOwnProperty(f))materials.push(_mat[f]);
            }
            //custom convert materials list
            for (let i = 0; i < materials.length; i++) {
                let _curMat = materials[i];
                materials[i].name = materials[i].name || materials[i]['Material name'] || materials[i]['finishes_Name'] || materials[i].id || 'Material ' + i;
                materials[i].itemNum = i;
                materials[i].Category = '';
                for (let d = 0; d < this.MAPS.length; d++) {
                    let _or = this.MAPS[d];
                    for (let _mapId = 0; _mapId < matches.length; _mapId++) {
                        let _curM = matches[_mapId];
                        for (let subI = 0, _listS = _curM[_or].split("."), editMat = _curMat; subI < _listS.length; subI++) {
                            if (editMat[_listS[subI]]) {
                                editMat = editMat[_listS[subI]];
                            }
                            if (subI == _listS.length - 1) {
                                if (editMat)_curMat[_or] = editMat;
                                if (d == 0 && !_curMat.preview)_curMat.preview = editMat;
                            }
                        }
                    }
                }
            }
            if (opt.next)opt.next();
        } else {
            this.http.get(opt.url || (Utils.Config.REMOTE_DATA + 'models/materials.json'), null, (res)=> {
                let materials = this.materials = res.json();
                this.MAPS = Utils.Config.MAT_MAPS.concat([]);
                let parameters = {
                    "map": "Material Diffuse Image Path",
                    "metalnessMap": "Material Metalness Image Path",
                    "roughnessMap": "Material Roughness Image Path",
                    "normalMap": "Material Normal Image Path",
                    "displacementMap": "Material Displacement Image Path",
                    "bumpMap": "Material Cavity Image Path"
                }, parametersAlternative = {
                    "map": "Thumbnail",
                    "metalnessMap": "Material Metalness",
                    "roughnessMap": "Material Roughness",
                    "normalMap": "Material Normal",
                    "displacementMap": "Material Displacement",
                    "bumpMap": "Material Cavity"
                };
                for (let i = 0; i < materials.length; i++) {
                    if (this.matCategories.indexOf(materials[i].Category) < 0) {
                        this.matCategories.push(materials[i].Category);
                    }
                    materials[i].name = materials[i].name || materials[i]['Material name'];
                    materials[i].id = i;
                    for (let d = 0; d < this.MAPS.length; d++) {
                        materials[i][this.MAPS[d]] = materials[i][(parameters[this.MAPS[d]] || parametersAlternative[this.MAPS[d]])];
                        if (d == 0)materials[i].preview = materials[i][this.MAPS[d]];
                    }
                }
                this.selectedMatCat = this.matCategories[0];
                this.filter(null, Utils.Config.MODELS.TYPES.MATERIAL);
                if (opt.next)opt.next();
            });
        }
    }

    loadParts(opt) {
        if (opt.parts) {
            this.parts = [{parts: opt.parts}];
            if (opt.next)opt.next();
        } else {
            this.http.get(opt.url || (Utils.Config.REMOTE_DATA + 'models/parts.json'), null, (parts)=> {
                this.parts = parts.json();
                if (opt.next)opt.next();
            });
        }
    }

    /**
     * call it to tggle visibility of background env
     * @param {boolean} [shows = false] -  show/hide background
     * */
    toggleBackVisible(shows) {

        let app = this.glViewer,
            show = !!shows;
        switch (app.lastBackSelected) {
            case 'suyn':
            {
                app.sun.visible = show;
                break;
            }
            case 'hdr':
            {
                app.hdr.visible = show;
                break;
            }
            case 'garage':
            {
                app.garage.visible = show;
                break;
            }
            default :
            {
                if (app.scene.background) app.scene.backgroundLast = app.scene.background;
                if (show) {
                    app.scene.background = app.scene.backgroundLast;
                } else {
                    app.scene.background = null;
                }
                break;
            }
        }
        app.refresh();

    }

    getFromLibrary( src )
    {
        if(!this.texturesLibrary) window.texturesLibrary = this.texturesLibrary = []; // {src: "", texture: THREE.Texture}
        var foundTexture = null;
        for(var i = 0; i < this.texturesLibrary.length; i++)
        {
            if(this.texturesLibrary[i].src === src)
            {
                foundTexture = this.texturesLibrary[i];
                break;
            }
        }
        return foundTexture;
    }

    addToLibrary( src, texture, stayAlive )
    {
        if(!this.texturesLibrary) this.texturesLibrary = []; // {src: "", texture: THREE.Texture, stayAlive: false}
        if(this.getFromLibrary( src )) return false;
        this.texturesLibrary.push({src:src, texture:texture, stayAlive: stayAlive || false});
    }

    clearMemory()
    {
        if(!this.texturesLibrary) this.texturesLibrary = []; // {src: "", texture: THREE.Texture}
        var usedTextures = [];
        this.glViewer.scene.traverse((obj)=>{
            if(obj.material)
                for(var key in obj.material)
                    if(obj.material[key] instanceof THREE.Texture)
                        usedTextures.push(obj.material[key]);
        });
        var texturesToDelete = [];
        for(var i = 0; i < this.texturesLibrary.length; i++)
        {
            var isUsed = false;

            for(var j = 0; j < usedTextures.length; j++)
            {
                if(usedTextures[j] === this.texturesLibrary[i].texture)
                {
                    isUsed = true;
                    break;
                }
            }

            if(!isUsed)
                texturesToDelete.push(this.texturesLibrary[i])
        }

        for(var i = 0; i < texturesToDelete.length; i++)
        {
            if (texturesToDelete[i].stayAlive) continue;
            texturesToDelete[i].texture.dispose();
            this.texturesLibrary.splice(this.texturesLibrary.indexOf(texturesToDelete[i]), 1);
        }

    }

    selectMaterial(opt) {

        let mat = opt.material ? opt.material : this.materials[opt.index],
            __next = ()=> {
                if (opt.next)opt.next();
            },
            _parts = opt.parts;



        let selectedModel = opt.model ? opt.model : this.curModel,
            selectedModels = [];

        if (_parts && _parts.length) {
            for (var i = 0; i < _parts.length; i++) {
                let _m = this.glViewer.model.getObjectByName(_parts[i]);
                if (_m) {
                    selectedModels.push(_m);
                } else {
                    console.log("-------THE model " + _parts[i] + " was not found on scene")
                }

            }
        } else if (!(selectedModel instanceof THREE.Mesh)) {
            console.error('selected model should be Mesh');
            return __next();
        }

        if (!selectedModels.length && selectedModel) selectedModels.push(selectedModel);


        this.glViewer.preloader.fade(true);
        let resultsMap = {};
        let loader = this.glViewer.textureLoader;
        if (!this.MAPS) this.MAPS = Utils.Config.MAT_MAPS.concat(['panoramMap']);
        let mode = this.MAPS,
            self = this;


        let maps = mat.maps;

        selectedModels.forEach((model, i)=> {

            model.material.dispose();
            model.material = new THREE.MeshStandardMaterial();
            model.material.category = 1;
            model.material.roughness = 0.9;
            model.material._mat = mat;

            async.series([
                    (done) => { maps.map ? this.loadTexture('map', maps.map, mat.repeat, model, done) : done()},
                    (done) => { maps.metalnessMap ? this.loadTexture('metalnessMap', maps.metalnessMap, mat.repeat, model, done) : done()},
                    (done) => { maps.normalMap ? this.loadTexture('normalMap', maps.normalMap, mat.repeat, model, done) : done()},
                    (done) => { maps.bumpMap ? this.loadTexture('normalMap', maps.bumpMap, mat.repeat, model, done) : done()},
                    (done) => { maps.alphaMap ? this.loadTexture('alphaMap', maps.alphaMap, mat.repeat, model, done) : done()},
                    (done) => { maps.roughnessMap ? this.loadTexture('roughnessMap', maps.roughnessMap, mat.repeat, model, done) : done()},
                    (done) => { maps.displacementMap ? this.loadTexture('displacementMap', maps.displacementMap, mat.repeat, model, done) : done()},
                    (done) => {
                        if(maps.envMap){
                            this.loadTexture('envMap', maps.envMap, model, done)
                        } else if (self.mainSettings && self.mainSettings.local_render.envMap) {
                            let suffix = '';
                            if (mat.maps.alphaMap) {
                                suffix = 'refraction'
                            }else {
                                suffix = 'reflection'
                            }
                            let onLoad = ( env ) => {
                                if (mat.maps.alphaMap) {
                                    env.mapping = THREE.EquirectangularRefractionMapping;
                                    env.refractionRatio = 0.98;
                                    env.reflectivity = 0.9;
                                    env.roughness = 1;

                                    env.metalness = 1;
                                    env.magFilter = THREE.LinearFilter;
                                    env.minFilter = THREE.LinearMipMapLinearFilter;
                                }
                                model.material.envMap = env;
                                model.material.needsUpdate = true;
                                done();
                            };

                            let fromLibrary = this.getFromLibrary(self.mainSettings.local_render.envMap + suffix);
                            if(fromLibrary) onLoad(fromLibrary.texture);
                            else {
                                let env = new THREE.TextureLoader().load(self.mainSettings.local_render.envMap,
                                    // onLoad callback
                                    ( texture ) => {

                                        this.addToLibrary(self.mainSettings.local_render.envMap  + suffix, texture, true);
                                        onLoad(texture);
                                    });
                                env.mapping = THREE.EquirectangularReflectionMapping;
                            }

                        } else {
                            done();
                        }
                    },
                ],
                (err) => {
                    if(err){
                        console.warn(err)
                    } else {
                        this.glViewer.preloader.fade();
                        this.glViewer.play();
                        if(opt.next && i == 0) opt.next()
                    }
                });


        })
        this.clearMemory();

    }

    loadTexture(key, url, repeat, model, done) {
        let onLoad = // onLoad callback
            ( texture ) => {
                model.material[key] = texture;
                model.material.side = THREE.DoubleSide;
                model.material[key].wrapS = model.material[key].wrapT = THREE.RepeatWrapping;
                model.material[key].anisotropy = this.glViewer.anisotropy;
                if(key === 'displacementMap') model.material.displacementScale = 0.01;
                if(key === 'alphaMap') {
                    model.material.transparent = true;
                }
                model.material.needsUpdate = true;
                model.material[key].repeat.set( repeat ? repeat.x : 1, repeat ? repeat.y : 1 );
                done();
            };
        let fromLibrary = this.getFromLibrary(url);
        if(fromLibrary) onLoad(fromLibrary.texture);
        else
            this.glViewer.textureLoader.load(url, (texture) => {
                this.addToLibrary(url, texture);
                onLoad(texture);
            } );
    }

    createPreset(mat) {
        let material = this.glViewer._defMaterial();
        //material.color = new THREE.Color();

        if (!mat["Category"])mat["Category"] = '';

        return material;
    }

    /**
     * call it to change the part
     * @param {Object} [opt = null] -  settings for loaded parts data  __REQUIRED__
     * @param {Number} [opt.index = null] -  item index from loaded parts data. __REQUIRED__
     * @param {Function} [opt.next = null] -  callback on finish load part
     * @param {Boolean} [opt.isReplacement = null] -  if true will replace APP.curModel
     * */
    changeModelPart(opt) {
        let selectedPart = this.parts[0].parts[opt.index],
            testIndex = 514 + Utils.Config.DELIMETER,
            url = selectedPart.model ? selectedPart.model.url : Utils.Config.REMOTE_DATA + Utils.Config.MODELS.URL.CUPS_DECOR + testIndex + selectedPart.name;
        if (!selectedPart)return console.error('no such part');
        this.options.modelName = url;
        this.options.params = selectedPart ? selectedPart : selectedPart.params;

        this.options.params.modelToChange = this.curModel;
        this.options.params.isReplacement = opt.isReplacement;
        this.glViewer.preloader.fade(true);
        this.glViewer.loadModel((commands = [])=> {
            if (opt.next)opt.next(this.options.params.listParts);
            this.options.params = false;
            this.glViewer.refresh();
            this.glViewer.preloader.fade();
            this.commands.loadedModelsDetail.push(...(commands.concat([
                new Command(Command.REQUEST.SCENE.IMPORT_EL, {
                    block: "true",
                    filename: Utils.Config.REALITY_DOMAIN_REF + Utils.Config.MODELS.URL.CUPS_DECOR + testIndex + (selectedPart.model ? selectedPart.model.name : selectedPart.name)
                })/*,
                 new Command(Command.REQUEST.GROUP.ATTACH, {
                 group_name: Command.SCENE.MAIN_GROUP,
                 item_name: Command.SCENE.LOADED_GROUP
                 })*/
            ])));//.concat(this.glViewer.updateSceneMeshMatrix()));
        });

    }

    filter(ev, type) {
        if (type == Utils.Config.MODELS.TYPES.MATERIAL) {
            if (this.selectedMatCat == this.matCategories[0]) {
                this.curMatCategories = this.materials.concat([]);
            } else {
                this.curMatCategories = [];
                for (let i = 0; i < this.materials.length; i++) {
                    if (this.materials[i].Category == this.selectedMatCat) {
                        this.curMatCategories.push(this.materials[i]);
                    }
                }

            }
            this.onFindMaterialByName();

        }
    }

    onFindMaterialByName() {
        let _f = this.seachWord ? this.seachWord.toLowerCase() : '';
        this.curMats = this.curMatCategories.filter((el)=> {
            return el["Material Name"] && el["Material Name"].toLowerCase().match(_f) instanceof Array;
        });
    }

    dispoce() {
        this.commands = {
            loadedModels: [],
            loadedModelsPst: [],
            loadedModelsDetail: [],
            materials: {}
        };
        this.glViewer.dispoce();
    }

    /**
     * @param {IMainSettins} [options = null] - settings data
     * */
    applyMainSettings(options) {
        if (!options)throw Error('settings not selected');
        let self = this,
            gui = self.glViewer._datGui,
            _sets = new Interfaces.IMainSettins(options),
            typeBack,
            onFinish = [];

        this.mainSettings = _sets;
        if (_sets.remote_render) {
            if (_sets.remote_render.image_size) {
                if (gui.decorParams.formatsImg.indexOf(_sets.remote_render.image_size) < 0)gui.decorParams.formatsImg.push(_sets.remote_render.image_size);
                gui.decorParams.formatRenderImg = _sets.remote_render.image_size;
            }
        }
        if (_sets.local_render) {
            self.glViewer._datGui.decorParams.blur = _sets.local_render.blur || 0;
            if (_sets.local_render.hdr) {
                onFinish.push((next)=>self.glViewer.updateHDR(_sets.local_render.hdr, next));
                typeBack = 'hdr';
            } else if (_sets.local_render.cubeMap) {
                let _v = self.glViewer.envMaps.length;
                self.glViewer.envMaps.push({urls: _sets.local_render.cubeMap});
                gui.decorParams.backgroundImages.push(_v);
                typeBack = _v;
            }
            // let _renderSettings = _sets.local_render;
            // for (let i = 0, attrs = ['antialias', 'alpha', 'preserveDrawingBuffer', 'gammaInput', 'gammaOutput']; i < attrs.length; i++) {
            //     if (_renderSettings[attrs[i]] != self.glViewer.gl[attrs[i]]) {
            //         self.glViewer.updateRender(_renderSettings);
            //         break;
            //     }
            // }
            if (_sets.local_render.pixelRatio)self.glViewer.gl.setPixelRatio(_sets.local_render.pixelRatio);
            if (_sets.local_render.lights.length)this.glViewer.lights.traverse((light)=> {
                if (light instanceof THREE.Light) {
                    light.visible = false;
                    for (let i = 0; i < _sets.local_render.lights.length; i++) {
                        let _l = _sets.local_render.lights[i];
                        if (_l.type == light.type) {
                            light.visible = true;
                            for (let _fL in _l) {
                                if (!Utils.Config.isUndefined(_l[_fL])) {
                                    if (_fL == 'color') {
                                        light[_fL].setStyle(_l[_fL]);
                                    } else if (_fL == 'position') {
                                        light[_fL].copy(_l[_fL]);
                                    } else {
                                        light[_fL] = (_l[_fL]);
                                    }
                                }

                            }
                            break;
                        }
                    }
                }

            });

            if (_sets.local_render.envMap) {
                let env = new THREE.TextureLoader().load(_sets.local_render.envMap);

                let geometry = new THREE.SphereBufferGeometry( 500, 60, 40 );
                // invert the geometry on the x-axis so that all of the faces point inward
                geometry.scale( - 1, 1, 1 );

                let material = new THREE.MeshBasicMaterial( {
                    map: env
                } );

                let mesh = new THREE.Mesh( geometry, material );
                mesh.rotation.y = Math.PI;
                self.glViewer.envSphere = mesh;
                self.glViewer.scene.add(mesh);
            }
        }
        if (_sets.controls) {
            if (_sets.controls.mouse) {
                if (Utils.Config.isNumber(_sets.controls.mouse.autoRotateSpeed)) self.glViewer.controls.autoRotateSpeed = _sets.controls.mouse.autoRotateSpeed;
                if (Utils.Config.isNumber(_sets.controls.mouse.rotateSpeed)) self.glViewer.controls.rotateSpeed = _sets.controls.mouse.rotateSpeed;
                if (Utils.Config.isNumber(_sets.controls.mouse.rotateSpeedUP)) self.glViewer.controls.rotateSpeedUP = _sets.controls.mouse.rotateSpeedUP;
            }
            if (_sets.controls.selectedColor)this.glViewer._events._CONSTANTS.SELECT_COLOR.setStyle(_sets.controls.selectedColor);

            if (_sets.controls.keys) {
                if (self.glViewer.controls.keysDef)self.glViewer.controls.keys = self.glViewer.controls.keysDef;
            } else {
                if (!self.glViewer.controls.keysDef)self.glViewer.controls.keysDef = self.glViewer.controls.keys;
                self.glViewer.controls.keys = {};
            }

            self.glViewer._cntxMenu.enabled = _sets.controls.contextMenu;
            self.glViewer._events._CONSTANTS.HIGHLIGHT_SELECTED = _sets.controls.highlightSelected;
        }

        if (_sets.pattern) {
            if (Utils.Config.isNumber(_sets.pattern.scaleSpeed)) gui.decorParams.scaleSpeed = _sets.pattern.scaleSpeed;
            if (Utils.Config.isNumber(_sets.pattern.rotateSpeed)) gui.decorParams.rotateSpeed = _sets.pattern.rotateSpeed;
        }


        (function _init() {
            if (onFinish.length) {
                onFinish.shift()(_init);
            } else {
                self.glViewer.refresh();
                //if (!typeBack)gui.onChangeBackGroud(typeBack);
                if (options.next)options.next();
            }
        })()
    }

    /**
     * @return {IMainSettins}
     * */
    extractMainSettings() {
        return this.mainSettings;
    }

    /**
     * @param {Object} opt - handler json options
     * @param {IProductModelPart} opt.part - part data
     * @param {Function} opt.next - callback on finish load part, arguments[0] - list of added parts
     * @param {boolean} opt.isReplacement - flag, if true will remove this.curModel(selected part) from scene
     * @param {string} [opt.modelName=null] - if set will replace the model matched with this name,
     * @param {THREE.Mesh} [opt.model=null] - model for replace,required if isReplacement is true,
     * by defult if part.transform is not set -> will apply model part with same transform wich model are provided only else
     * if part.transform is set, will apply the transforms from  part.transform
     * */
    applyPart(opt) {
        if (!opt)return console.error('No part provided');
        let replac;
        if (opt.modelReplaceName) {
            replac = this.glViewer.model.getObjectByName(opt.modelReplaceName);
            if (!replac)console.warn('-----No model ' + opt.modelReplaceName + " found")
        }

        if (!replac)replac = opt.model;
        if (!replac)replac = this.curModel;
        if (opt.isReplacement && !replac)return console.error('model not provided');
        let selectedPart = opt.part;
        if (!selectedPart)return console.error('no such part');

        if (this.isUploadingPart) {
            if (!this.onFinishUploadPart)this.onFinishUploadPart = [];
            this.onFinishUploadPart.push(()=> {
                this.applyPart(opt);
            });
            return;
        }
        this.options.modelName = selectedPart.absUrl;
        this.options.params = selectedPart;

        this.options.params.modelToChange = replac;
        this.options.params.isReplacement = opt.isReplacement;
        this.glViewer.preloader.fade(true);
        this.isUploadingPart = true;
        this.glViewer.loadModel((commands = [])=> {
            let list = this.options.params.listParts;
            this.curModel = list[0];
            this.options.params = this.isUploadingPart = false;
            this.glViewer.refresh();
            this.glViewer.preloader.fade();
            this.commands.loadedModelsDetail.push(...(commands.concat([
                new Command(Command.REQUEST.SCENE.IMPORT_EL, {
                    block: "true",
                    filename: selectedPart.absUrlRS
                })
            ])));
            if (this.onFinishUploadPart && this.onFinishUploadPart.length)this.onFinishUploadPart.shift()();
            if (opt.next)opt.next(list);
        });
    }

    /**
     * extract part structure data
     * @param {Object} opt - settings
     * @param {THREE.Mesh} opt.model - threejs mesh part
     * @param {IProductConfiguration} [opt.conf=null] - configurations for this part
     * */
    extractPart(opt) {
        if (!opt || !opt.model)return console.error("some data missing");
        let child = opt.model,
            conf = opt.conf,
            partModel = new Interfaces.IProductModelPart({
                transform: child.matrix.toArray()
            });
        if (conf) {
            partModel.product_conf_id = conf._id;
            conf.model_parts.push(partModel);
        }


        if (child.params) {
            partModel.product_part_id = child.params._id;
            //if (child.params.isReplacement) partModel.data.product_part_change = child.params.modelToChange.userData.originName;
        }
        partModel.product_part_name = child.userData.originParent + child.userData.originName;
        return partModel;
    }

    /**
     * @param {Array<IProductModelPart>} opt - list of parts
     * */
    apllyParts(opt) {
        if (!opt || !(opt instanceof Array)) {
            throw Error('need list of parts');
        } else {
            this.parts = opt;
        }
    }

    /**
     * @return {Array<IProductModelPart>}
     *
     * */
    extractShapes() {
        let _result = [];
        this.glViewer.model.traverse((mesh)=> {
            if (mesh.type == Utils.Config.MODELS.TYPES.MESH) {
                let part = mesh.userData._part;
                if (part) {

                } else {
                    part = new Interfaces.IProductModelPart();
                    part.name = mesh.userData.originName;
                    mesh.userData._part = part;
                }
                _result.push(part);

                part.transform = mesh.matrix.elements;
                if (mesh.params) {
                    part.absUrl = mesh.params.absUrl;
                    part.absUrlRS = mesh.params.absUrlRS;
                    part.product_part_id = mesh.params.id;
                }
                if (mesh.material._mat) {
                    part.material_id = mesh.material._mat.id;
                }
                if (mesh.material.logos) {
                    part.product_part_patterns = [];
                    for (let i = 0, _logos = mesh.material.logos; i < _logos.length; i++) {
                        let _l = _logos[i].getPattern();
                        _l.product_part_id = part._id;
                        part.product_part_patterns.push();
                    }
                }

            }
        });
        return _result;
    }


    /**
     *
     * async load file and get the model from file and add this mode to scene
     * @param {Object} option - data handler
     * @param {IProductModel} option.model - model data
     * @param {Function} option.next - callback on finish work
     * */
    applyProductModel(option) {
        if (!option || !option.model)throw Error('option model is required');
        let _model = option.model;
        this.commands = {
            loadedModels: [],
            loadedModelsPst: [],
            loadedModelsDetail: [],
            materials: {}
        };
        this.productModel = _model;
        this.commands_model = [];
        this.options = {
            modelName: _model.model.absUrl,
            hideEnv: _model.model.hideEnv,
            onFinish: (commands)=> {
                if (option.next)option.next();
            }
        };
        this.glViewer.options = this.options;
        this.dispoce();
        this.glViewer.loadModel(()=> {
            if (option.next)option.next();
            this.commands.loadedModels = [
                /*new Command(Command.REQUEST.SCENE.IMPORT_EL, {
                 block: "true",
                 filename: _model.model.absUrlRS
                 })*/
            ];
        });
    }

    /**
     * @param {Object} options - settings for set  materials
     * @param {Array<IMaterial>} [options.materials=null] - list of materials
     * @param {Function} options.next - callback on finish set materials
     * */
    applyMatterials(options) {
        if (!options.materials || !(options.materials instanceof Array))throw Error('some bad materials data');
        try {
            this.loadMaterials(options);
            if (this.customize)this.customize.FinishesTemplate();
        } catch (e) {
            console.log(e);
        }

    }


    /**
     * @return {Array<IMaterial>}  - list of materials
     * */
    extractAllPartsMaterials() {
        let _matList = [];
        this.glViewer.model.traverse((mesh)=> {
                if (mesh.type == Utils.Config.MODELS.TYPES.MESH && mesh.category == Utils.Config.MODELS.TYPES._MESH) {
                    _matList.push(mesh.getProductMaterial());
                }
            }
        );
        return _matList;
    }

    /**
     * @param {number} [opt.index=null] - if set item index from materials list,  wil apply the material only if this.curModel are selected,
     * @param {IMaterial} [opt.material=null] - if set will apply this material,
     * @param {Array<string>} [opt.parts=null] - list of parts name,if set will apply the material to them,
     * @param {THREE.Mesh} [opt.model = null]- if set will attach material to this model part, the models of current scene are available from APP.glViewer.model.children[1] to to APP.glViewer.model.children[APP.glViewer.model.children.length-1],
     * @param {Function} opt.next - callback on finish set maps for curent model part, to set cur model part attach to APP.curModel = APP.glViewer.model.children[someindex, exept 0 ]
     * all models parts are available from APP.glViewer.model.children
     *
     * */
    applyMatterial(opt) {
        this.isUpplyMaterial = true;
        let onFinish = ()=> {
            this.isUpplyMaterial = false;
            if (this.onFinishUpplyMat) {
                setTimeout(()=> {
                    this.onFinishUpplyMat();
                }, 500);
            } else {
                if (opt.onFinish)opt.onFinish();
                this.glViewer.play();
            }
        };
        this.glViewer.stop();
        opt.onFinish = opt.next;
        opt.next = onFinish;
        this.selectMaterial(opt);
    }

    /**
     *  @param {Object} opt -   options,
     *  @param {THREE.Mesh} [opt.model=null] -   current editabel model,
     *  @param {IMaterialSettings} [opt.settings=null] -   will apply the settings for material of opt.model,
     * */
    applyMatSettings(opt) {
        if (!opt || !opt.model || !opt.settings)throw Error('no model or settings available');
        opt.model.applyMatSettings(opt.settings);
        this.glViewer.refresh();
    }

    /**
     * @param {Object} options - settings for patterns
     * @param {Array<IPattern>} options.parts - list of patterns
     * WARNING will attach only  patterns where on current scene pattern have reference to some part
     * @param {Function} options.next - callback on finish apply patterns
     * */
    applyPatterns(options) {
        if (!options || !(options.parts instanceof Array)) throw Error('incorrect patterns data');
        this.glViewer.decorations = [];
        let items = 0,
            parts = options.parts,
            self = this;
        (function apply() {
            let _opt = parts[items++];
            self.glViewer.model.traverse((mesh)=> {
                if (mesh.userData._part && mesh.type == Utils.Config.MODELS.TYPES.MESH && _opt.product_part_id == mesh.userData._part._id) {
                    return self.applyPattern({
                        pattern: _opt, model: mesh, next: ()=> {
                            if (items < parts.length) {
                                apply();
                            } else {
                                if (self.customize) {
                                    self.customize._decor.updateTemplate();
                                }
                                if (options.next)options.next();
                            }
                        }
                    });
                }
            });
        })()
    }

    /**
     * @param {Object} [opt=null] - options
     * @param {IPattern} [opt.pattern=null] - pattern
     * @param {THREE.Mesh} [opt.model=this.curModel] - mesh part,
     * WARNING if not set, will attach the  pattern to the current model
     * @param {Function} [opt.next=null]] - on finish apply pattern,
     * */
    applyPattern(opt) {
        let pattern = opt.pattern, model = opt.model, next = opt.next;
        let mesh = model ? model : this.curModel;
        if (!mesh || !pattern) throw Error('something missing on apply pattern');
        if (this.isUpplyPattern) {
            return this.onFinishUploadPattern = (()=> {
                this.onFinishUploadPattern = false;
                this.applyPattern({pattern: pattern, model: model, next: next});
            });
        }
        this.isUpplyPattern = true;
        if (!mesh.material.map)mesh.material.map = mesh.material.roughnessMap ? mesh.material.roughnessMap : mesh.material.bumpMap;
        if (this.lastLogo)this.lastLogo.setActive(false, null, true);
        mesh.material.needsUpdate = true;
        let _next = ()=> {
            let decor = new ModelCustomImage(this.glViewer, pattern, ()=> {
                decor._patern = pattern;
                decor.updateActiveArea({x: pattern.area.center_x, y: pattern.area.center_y});
                decor.setActive(true);
                this.glViewer.decorations.push(decor);
                decor.updateMaterial(mesh.material, {x: pattern.area.center_x, y: pattern.area.center_y});
                this.glViewer.refresh();
                this.lastLogo = decor;
                this.isUpplyPattern = false;
                if (this.onFinishUploadPattern)this.onFinishUploadPattern();
                if (next)next();
            });
        }
        if (!(pattern.preview instanceof Image)) {
            pattern.preview = Utils.Config.getImage(pattern.image, _next);
        } else {
            _next();
        }

    }

    /**
     * @param {THREE.Mesh} [model = null] - if set will return only the patterns of this model
     * @return {Array<IPattern>}  - list of patterns
     * */
    extractPatterns(model) {
        let result = [];
        if (model) {
            if (model.material.logos)return model.material.logos.map((pat)=> {
                return pat.getPattern()
            });
        } else {
            this.glViewer.decorations.forEach((pattern)=> {
                result.push(pattern.getPattern());
            });
        }

        return result;
    }


    /**
     * @return {IProductConfiguration}
     * */
    extractCnf() {
        let _self = this,
            app = _self,
            webGl = app.glViewer,
            conf = new Interfaces.IProductConfiguration({
                preview: webGl.gl.domElement.toDataURL()
            }),
            modelsPart = [];
        conf.main_settings = app.extractMainSettings();

        webGl.model.traverse((child)=> {
            if (child.type == Utils.Config.MODELS.TYPES.MESH) {
                child.updateMatrix();
                let partModel = new Interfaces.IProductModelPart({
                    transform: child.matrix.toArray()
                });
                conf.model_parts.push(partModel);

                if (child.params) {
                    partModel.product_part_id = child.params;
                }
                partModel.product_part_name = child.userData.originParent + child.userData.originName;

                if (child.material.logos) {
                    partModel.product_part_patterns = [];
                    for (let logos = 0; logos < child.material.logos.length; logos++) {
                        let _l = child.material.logos[logos];
                        let pattern = _l.getPattern(true);
                        partModel.product_part_patterns.push(pattern);
                    }
                }

                let _set = partModel.mat_settings = new Interfaces.IMaterialSettings(child.material);
                delete _set._id;
                if (child.material._mat && child.material._mat._id) {
                    partModel.mat_settings.material_id = child.material._mat._id;
                }
            }
        });
        return conf;
    }


    applyConfigurationModelParts(option) {
        let selected = option,
            app = this,
            _matrix = new THREE.Matrix4(),
            onFinish = ()=> {
                if (option.next)option.next();
            };

        let _part = option.part;
        if (_part.data)_part = _part.data;
        let _model;
        let applySettings = (_model)=> {
            if (_model) {
                let applyPatterns = ()=> {
                    let prtsItem = 0;
                    (function patterns() {
                        if (_part.product_part_patterns && prtsItem < _part.product_part_patterns.length) {
                            let _curP = _part.product_part_patterns[prtsItem++];
                            if (_part._id == _curP.product_part_id) {
                                app.applyPattern({pattern: _curP, model: _model, next: patterns});
                            } else {
                                patterns();
                            }
                        } else {
                            onFinish();
                        }
                    })()
                }
                app.glViewer.model.add(_model);
                app.curModel = _model;
                app.glViewer.clearTooltipInfo(_model);
                _model._part = _part;
                _matrix.elements = _part.transform;
                _matrix.decompose(_model.position, _model.quaternion, _model.scale);
                if (_part.mat_settings) {
                    for (let m = 0; m < app.materials.length; m++) {
                        if (app.materials[m]._id == _part.mat_settings.material_id) {
                            return app.applyMatterial({
                                index: m, next: ()=> {
                                    _model.applyMatSettings(_part.mat_settings);
                                    applyPatterns();
                                }
                            });
                        }
                    }
                    _model.applyMatSettings(_part.mat_settings);
                }
                applyPatterns();

            }
        }
        if (app.glViewer.cash.models[_part.product_part_name]) {
            applySettings(app.glViewer.cash.models[_part.product_part_name]);

        } else if (_part.product_part_id) {
            _model = app.glViewer.cash.models[_part.product_part_id.product_part_name];
            if (_model) {
                applySettings(_model);
            } else {
                app.applyPart({
                    part: _part.product_part_id,
                    next: (parts)=> {
                        if (app.glViewer.cash.models[_part.product_part_name]) {
                            applySettings(app.glViewer.cash.models[_part.product_part_name]);

                        } else {
                            //parts.forEach((child)=>{child.parent.remove(child);});
                            applySettings(parts[0]);
                        }

                    }
                });
            }

        } else {
            onFinish();
        }
    }


    /**
     * apply configuration
     * @param {Object} option - settings
     * @param {IProductConfiguration} option.conf - configuration
     * @param {Function} option.next - on finish apply configuration
     *
     * */
    applyConfiguration(option) {
        let selected = option.conf,
            _self = this,
            app = this,
            items = 0;

        if (!selected)throw Error('Configuration not selected');
        this.dispoce();


        app.applyMainSettings(selected.main_settings);
        app.glViewer.decorations = [];
        (function apply() {
            _self.applyConfigurationModelParts({
                part: selected.model_parts[items++],
                next: ()=> {
                    if (items >= selected.model_parts.length) {
                        app.glViewer.refresh();
                        app.glViewer.reCalcRadius();
                        app.glViewer.zoomCamera();
                        if (option.next)option.next();
                        if (app.customize) {
                            app.customize._decor.updateTemplate();
                            app.customize.updateListOfParts();
                        }
                    } else {
                        apply();
                    }
                }
            })
        })()
    }

    /**
     * @param {Array<IRoom>} [rooms=[]] - list of rooms
     * */
    applyRoomsList(rooms) {
        this.rooms = rooms ? rooms : [];
        if (this._preset)this._preset.updateListOfRoomsHTML();
    }

    /**
     * @param {THREE.Mesh}[model=null] - get volume info
     * @return {string}
     * */
    getVolume(model) {

        let _m = model;
        if (!_m)_m = this.curModel;
        if (!_m || !(_m instanceof THREE.Mesh))throw ('missing the model');
        let volume = _m.getVolume(),
            sphere = volume.geometry.boundingSphere,
            round = 4;
        return ("For (" + _m.name + "),the VOLUME:" + volume.volume.toFixed(round) + ", the SQUARE:" + volume.square.toFixed(round) +
            ", RADIUS described sphere from model: " + sphere.radius.toFixed(round) +
            ", with center on X: " + sphere.center.x.toFixed(round) + ", Y: " + sphere.center.y.toFixed(round) + ", Z: " + sphere.center.z.toFixed(round) +
            ". Bouding Box with HEIGHT: " + volume._size.y.toFixed(round) + ", WIDTH: " + volume._size.z.toFixed(round) + ", DEPTH: " + volume._size.x.toFixed(round));
    }

    /**
     * on start dragging
     * @param {IPattern} pattern - your pattern
     * @param {Function} [next=null] - on finish apply your pattern
     * */
    onDragStart(pattern, next) {
        this.glViewer._events.curDrag = {pattern: pattern, next: next};
    }

    /**
     * on finish drag
     * */
    onDragEnd() {
        this.glViewer._events.curDrag = false;
    }


    makeScreenshot(width, height) {

        let _v = this.glViewer,
            _render = _v.gl,
            _lastW = _render.domElement.clientWidth,
            _lastH = _render.domElement.clientHeight,
            _w = typeof width == 'number' ? width : _lastW,
            _h = typeof height == 'number' ? height : _lastH;

        //_v.stop();


        _v.controls.autoRotate = false;
        _v.zoomCamera(null, null, null, function () {
        }, null, new THREE.Vector3(1, 1, 1));
        _v.camera.aspect = _w / _h;
        _v.camera.updateProjectionMatrix();
        _v.controls.update();
        _render.setSize(_w, _h);
        _render.render(_v.scene, _v.camera);

        let image = _render.domElement.toDataURL();
        _render.setSize(_lastW, _lastH);
        _v.camera.aspect = _lastW / _lastH;
        _v.camera.updateProjectionMatrix();
        _v.refresh();
        return image;
    }

    /**
     * Call this method to toggle visibility of dimension box
     * */
    toggleDimensionBoxVisible() {
        if (!this.glViewer) return;
        let box = this.glViewer.dimensionHelper;
        box.visible = !box.visible;
        this.mainSettings.local_render.showDimensionHelper = !this.mainSettings.local_render.showDimensionHelper;
    }

    /**
     * Call this method to change unit dimension of the labels
     * @param {String} newUnit - possible values 'in', 'mm', 'cm'
     * */
    changeLabelUnit(newUnit) {
        if (!this.glViewer) return;
        this.glViewer.dimensionMetrics.unit = newUnit;
        this.glViewer.updateDimensionLabels();
    }

    getProductDimensions(){
        return this.glViewer.productSize;
    }

    UpdatePackageboxDimension(x, y, z){
        this.glViewer.package.geometry.scale(x/this.glViewer.package.size.x, y/this.glViewer.package.size.y, z/this.glViewer.package.size.z );
        this.glViewer.package.size = {x, y, z}
        this.glViewer.render();
    }

    GetPackageboxDimension(){
        return this.glViewer.package.size;
    }

    ChangePackageboxOpacity(opacity){
        this.glViewer.package.material.opacity = opacity;
        this.glViewer.render();
    }

    TogglePackageboxVisibility(){
        this.glViewer.package.visible = !this.glViewer.package.visible;
        this.glViewer.render();
    }

    ApplyPackageboxMaterial(material, done){
        if (!material) material = {
            name: 'revert',
            preview: "assets/images/test-text/revert/diffuse.jpeg",
            repeat: {
                x: 1,
                y: -1
            },
            maps: {
                map:            "assets/images/test-text/revert/diffuse.jpeg",
                metalnessMap:   "assets/images/test-text/revert/diffuse.jpeg",
                normalMap:      "assets/images/test-text/revert/normal.jpeg",
                roughnessMap:   "assets/images/test-text/revert/roughness.jpeg",

            }
        };
        let oldOpacity = this.glViewer.package.material.opacity;
        let next = ()=>{
            this.glViewer.package.material.transparent = true;
            this.glViewer.package.material.opacity = oldOpacity;
            if(done) done();
        };
        let opt = {
            model: this.glViewer.package,
            material,
            next
        }
        this.selectMaterial(opt);
    }

    ChangePackageboxColor(color){
        this.glViewer.package.material.color = new THREE.Color(color);
        this.glViewer.render();
    }
}

UniApp.Interfaces = Interfaces;
UniApp.Utils = Utils;
UniApp.PreSet = PreSet;
UniApp.Customize = Costumize;
UniApp.TESTProductAction = TESTProductAction;
