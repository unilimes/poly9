import{Config}from "./Config.js"
export class MHttp {
    constructor() {
        let _XHR = this._XHR = window.XMLHttpRequest || window.XDomainRequest || window.ActiveXObject;
        this.updateXHR();
        this.xhr.timeout = 30000;
        this.xhr.ontimeout = function () {
            console.log(" time is out");
        };
        /*(function(open) {
         _XHR.prototype.open = function(method, url, async, user, pass) {
         this.addEventListener("readystatechange", function() {
         if (this.readyState==4 && (this.status != 200 && this.status != 301 && this.status != 302))  {
         return console.error("Error: "+this.statusText)
         }
         }, false);
         open.call(this, method, url, async, user, pass);
         };
         })(_XHR.prototype.open);
         (function(open) {
         _XHR.prototype.send = function(method, url, async, user, pass) {
         this.addEventListener("readystatechange", function() {
         if (this.readyState==4 && (this.status != 200 && this.status != 301 && this.status != 302))  {
         return console.error("Error: "+this.statusText)
         }
         }, false);
         open.call(this, method, url, async, user, pass);
         };
         })(_XHR.prototype.send);*/
    }


    updateXHR() {
        this.xhr = new this._XHR("Microsoft.XMLHTTP");
    }

    formData(data) {
        this.updateXHR();
        let xhr = this.xhr;
        xhr.open('POST', Config.REMOTE_DATA + "public/uploadModel", false);
        xhr.upload.onprogress = function (e) {
            if (e.lengthComputable) {
                var percentComplete = (e.loaded / e.total) * 100;
                console.log(percentComplete + '% uploaded');
            }
        };
        xhr.send(data);
        return xhr.responseText;
    }

    post(url, data, onSuccess, onError) {
        try {
            this.xhr.open('POST', url, true);
            this.xhr.setRequestHeader('Content-Type', 'application/json');
            this.send(JSON.stringify(data), onSuccess, onError);
        } catch (e) {
            if (onError)onError(e)
        }
    }
    delete(url, data, onSuccess, onError) {
        try {
            this.xhr.open('DELETE', url, true);
            this.xhr.setRequestHeader('Content-Type', 'application/json');
            this.send(JSON.stringify(data), onSuccess, onError);
        } catch (e) {
            if (onError)onError(e)
        }
    }

    postForm(url, data, onSuccess, onError) {
        this.updateXHR();
        this.xhr.open('POST', url, true);
        this.send(data, onSuccess, onError);
    }

    get(url, data, onSuccess, onError) {
        this.xhr.open('GET', url + '?r=' + Math.random(), true);
        this.xhr.setRequestHeader('Content-Type', null);
        this.send(JSON.stringify(data), onSuccess, onError);
    }

    send(data, onSuccess, onError) {

        this.xhr.onreadystatechange = (e)=> {
            //console.log(this.xhr.status);
            if (this.xhr.readyState != 4) return;
            if (this.xhr.status != 200) {
                if (onError)return onError(this.xhr.status + ': ' + this.xhr.statusText);
                else console.error((this.xhr.status + ': ' + this.xhr.statusText));
            } else {
                onSuccess(this.xhr.responseText);
            }

        };
        try {
            this.xhr.send(data);
        } catch (e) {
            if (onError)onError(e)
        }
    }
}