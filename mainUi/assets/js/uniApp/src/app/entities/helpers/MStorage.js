export class MStorage {

    static getItem(key) {
        return JSON.parse(localStorage.getItem(MStorage.PREF + key)) || null;
    }

    static setItem(key, value) {
        localStorage.setItem(MStorage.PREF + key, JSON.stringify(value));
    }

    static removeItem(key) {
        localStorage.removeItem(MStorage.PREF + key);
    }

    static size() {
        return localStorage.length;
    }

    static keyByIndex(i) {
        let key = localStorage.key(i);
        if (key.match(MStorage.PREF))
            return localStorage.key(i).split(MStorage.PREF)[1];
        else  return null;
    }

}
MStorage.PREF = 'poly9:';
