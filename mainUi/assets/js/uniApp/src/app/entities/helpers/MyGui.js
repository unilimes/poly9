import {GLMain} from '../viewer/glMain.js';
import {Config} from '../helpers/Config.js';

/*
 * MyGui - ui controls (on top right drop-down menu by default)
 * */
export class MyGui extends GLMain {

    constructor(app) {
        super();
        let gui = this.gui = new dat.GUI({width: 350});
        document.querySelector('.dg.ac').style.zIndex = 9999;
        gui.close();
        this.params = null;
        this.curMat = null;
        this.hasMaterialEd = false;
        this.custom = null;
        this.app = app;


        let lights = gui.addFolder('Lights'),
            par = ['position_x_y_z', 'intensity', 'distance', 'decay', 'color', 'emissive', 'angle', 'penumbra', 'visible', 'skyColor', 'groundColor'];
        app.lights.children.forEach((light, key)=> {
            let lightsF = lights.addFolder(light.type + key);
            par.forEach((f)=> {
                let _f = f.split("_");
                if (_f.length < 2 && (typeof light[f] == 'undefined'))return;
                if (_f.length > 1) {
                    let _p = lightsF.addFolder(_f[0]);
                    for (let i = 1; i < _f.length; i++) {
                        _p.add(light[_f[0]], _f[i]).listen().min(-1000).max(1000).onChange(()=> {
                            this.onAnyWay();
                        });
                    }
                } else if (f.toLowerCase().match('color')) {
                    light['def' + f] = "#" + light[f].getHexString();
                    lightsF.addColor(light, 'def' + f).onChange(()=> {
                        light[f].setHex('0x' + light['def' + f].substr(1));
                        this.onAnyWay();
                    });
                } else if (typeof light[f] == 'number') {
                    lightsF.add(light, f).listen().min(0).max(100 * 100).onChange(()=> {
                        this.onAnyWay();
                    });
                } else {
                    lightsF.add(light, f).listen().onChange(()=> {
                        this.onAnyWay();
                    })
                }
            });

            if (light._helper) {
                light._helperVisible = light._helper.visible;
                lightsF.add(light, '_helperVisible').onChange(()=> {
                    light._helper.visible = light._helperVisible;
                    console.log(light._helper);
                })
            }


        });
        lights.close();
        this.decorParams = {
            local_render: {
                shadows: false,
                gammaInput: true,
                gammaOutput: true,
                alpha: true,
                antialias: true
            },

            post_ureal_bloom: {
                exposure: 1.024,
                threshold: 0.85,
                strength: 0.85,
                radius: 0.85
            },
            modelsWrapHelper: app.model._helper.visible,
            scaleSpeed: 0.01,
            rotateSpeed: 0.01,
            patternQuality: 1,
            formatsImg: [512, 1024, 2048, 4096],
            blurTypes: [0, 2, 5, 10],
            blur: 0,
            pixelRatio: window.devicePixelRatio,
            backgroundImage: -1,
            backgroundImages: app.envMaps.map((map, key)=> {
                return key
            })
        };
        this.decorParams.formatRenderImg = this.decorParams.formatsImg[2];

        let decor = gui.addFolder('Decoration');
        let orbitControls = gui.addFolder('MouseControls');
        let step = 0.00001;
        decor.add(this.decorParams, "scaleSpeed").min(0).max(1).step(0.001);
        decor.add(this.decorParams, "rotateSpeed").min(0).max(1).step(0.001);
        orbitControls.add(app.controls, "autoRotateSpeed").min(0).max(1).step(step);
        //orbitControls.add(app.controls.constraint, "smoothZoom");
        //orbitControls.add(app.controls.constraint, "zoomDampingFactor").min(0).max(1).step(step);
        orbitControls.add(app.controls, "dampingFactor").min(0).max(1).step(step);
        orbitControls.add(app.controls, "rotateSpeed").min(0).max(1).step(step);
        orbitControls.add(app.controls, "rotateSpeedUP").min(0).max(1).step(step);
        orbitControls.add(app.controls, "autoRotate");
        if (app._transformUI) {
            let transformUIControls = gui.addFolder('TransformUIControls');
            for (let set in app._transformUI.settings) {
                transformUIControls.add(app._transformUI.settings, set).min(-10).max(10).step(step);
            }

        }

        let renderer = gui.addFolder('Render');
        renderer.open();
        let bloomUnreal = renderer.addFolder('Unreal bloom');
        bloomUnreal.add(app.composer, "enabled");
        for (let sengs in this.decorParams.post_ureal_bloom) {
            if (sengs == 'exposure')  app.gl.toneMappingExposure = Math.pow(Number(this.decorParams.post_ureal_bloom[sengs]), 4.0);
            bloomUnreal.add(this.decorParams.post_ureal_bloom, sengs).min(-10).max(10).step(0.001).onChange((value)=> {
                if (sengs == 'exposure') {
                    app.gl.toneMappingExposure = Math.pow(Number(value), 4.0);
                } else {
                    app.bloomPass[sengs] = Number(value);
                }

                app.refresh();
            });
        }
        for (let sengs in this.decorParams.local_render) {
            renderer.add(this.decorParams.local_render, sengs).onChange(()=> {
                app.updateRender(this.decorParams.local_render);
            });
        }


        renderer.add(this.decorParams, "pixelRatio", ['none', "default", 0.5, 2, 3, 4, 5]).onChange((val)=> {
            if (val == 'none') {
                app.gl.setPixelRatio(1);

            } else if (val == 'default') {
                app.gl.setPixelRatio(window.devicePixelRatio);

            } else {
                app.gl.setPixelRatio(val);
            }

            app._events.onWindowResize();
        });
        renderer.add(this.decorParams, "formatRenderImg", this.decorParams.formatsImg);

        renderer.add(this.decorParams, "blur", this.decorParams.blurTypes).onChange((val)=> {
            this.onBlurBack();
        });
        renderer.add(this.decorParams, "backgroundImage", this.decorParams.backgroundImages.concat(['sun', 'none', 'garage', 'hdr'])).onChange((val)=> {
            this.onChangeBackGroud(val);
        });

        gui.add(this.decorParams, 'modelsWrapHelper').onChange(()=> {
            app.model._helper.visible = this.decorParams.modelsWrapHelper;
        });
    }

    onBlurBack() {
        let val = this.decorParams.blur,
            app = this.app;

        //if(!val)return
        try {

            if (app.scene.background) {
                if (!app.scene.background._images)app.scene.background._images = app.scene.background.image.concat([]);
                let _cnv = document.createElement('canvas');
                app.scene.background._images.forEach((img, index)=> {
                    StackBlur.image(img, _cnv, val * 10);
                    app.scene.background.image[index] = Config.getImage(_cnv.toDataURL());
                });
                app.scene.background.needsUpdate = true;
            } else if (app.hdr.visible && app.hdr.material.map) {
                //this.hdr.material.uniforms.exposure.value= +val ;
                if (!app.hdr.material._map)app.hdr.material._map = app.hdr.material.map.image;
                let _cnv = document.createElement('canvas');
                StackBlur.image(app.hdr.material._map, _cnv, val * 10);

                app.hdr.material.map = new THREE.Texture(_cnv);
                app.hdr.material.map.mapping = THREE.EquirectangularReflectionMapping;
                app.hdr.material.map.magFilter = THREE.LinearFilter;
                app.hdr.material.map.minFilter = THREE.LinearMipMapLinearFilter;
                app.hdr.material.map.needsUpdate = true;
            }
        } catch (e) {
            console.log(e);
        }

    }

    onChangeBackGroud(val) {
        let app = this.app;
        app.sun.visible = false;
        app.scene.background = null;
        if (app.garage)app.garage.visible = false;
        app.hdr.visible = false;
        app.lastBackSelected = val;

        if (val == 'sun') {
            app.sun.visible = true
        } else if (val == 'hdr') {
            if (!app.hdr.material.map)app.updateHDR(Config.REMOTE_DATA + 'images/hdr/tasmania__hdr_panorama_by_0_circus_freak_0-d3rgb3j.jpg');
            app.hdr.visible = true;
            if (this.decorParams.blur)this.onBlurBack();
        } else if (val == 'garage') {
            if (!app.garage) {
                app._loadGarage();
            } else {
                app.garage.visible = true;
            }

        } else if (val == 'none') {
        } else {
            app.checkIfMapsLoaded({
                val: val,
                next: (texture)=> {
                    app.scene.background = texture;
                    this.onAnyWay();
                }
            });
            if (this.decorParams.blur)this.onBlurBack();
        }
    }

    onSelect(mesh) {
        this.addMaterialEdit();
        this.curMat = mesh.material;
        this.curMat.needsUpdate = true;
        this.gui._matEditFolder.open();
        for (let fied in this.params) {
            if (fied == 'color') {
                this.params[fied] = this.curMat.color.getHex();
            } else if (fied == 'maps') {

            } else {
                this.params[fied] = this.curMat[fied];
            }
        }
        this.params.name = mesh.name;

        for (var i in this.gui.__controllers) {
            this.gui.__controllers[i].updateDisplay();
        }
        this.gui.open();
    }

    addMaterialEdit() {
        if (this.hasMaterialEd)return;
        this.hasMaterialEd = true;
        let gui = this.gui, _self = this, app = this.app;
        this.params = {
            color: 0xffffff,
            emissive: 0x000000,
            name: 'none',
            reflectivity: 1,
            clearCoat: 0,
            //refractionRatio: 1,
            envMapIntensity: 1,
            //clearCoat: 1,
            clearCoatRoughness: 1,
            metalness: 1,
            lightMapIntensity: 1,
            roughness: 1,
            opacity: 1,
            bumpScale: 1,
            displacementScale: 1,
            maps: {
                normalMap: {
                    normalScale_x: 1,
                    normalScale_y: 1,
                    repeat_x: 1,
                    repeat_Y: 1,
                    visible: true,
                },
                map: {
                    repeat_x: 1,
                    repeat_y: 1,
                    offset_x: 0,
                    offset_y: 0,
                    visible: true,
                }, metalnessMap: {
                    repeat_x: 1,
                    repeat_y: 1,
                    visible: true,
                }, roughnessMap: {
                    repeat_x: 1,
                    repeat_y: 1,
                    offset_x: 0,
                    offset_y: 0,
                    visible: true,
                }, bumpMap: {
                    repeat_x: 1,
                    repeat_y: 1,
                    offset_x: 0,
                    offset_y: 0,
                    visible: true
                }, displacementMap: {
                    repeat_x: 1,
                    repeat_y: 1,
                    offset_x: 0,
                    offset_y: 0,
                    displacementScale: 0,
                    displacementBias: 0,
                    visible: true
                },
                envMap: {
                    list: app.envMaps.map((map, key)=> {
                        return key
                    }),
                    visible: true
                }
            }
        };
        let custom = this.custom = {
            help: function () {
                alert('Hold Ctrl and click on object to select material for edit. Mouse Right click and move/ PAN to move the element');
            },
            addText: function () {
                //_self.app.decorations.push(new ModelCustomImage(_self.app, {text: prompt("Please input text", 'Hello U')}));
            },
            renderWithRoom: false
        }
        let matEditFolder = gui._matEditFolder = gui.addFolder('MatEdit');
        for (let fied in this.params) {
            if (fied == 'color' || fied == 'emissive') {
                matEditFolder.addColor(this.params, fied).listen().onChange(()=>this.onChange());
            } else if (fied == 'maps') {
                let mapsFolder = matEditFolder.addFolder('maps');
                for (let mapsF in this.params[fied]) {
                    let mapFolder = mapsFolder.addFolder(mapsF);
                    for (let mapFields in this.params[fied][mapsF]) {
                        let _d;
                        if (this.params[fied][mapsF][mapFields] instanceof Array) {
                            _d = mapFolder.add(this.params[fied][mapsF], mapFields, this.params[fied][mapsF][mapFields]).listen();
                        } else {
                            _d = mapFolder.add(this.params[fied][mapsF], mapFields).listen();
                            if (typeof this.params[fied][mapsF][mapFields] == 'number') _d = _d.min(-10).max(10).step(0.01);
                        }

                        _d.onChange((val)=> {
                            if (!this.curMat)return;
                            let _s = mapFields.split("_"),
                                curVal = this.params[fied][mapsF][mapFields];
                            if (mapFields.match('normalS')) {
                                this.curMat[_s[0]][_s[1]] = curVal;
                            } else if (mapFields == 'visible') {
                                if (!this.curMat["_" + mapsF])this.curMat["_" + mapsF] = this.curMat[mapsF];
                                this.curMat[mapsF] = this.curMat[curVal ? "_" + mapsF : "__NONE__"];
                            } else if (mapFields == 'list') {
                                app.checkIfMapsLoaded({
                                    val: val,
                                    next: (texture)=> {
                                        this.curMat[mapsF] = texture;
                                    }
                                });
                            } else {
                                if (this.curMat[mapsF]) {
                                    if (_s.length > 1) {
                                        if (typeof this.curMat[mapsF][_s[0]][_s[1]] == 'undefined') {
                                            this.curMat[_s[0]][_s[1]] = curVal;
                                        } else {
                                            this.curMat[mapsF][_s[0]][_s[1]] = curVal;
                                        }
                                    } else {
                                        if (typeof this.curMat[mapsF][_s[0]] == 'undefined') {
                                            this.curMat[_s[0]] = curVal;
                                        } else {
                                            this.curMat[mapsF][_s[0]] = curVal;
                                        }
                                    }

                                }

                            }
                            this.curMat.needsUpdate = true;
                            this.app._animation.play();
                        });
                    }
                }

            } else {
                let _d = matEditFolder.add(this.params, fied).listen();
                if (fied != 'name')_d.min(-10).max(10).step(0.01).onChange(()=>this.onChange());
            }
        }
        //gui.add(custom, 'help');
        //gui.add(custom, 'addText');
    }

    onChange() {
        if (!this.curMat)return;
        for (let fied in this.params) {
            if (fied == 'color' || fied == 'emissive') {
                this.curMat[fied].setHex(this.params[fied]);
            } else if (fied == 'maps') {

            } else {
                this.curMat[fied] = this.params[fied] + 0.0;
            }
        }
        this.curMat.needsUpdate = true;
        this.onAnyWay();
    }

    onAnyWay() {
        this.app.refresh();
    }

    destroy() {
        this.gui.destroy();
    }
}