/**
 *
 * Class representing a {@link  https://www.migenius.com/articles/creating-a-simple-scene-programmatically | command} request standart.
 */
export class Command {
    /**
     *@constructor
     *@param {string} [method=""] - method request.
     *@param {Object} [params={}] - request parametress.
     *@param {Function} [onFinishCmnds=null] - on finish execute command.
     *@param {number} [id = Date.now()] - unique id.
     */
    constructor(method, params, onFinishCmnds = null, id = Date.now()) {
        this.jsonrpc = '2.0';
        this.method = method || '';
        this.params = params || {};
        this.id = id || Date.now();
        if (onFinishCmnds)this.onFinishCmnds = onFinishCmnds;


    }
}
Command.SCENE = {
    CNTX_TIME_OUT: 1,
    TYPES: {
        GROUP: 'Group',
        OPTIONS: 'Options',
        CAMERA: 'Camera',
        INSTANCE: 'Instance',
        ATTR_REF: 'Ref',
        STRING: 'String',
        FLOAT: 'Float32',
        SINT_32: 'Sint32',
        BOOLEAN: 'Boolean',
        COLOR: "Color",
    },
    LIGHTS: {
        SKY: 'exSunSky'
    },
    NAME: 'demo_scene_ex',
    OPTIONS: 'exOptions',
    MAIN_GROUP: 'exRootGroup',
    RENDER: {
        LOOP: "1111"
    },
    ROOM_GROUP: 'room_Group',
    LOADED_GROUP: 'obj_root_group_1',
    CAMERA: {
        NAME: 'exCamera',
        INSTANCE: 'exCameraInstance'
    }
};
Command.REQUEST = {
    CREATE_FUNCTION_CALL_FROM_DEFINITION: 'create_function_call_from_definition',
    MDL_ATTACH_FUNC_2_ARG: 'mdl_attach_function_to_argument',
    CREATE_SCOPE: 'create_scope',
    BATCH: 'batch',
    USE_SCOPE: 'use_scope',
    IS_SCOPE: 'scope_exists',
    DEL_SCOPE: 'delete_scope',
    MESH: {
        CREATE: 'generate_mesh'
    },
    MATERIAL: {
        CREATE_DEF: 'create_material_instance_from_definition',
        BUMP: "material_attach_bump_texture_to_argument",
        DIFFUSE: "material_attach_texture_to_argument"
    },
    ELEMENT: {
        COPY: 'copy_element',
        CREATE: "create_element",
        SET_ATTR: "element_set_attribute",
        SET_ATTRS: "element_set_attributes",
        GET_ATTRS: "element_get_attributes",
    },
    SCENE: {
        IMPORT: "import_scene",
        IMPORT_EL: "import_scene_elements",
        CREATE: 'create_scene',
        SET_CAMERA_INSTANCE: 'scene_set_camera_instance',
        SET_OPTIONS: "scene_set_options",
        SET_ROOTFROUP: "scene_set_rootgroup",
        RENDER: 'render'
    },
    CAMERA: {
        ASPECT: "camera_set_aspect",
        RESOLUTION: "camera_set_resolution",
        FOCAL: "camera_set_focal",
        APERTURE: 'camera_set_aperture'
    },
    GROUP: {
        ATTACH: "group_attach",
        DATACH: "group_detach"
    },
    INSTANCE: {
        WORLD_TO_OBJ: "instance_set_world_to_obj",
        SET_TRANSFORMS: "instance_set_transforms",
        ATTACH: "instance_attach",
        DETACH: "instance_detach",
        SET_CAMERA: "scene_set_camera_instance",
        SET_MATRL: "instance_set_material",
        SETS_MATRL: "instances_set_material",
    },
    TEXTURE: {
        CREATE: 'create_texture'
    }
};