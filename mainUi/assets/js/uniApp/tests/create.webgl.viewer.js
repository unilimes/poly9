document.addEventListener("DOMContentLoaded", function () {
    let APP = window.APP = new UniApp({showFPS: true});
    APP.transactions = new UniApp.TESTProductAction(APP);
    try {

        APP.transactions.loadProductData({
            next: (loadData)=> {
                //return
                var materials = [
                {
                    name: 'revert',
                    preview: "assets/images/test-text/revert/diffuse.jpeg",
                    repeat: {
                        x: 1,
                        y: -1
                    },
                    maps: {
                        map:            "assets/images/test-text/revert/diffuse.jpeg",
                        metalnessMap:   "assets/images/test-text/revert/diffuse.jpeg",
                        normalMap:      "assets/images/test-text/revert/normal.jpeg",
                        roughnessMap:   "assets/images/test-text/revert/roughness.jpeg",
                    }
                },
                {
                    name: 'Back-lining-32',
                    preview: "assets/images/test-text/Back-lining-32/back_lining.jpg",
                    repeat: {
                      x: 64,
                      y: 64
                    },
                    maps: {
                        map:            "assets/images/test-text/Back-lining-32/Back-lining-32_BASE.jpg",
                        normalMap:      "assets/images/test-text/Back-lining-32/Back-lining-32_NRM.jpg",
                        metalnessMap:   "assets/images/test-text/Back-lining-32/Back-lining-32_MTL.jpg",
                        roughnessMap:   "assets/images/test-text/Back-lining-32/Back-lining-32_ROUGH.jpg",
                        alphaMap:       "assets/images/test-text/Back-lining-32/Back-lining-32_ALPHA.jpg",

                    }
                },
                {
                    name: 'Back-strap-j-3549-bottom-64',
                    preview: "assets/images/test-text/Back-strap-j-3549-bottom-64/back_strap_j_3549_bo_490Ht.jpg",
                    repeat: {
                        x: 32,
                        y: 32
                    },
                    maps: {
                        map:            "assets/images/test-text/Back-strap-j-3549-bottom-64/Back-strap-j-3549-bottom-64_BASE_1.jpg",
                        normalMap:      "assets/images/test-text/Back-strap-j-3549-bottom-64/Back-strap-j-3549-bottom-64_NRM_1.jpg",
                        metalnessMap:   "assets/images/test-text/Back-strap-j-3549-bottom-64/Back-strap-j-3549-bottom-64_MTL_1.jpg",
                        roughnessMap:   "assets/images/test-text/Back-strap-j-3549-bottom-64/Back-strap-j-3549-bottom-64_ROUGH_1.jpg",
                        // alphaMap:       "assets/images/test-text/Back-strap-j-3549-bottom-64/Back-strap-j-3549-bottom-64_ALPHA_1.jpg",

                    }
                },
                {
                    name: 'Bottom-band-j-756-256',
                    preview: "assets/images/test-text/Bottom-band-j-756-256/bottom_band_j_756.jpg",
                    repeat: {
                        x: 8,
                        y: 8
                    },
                    maps: {
                        map:            "assets/images/test-text/Bottom-band-j-756-256/Bottom-band-j-756-256_BASE.jpg",
                        normalMap:      "assets/images/test-text/Bottom-band-j-756-256/Bottom-band-j-756-256_NRM.jpg",
                        metalnessMap:   "assets/images/test-text/Bottom-band-j-756-256/Bottom-band-j-756-256_MTL.jpg",
                        roughnessMap:   "assets/images/test-text/Bottom-band-j-756-256/Bottom-band-j-756-256_ROUGH.jpg",
                        // alphaMap:       "assets/images/test-text/Bottom-band-j-756-256/Bottom-band-j-756-256_ALPHA.jpg",

                    }
                },
                {
                    name: 'Front-strap-L-3286-512',
                    preview: "assets/images/test-text/Front-strap-L-3286-512/front_strap_l_3286.jpg",
                    repeat: {
                        x: 8,
                        y: 8
                    },
                    maps: {
                        map:            "assets/images/test-text/Front-strap-L-3286-512/Front-strap-L-3286-512_BASE.jpg",
                        normalMap:      "assets/images/test-text/Front-strap-L-3286-512/Front-strap-L-3286-512_NRM.jpg",
                        metalnessMap:   "assets/images/test-text/Front-strap-L-3286-512/Front-strap-L-3286-512_MTL.jpg",
                        roughnessMap:   "assets/images/test-text/Front-strap-L-3286-512/Front-strap-L-3286-512_ROUGH.jpg",
                        // alphaMap:       "assets/images/test-text/Front-strap-L-3286-512/Front-strap-L-3286-512_ALPHA.jpg",

                    }
                },
                {
                    name: 'Strat-strap-tabs-j-4315(Top)-512',
                    preview: "assets/images/test-text/Strat-strap-tabs-j-4315(Top)-512/strat_strap_tabs_j_4_ujOIh.jpg",
                    repeat: {
                        x: 8,
                        y: 8
                    },
                    maps: {
                        map:            "assets/images/test-text/Strat-strap-tabs-j-4315(Top)-512/Strat-strap-tabs-j-4315(Top)-512_BASE.jpg",
                        normalMap:      "assets/images/test-text/Strat-strap-tabs-j-4315(Top)-512/Strat-strap-tabs-j-4315(Top)-512_NRM.jpg",
                        metalnessMap:   "assets/images/test-text/Strat-strap-tabs-j-4315(Top)-512/Strat-strap-tabs-j-4315(Top)-512_MTL.jpg",
                        roughnessMap:   "assets/images/test-text/Strat-strap-tabs-j-4315(Top)-512/Strat-strap-tabs-j-4315(Top)-512_ROUGH.jpg",
                        // alphaMap:       "assets/images/test-text/Strat-strap-tabs-j-4315(Top)-512/Strat-strap-tabs-j-4315(Top)-512_ALPHA.jpg",

                    }
                },
                {
                    name: 'Upper-hem-elastic-j-606(Bottom)-128',
                    preview: "assets/images/test-text/Upper-hem-elastic-j-606(Bottom)-128/upper_hem_elastic_j__V8u0H.jpg",
                    repeat: {
                        x: 16,
                        y: 16
                    },
                    maps: {
                        map:            "assets/images/test-text/Upper-hem-elastic-j-606(Bottom)-128/Upper-hem-elastic-j-606(Bottom)-128_BASE.jpg",
                        normalMap:      "assets/images/test-text/Upper-hem-elastic-j-606(Bottom)-128/Upper-hem-elastic-j-606(Bottom)-128_NRM.jpg",
                        metalnessMap:   "assets/images/test-text/Upper-hem-elastic-j-606(Bottom)-128/Upper-hem-elastic-j-606(Bottom)-128_MTL.jpg",
                        roughnessMap:   "assets/images/test-text/Upper-hem-elastic-j-606(Bottom)-128/Upper-hem-elastic-j-606(Bottom)-128_ROUGH.jpg",
                        // alphaMap:       "assets/images/test-text/Upper-hem-elastic-j-606(Bottom)-128/Upper-hem-elastic-j-606(Bottom)-128_ALPHA.jpg",

                    }
                },
                {
                    name: 'CF-fram-back-lace-F-2229-1024',
                    preview: "assets/images/test-text/CF-fram-back-lace-F-2229-1024/cf_fram_back_lace_f__PcTc5.jpg",
                    repeat: {
                        x: 2,
                        y: 2
                    },
                    maps: {
                        map:            "assets/images/test-text/CF-fram-back-lace-F-2229-1024/CF-fram-back-lace-F-2229-1024_BASE.jpg",
                        normalMap:      "assets/images/test-text/CF-fram-back-lace-F-2229-1024/CF-fram-back-lace-F-2229-1024_NRM.jpg",
                        metalnessMap:   "assets/images/test-text/CF-fram-back-lace-F-2229-1024/CF-fram-back-lace-F-2229-1024_MTL.jpg",
                        roughnessMap:   "assets/images/test-text/CF-fram-back-lace-F-2229-1024/CF-fram-back-lace-F-2229-1024_ROUGH.jpg",
                        alphaMap:       "assets/images/test-text/CF-fram-back-lace-F-2229-1024/CF-fram-back-lace-F-2229-1024_ALPHA.jpg",

                    }
                },
                {
                    name: 'Top-cup-centor-front-gore-lace-with-out-border-1024',
                    preview: "assets/images/test-text/Top-cup-centor-front-gore-lace-with-out-border-1024/top_cup_centor_front_o69St.jpg",
                    repeat: {
                        x: 2,
                        y: 2
                    },
                    maps: {
                        map:            "assets/images/test-text/Top-cup-centor-front-gore-lace-with-out-border-1024/Top-cup-centor-front-gore-lace-with-out-border-1024_BASE.jpg",
                        normalMap:      "assets/images/test-text/Top-cup-centor-front-gore-lace-with-out-border-1024/Top-cup-centor-front-gore-lace-with-out-border-1024_NRM.jpg",
                        metalnessMap:   "assets/images/test-text/Top-cup-centor-front-gore-lace-with-out-border-1024/Top-cup-centor-front-gore-lace-with-out-border-1024_MTL.jpg",
                        roughnessMap:   "assets/images/test-text/Top-cup-centor-front-gore-lace-with-out-border-1024/Top-cup-centor-front-gore-lace-with-out-border-1024_ROUGH.jpg",
                        alphaMap:       "assets/images/test-text/Top-cup-centor-front-gore-lace-with-out-border-1024/Top-cup-centor-front-gore-lace-with-out-border-1024_ALPHA.jpg",

                    }
                },
                {
                    name: '123 2k maps',
                    preview: "https://s3.ap-south-1.amazonaws.com/p9-platform/finishes/2ae5ad99-4402-4ac6-88cb-6db98e35c26f.jpg",
                    maps: {
                        map: "https://s3.ap-south-1.amazonaws.com/p9-platform/finishes/0cbaf22f-48e6-4144-9913-8e0d9ca29284.jpg",
                        normalMap: "https://s3.ap-south-1.amazonaws.com/p9-platform/finishes/714c2eb9-a868-4e93-8642-ff67ddf7a5be.jpg",
                        metalnessMap: "https://s3.ap-south-1.amazonaws.com/p9-platform/finishes/68c703df-7953-4620-8b5c-a8f1b299fea2.jpg",
                        roughnessMap: "https://s3.ap-south-1.amazonaws.com/p9-platform/finishes/0a31a5a5-6d89-48a9-a196-bdd2246a6396.jpg",

                    }
                },
                    {
                        name: 'Satin',
                        preview: "https://source-imapi.allegorithmic.com/images/7c8da21f3190cb218ca960ed08c440ee",
                        maps: {
                            map: "https://source-imapi.allegorithmic.com/images/7c8da21f3190cb218ca960ed08c440ee",
                            normalMap: "https://source-imapi.allegorithmic.com/images/a0a29df5b173c3423b1d4726138f193f",
                            metalnessMap: "https://source-imapi.allegorithmic.com/images/5019f4919025a5748b473222ee596b76",
                            roughnessMap: "https://source-imapi.allegorithmic.com/images/56d93dc8ed03a6441cd6b971af0e73b5",
                            displacementMap: "https://source-imapi.allegorithmic.com/images/c1291a3bb65ff93ac98a203a905c8f99",

                        },
                    },
                {
                    name: 'Metal',
                    preview: " http://d2ww3metzfrng3.cloudfront.net/demo/finishes/fCCrJcjFSKQhWDtu2.png",
                    maps: {
                        map: "http://d2ww3metzfrng3.cloudfront.net/demo/finishes/gPNMfpSqFeyfQCtdy.jpg",
                        normalMap: "http://d2ww3metzfrng3.cloudfront.net/demo/finishes/3GMwMsfgMZmy8Fs5X.jpg",
                        metalnessMap: "http://d2ww3metzfrng3.cloudfront.net/demo/finishes/PzGobtsGJcED9mrnu.jpg",
                        roughnessMap: "http://d2ww3metzfrng3.cloudfront.net/demo/finishes/nPrwS5HSyj92TZqdA.jpg"

                    }
                },
                {
                    name: 'Metal2',
                    preview: "https://d2ww3metzfrng3.cloudfront.net/demo/finishes/8SGGkJCyKPGwQsWt5.jpg",
                    maps: {
                        map: "https://d2ww3metzfrng3.cloudfront.net/demo/finishes/8SGGkJCyKPGwQsWt5.jpg",
                        normalMap: "https://d2ww3metzfrng3.cloudfront.net/demo/finishes/QLuyxqeidXd8dpKXt.jpg",
                        metalnessMap: "https://d2ww3metzfrng3.cloudfront.net/demo/finishes/Yyi3pJhiLbvqyHsm6.jpg",
                        roughnessMap: "https://d2ww3metzfrng3.cloudfront.net/demo/finishes/kBkPL35S6J3cYmxcq.jpg"

                    }
                },
                {
                    name: 'Metal2',
                    preview: "https://d2ww3metzfrng3.cloudfront.net/demo/finishes/8SGGkJCyKPGwQsWt5.jpg",
                    maps: {
                        map: "https://d2ww3metzfrng3.cloudfront.net/demo/finishes/8SGGkJCyKPGwQsWt5.jpg",
                        normalMap: "https://d2ww3metzfrng3.cloudfront.net/demo/finishes/QLuyxqeidXd8dpKXt.jpg",
                        metalnessMap: "https://d2ww3metzfrng3.cloudfront.net/demo/finishes/Yyi3pJhiLbvqyHsm6.jpg",
                        roughnessMap: "https://d2ww3metzfrng3.cloudfront.net/demo/finishes/kBkPL35S6J3cYmxcq.jpg"

                    }
                },
                //     normalMap: 'https://d2ww3metzfrng3.cloudfront.net/demo/finishes/QLuyxqeidXd8dpKXt.jpg',
                //
                //
                //     metalnessMap: 'https://d2ww3metzfrng3.cloudfront.net/demo/finishes/Yyi3pJhiLbvqyHsm6.jpg',
                //
                //
                //     roughnessMap: 'https://d2ww3metzfrng3.cloudfront.net/demo/finishes/kBkPL35S6J3cYmxcq.jpg',
                //
                //     Diffuse:
                // 'https://d2ww3metzfrng3.cloudfront.net/demo/finishes/8SGGkJCyKPGwQsWt5.jpg'
                {
                    name: 'Glass',
                    preview: "http://d2ww3metzfrng3.cloudfront.net/demo/finishes/qFg4PG3GYtG2XKEfx.png",
                    maps: {
                        map: "http://d2ww3metzfrng3.cloudfront.net/demo/finishes/yLzGZ7hZd2iM5QF8G.jpg",
                        normalMap: "http://d2ww3metzfrng3.cloudfront.net/demo/finishes/4kwEhRtiGADd62dnz.jpg",
                        metalnessMap: "http://d2ww3metzfrng3.cloudfront.net/demo/finishes/t2StdSxq8qpgLxaAm.jpg",
                        roughnessMap: "http://d2ww3metzfrng3.cloudfront.net/demo/finishes/yxMBCj3D4pa7Y2YWY.jpg",
                        alphaMap: "http://d2ww3metzfrng3.cloudfront.net/demo/finishes/b4g3LeznQgBeXMeFj.jpg"

                    }
                }
                //     Glass:
                //
                // Thumb : http://d2ww3metzfrng3.cloudfront.net/demo/finishes/qFg4PG3GYtG2XKEfx.png
                //
                //     Alpha : http://d2ww3metzfrng3.cloudfront.net/demo/finishes/b4g3LeznQgBeXMeFj.jpg
                //
                //         Diffuse :
                //             http://d2ww3metzfrng3.cloudfront.net/demo/finishes/yLzGZ7hZd2iM5QF8G.jpg
                //
                //                 Metalness :
                //                     http://d2ww3metzfrng3.cloudfront.net/demo/finishes/t2StdSxq8qpgLxaAm.jpg
                //
                //                         Normals:
                //                             http://d2ww3metzfrng3.cloudfront.net/demo/finishes/4kwEhRtiGADd62dnz.jpg
                //
                //                                 Roughness:
                //                                     http://d2ww3metzfrng3.cloudfront.net/demo/finishes/yxMBCj3D4pa7Y2YWY.jpg
                ];
                materials = materials.concat(loadData[2].data);
                loadData[0].data[0].model.absUrl = loadData[0].data[0].model.absUrl.replace('54.164.221.209', '54.227.198.145');


                APP.applyMainSettings({
                    local_render: {
                        antialias: true,
                        envMap: "https://d2ww3metzfrng3.cloudfront.net/threejs/panorama/panorama.jpg",
                        // showDimensionHelper: true,
                        alpa: true,
                        blur: 3
                    },
                    controls:{
                        contextMenu:true,
                        // highlightSelected: true
                    },
                    next: ()=> {

                    }

                });

                APP.applyProductModel({
                    //model: {model: {absUrl: 'http://localhost/poly9/mainUi/assets/js/uniApp/assets/models/15622698.obj'}},//loadData[0].data[0],
                    //model: {model: {absUrl: 'http://localhost/polyRepo/poly9/mainUi/sqtray_3.drc'}},//loadData[0].data[0],
                    // model: {model: {absUrl: 'http://localhost/polyRepo/poly9/mainUi/plate.obj', hideEnv: true}},//loadData[0].data[0],
                    //model: {model: {absUrl: 'http://localhost/polyRepo/poly9/mainUi/double-side.gltf', hideEnv: true}},//loadData[0].data[0],
                    //model: {model: {absUrl: 'http://localhost/polyRepo/poly9/mainUi/iphone_7.obj'}},//loadData[0].data[0],
                    model: {model: {absUrl: 'http://54.227.198.145/models/my_models/cups/barware_s11624/barware_s11624.obj', hideEnv: true}},//loadData[0].data[0],
                    // model: {model: {absUrl: 'https://d2ww3metzfrng3.cloudfront.net/tisana/product_Gltf/qzaoxicorNfSXC4Y3.gltf', hideEnv: true}},//loadData[0].data[0],
                    // model: {model: {absUrl: 'http://d2ww3metzfrng3.cloudfront.net/mahabharat/product_Gltf/YKKqRwGQN6ejcpbpj.gltf', hideEnv: true}},//loadData[0].data[0],
                    //model: {model: {absUrl: 'http://54.227.198.145/models/my_models/cups/barware_s11624/barware_s11624.obj'}},//loadData[0].data[0],
                    next: ()=>{
                        APP.applyMatterials({materials: materials});
                        APP.apllyParts(loadData[1].data);
                        APP.customize = new UniApp.Customize(APP);
                        APP._preset = new UniApp.PreSet(APP);
                        //APP.glViewer.container.appendChild(APP.transactions.elems.save);
                        APP.glViewer.refresh();

                        let defaultMaterials = {
                            name: 'Test', maps: {
                                map: "http://d2ww3metzfrng3.cloudfront.net/demo/finishes/oX9xHfgWbWYRR3bcp.jpg",
                                alphaMap: "http://d2ww3metzfrng3.cloudfront.net/demo/finishes/dwYCPeLnkL7rHdTFZ.jpg",
                                metalnessMap: "http://d2ww3metzfrng3.cloudfront.net/demo/finishes/EYmewWmaw9wrM3bfW.jpg",
                                normalMap: "http://d2ww3metzfrng3.cloudfront.net/demo/finishes/hi9txaKscdyz6CFbB.jpg",
                                roughnessMap: "http://d2ww3metzfrng3.cloudfront.net/demo/finishes/ZkKn7kqCNQczCrCrf.jpg",

                            }
                        };

                    }
                });
            }
        });
    } catch (e) {
        console.log(e);
    }

    console.log(APP.glViewer.model.position);

    document.querySelector('button[data-target="#modal_render"]').addEventListener("click", function (e) {
        if (window.APP._preset)window.APP._preset.toggleModal();
    });
});

function TEST() {

    [6, 5].forEach(
        function (index) {
            var s = {
                _id: "1",
                image: "http://54.227.198.145:3009/public/remote_data?absUrl=http://pngimg.com/uploads/simpsons/simpsons_PNG96.png",
                area: {angle: 0, scale: 1, center_x: 0.5, center_y: 0.5}
            };
            var _model = APP.glViewer.model.children[index];
            APP.applyPattern(s, _model, ()=> {
                    console.log("pattern was applied for " + _model.name)
                }
            )
        })
}
function TEST1() {
    [
        {
            absUrl: "http://54.227.198.145:3009/models/my_models/cups_decor/514/0.obj",
            transform: [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1]
        },
        {
            absUrl: "http://54.227.198.145:3009/models/my_models/cups_decor/514/1.obj",
            transform: [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1]
        }
    ].forEach(
        function (part) {
            APP.applyPart(
                {
                    next: (model)=> {
                        console.log(model);
                    },
                    part: part
                }
            )
        })
}
