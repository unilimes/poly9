# README #


* [demo](http://54.227.198.145:80/product_detail.html)

* Quick summary
* Version 1.0.1

### How do I get set up? ###
* `cd mainUi/assets/js/uniApp` - to use pure ES6 project

* `npm install` - install all dev dependecices
* `bower install` - install all libs

* `gulp` - run app tasks

* `localhost/poly9/mainUi/product_detail.html` - open it on your browser


* __WARNING__ for Remote render on Reality Server(__RS__) you have to storage all your data(.obj, images, etc.) on same
host name as RS
* __WARNING__ if you have some absolute path to image, .obj etc., and this data are stored on NOT your domain, change the link like in [example](http://54.227.198.145:80/assets/js/uniApp/out/data.structure_examples.js.html#line137)
* __WARNING__ you have to have such libs   "jquery.min.js", "jquery-ui.js", "bootstrap.min.js", "dropzone.min.js"  or just include them in gulp js task

* config the **UniApp** in your app:
    1. Load builded [script](http://54.227.198.145:80/assets/js/uniApp/build/uniThreeDApp.js) and
    [styles](http://54.227.198.145:80/assets/js/uniApp/build/uniThreeDApp.css) or build them by
    yourself. In your HTML mark your container with id = "_WEBGLVIEW_" - for defualt load 3dUI, if not will be
     in _document.body_, mark popup container with id ="_CUSTOMIZE_GL_" - for default view for pop up costumization";
    2. Create an object in your namespace from class UniApp (__window.APP = new UniApp()__);
    3. For HTML UI Customize (Shapes, Finishes,Patten) create an object from class __Customize__ (APP._customize = new UniApp.Customize(APP));
    _REQUIRE_ tag container with classes = "modal-body","modal-pad". All static data have to be loaded: model,
    materials, parts;
    4. For RS:
        1. For RS render rooms create an object from class _Preset_ (APP.preset = new UniApp.Preset(APP));
        2. Methods:
            1. [_APP.applyRoomsList()_](http://54.227.198.145:80/assets/js/uniApp/out/UniApp.html#applyRoomsList) - call to apply rooms, [example](http://54.227.198.145:80/assets/js/uniApp/out/data.structure_examples.js.html#line155);
            1. [_APP.preset.selectRoom()_](http://54.227.198.145:80/assets/js/uniApp/out/PreSet.html#selectRoom) - call to select room, [example](http://54.227.198.145:80/assets/js/uniApp/out/data.structure_examples.js.html#line207);
            1. [_APP.preset.selectQuad()_](http://54.227.198.145:80/assets/js/uniApp/out/UniApp.html#selectQuad) - call to select quad for locate your model, [example](http://54.227.198.145:80/assets/js/uniApp/out/data.structure_examples.js.html#line259);
            1. [_APP.preset.render()_](http://54.227.198.145:80/assets/js/uniApp/out/UniApp.html#render) - call to start RS render, [example](http://54.227.198.145:80/assets/js/uniApp/out/data.structure_examples.js.html#line274);
            1. [_APP.preset.getRemoteRenderedImage()_](http://54.227.198.145:80/assets/js/uniApp/out/PreSet.html#getRemoteRenderedImage) - call to get the RS result image, [example](http://54.227.198.145:80/assets/js/uniApp/out/entities_preset_preset.js.html#line192);
    5. **API**:
        1. Methods:
            1. [_APP.applyMainSettings()_](http://54.227.198.145:80/assets/js/uniApp/out/UniApp.html#applyMainSettings) - call to aplly settings;
            2. [_APP.extractMainSettings()_](http://54.227.198.145:80/assets/js/uniApp/out/UniApp.html#extractMainSettings) - call to extract settings;
            2. [_APP.applyProductModel()_](http://54.227.198.145:80/assets/js/uniApp/out/UniApp.html#applyProductModel) - call to aplly new product model;

            2. [_APP.applyMatterials()_](http://54.227.198.145:80/assets/js/uniApp/out/UniApp.html#applyMatterials) - call to aplly new list of materials;
            2. [_APP.applyMatterial()_](http://54.227.198.145:80/assets/js/uniApp/out/UniApp.html#applyMatterial) - call to aplly material from your material list;
            2. [_APP.applyMatSettings()_](http://54.227.198.145:80/assets/js/uniApp/out/UniApp.html#applyMatSettings) - call to aplly material settings;

            2. [_APP.extractAllPartsMaterials()_](http://54.227.198.145:80/assets/js/uniApp/out/UniApp.html#extractAllPartsMaterials) - call to get all materials of all parts;
            2. [_APP.extractPart()_](http://54.227.198.145:80/assets/js/uniApp/out/UniApp.html#extractPart) - call to get some part structure from model part;
            2. [_APP.apllyParts()_](http://54.227.198.145:80/assets/js/uniApp/out/UniApp.html#apllyParts) - call to aplly new list of parts;
            2. [_APP.applyPart()_](http://54.227.198.145:80/assets/js/uniApp/out/UniApp.html#applyPart) - call to aplly part;

            2. [_APP.extractPatterns()_](http://54.227.198.145:80/assets/js/uniApp/out/UniApp.html#extractPatterns) - call to get all patterns;
            2. [_APP.applyPatterns()_](http://54.227.198.145:80/assets/js/uniApp/out/UniApp.html#applyPatterns) - call to aplly list of patterns;
            2. [_APP.applyPattern()_](http://54.227.198.145:80/assets/js/uniApp/out/UniApp.html#applyPattern) - call to aplly single pattern;

            2. [_APP.extractCnf()_](http://54.227.198.145:80/assets/js/uniApp/out/UniApp.html#extractCnf) - call to extract configuration;
            2. [_APP.applyConfiguration()_](http://54.227.198.145:80/assets/js/uniApp/out/UniApp.html#applyConfiguration) - call to apply configuration;

            2. [_APP.getVolume()_](http://54.227.198.145:80/assets/js/uniApp/out/UniApp.html#getVolume) - call to get volume;
            2. [_APP.onDragStart()_](http://54.227.198.145:80/assets/js/uniApp/out/UniApp.html#onDragStart) - call this callback in your event on drag start;
            2. [_APP.onDragEnd()_](http://54.227.198.145:80/assets/js/uniApp/out/UniApp.html#onDragEnd) - call this callback in your event on drag end;

            2. [_APP.loadZip()_](http://54.227.198.145:80/assets/js/uniApp/out/UniApp.html#loadZip) - load zip;

        2. _EXAMPLES_:
            1. [EXAMPLE(1)](http://54.227.198.145:80/assets/js/uniApp/out/data.structure_examples.js.html#line5) - simple use;
            1. [EXAMPLE(2)](http://54.227.198.145:80/assets/js/uniApp/out/data.structure_examples.js.html#line56); - log all your model parts(THREE.Mesh)
            1. [EXAMPLE(3)](http://54.227.198.145:80/assets/js/uniApp/out/data.structure_examples.js.html#line63) - set current model part from list of parts from scene;
            1. [EXAMPLE(5)](http://54.227.198.145:80/assets/js/uniApp/out/data.structure_examples.js.html#line78) - get current screen;
            1. [EXAMPLE(4)](http://54.227.198.145:80/assets/js/uniApp/out/data.structure_examples.js.html#line69) - extract/apply configuration;
            1. [EXAMPLE(6)](http://54.227.198.145:80/assets/js/uniApp/out/data.structure_examples.js.html#line85) - white background for screenshot;
            1. [EXAMPLE(7)](http://54.227.198.145:80/assets/js/uniApp/out/data.structure_examples.js.html#line91) - volume info;
            1. [EXAMPLE(8)](http://54.227.198.145:80/assets/js/uniApp/out/data.structure_examples.js.html#line101) - using drag&drop;
            1. [EXAMPLE(9)](http://54.227.198.145:80/assets/js/uniApp/out/data.structure_examples.js.html#line126) - how to add callback on click event;
            1. [EXAMPLE(11)](http://54.227.198.145:80/assets/js/uniApp/out/data.structure_examples.js.html#line144) - apply mat settings;
            1. [EXAMPLE(17)](http://54.227.198.145:80/assets/js/uniApp/out/data.structure_examples.js.html#line280) - apply hdr image;
            1. [EXAMPLE(18)](http://54.227.198.145:80/assets/js/uniApp/out/data.structure_examples.js.html#line292) - wrap part to apply all default fields;
            1. [EXAMPLE(19)](http://54.227.198.145:80/assets/js/uniApp/out/data.structure_examples.js.html#line303) - wrap pattern to apply all default fields;
            1. [EXAMPLE(20)](http://54.227.198.145:80/assets/js/uniApp/out/data.structure_examples.js.html#line314) - multiply scene on page;
            1. [EXAMPLE(21)](http://54.227.198.145:80/assets/js/uniApp/out/data.structure_examples.js.html#line323) - extract model;
            1. [EXAMPLE(23)](http://54.227.198.145:80/assets/js/uniApp/out/data.structure_examples.js.html#line332) - apply material;
            1. [EXAMPLE(24)](http://54.227.198.145:80/assets/js/uniApp/out/data.structure_examples.js.html#line345) - apply material with parts;
            1. [EXAMPLE(25)](http://54.227.198.145:80/assets/js/uniApp/out/data.structure_examples.js.html#line373) - apply hdr;
            1. [EXAMPLE(26)](http://54.227.198.145:80/assets/js/uniApp/out/data.structure_examples.js.html#line391) - load zip;
            1. [EXAMPLE(26)](http://54.227.198.145:80/assets/js/uniApp/out/data.structure_examples.js.html#line404) - some helper settings;
            1. [EXAMPLE(31)](http://54.227.198.145:80/assets/js/uniApp/out/data.structure_examples.js.html#line488) - new global settings change;
            1. [EXAMPLE(32)](http://54.227.198.145:80/assets/js/uniApp/out/data.structure_examples.js.html#line502) - additional resources for Draco loader;
            1. [EXAMPLE(33)](http://54.227.198.145:80/assets/js/uniApp/out/data.structure_examples.js.html#line508) - hide envMap for certain product;
            1. [EXAMPLE(34)](http://54.227.198.145:80/assets/js/uniApp/out/data.structure_examples.js.html#line522) - toggle visibility of the dimension box;
            1. [EXAMPLE(35)](http://54.227.198.145:80/assets/js/uniApp/out/data.structure_examples.js.html#line528) - change label's unit;
        2. Simple [visualization](https://repository.genmymodel.com/paskusam/poly9) for data connections, server [api](https://bitbucket.org/unilimes/poly9/src/01853d9c3aebbc5f5b9596ecdbf24115157b13cc/server/?at=master) for our demo
        2. How to use:
            1. ![Select method](https://image.prntscr.com/image/4IUezuqPTLugGNWodS001Q.png)
            2. ![View documentation](https://image.prntscr.com/image/Zg_oNUD0Q_i5qZU71dVK_Q.png)
            3. ![View documentation and json structure](https://image.prntscr.com/image/mPEUXh4aQT6pEUZoP7lCYQ.png)
