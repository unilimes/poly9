import { Poly9Page } from './app.po';

describe('poly9 App', () => {
  let page: Poly9Page;

  beforeEach(() => {
    page = new Poly9Page();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
