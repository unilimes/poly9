const hbs = require("hbs");
const path = require("path");
const http = require("http");
const morgan = require("morgan");
const express = require("express");
//const passport = require("passport");
//const session = require("express-session");
const busboy = require("connect-busboy");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const multer  = require('multer');

const config = require("./config");
const routes = require("./routes");
const mongoose = require("./middleware/mongoose");

const app = express();
const server = http.createServer(app);


app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'origin, content-type, accept, X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});


hbs.localsAsTemplateData(app);
hbs.registerPartials(path.join(__dirname, "views/partials"));

app.set("views", path.join(__dirname, "views"));
app.set("view engine", "hbs");
app.set("view options", {
    layout: "layout"
})

//var storage = multer.diskStorage({
//    dest:'/',
//    destination: function (req, file, cb) {
//        console.log("------------");
//        cb(null, '/resources')
//    },
//    filename: function (req, file, cb) {
//        console.log("***********");
//        cb(null, file.fieldname + '-' + Date.now())
//    }
//});
var upload = multer({ dest: config.DIR.PUBLIC }).fields(
    [
        { name: config.FILE_UPLOAD_ATTR[0], maxCount: 10 },
        { name: config.FILE_UPLOAD_ATTR[2], maxCount: config.MAPS.length },
        { name: config.FILE_UPLOAD_ATTR[1], maxCount: 2 }
    ]
);

app.use( upload);
app.use(morgan("dev"));
//
app.use(busboy());

app.use(cookieParser());

app.use(bodyParser.urlencoded({
    limit: "50mb",
    extended: false
}));

app.use(bodyParser.json({ limit: "50mb" }));

//var expireTime =  1000*60*60*24*3;//3 days
//app.use(session({
//    secret: config.security.secret,
//    resave: true,
//    saveUninitialized: true,
//    cookie:{expires: new Date(Date.now() +expireTime), maxAge : expireTime },
//    store: new MongoStore({
//        mongooseConnection: mongoose.connection
//    })
//}));

//app.use(passport.initialize());
//app.use(passport.session());


app.use(express.static(path.join(__dirname, "../assets")));
app.use(express.static(path.join(__dirname, "./resources")));
app.use(express.static(path.join(__dirname, "./mainUi")));
//app.use(express.static( ("/home/edinorog/Downloads/poly9/assets")));///home/edinorog/Documents/Web/poly9/server/index.html

app.use("/", routes);

app.use("*", function (req, res) {

    if(req.params.available =='true'){
        res.sendFile(path.join(__dirname, "./mainUi/index.html"));
    }else{
        return  res.render("error", {
            message: 'Permission denied'
        });
    }

});

app.use(function (req, res, next) {
    var err = new Error("Not Found");
    err.status = 404;
    next(err);
});

if (app.get("env") === "development") {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);

        res.render("error", {
            message: err.message,
            error: err
        });
    });
}

app.use(function (err, req, res, next) {
    res.status(err.status || 500);

    res.render("error", {
        message: err.message,
        error: {}
    });
});

server.on("listening", function () {
    var addr = server.address();
    var bind = typeof addr === "string" ? "pipe " + addr : "port " + addr.port;
    //console.log(__dirname);
    console.log("Listening on " + bind);
});

server.on("error", function (error) {
    if (error.syscall !== "listen") {
        throw error;
    }

    var bind = typeof config.port === "string" ? "Pipe " + config.port : "Port " + config.port;

    switch (error.code) {
        case "EACCES":
            console.error(bind + " requires elevated privileges");
            process.exit(1);
            break;
        case "EADDRINUSE":
            console.error(bind + " is already in use");
            process.exit(1);
            break;
        default:
            throw error;
    }
});

server.listen(config.port);
