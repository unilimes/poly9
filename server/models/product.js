/**
 * Created by edinorog on 18.10.17.
 */

const mongoose = require("mongoose");
const CONFIG = require("../config");
const Schema = mongoose.Schema;

const product_schema = new Schema({
    name: {type: String},
    preview: {type: String},
    model:{
        name: {type: String},
        absUrl: {type: String},
        absUrlRS: {type: String }
    },
    created: {
        type: Number,
        default: Date.now()
    }
});
product_schema.pre('save', function (next) {
    var self = this ;
    /*------@absUrl:--------- */
    if (!( CONFIG.help.isUndefined(self.model))) return CONFIG.help.checkSchema('model', 'model is require ', self, next);
    if (!( CONFIG.help.isString(self.model.absUrl)) || self.model.absUrl.length > 100) return CONFIG.help.checkSchema('model', 'model absUrl require a string with no more 100 characters', self, next);
    if (!CONFIG.help.isString(self.model.name) || self.model.name.length > 100) return CONFIG.help.checkSchema('name', 'model name require a string with no more 100 characters', self, next);
    next();
});
module.exports = mongoose.model(CONFIG.SCHEMAS.PRODUCT, product_schema);