/**
 * Created by edinorog on 18.10.17.
 */
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const CONFIG = require("../config");
const fs = require("fs");


const productCnfSchema = new Schema({
    product_model_id: {required: true, type: Schema.ObjectId, ref: CONFIG.SCHEMAS.PRODUCT},
    model_parts: [{type: Schema.ObjectId, ref: CONFIG.SCHEMAS.PRODUCT_MODEL_PART}],
    main_settings: {type: Schema.ObjectId, ref: CONFIG.SCHEMAS.PRODUCT_MAIN_SET},
    preview: {type: String},
    created: {
        type: Number,
        default: Date.now()
    }
});

productCnfSchema.pre('save', function (next) {
    var self = this;
    if (!CONFIG.help.isString(self.preview)) return CONFIG.help.checkSchema('preview', 'preview is not correct', self, next);
    if ((self.preview.length > 100)) return CONFIG.help.checkSchema('preview', 'preview is too long', self, next);

    next();
});
productCnfSchema.pre('remove', function (next) {
    var self = this;
    fs.unlink(CONFIG.DIR.PUBLIC + CONFIG.DIR.UPLOADS + CONFIG.DIR.IMAGES + self.preview, function (err) {
        //if (err) return next(err);
        self.model(CONFIG.SCHEMAS.PRODUCT_MODEL_PART).find({product_conf_id: self._id}, function (er, d) {
            if (er) {
                return next(er);
            } else {
                d.forEach((function (el) {
                    el.remove();
                }));
                self.model(CONFIG.SCHEMAS.PRODUCT_MAIN_SET).findOne({product_conf_id: self._id}, function (er, d) {
                    if (er) {
                        return next(er);
                    } else {
                        if(d)d.remove();
                        next();
                    }
                })
            }
        });
    });
});
module.exports = mongoose.model(CONFIG.SCHEMAS.PRODUCT_CONF, productCnfSchema);