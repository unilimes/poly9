/**
 * Created by edinorog on 18.10.17.
 */

const mongoose = require("mongoose");
const CONFIG = require("../config");
const Schema = mongoose.Schema;

const material_schema = new Schema({
    preview: {
        type: String
    },
    name:{type: String},
    maps: {
        type: new Schema({
            map: {type: String},
            bumpMap: {type: String},
            normalMap: {type: String},
            metalnessMap: {type: String},
            roughnessMap: {type: String},
            displacementMap: {type: String}
        }, {_id: false}),
        required: true
    },
    settings: {type: Schema.ObjectId, ref: CONFIG.SCHEMAS.PRODUCT_MAT_SET},
    created: {
        type: Number,
        default: Date.now()
    }
});

material_schema.pre('save', function (next) {
    var self = this,
        _maps = CONFIG.MAPS;
    /*------@thumb:-------- */
    if (!(CONFIG.help.isString(self.preview))) return CONFIG.help.checkSchema('preview', 'preview is not correct', self, next);

    /*------@maps:-------- */
    if (CONFIG.help.isUndefined(self.maps)) return CONFIG.help.checkSchema('maps', 'maps is required', self, next);

    for (var i = 0; i < _maps.length; i++) {
        var cur = self.maps[_maps[i]];
        if (!CONFIG.help.isUndefined(cur)) {
            if (!CONFIG.help.isString(cur))return CONFIG.help.checkSchema('maps', ' incorect value for ' + _maps[i], self, next);
            if (cur.length > 250) return CONFIG.help.checkSchema('maps', ' the value for ' + _maps[i] + " is too long (" + cur.length + ")", self, next);
        }
    }
    next();
});
module.exports = mongoose.model(CONFIG.SCHEMAS.PRODUCT_MAT, material_schema);