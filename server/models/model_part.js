/**
 * Created by edinorog on 18.10.17.
 */

const mongoose = require("mongoose");
const CONFIG = require("../config");
const Schema = mongoose.Schema;

const model_part_schema = new Schema({
    preview:{type: String},
    name:{type: String},
    absUrl:{type: String},
    absUrlRS:{type: String},
    product_model_id: {type: Schema.ObjectId, ref: CONFIG.SCHEMAS.PRODUCT_CONF},
    product_conf_id: {type: Schema.ObjectId, ref: CONFIG.SCHEMAS.PRODUCT_CONF},
    material_settings_id: {type: Schema.ObjectId, ref: CONFIG.SCHEMAS.PRODUCT_MAT_SET},
    product_part_patterns: [{type: Schema.ObjectId, ref: CONFIG.SCHEMAS.PRODUCT_PATTERN}],
    transform: {
        type: Array
    },
    category: {type: Number, default: CONFIG.CATEGORY.PARTS.DEFAULT},
    product_part_id: {type: Schema.ObjectId, ref: CONFIG.SCHEMAS.PRODUCT_MODEL_PART},//wich model are in use
    product_part_name: {type: String},//wich model are in use
    created: {
        type: Number,
        default: Date.now()
    }
});

model_part_schema.pre('save', function (next) {
    var self = this;
    /*------@tranform:---only Matrix 4*4 with float value------ */
    if (!CONFIG.help.isUndefined(this.transform)) {
        if (!(CONFIG.help.isArray(this.transform))) return CONFIG.help.checkSchema('transform', 'transform is not valid', self, next);
        if (this.transform.length) {
            if (this.transform.length != 16) return CONFIG.help.checkSchema('transform', 'transform has incorrect type for save', self, next);
            for (var i = 0; i < this.transform.length; i++) {
                if (!CONFIG.help.isNumber(this.transform[i])) {
                    return CONFIG.help.checkSchema('transform', 'transform item value has incorrect value', self, next);
                }
            }
        }
    }


    /*------@product_part_name:--------- */
    if (!CONFIG.help.isUndefined(self.product_part_name)) {
        if (!(CONFIG.help.isString(self.product_part_name))) return CONFIG.help.checkSchema('product_part_name', 'product_part_name is not correct', self, next);
        if (self.product_part_name.length > 250) return CONFIG.help.checkSchema('product_part_name', 'product_part_name is too long, '+self.product_part_name, self, next);
    }


    next();
});
model_part_schema.pre('remove', function (next) {
    var self = this;
    try {
        self.model(CONFIG.SCHEMAS.PRODUCT_PATTERN).find({product_part_id: self._id}, function (er, data) {
            for (var i = 0; i < data.length; i++)data[i].remove();
            self.model(CONFIG.SCHEMAS.PRODUCT_MAT_SET).find({product_part_id: self._id}, function (er, data) {
                for (var i = 0; i < data.length; i++)data[i].remove();
                next(er);
            });
        });
    } catch (e) {
        return next(e);
    }

});
module.exports = mongoose.model(CONFIG.SCHEMAS.PRODUCT_MODEL_PART, model_part_schema);