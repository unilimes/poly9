/**
 * Created by edinorog on 18.10.17.
 */

const mongoose = require("mongoose");
const CONFIG = require("../config");
const Schema = mongoose.Schema;
const fs = require("fs");

const pattern_schema = new Schema({
    product_part_id: {required: true, type: Schema.ObjectId, ref: CONFIG.SCHEMAS.PRODUCT_MODEL_PART},
    image: {
        type: String,
        required: true
    },
    area: {
        type: new Schema({
            angle: {type: Number, default: 0},//radians
            scale: {type: Number, default: 1},
            center_x: {type: Number, default: 0.5},
            center_y: {type: Number, default: 0.5}
        }, {_id: false}),
        required: true
    },
    created: {
        type: Number,
        default: Date.now()
    }
});

pattern_schema.pre('save', function (next) {
    var self = this;
    /*------@image:---only hash link to image------ */
    if (!CONFIG.help.isString(self.image)) return CONFIG.help.checkSchema('image', 'image is not correct', self, next);
    if (self.image.length > 100) return CONFIG.help.checkSchema('image', 'image name is too long', self, next);

    /*------@area:-------- */
    if (CONFIG.help.isUndefined(self.area)) return CONFIG.help.checkSchema('area', 'area is required', self, next);
    if (!CONFIG.help.isNumber(self.area.angle)) return CONFIG.help.checkSchema('area', 'angle is required', self, next);
    if (!CONFIG.help.isNumber(self.area.scale)) return CONFIG.help.checkSchema('area', 'scale is required', self, next);
    if (!CONFIG.help.isNumber(self.area.center_x) ) return CONFIG.help.checkSchema('area', 'center_x is incorrect', self, next);
    if (!CONFIG.help.isNumber(self.area.center_y) ) return CONFIG.help.checkSchema('area', 'center_y is incorrect', self, next);
    next();
});
pattern_schema.pre('remove', function(next) {
    fs.unlink(CONFIG.DIR.PUBLIC +CONFIG.DIR.UPLOADS + CONFIG.DIR.IMAGES+this.image,next);
});
module.exports = mongoose.model(CONFIG.SCHEMAS.PRODUCT_PATTERN, pattern_schema);