const router = require("express").Router();
const config = require("../../config");

const fs = require('fs');
const path = require('path');
var http = require('http'),
    url = require('url'),
    https = require('https');


function checkFIlesTimeSpend(urlSaveFile,dir) {
    var rUrlSaveFile = urlSaveFile,
        _dir = dir;
    fs.readdirSync(_dir).forEach(function (list, file) {
        var name = path.join(_dir, list);
        var timeCreated = new Date(fs.statSync(name).ctime);
        if (fs.statSync(name).isDirectory()) {
            if (timeCreated.getTime() + 1000 * 60 * 60 * 24 < Date.now()) {
                config.help.deleteFolderRecursive(name, true);
            } else {
                rUrlSaveFile = name;
            }
        }
    });

    if (!fs.existsSync(rUrlSaveFile)) {
        fs.mkdirSync(rUrlSaveFile, config.FILE_UPLOAD_ACCEC);
    }
    return rUrlSaveFile;
}
router.post("/uploadModel", function (req, res) {
    try{
        if (!req.files || !req.files[config.FILE_UPLOAD_ATTR[1]]) {
            return res.json({
                status: false,
                message: "forgot something"
            });
        } else {
            var
                _mainDir = '',
                _dir = config.DIR.PUBLIC+config.DIR.UPLOADS+config.DIR.MODEL_UPLOADS,
                //urlSaveFile = _dir + config.help.randomStr(),
                respData = [];
            //urlSaveFile = checkFIlesTimeSpend(urlSaveFile,_dir);

            var _urlDir = '';
            while (fs.existsSync(_urlDir = _dir + "/" + (_mainDir = config.help.randomStr()))) {}

            fs.mkdirSync(_urlDir, config.FILE_UPLOAD_ACCEC);

            if(req.files[config.FILE_UPLOAD_ATTR[2]]){
                var map_urlDir =_urlDir+"/maps/";
                fs.mkdirSync(map_urlDir, config.FILE_UPLOAD_ACCEC);
                for (var i = 0; i < req.files[config.FILE_UPLOAD_ATTR[2]].length; i++) {
                    var _file = req.files[config.FILE_UPLOAD_ATTR[2]][i],
                        buffer = fs.readFileSync(_file.path),
                        _fileName = map_urlDir   + _file.originalname;


                    fs.writeFileSync(_fileName, buffer, 'utf8');
                    fs.unlinkSync(_file.path);
                    respData.push({originName: _file.originalname, newName: _fileName.replace(_dir, "")});
                }
            }
            for (var i = 0; i < req.files[config.FILE_UPLOAD_ATTR[1]].length; i++) {
                var _file = req.files[config.FILE_UPLOAD_ATTR[1]][i],
                    buffer = fs.readFileSync(_file.path),
                    _fileName = _urlDir + "/" + _file.originalname;


                fs.writeFileSync(_fileName, buffer, 'utf8');
                fs.unlinkSync(_file.path);
                respData.push({originName: _file.originalname, newName: _fileName.replace(_dir, ""),modelFolder:_mainDir});
            }



            return res.json({
                status: true,
                message: "modes was saved",
                data: respData,
            });
        }
    }catch(e){
        console.log(e);
        return res.json({
            status: false
        });
    }

});
router.post("/clearModel", function (req, res) {
    try{
        if (!req.body.path) {
            return res.json({
                status: false,
                message: "forgot something"
            });
        } else {
            var
                _dir = config.DIR.PUBLIC+config.DIR.UPLOADS+config.DIR.MODEL_UPLOADS+req.body.path;

            if(fs.existsSync(_dir)){
                config.help.deleteFolderRecursive(_dir, true);
                return res.json({
                    status: true,
                    message: "models was clear"
                });
            }else{
                return res.json({
                    status: false,
                    message: "no such model"
                });
            }



        }
    }catch(e){
        console.log(e);
        return res.json({
            status: false
        });
    }

});

router.post("/uploadImg", function (req, res) {


    if (!req.files || !req.files[config.FILE_UPLOAD_ATTR[0]]) {
        return res.json({
            status: false,
            message: "forgot something"
        });
    } else {
        var
            MAX_HASH_STR = 32,
            urlSaveFile = config.DIR.PUBLIC + config.help.randomStr(),
            respData = [];

        urlSaveFile = checkFIlesTimeSpend(urlSaveFile);


        for (var i = 0; i < req.files[config.FILE_UPLOAD_ATTR[0]].length; i++) {
            var _file = req.files[config.FILE_UPLOAD_ATTR[0]][i],
                buffer = fs.readFileSync(_file.path),
                _fileName = urlSaveFile + "/" + config.help.randomStr() + Date.now() + "." + _file.originalname.split(".")[1];
            fs.writeFileSync(_fileName, buffer, 'utf8');
            fs.unlinkSync(_file.path);
            respData.push({originName: _file.originalname, newName: _fileName});
        }

        return res.json({
            status: true,
            message: "image(s) was saved",
            data: respData,
        });
    }

});
router.get("/remote_data", function (req, res) {
    if (!req.query.absUrl) {
        return res.end('empty data', 'binary');
    } else {
        var options = url.parse(req.query.absUrl, true),
            callback = function (response) {
                if (response.statusCode === 200) {
                    res.writeHead(200, {
                        'Content-Type': response.headers['content-type']
                    });
                    response.pipe(res);
                } else {
                    res.writeHead(response.statusCode);
                    res.end();
                }
            };

        if ("https:" == options.protocol) {
            https.request(options, callback).end();
        } else {
            http.request(options, callback).end();
        }

    }

});

module.exports = router;
