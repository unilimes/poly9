const path = require("path");
const router = require("express").Router();



router.use("/public", require("./public"));
router.use("/api", require("./api"));

module.exports = router;
