/**
 * Created by edinorog on 18.10.17.
 */
const router = require("express").Router();


router.use("/parts", require("./model_part"));
router.use("/material", require("./material"));
router.use("/materil_setting", require("./materil_setting"));
router.use("/product_main_settings", require("./product_main_settings"));
router.use("/product", require("./product"));
router.use("/product_model", require("./product_model"));
router.use("/product_configuration", require("./product_configuration"));
router.use("/pattern", require("./pattern"));

module.exports = router;