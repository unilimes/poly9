/**
 * Created by edinorog on 18.10.17.
 */
const router = require("express").Router();
const Pattern = require("../../models/pattern");
const Parts = require("../../models/model_part");
const CONFIG = require("../../config/");

router.post("/", function (req, res) {
    try {
        var _body = req.body;
        CONFIG.help.saveBaseData([_body.image], function (err, urls) {
            if (err) {
                res.json({status: false, message: err});
            } else {
                _body.image = urls[0];
                var _p = new Pattern(_body);
                _p.save(function (err, el) {
                    if (err) {
                        res.json({status: false, message: err});
                    } else {
                        Parts.update(
                            {_id: el.product_part_id},
                            {$push: {"product_part_patterns": el._id}},
                            {safe: true, upsert: true},
                            function (er) {
                                if (er)console.log(er);
                                res.json({status: true, data: el});
                            })
                    }
                })
            }
        })

    } catch (e) {
        res.json({status: false, message: e});
    }
});
module.exports = router;