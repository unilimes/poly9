/**
 * Created by edinorog on 18.10.17.
 */
const router = require("express").Router();
const ProductConf = require("../../models/product_configuration");
const Parts = require("../../models/model_part");
const CONFIG = require("../../config/");

router.post("/", function (req, res) {
    try {
        var _body = req.body;
        CONFIG.help.saveBaseData([_body.preview], function (err, urls) {
            if (err) {
                res.json({status: false, message: err});
            } else {
                _body.preview = urls[0];
                var conf = new ProductConf(_body);
                conf.save(function (err, el) {
                    if (err) {
                        res.json({status: false, message: err});
                    } else {
                        res.json({status: true, data: el});
                    }
                })
            }
        });

    } catch (e) {
        res.json({status: false, message: e})
    }
});
router.get("/:product_model_id", function (req, res) {
    ProductConf.find({"product_model_id": req.params.product_model_id})
        .exec(function (err, data) {
            if (err) {
                return res.json({status: false, message: err});
            } else {
                return res.json({
                    status: true, data: data
                });
            }
        });
});
router.get("/settings/:id", function (req, res) {
    ProductConf.findOne({"_id": req.params.id})
        .populate([{
            path: 'model_parts',
            populate: [{
                path: 'product_part_patterns',
                model: CONFIG.SCHEMAS.PRODUCT_PATTERN
            }, {
                path: 'material_settings_id',
                model: CONFIG.SCHEMAS.PRODUCT_MAT_SET
            }, {
                path: 'product_part_id',
                model: CONFIG.SCHEMAS.PRODUCT_MODEL_PART
            }]
        }, {
            path: 'main_settings',
            model: CONFIG.SCHEMAS.PRODUCT_MAIN_SET
        }])
        .exec(function (err, data) {
            if (err) {
                return res.json({status: false, message: err});
            } else {
                return res.json({
                    status: true, data: data
                });
            }
        });
});
router.delete("/", function (req, res) {
    ProductConf.findOne({_id: req.body.id}, function (err, data) {
        if (err) {
            return res.json({status: false, message: err});
        } else {
            data.remove();
            return res.json({
                status: true
            });
        }
    });
});
module.exports = router;