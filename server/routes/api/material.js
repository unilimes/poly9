/**
 * Created by edinorog on 18.10.17.
 */
const router = require("express").Router();
const Material = require("../../models/material");

router.post("/fillWithTestMaterials", function (req, res) {
    var data = req.body;
    try {
        var startItem = 0;
        (function saveMat() {
            if (startItem >= data.list.length) {
                return res.json({status: true, message: 'materials was saved'});
            } else {
                var mat = new Material(data.list[startItem++]);
                mat.save(function (err, mat) {
                    if (err) {
                        return res.json({status: false, message: err})
                    } else {
                        saveMat();
                    }
                });
            }
        })();
    }catch(e){
        res.json({status: false, message: e})
    }
});


router.get("/", function (req, res) {
    Material.find({},function(err,mats){
        if (err) {
            return res.json({status: false, message: err});
        } else {
            return res.json({status: true, data: mats});
        }
    });
});
module.exports = router;