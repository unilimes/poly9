/**
 * Created by edinorog on 18.10.17.
 */
const router = require("express").Router();
const Product = require("../../models/product");
const ProductModel = require("../../models/product_model");
const CONFIG = require("../../config/");

router.post("/fillWithTestProduct", function (req, res) {
    var data = req.body;
    try {
        var startItem = 0,
            prodList = [];
        (function saveData() {
            if (startItem >= data.list.length) {
                return res.json({status: true, message: 'products was saved', data: prodList});
            } else {
                var prod = new Product(data.list[startItem++]);
                prod.save(function (err, _prod) {
                    if (err) {
                        return res.json({status: false, message: err})
                    } else {
                        prodList.push(_prod);
                        saveData();
                    }
                });
            }
        })();
    } catch (e) {
        res.json({status: false, message: e})
    }
});

router.get("/:name", function (req, res) {
    Product.find({"model.name":req.params.name}).exec(function (err, data) {
        if (err || !data) {
            return res.json({status: false, message: err || 'can`t find product'});
        } else {
            return res.json({
                status: true, data: data
            });
        }
    })


});
module.exports = router;