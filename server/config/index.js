// process.env.NODE_ENV = 'production';
const fs = require("fs");
const mongoose = require("mongoose");
const ValidationError = mongoose.Error.ValidationError;
const ValidatorError = mongoose.Error.ValidatorError;
const crypto = require("crypto");

var CONFIG = {
    env: process.env.NODE_ENV,
    DIR: {
        PUBLIC: 'resources/',
        UPLOADS: 'uploads/',
        MODEL_UPLOADS: 'models/',
        PROJECTS: 'projects/',
        IMAGES: 'images/',
        IMG_TYPES: ['webp/', 'low/'],
        ALIGN_IMAGES: 'align_images/',
        SITE_STRUCTURE: '/site_structure.json',
        DELIMETER: '/',
        PROJECT_TEMPLATE: {
            NAME: 'templates/',
            CSS: 'style.css',
            HTML: 'index.html',
            TYPES: ['controls/', 'tooltip/', 'preloader/'],
            _TYPE: {
                PRELOADER: 2,
                TOOLTIP: 1,
                CONTROLS: 0
            }

        }
    },
    FILE_UPLOAD_EXT: ['.obj', 'image/', '.svg'],
    FILE_UPLOAD_ATTR: ['textures[]', 'models[]','maps[]'],
    FILE_UPLOAD_ACCEC: parseInt("0777", 8),
    port: process.env.PORT || 3009,
    mongoose: {
        uri: "mongodb://localhost/poly9_unilimes"
    },
    security: {
        secret: "t45g3w45r34tw5ye454uhdgdf",
        expiresIn: "24h"
    },
    SCHEMAS: {
        PRODUCT: 'Product',
        PRODUCT_MODEL: 'ProductModel',
        PRODUCT_CONF: 'ProductConfiguration',
        PRODUCT_MAIN_SET: 'ProductMainSettings',
        PRODUCT_MODEL_PART: 'ProductModelPart',
        PRODUCT_MAT: 'ProductMaterial',
        PRODUCT_MAT_SET: 'ProductMaterialSetting',
        PRODUCT_PATTERN: 'ProductPattern'
    },
    CATEGORY: {
        PARTS: {
            DEFAULT: 1,
            UPLOAD: 2,
            CONFIGS: 3
        }
    },
    MAPS: ['map', "normalMap", "bumpMap", "metalnessMap", "roughnessMap", "displacementMap"],
    help: {
        randomStr: function (length) {
            return crypto.createHash('sha1').update(crypto.randomBytes(length || 32)).digest('hex');
        },
        deleteFolderRecursive: function (path, flag) {
            var _self = this;
            if (fs.existsSync(path)) {
                for (var u = 0, files = fs.readdirSync(path); u < files.length; u++) {
                    var file = files[u],
                        curPath = path + "/" + file;
                    if (fs.lstatSync(curPath).isDirectory()) { // recurse
                        _self.deleteFolderRecursive(curPath, true);
                    } else {
                        fs.unlinkSync(curPath);
                    }
                }
                if (flag)fs.rmdirSync(path);
            }
        },
        checkSchema: function (field, msg, cntx, next) {
            var error = new ValidationError(cntx);
            error.errors[field] = new ValidatorError(field, msg, 'notvalid', cntx[field]);
            error.message += ', ' + msg;
            return next(error);
        },
        decodeBase64Image: function (dataString) {
            var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
            var response = {};
            if (matches.length !== 3)
                return new Error('Invalid input string');
            response.type = matches[1];
            response.data = new Buffer(matches[2], 'base64');

            return response;
        },
        saveBaseData: function (urls, next) {

            var results = [];
            for (var i = 0; i < urls.length; i++) {
                var imgData = urls[i];
                if (!imgData) {
                    return next("No image data provided");
                }

                if(imgData.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/)){
                    var imageBuffer = this.decodeBase64Image(imgData);
                    var userUploadedImagePath = this.randomStr() + '.' + imageBuffer.type.match(/\/(.*?)$/)[1];
                    try {
                        fs.writeFileSync( CONFIG.DIR.PUBLIC +CONFIG.DIR.UPLOADS + CONFIG.DIR.IMAGES +userUploadedImagePath, imageBuffer.data, 'base64');
                        results.push(userUploadedImagePath);
                    } catch (e) {
                        console.log(e);
                        return next("Cannot save image");
                    }
                }else{
                    results.push(imgData);
                }

            }
            next(null,results);
        },
        isUndefined: function (val) {
            return typeof val == "undefined" || val ==null
        },
        isString: function (val) {
            return typeof val == "string"
        },
        isArray: function (val) {
            return val instanceof Array
        },
        isNumber: function (val) {
            return typeof val == "number"
        },
    },
    USER_ROLE: {
        SUPER: 1,
        ADMIN: 2,
        USER: 3
    }
};
module.exports = CONFIG;