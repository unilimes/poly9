/**
 * Created by edinorog on 18.10.17.
 */
const mongoose = require("mongoose");

const config = require("../config");

module.exports = mongoose.connect(config.mongoose.uri);