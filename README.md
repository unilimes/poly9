# Poly9 -it is AngularJS version, currently not supported , move to `mainUi/assets/js/uniApp`
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.1.0.

## Developnet mode
    # -- npm install
    # -- npm run start

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build
Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## RealityServer doc
[Getting Started with RealityServer](http://www.migenius.com/articles/getting-started-with-realityserver)


[Exploring the RealityServer JSON-RPC API](http://www.migenius.com/articles/exploring-the-realityserver-json-rpc-api)

[Getting Started with RealityServer](http://www.migenius.com/articles/getting-started-with-realityserver)

[RealityServer Document Centerl](http://www.migenius.com/doc/realityserver/4.4/index.html#/index.html)
